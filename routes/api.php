<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/** Users **/
Route::get('users/salt/{userID}', 'UserController@getUserSalt');

Route::post('users/login/{userID}/', 'UserController@login');

Route::post('users/newlogin/{userID}/', 'UserController@newLogin');

Route::post('users/fb/login', 'UserController@facebookLogin');

Route::post('users/', 'UserController@newUser');

Route::post('users/register', 'UserController@registerUser');

/** Employees **/
Route::post('employees/login/{employeeID}/company/{companyID}/', 'EmployeeController@login');

Route::post('employees/', 'EmployeeController@newEmployee');

Route::get('employees/salt/{employeeID}/company/{companyID}', 'EmployeeController@getEmployeeSalt');

Route::post('employees/device/login/', 'EmployeeController@deviceLogin');
/** Employees **/

Route::post('gps/service' , 'GpsServiceController@recordInfo');

Route::patch('gps/service' , 'GpsServiceController@recordInfo');

Route::post('companies/{companyID}/profile/picture', 'CompanyController@uploadCompanyProfilePicture');

Route::post('companies/{companyID}/catalogue/posts/picture/{postID}', 'CompanyController@uploadCataloguePostPicture');

Route::post('companies/{companyID}/posts/picture/{postID}', 'CompanyController@uploadCompanyPostPicture');

Route::post('users/{userID}/profile/picture', 'UserController@uploadUserProfilePicture');

Route::post('employees/{employeeID}/company/{companyID}/profile/picture', 'EmployeeController@uploadEmployeeProfilePicture');

Route::post('companies/{deviceId}/logs', 'CompanyController@uploadDeviceLog');

Route::post('employees/{employeeID}/company/{companyID}/logs', 'EmployeeController@uploadEmployeeLogs');

Route::post('/companies/{companyId}/delete/file', 'CompanyController@deleteFile');

Route::post('companies/{companyID}/catalogue/{postId}/file', 'CompanyController@uploadCatalogueFile');

Route::group(['middleware' => ['App\Http\Middleware\ApiAuthentication']], function() {
    
    Route::post('/companies', 'CompanyController@newCompany');
    
    Route::patch('/companies', 'CompanyController@editCompanyProfile');
    
    Route::get('/companies/{companyID}', 'CompanyController@getCompanyDetails');

    Route::get('/companies/', 'CompanyController@getAllCompanies');
    
    Route::post('/companies/route', 'CompanyController@saveCompanyRoute');

    Route::get('/companies/{companyID}/route/{routeID}', 'CompanyController@getRoute');

    Route::post('/companies/routes', 'CompanyController@getRoutes');
    
    Route::get('companies/{companyID}/followers', 'CompanyController@getCompanyFollowers');
    
    Route::get('companies/{companyID}/posts/{type}', 'CompanyController@getCompanyPosts');

    Route::get('companies/{companyID}/employees/{employeeId}/posts', 'CompanyController@getEmployeeCompanyPosts');

    Route::get('companies/{companyID}/promotions', 'CompanyController@getCompanyPromotions');

    Route::get('companies/{companyID}/employees/{employeeId}/promotions', 'CompanyController@getEmployeeCompanyPromotions');

    Route::get('companies/{companyID}/user/posts/{userID}', 'CompanyController@getCompanyUserPosts');

    Route::get('companies/user/{userID}/posts', 'CompanyController@getUserPosts');

    Route::get('companies/{companyID}/catalogue', 'CompanyController@getCompanyCatalogue');

    Route::get('companies/{companyID}/employees/{employeeId}/catalogue', 'CompanyController@getEmployeeCompanyCatalogue');

    Route::get('companies/user/{userID}/promotions', 'CompanyController@getUserPromotions');

    Route::get('companies/{companyID}/user/promotions/{userID}', 'CompanyController@getCompanyUserPromotions');

    Route::get('companies/{companyID}/employee/posts/{employeeID}', 'CompanyController@getCompanyEmployeePosts');

    Route::post('companies/{companyID}/user/{userID}/seen/{postID}', 'CompanyController@seenCompanyUserPosts');

    Route::post('companies/{companyID}/user/{userID}/seen/promotion/{promotionID}', 'CompanyController@seenCompanyUserPromotions');

    Route::post('companies/{companyID}/employee/{employeeID}/seen/{postID}', 'CompanyController@seenCompanyEmployeePosts');

    Route::post('companies/{companyID}/user/{userID}/post/{postID}/like', 'CompanyController@likeCompanyUserPosts');

    Route::post('companies/{companyID}/user/{userID}/promotion/{promotionID}/like', 'CompanyController@likeCompanyUserPromotions');

    Route::post('companies/{companyID}/employee/{employeeID}/post/{postID}/like', 'CompanyController@likeCompanyEmployeePosts');

    Route::post('companies/{companyID}/employee/{employeeID}/promotion/{promotionID}/like', 'CompanyController@likeCompanyEmployeePromotions');

    Route::post('companies/{companyID}/user/{userId}/catalogue/{postId}/like', 'CompanyController@likeCompanyCataloguePost');

    Route::post('companies/{companyID}/promotions/picture/{promotionID}', 'CompanyController@uploadCompanyPromotionPicture');

    Route::get('employees/logs/route/date/{employeeID?}/{companyID}/{routeId?}/{date?}', 'EmployeeController@getEmployeeLogs');

    Route::get('companies/{companyID}/employees', 'CompanyController@getCompanyEmployees');

    Route::post('companies/user/{userId}/employees', 'CompanyController@getUserEmployees');
    
    Route::post('companies/{companyID}/posts/{type}', 'CompanyController@createCompanyPost');

    Route::post('companies/{companyID}/promotions', 'CompanyController@createCompanyPromotion');

    Route::post('companies/{companyID}/catalogue/posts', 'CompanyController@createCataloguePost');

    Route::delete('companies/{companyID}/promotion/{promotionID}', 'CompanyController@deleteCompanyPromotion');

    Route::delete('companies/{companyID}/catalogue/post/{postId}', 'CompanyController@deleteCataloguePost');

    Route::delete('companies/{companyID}/post/{postID}/type/{type}', 'CompanyController@deletePost');
    
    Route::patch('companies/{companyID}/location', 'CompanyController@setCompanyLocation');
    
    Route::patch('companies/{companyID}/block/{userID}', 'CompanyController@blockCompanyFollower');

    Route::patch('companies/{companyID}/unblock/{userID}', 'CompanyController@unblockCompanyFollower');

    Route::get('/companies/{companyID}/logs/', 'CompanyController@getEmployeeLogs');
    
    Route::patch('users/', 'UserController@editUserProfile');

    Route::get('users/{userID}', 'UserController@getUserDetails');
    
    Route::get('users/{userID}/companies/', 'UserController@getUserCompanies');

    Route::get('users/{userID}/country/{countryCode}/companies/all', 'UserController@getAllCompanies');

    Route::get('users/{userID}/companies/all', 'UserController@getAllCompanies');
    
    Route::get('users/{userID}/friends/', 'UserController@getUserFriends');
    
    Route::get('users/{userID}/requests/received', 'UserController@getUserRequests');
    
    Route::get('users/{userID}/requests/sent', 'UserController@getUserRequested');
    
    Route::get('users/{userID}/all/', 'UserController@getAllUsers');

    Route::get('users/{userID}/search/{searchString}', 'UserController@searchForUsers');
    
    Route::get('users/{userID}/messages', 'UserController@getUserMessages');

    Route::post('users/{userID}/messages', 'UserController@sendMessage');

    Route::post('users/{userID}/messages/seen', 'UserController@seenUserMessage');
    
    Route::post('users/{userID}/alert/on', 'UserController@setUserAlert');
    
    Route::delete('users/{userID}/alert/off', 'UserController@cancelUserAlert');

    Route::post('users/{userID}/seen/alert/{friendID}', 'UserController@seenUserAlert');

    Route::get('users/alerting/{latitude}/{longitude}/employee/{employeeID}/company/{companyID}', 'UserController@getAlertingUsers');
    
    Route::patch('users/{userID}/settings', 'UserController@editUserSettings');
    
    Route::patch('users/{userID}/location/visibility', 'UserController@setLocationVisibility');

    Route::patch('users/{userID}/location/hide/{friendID}', 'UserController@hideUserLocation');

    Route::patch('users/{userID}/location/show/{friendID}', 'UserController@showUserLocation');
    
    Route::delete('users/{userID}/unfriend/{friendId}', 'UserController@unfriend');
    
    Route::post('users/{userID}/accept/{friendID}','UserController@acceptRequest');
    
    Route::delete('users/{userID}/reject/{friendID}', 'UserController@rejectRequest');
    
    Route::post('users/{userID}/request/{friendID}', 'UserController@sendRequest');
    
    Route::delete('users/{userID}/cancel/{friendID}', 'UserController@cancelRequest');
    
    Route::post('users/{userID}/follow/{companyID}','UserController@followCompany');
    
    Route::delete('users/{userID}/unfollow/{companyID}', 'UserController@unfollowCompany');
    
    Route::post('users/{userName}/comment/{postID}/from/{companyID}','UserController@postComment');

    Route::post('users/{userID}/promotion/comment/{promotionID}/from/{companyID}','UserController@postPromotionComment');

    Route::post('users/{userID}/catalogue/comment/{postId}/from/{companyID}','UserController@postCatalogueComment');

    Route::post('employees/change/{employeeID}/company/{companyID}/', 'EmployeeController@changeEmployee');

    Route::patch('employees/', 'EmployeeController@editEmployeeProfile');

    Route::patch('/employees/route', 'EmployeeController@setCompanyRoute');

    Route::get('employees/{employeeID}/company/{companyID}', 'EmployeeController@getEmployeeDetails');
    
    Route::get('employees/{employeeID}/company/{companyID}/friends', 'EmployeeController@getEmployeeFriends');
    
    Route::get('employees/{employeeID}/company/{companyID}/messages', 'EmployeeController@getEmployeeMessages');

    Route::post('employees/{employeeID}/company/{companyID}/messages/seen', 'EmployeeController@seenEmployeeMessage');

    Route::post('employees/{employeeID}/company/{companyID}/messages/send', 'EmployeeController@sendEmployeeMessage');
    
    Route::post('employees/{employeeID}/alert/on','EmployeeController@setEmployeeAlert');

    Route::delete('employees/{employeeID}/alert/off/{companyID}','EmployeeController@cancelEmployeeAlert');

    Route::post('employees/{employeeID}/seen/alert/{friendID}', 'EmployeeController@seenEmployeeAlert');

    Route::post('employees/{employeeID}/company/{companyID}/seen/user-alert/{userID}', 'EmployeeController@seenUserAlert');
    
    Route::patch('employees/{employeeID}/settings', 'EmployeeController@editEmployeeSettings');
    
    Route::post('employees/{employeeID}/comment/{postId}', 'EmployeeController@postEmployeeComment');

    Route::post('employees/{employeeID}/promotion/comment/{promotionId}', 'EmployeeController@postPromotionComment');

    Route::get('alerts/{latitude}/{longitude}', 'UserController@getLocationAlerts');

    Route::get('alerts', 'UserController@getAlerts');
});


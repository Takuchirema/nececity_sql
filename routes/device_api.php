<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Device API Routes
|--------------------------------------------------------------------------
|
| This is only for handling gps trackers sending their location to the app
|
*/

Route::any('/', 'DeviceController@recordLocation');



<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function()
{
    return View::make('home');
});

Route::get('/business', function () {
    return view('welcome');
});

Route::get('/business', function()
{
	return View::make('welcome');
});

Auth::routes();

Route::post('/companies', 'CompanyController@newCompany');

Route::post('users/', 'UserController@newUser');

Route::get('privacy', 'HomeController@privacy');

Route::get('forgot_password', 'HomeController@passwords');

Route::get('tutorial', 'HomeController@tutorial');

Route::get('/user/login','Auth\User\LoginController@showLoginForm');

Route::post('/user/login','Auth\User\LoginController@login');

Route::get('/user/logout','Auth\User\LoginController@logout');

// Registration Routes...
Route::get('/user/register', 'Auth\User\RegisterController@showRegistrationForms')->name('register');

Route::post('/user/register', 'Auth\User\RegisterController@register');

Route::get('password/user/reset', 'Auth\User\ForgotUserPasswordController@showLinkRequestForm');

Route::post('password/user/email', 'Auth\User\ForgotUserPasswordController@sendResetLinkEmail');

Route::get('password/user/reset/{token}', 'Auth\User\ResetUserPasswordController@showResetForm');

Route::post('password/user/reset', 'Auth\User\ResetUserPasswordController@reset');

Route::group(['middleware' => ['App\Http\Middleware\Authentication']], function() {

    /*** companies ***/
    Route::patch('/companies', 'CompanyController@editCompanyProfile');
    
    Route::get('/companies/{companyID}', 'CompanyController@getCompanyDetails');

    Route::get('/companies/', 'CompanyController@getAllCompanies');
    
    Route::get('companies/{companyID}/followers', 'CompanyController@getCompanyFollowers');
    
    Route::get('companies/{companyID}/posts/{type}', 'CompanyController@getCompanyPosts');

    Route::get('companies/{companyID}/catalogue', 'CompanyController@getCompanyCatalogue');
    
    Route::get('companies/{companyID}/employees', 'CompanyController@getCompanyEmployees');
    
    Route::post('companies/{companyID}/posts/{type}', 'CompanyController@createCompanyPost');
    
    Route::patch('companies/{companyID}/location', 'CompanyController@setCompanyLocation');
    
    Route::post('companies/{companyID}/block/{userID}', 'CompanyController@blockCompanyFollower');

    Route::post('companies/{companyID}/unblock/{userID}', 'CompanyController@unblockCompanyFollower');

    Route::get('followers', 'CompanyController@followers');
    
    Route::get('settings', 'CompanyController@settings');

    Route::post('settings/save', 'CompanyController@saveSettings');
    
    Route::post('notifications/reply', 'CompanyController@replyNotification');

    Route::post('notifications/delete', 'CompanyController@deleteNotification');

    /*** users ***/
    Route::patch('users/', 'UserController@editUserProfile');
    
    Route::get('users/{userID}', 'UserController@getUserDetails');
    
    Route::get('users/{userID}/companies/', 'UserController@getUserCompanies');

    Route::get('users/{userID}/companies/all', 'UserController@getAllCompanies');
    
    Route::get('users/{userID}/friends/', 'UserController@getUserFriends');
    
    Route::get('users/{userID}/requests/received', 'UserController@getUserRequests');
    
    Route::get('users/{userID}/requests/sent', 'UserController@getUserRequested');
    
    Route::get('users/{userID}/all/', 'UserController@getAllUsers');
    
    Route::get('users/{userID}/messages', 'UserController@getUserMessages');

    Route::post('users/{userID}/messages', 'UserController@sendMessage');

    Route::post('users/{userID}/messages/seen', 'UserController@seenUserMessage');
    
    Route::post('users/{userID}/alert/on', 'UserController@setUserAlert');
    
    Route::delete('users/{userID}/alert/off', 'UserController@cancelUserAlert');
    
    Route::patch('users/{userID}/settings', 'UserController@editUserSettings');
    
    Route::patch('users/{userID}/location/visibility', 'UserController@setLocationVisibility');
    
    Route::delete('users/{userID}/unfriend/{friendId}', 'UserController@unfriend');
    
    Route::post('users/{userID}/accept/{friendID}','UserController@acceptRequest');
    
    Route::delete('users/{userID}/reject/{friendID}', 'UserController@rejectRequest');
    
    Route::post('users/{userID}/request/{friendID}', 'UserController@sendRequest');
    
    Route::delete('users/{userID}/cancel/{friendID}', 'UserController@cancelRequest');
    
    Route::post('users/{userID}/follow/{companyname}','UserController@followCompany');
    
    Route::delete('users/{userID}/unfollow/{companyname}', 'UserController@unfollowCompany');
    
    Route::post('users/{userID}/comment/{postID}/from/{companyname}','UserController@postComment');
    
    /*** employees ***/
    Route::post('employees/', 'EmployeeController@newEmployee');
    
    Route::patch('employees/', 'EmployeeController@editEmployeeProfile');
    
    Route::get('employees/{employeeID}/company/{companyID}', 'EmployeeController@getEmployeeDetails');
    
    Route::get('employees/{employeeID}/company/{companyID}/friends', 'EmployeeController@getEmployeeFriends');
    
    Route::get('employees/{employeeID}/company/{companyID}/messages', 'EmployeeController@getEmployeeMessages');

    Route::post('employees/{employeeID}/company/{companyID}/messages/seen', 'EmployeeController@seenEmployeeMessage');

    Route::get('employees/{employeeID}/company/{companyID}/messages/send', 'EmployeeController@sendEmployeeMessage');
    
    Route::post('employees/{employeeID}/alert/on','EmployeeController@setEmployeeAlert');

    Route::delete('employees/{employeeID}/alert/off','EmployeeController@cancelEmployeeAlert');
    
    Route::patch('employees/{employeeID}/settings', 'EmployeeController@editEmployeeSettings');
    
    Route::post('employees/{employeeID}/comment/{postId}', 'EmployeeController@postEmployeeComment');
    
    Route::post('dashboard/employees', 'EmployeeController@newEmployees');

    Route::post('dashboard/employees/delete', 'EmployeeController@deleteEmployees');

    Route::post('dashboard/employees/edit', 'EmployeeController@editEmployees');

    Route::post('shifts/start', 'ShiftController@startShift');
    
    Route::post('shifts/update', 'ShiftController@updateShift');
    
    Route::get('shifts/{shiftId}/update', 'ShiftController@updateShift');
    
    Route::post('shifts/{shiftId}/new/route', 'ShiftController@createRoute');
    
    Route::post('shifts/stop', 'ShiftController@stopShift');
    
    Route::get('shifts', 'ShiftController@shifts');
    
    Route::get('shifts/view', 'ShiftController@getShifts');
    
    Route::get('shifts/{shiftId}/view', 'ShiftController@viewShift');
    
    Route::get('shifts/{shiftId}/delete', 'ShiftController@deleteShift');
    
    Route::get('shifts/{shiftId}/info', 'ShiftController@getShiftInfo');
    
    //----
    Route::post('schedules/create', 'ScheduleController@createSchedule');
    
    Route::post('schedules/update', 'ScheduleController@updateSchedule');
    
    Route::post('schedules/{scheduleId}/update', 'ScheduleController@updateSchedule');
    
    Route::get('schedules', 'ScheduleController@schedules');
    
    Route::get('schedules/view', 'ScheduleController@getSchedules');
    
    Route::post('schedules/{scheduleId}/delete', 'ScheduleController@deleteSchedule');
    //-----
    
    Route::get('dispatcher', 'ShiftController@dispatcher');
    
    Route::get('employees', 'EmployeeController@employees');
    
    Route::get('time-logs', 'EmployeeController@timeLogs');

    Route::post('dashboard/time-logs/delete', 'EmployeeController@deleteTimeLog');
    
    /*** authentication ***/
    Route::post('authenticate', 'DashBoardController@authenticate');

    Route::get('dashboard/company', 'DashBoardController@dashboard')->middleware('auth:web_company');

    Route::get('dashboard/employee', 'DashBoardController@dashboard')->middleware('auth:web_employee');

    /*** posts ***/
    Route::get('messages', 'PostController@messages');

    Route::delete('messages', 'PostController@deleteMessages');
    
    Route::post('messages/new', 'PostController@newPost');

    Route::post('messages/edit', 'PostController@editPost');

    Route::post('messages/delete', 'PostController@deletePost');
    
    Route::post('messages/{postID}','PostController@newPostComment');
    
    /*** promotions ***/
    Route::get('promotions', 'PromotionController@promotions');

    Route::post('promotion/new', 'PromotionController@newPromotion');

    Route::post('promotion/edit', 'PromotionController@editPromotion');

    Route::post('promotion/delete', 'PromotionController@deletePromotion');
    
    Route::post('promotions/{promotionID}','PromotionController@newPromotionComment');
    
    /*** catalogues ***/
    Route::get('catalogue', 'CataloguePostController@catalogue');

    Route::post('catalogue/new', 'CataloguePostController@newCatalogue');

    Route::post('catalogue/edit', 'CataloguePostController@editCatalogue');

    Route::post('catalogue/delete', 'CataloguePostController@deleteCatalogue');

    Route::post('catalogue/{postID}','CataloguePostController@newCatalogueComment');
    
    /*** devices ***/
    Route::get('devices', 'DeviceController@devices');

    Route::post('dashboard/devices/edit', 'DeviceController@editDevice');

    Route::post('dashboard/devices/delete', 'DeviceController@deleteDevice');

    Route::post('dashboard/devices/logout', 'DeviceController@logoutDevice');

    Route::post('dashboard/devices/login', 'DeviceController@loginDevice');

    
    /**** routes ****/
    Route::get('routes', 'RouteController@routes');

    Route::post('routes/new', 'RouteController@newRoute');

    Route::post('routes/save', 'RouteController@saveRoute');

    Route::post('routes/reverse', 'RouteController@reverseRoute');

    Route::get('edit_routes', 'RouteController@editRoutes');

    Route::get('edit_route/{routeId}', 'RouteController@editRoute');

    Route::post('routes', 'RouteController@saveCompanyRoute');

    Route::post('routes/delete', 'RouteController@deleteRoute');

    Route::get('companies/{companyId}/download/route/{routeId}','RouteController@downloadRoute');
    
    Route::get('companies/{companyId}/download/routes','RouteController@downloadRoutes');
    
    Route::get('company_routes/{companyId?}', 'RouteController@companyRoutes');

    Route::post('company_routes', 'RouteController@company_routes_fb');
    
    Route::get('company_routes/{companyId}/update', 'CompanyController@getCompanyEmployees');

    Route::get('track', 'RouteController@track');
    
    Route::post('/route/update','RouteController@updateEmployeeRoute');

    /*** deliveries ***/
    Route::get('deliveries', 'DeliveriesController@getDeliveries');
    
    Route::post('deliveries/new', 'DeliveriesController@createDelivery');

    Route::post('deliveries/edit', 'DeliveriesController@editDelivery');

    Route::post('deliveries/delete', 'DeliveriesController@deleteDelivery');

    /*** gps service ***/
    Route::post('gps/service/start','GpsServiceController@startService');

    Route::post('gps/service/restart','GpsServiceController@restartService');
    
    Route::post('gps/service/stop','GpsServiceController@stopService');

    Route::get('gps/service','GpsServiceController@getServiceDetails');
});

Route::group(['middleware' => ['App\Http\Middleware\AdminPrivilege']], function () {
    
    Route::get('/upload', 'HomeController@upload');

    Route::post('/upload', 'HomeController@apkupload');

    Route::post('/delete/apk', 'HomeController@deleteApk');

    Route::get('/download', 'HomeController@apkdownload');

    Route::get('/error/logs', 'HomeController@errorLogs');

    Route::post('/clear/logs', 'HomeController@clearLogs');

    Route::get('/php/info', 'HomeController@phpInfo');

    Route::get('/cache', 'HomeController@cacheInfo');
    
    /*** gps services ***/
    Route::post('gps/service/clear/cache','GpsServiceController@clearCache');
    
    /*** devices ***/
    Route::get('manage/devices', 'DeviceController@manageDevices');
    
    /*** variables ***/
    Route::get('app_variables', 'DashBoardController@appVariables');
    
    Route::post('dashboard/app_variables/edit', 'DashBoardController@editVariable');

    Route::post('dashboard/app_variables/delete', 'DashBoardController@deleteVariable');

    Route::post('dashboard/app_variables/new', 'DashBoardController@newVariable');
    
    /*** companies ***/
    Route::get('approve-companies', 'CompanyController@companyApprovals');
    
    Route::post('dashboard/company/approve', 'CompanyController@approveCompany');

    Route::post('dashboard/company/disapprove', 'CompanyController@disapproveCompany');

    Route::post('dashboard/company/delete', 'CompanyController@deleteCompany');
    
});

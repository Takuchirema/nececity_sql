<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncreasePostPromotionCataloguePostLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('post',2000)->change();
        });
        
        Schema::table('promotions', function (Blueprint $table) {
            $table->string('promotion',2000)->change();
        });
        
        Schema::table('catalogue_posts', function (Blueprint $table) {
            $table->string('post',2000)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {}
}

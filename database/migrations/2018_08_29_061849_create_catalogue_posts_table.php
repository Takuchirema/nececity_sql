<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCataloguePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogue_posts', function (Blueprint $table) {
            $table->increments('postId');
            $table->string('companyId');
            $table->string('title');
            $table->dateTime('time');
            $table->string('post');
            $table->string('price');
            $table->string('units');
            $table->string('amenities')->nullable();
            $table->string('duration')->nullable();
            $table->string('likes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogue_posts');
    }
}

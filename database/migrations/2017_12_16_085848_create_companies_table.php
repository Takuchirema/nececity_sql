<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->string('companyId')->unique();
            $table->string('name')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('address')->nullable();
            $table->string('password');
            $table->string('password_token')->nullable();
            $table->string('sector')->nullable();
            $table->string('about')->nullable();
            $table->string('type')->nullable();
            $table->string('email')->nullable();
            $table->string('logTime')->nullable();
            $table->string('token')->nullable();
            $table->string('approved')->nullable();
            $table->string('location')->nullable();
            $table->string('businessHoursId')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}

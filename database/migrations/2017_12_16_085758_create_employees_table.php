<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->string('employeeId');
            $table->string('privilege');
            $table->string('companyId');
            $table->string('password');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('salt')->nullable();
            $table->string('routeId')->nullable();
            $table->string('token')->nullable();
            $table->string('logout')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('status')->default('offline');
            $table->string('lastSeen')->nullable();
            $table->string('location')->nullable();
            $table->string('locationPath')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->unique(['companyId','employeeId']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

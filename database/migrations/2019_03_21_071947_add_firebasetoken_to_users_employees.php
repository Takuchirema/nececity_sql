<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirebasetokenToUsersEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('firebasetoken');
        });
        
        Schema::table('employees', function (Blueprint $table) {
            $table->text('firebasetoken');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('firebasetoken');
        });
        
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('firebasetoken');
        });
    }
}

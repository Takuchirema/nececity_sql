<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->string('countryCode')->nullable();
        });
        
        Schema::table('companies', function (Blueprint $table) {
            $table->string('countryCode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('countryCode');
        });
        
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('countryCode');
        });
    }
}

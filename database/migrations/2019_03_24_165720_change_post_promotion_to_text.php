<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePostPromotionToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->text('post')->change();
        });
        
        Schema::table('promotions', function (Blueprint $table) {
            $table->text('promotion')->change();
        });
        
        Schema::table('catalogue_posts', function (Blueprint $table) {
            $table->text('post')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

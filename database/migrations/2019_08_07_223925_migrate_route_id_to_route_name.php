<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateRouteIdToRouteName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update foreign keys referencing companyId
        DB::table('employees as e')
            ->join('routes as r', 'e.routeId', '=', 'r.routeName')
            ->update([ 'e.routeId' => DB::raw('r.routeId') ]);
        
        DB::table('timelogs as e')
            ->join('routes as r', 'e.routeId', '=', 'r.routeName')
            ->update([ 'e.routeId' => DB::raw('r.routeId') ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

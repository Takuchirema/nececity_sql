<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('alertId')->unique();
            $table->string('location')->nullable();
            $table->datetime('time');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('dpLatitude');
            $table->string('dpLongitude');
            $table->string('userId')->nullable();
            $table->string('employeeId')->nullable();
            $table->string('companyId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}

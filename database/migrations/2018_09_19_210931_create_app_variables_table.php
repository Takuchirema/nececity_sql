<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_variables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('value',1000);
            $table->timestamps();
        });
        
        DB::table('app_variables')->insert(array(
            array('code'=>'AppName','value'=>'NeceCity'),
            array('code'=>'Versions','value'=>'1,2,3,4,5,6,7,8,9,10,11,12,13,14'),
            array('code'=>'AdminVersions','value'=>'1,2,3,4,5,6,7,8,9,10,11,12,13,14')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_variables');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_variables', function (Blueprint $table) {
            $table->string('description')->nullable();
        });
        
        DB::table('app_variables')->insert(array(
            array('code'=>'ClusterRefreshTime','value'=>'5',
                'description'=>'Time in between cluster calculation for a route on the server.'
                . 'This is in seconds. With many users the server might be over loaded if refresh time too high'),
            array('code'=>'BusStopWaitingTime','value'=>'5',
                'description'=>'Time of allowance for a cluster to be at a stop. This is in minutes.'
                . 'When bus is stationary, the cluster migh disappear. Maybe it is briefly stopping'),
            array('code'=>'LocationUpdateTime','value'=>'20',
                'description'=>'Time in which a user can still contribute to cluster. This is in seconds.'
                . 'If user is not active for more than the time, they are excluded from cluster.'),
            array('code'=>'ClusterMinDistance','value'=>'100',
                'description'=>'Min distance for getting members of the same cluster. This is in metres.'),
            array('code'=>'ClusterMinDirection','value'=>'120',
                'description'=>'Min bearing for which members are of the same cluster. This is in degrees.'
                . 'When a group of people are in same bus they are same direction hence same bearing.'),
            array('code'=>'UseCache','value'=>'true',
                'description'=>'Use cache for server efficiency.'),
            array('code'=>'RouteClustering','value'=>'true',
                'description'=>'Doing route clustering or not.')
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_variables', function (Blueprint $table) {
            //
        });
    }
}

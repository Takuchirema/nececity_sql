<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRouteNameToRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->string('routeName');
        });
        
        DB::table('routes')->update(['routeName' => DB::raw('routes.routeId')]);
        
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('routeId');
        });
        
        Schema::table('routes', function (Blueprint $table) {
            $table->increments('routeId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}

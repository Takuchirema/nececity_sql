<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('scheduleId');
            $table->string('companyId');
            $table->string('employeeId');
            $table->string('deviceId');
            $table->integer('start');
            $table->integer('end');
            $table->integer('active');
            $table->string('days');
            $table->string('routeId')->nullable();
            $table->string('shiftId')->nullable();
            $table->timestamps();
        });
        
        Schema::table('devices', function (Blueprint $table) {
            $table->integer('scheduleId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}

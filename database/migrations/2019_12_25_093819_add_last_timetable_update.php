<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddlastTimetableUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->dateTime('lastTimetableRefresh')->nullable();
        });
        
        DB::table('routes')->update(['lastTimetableRefresh' => DB::raw('CURRENT_TIMESTAMP')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->dropColumn('lastTimetableRefresh');
        });
    }
}

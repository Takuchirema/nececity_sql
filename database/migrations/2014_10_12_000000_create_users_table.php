<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('userId')->unique();
            $table->string('email');
            $table->string('password')->nullable();
            $table->string('status');
            $table->string('name')->nullable();
            $table->string('password_token')->nullable();
            $table->string('salt')->nullable();
            $table->string('token')->nullable();
            $table->string('phoneNumber')->nullable();
            $table->string('fbId')->nullable();
            $table->string('fbEmail')->nullable();
            $table->string('fbName')->nullable();
            $table->string('lastSeen')->nullable();
            $table->string('location')->nullable();
            $table->string('locationPath')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

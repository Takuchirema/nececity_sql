<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('settingsId');
            $table->string('userId');
            $table->string('companyId')->nullable();
            $table->string('postsRefreshInterval')->nullable();
            $table->string('messagesRefreshInterval')->nullable();
            $table->string('usersRefreshInterval')->nullable();
            $table->string('companiesRefreshInterval')->nullable();
            $table->string('visibilityRegion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

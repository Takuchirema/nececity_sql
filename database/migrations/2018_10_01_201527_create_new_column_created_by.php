<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewColumnCreatedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalogue_posts', function (Blueprint $table) {
            $table->string('createdBy')->nullable();
        });
        
        Schema::table('posts', function (Blueprint $table) {
            $table->string('createdBy')->nullable();
        });
        
        Schema::table('promotions', function (Blueprint $table) {
            $table->string('createdBy')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalogue_posts', function (Blueprint $table) {
            $table->dropColumn('createdBy');
        });
        
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('createdBy');
        });
        
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropColumn('createdBy');
        });
    }
}

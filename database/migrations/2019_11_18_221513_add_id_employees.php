<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropUnique('employees_companyid_employeeid_unique');
        });
        
        if (!Schema::hasColumn('employees', 'employeeCode'))
        {
            Schema::table('employees', function (Blueprint $table) {
                $table->string('employeeCode');
            });
        }
        
        DB::table('employees')->update(['employeeCode' => DB::raw('employees.employeeId')]);
        
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('employeeId');
        });
        
        Schema::table('employees', function (Blueprint $table) {
            $table->increments('employeeId');
        });
        
        DB::table('deliveries as e')
            ->join('employees as r', 'e.employeeId', '=', 'r.employeeCode')
            ->update([ 'e.employeeId' => DB::raw('r.employeeId') ]);
        
        DB::table('devices as e')
            ->join('employees as r', 'e.employeeId', '=', 'r.employeeCode')
            ->update([ 'e.employeeId' => DB::raw('r.employeeId') ]);
        
        DB::table('timelogs as e')
            ->join('employees as r', 'e.employeeId', '=', 'r.employeeCode')
            ->update([ 'e.employeeId' => DB::raw('r.employeeId') ]);
        
        DB::table('shifts as e')
            ->join('employees as r', 'e.employeeId', '=', 'r.employeeCode')
            ->update([ 'e.employeeId' => DB::raw('r.employeeId') ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanynameToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('companyName');
        });
        
        DB::table('companies')->update(['companyName' => DB::raw('companies.companyId')]);
        
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('companyId');
        });
        
        Schema::table('companies', function (Blueprint $table) {
            $table->increments('companyId');
        });
        
        //Update foreign keys referencing companyId
        DB::table('employees as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('posts as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('locations as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('comments as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('businesshours as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('settings as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('routes as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('promotions as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('devices as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('followers as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('timelogs as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('catalogue_posts as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('deliveries as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId') ]);
        
        DB::table('notifications as t')
            ->join('companies as c', 't.companyId', '=', 'c.companyName')
            ->join('companies as toc', 't.toCompanyId', '=', 'toc.companyName')
            ->update([ 't.companyId' => DB::raw('c.companyId'),'t.toCompanyId' => DB::raw('toc.companyId') ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
        });
    }
}

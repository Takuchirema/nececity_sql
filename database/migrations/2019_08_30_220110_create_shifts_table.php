<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->increments('shiftId');
            $table->string('companyId');
            $table->string('employeeId');
            $table->string('deviceId');
            $table->dateTime('start');
            $table->dateTime('end')->nullable();
            $table->string('routeId')->nullable();
            $table->string('mileage')->nullable();
            $table->string('topSpeed')->nullable();
            $table->text('routePoints')->nullable();
            $table->timestamps();
        });
        
        //Add shiftId to employees.
        Schema::table('employees', function (Blueprint $table) {
            $table->string('shiftId')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}

## About The Repository

This is the NeceCity code to use SQL instead of Neo4j.

The models are now Eloquent models and the previous models have been put into the App\Repositories namespace.

- Effort has been put to try and keep the Repository methods the same as the previous models for ease of integration
	with the controllers.
- Each Repository must have a test in the folder tests\repositories.
- To run a test:
        - create a test db called nececity_test.
        - migrate using php artisan migrate --database=mysql_test
	- ./vendor/bin/phpunit ./tests/repositories/CataloguePostRepositoryTest.php

The Controllers are still the same but now use the Repositories instead of the Models.

- Take a look at the CompanyController for illustration.
- Each controller must have a test in the folder tests\controllers

#Cache

- The cache is mainly centered around company properties.
	- For example company posts would be cached in Cache::get('company_posts');
	- The result returned would be a hashed array with companyId's as the keys.

## How to install

First install composer

- Then create the vendor folder
- Get all required php extensions for laravel
	- sudo apt-get install php-common php-mbstring php-xml php-zip
- We are using xml formatting and jpeg image processing for php so install simplexml extension for php
	- sudo apt-get install php7.2-xml
- We are using php7.2
	- sudo apt install apt-transport-https lsb-release ca-certificates
	- sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
	- sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
	- sudo apt update
	- sudo apt install php7.2
- If another version of php is installed like 7.0 disable it:
	- sudo a2dismod php7.0
	- sudo a2enmod php7.2
- We are also using this math library
	- sudo apt-get install php7.2-bcmath
	- depending on the php version you have
- Then run this command: composer update --no-scripts 

Now run XAMPP or your sql server

- Create a DB with name - nececity
- Create a user name for the DB: username - nececity, password - nececity
- Make sure the user is given priviledges to the DB
	- GRANT ALL PRIVILEGES ON nececity.* To 'nececity'@'localhost' IDENTIFIED BY 'nececity';
- To change the credentials you got to root folder
		- Go to config -> database.php and do your thing.
- Now you must have config/app.php
		- If you don't find one and copy it into that folder.
- Now run  - composer install
- Now run the migrations:
		- php artisan migrate
- Make sure you have the .env file. If not copy and paste it from somewhere.
- Create the key for your project:
		php artisan key:generate 
- Now run the project on a port:
		- php artisan serve --port=8080
- If using apache on production: Give proper permissions to the project:
	- sudo chgrp -R www-data /var/www/html/your-project
	- sudo chmod -R 775 /var/www/html/your-project/storage
- Copy the default site in /etc/apache2/sitea-available/default.conf
- Put this for url's to be redirected successfully
	<Directory /var/www/html/nececity/public>
                AllowOverride All
                allow from all
                Options +Indexes
    </Directory>
- Disable default site and enable your site
	- sudo a2dissite 000-default.conf
	- sudo a2ensite lara_app.conf
- Give sufficient rights to public folder
	- chmod 777 -R nececity/public
- Then run sudo systemctl reload apache2
- Now install Memcached

Test Environment
- Create the DB nececity_test
- Then update the .env and also the app config.
- Create another folder and pull the repository.
- Configure the Virtual Host on apache.
	- In /etc/apache2/ports.conf add the port 8080.
	- In /etc/apache2/sites-available add configurations for 8080 like shown below:
	- Then enable that site as shown above.

Site Config

	<VirtualHost *:8080>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        # ServerName www.nececity.net

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/nececity_test/public
        <Directory /var/www/html/nececity_test/public>
                AllowOverride All
                allow from all
                Options +Indexes
        </Directory>
        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error_test.log
        CustomLog ${APACHE_LOG_DIR}/access_test.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
	</VirtualHost>
	# vim: syntax=apache ts=4 sw=4 sts=4 sr noet

- Make sure you have allowed firewall on the port selected for test environment.
- On google compute engine you have to go to networks and edit firwall configs.

Device API

- The devices will be sending locations on port 8081.
	- This is without any further extension like /api.
	- So we need to listen on that port as well.
	- Set 8081 just like the test environment. But remove the test word.
	- Like /var/www/html/nececity/public not nececity_test.

Laravel API

- The payload is gzip encoded to reduce the payload.
	- The client side, Android or ios must decompress first to read the data.

# Memcached

Currently we are using the File Caching not Memcache or DB cache
- If we decide uisng memcache later this is how we would install it.

XAMPP Windows

- Put this into your php.ini file:
	- extension=php_memcache.dll
	-[Memcache]
	-memcache.allow_failover = 1
	-memcache.max_failover_attempts=20
	-memcache.chunk_size =8192
	-memcache.default_port = 11211
- Download the necessary php_memecache.dll file.
	- https://codeload.github.com/nono303/PHP7-memcache-dll/zip/master
- Unzip the php_memcache.dll file and put it into your php ext folder. In my case its E:/xampp/php/ext/
- Download memcached server for windows
	- http://s3.amazonaws.com/downloads.northscale.com/memcached-win64-1.4.4-14.zip
- Unzip and put the files in memcached folder into a desired directory (e.g. e:/memcached/)
	- Run the memcached.exe file as administrator.

- That might already be enough. As long as the exe is running the service is active.

- Install the memcached service
	- E:\memcached\memcached.exe -d install(If you dont get any errors it means it�s worked)
- Start memcached
	- E:\memcached\memcached.exe -d start, or net start �memcached Server�
- Restart Xampp Apache
- Test Memcache
	- Go to resources -> views -> php_info.blade and comment on phpinfo() and uncomment memcache lines.
	- When you go to the page there must not be an error.

Linux

- sudo apt-get install memcached
- sudo apt-get install php-memcached

## Authorization

For authorisation we have 3 guards at the moment: web_company, web_employee, web_user and api.

- web_company: this is for authentication on the website by inputing Company Name and Company Password.
	- The driver is Companies and the provider is Company. This is set in Config/Auth.
- web_employee: this is for authentication on the website by inputing Employee Name, Company Name and Employee Password.
	- The driver is Employees and the provider is Employee. This is set in Config/Auth.
- web_user: this is for authentication on the website by inputing User Name and User Password.
	- The driver is Users and the provider is User. This is set in Config/Auth.

The authentication is setup as follows.

- Driver gets the db model which will be accessed to authenticate it.
- Provider gets the object e.g. repository which will be provided to the application as authenticated item.
- Providers are specified in Providers/CustomAuthProviders.
	- The implementing classes for providers are in app/Auth.
	- These classes will take the credentials or id or token and get the authenticable object.
- The process is that we get our inputs from the login.
	- We find the validator we want in this case we have custom validator.
	- We call the validator in login control and specify we want to login.
	- Validator takes the data and finds the guard to use.
	- If it is web_company for example, it knows the Driver and Provider.
	- So we go to the provider with the data and call retrieveByCredentials with our data.
	- Using passed data we can easily get the repository. Now we will use the getAuthIdentifier in repository.
	- This gives us the id we will use to retrieve the Repository in further interactions.
	- For example when user exits the tab and opens up again, the session has the authenticable ID.
	- We use guard and provider to get the authenticable object again through provider retrieveById method.


## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

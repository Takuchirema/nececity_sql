
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>View Employee Shifts</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
            list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
        }
        .responstable{
            overflow-x: auto;
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Employee Shifts</h4></div>
        </div>

        <form action="/shifts/view" method="get" autocomplete="off">   
            <div class="row row-bordered">

                <div class="col-lg-3">
                    <h4>Employee</h4>
                    <select class="form-control" id="employeeId" name="employeeId">
                        <option value="">All</option>
                        @foreach ($company->getEmployees() as $indexKey => $employee)
                            @if ($parameters['employeeId'] == $employee->employeeId)
                                <option value="{{$employee->employeeId}}" selected>{{$employee->name}}</option>
                            @else
                                <option value="{{$employee->employeeId}}">{{$employee->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-3">
                    <h4>Route</h4>
                    <select class="form-control" id="routeId" name="routeId">
                        <option value="">All</option>
                        @foreach ($company->routes as $indexKey => $route)
                            @if ($parameters['routeId'] == $route->routeId)
                                <option value="{{$route->routeId}}" selected>{{$route->routeName}}</option>
                            @else
                                <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-3">
                    <h4>From</h4>
                    <div class='input-group date' id='from_date'>
                        @if (isset($parameters['from']))
                            <input name="from" type='text' value="{{$parameters['from']}}" class="form-control" />
                        @else
                            <input name="from" type='text' class="form-control" />
                        @endif
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <h4>To</h4>
                    <div class='input-group date' id='to_date'>
                        @if (isset($parameters['to']))
                            <input name="from" type='text' value="{{$parameters['to']}}" class="form-control" />
                        @else
                            <input name="to" type='text' class="form-control" />
                        @endif
                        
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>

            </div>
           
            <div class="row">
                <div style="margin-left: 30px" class="col-lg-2">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-default" type="submit">Show Shifts</button>
                </div>
            </div>
        </form>
        
        <div class="row row-bordered">
            <div class="col-lg-2"><h4>employee</h4></div>
            <div class="col-lg-2"><h4>route</h4></div>
            <div class="col-lg-2"><h4>start</h4></div>
            <div class="col-lg-2"><h4>end</h4></div>
            <div class="col-lg-1"><h4>view</h4></div>
            <div class="col-lg-1"><h4>update</h4></div>
            <div class="col-lg-2"><h4>delete</h4></div>
        </div>
        
        <div id="time-log-container" class="time-log-container">
            @foreach ($data as $indexKey => $shift)
                <form autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token() }}">
                        <input type="hidden" class="form-control" name="shiftId" value="{{$shift->shiftId}}">

                        <div class="col-lg-2">
                            <input type="text" class="form-control" value="{{$shift->getEmployee()->name}}" readonly>
                        </div>

                        <div class="col-lg-2">
                            <select class="form-control" name="routeId">
                                @foreach ($company->routes as $indexKey => $route)
                                    @if ($shift->routeId == $route->routeId)
                                        <option value="{{$route->routeId}}" selected>{{$route->routeName}}</option>
                                    @else
                                        <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <input class="btn btn-default" type="button" value="{{$shift->start}}" readonly>
                        </div>
                        
                        <div class="col-lg-2">
                            @if ($shift->isComplete())
                                <input class="btn btn-default" type="button" value="{{$shift->end}}" readonly>
                             @else
                                <input class="btn btn-default" type="button" value="In Progress" readonly>
                             @endif
                        </div>
                        
                        <div class="col-lg-1">
                            <a target="_blank" rel="noopener noreferrer" href="/shifts/{{$shift->shiftId}}/view">
                               <input method="get" class="btn btn-default orange" type="button" value="View">
                            </a>
                        </div>
                        
                        <div class="col-lg-1">
                            <input formmethod="get" formaction="/shifts/{{$shift->shiftId}}/update" class="btn btn-default green" type="submit" value="Update">
                        </div>
                        
                        <div class="col-lg-2">
                            <input formmethod="get" formaction="/shifts/{{$shift->shiftId}}/delete" class="btn btn-default red" type="submit" value="Delete">
                        </div>
                    </div>
                </form>
            @endforeach
        </div>
        
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#from_date").datetimepicker({
                    format: "DD/MM/YYYY"
            });

            $("#to_date").datetimepicker({
                    format: "DD/MM/YYYY"
            });
        });
    </script>

 @endsection
</body>
</html>
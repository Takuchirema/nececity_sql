<!DOCTYPE html>
<html>
<head>
</head>
  
<body id="app-layout"> 
    @extends('layouts.track_employees')
    <div id="toolbox">
        <div id="menu-buttons">
            @if (Auth::guard('web_company')->check())
                <button id="home-route" type="button" onclick="location.href='{{url('/dashboard/company')}}';" class="round-button orange">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                </button>
            @elseif (Auth::guard('web_employee')->check())
                <button id="home-route" type="button" onclick="location.href='{{url('/dashboard/employee')}}';" class="round-button orange">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                </button>
            @endif
            <input style="float:right;width:150px;margin: 10px;" type="text" placeholder="e.g. Forest Hill" class="form-control" id="usr" oninput="filter(this)">
        </div>
    </div>
    
    <form method="post" accept-charset="utf-8" id="map_form">
        <input type="hidden" name="current" value="" id="current"  />
        <input type="hidden" name="old-current" value="" id="old-current" />
    </form>
    <div id="bus-stop-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="bus-times-modal-body" class="modal-body"  style="text-align: center">
                    <div>
                        <h4 id ="bus-stop-name"></h4>
                        <div style="padding-left:30px;padding-right: 30px;" id="bus-times-div">
                            <table style="width:100% !important;;" class="responstable" id="bus-stop-time-table"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  </body>
</html>
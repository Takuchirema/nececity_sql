
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    
    <link href="{{url('/')}}/css/imageuploadify.min.css" rel="stylesheet">
    <title>Manage Routes</title>

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <script src="{{url('/')}}/js/jscolor.js"></script>
    <style>
        li{
            list-style-type: none;
        }

        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }
        .container-fluid{
            margin-bottom: 50px;
        }
        .fixed-footer {
            position: fixed;
            left: 0;
            bottom: 0px;
            width: 100%;
            background-color: white;
            color: white;
            z-index: 100;
            text-align: center;
        }
    </style>

</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid">
    
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Edit Routes</h4></div>
        </div>
        
        <div class="row row-bordered">
            <div class="col-lg-2"><h4>route name</h4></div>
            <div class="col-lg-2"><h4>color</h4></div>
            <div class="col-lg-2"><h4>upload</h4></div>
            <div class="col-lg-1"><h4>export</h4></div>
            <div class="col-lg-1"><h4>refresh</h4></div>
            <div class="col-lg-1"><h4>edit</h4></div>
            <div class="col-lg-1"><h4>reverse</h4></div>
            <div class="col-lg-1"><h4>save</h4></div>
            <div class="col-lg-1"><h4>delete</h4></div>
        </div>

        @foreach ( $data['routes'] as $route )
            <div class="bordered">
            <form method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="routeId" value="{{$route->routeId}}">
                
                 <div class="row">
                     <div class="col-lg-2">
                         <h4>{{$route->routeName}}</h4>
                     </div>

                     <div class="col-lg-2">
                         <input style='width: 100%' name="routeColor" class="jscolor colorpicker" value="{{$route->basicColor}}"> 
                     </div>

                     <div class='col-lg-2'>
                        <div class="input-group">
                           <span class="input-group-btn">
                               @if (count($route->points) > 0)
                                    <span class="btn btn-default btn-file orange">
                                        Import…
                                        <input class="fileInp" type="file" name="routePoints" id="fileInp" accept="text/xml">
                                    </span>
                                @else
                                   <span class="btn btn-default btn-file">
                                        Import...
                                        <input class="fileInp" type="file" name="routePoints" id="fileInp" accept="text/xml">
                                    </span>
                                @endif
                               
                           </span>
                           <input type="text" class="form-control" readonly>
                       </div>
                     </div>
                     
                     <div class="col-lg-1">
                         <input class="btn btn-default green" formaction="/companies/{{$route->companyId}}/download/route/{{$route->routeId}}" formmethod="get" type="submit" value="Export">
                     </div>
                     
                     <div class="col-lg-1">
                         <input class="btn btn-default" type="button" value="{{$route->getDaysSinceLastRefresh()}}">
                     </div>
                     
                     <div class="col-lg-1">
                         <a target="_blank" rel="noopener noreferrer" href="edit_route/{{$route->routeId}}">
                            <input class="btn btn-default orange" type="button" value="Edit">
                         </a>
                     </div>
                     
                     <div class="col-lg-1">
                         <input formaction="routes/reverse" class="btn btn-default" type="submit" value="Reverse">
                     </div>
                     
                     <div class="col-lg-1">
                         <input formaction="routes/save" class="btn btn-default green" type="submit" value="Save">
                     </div>

                     <div class="col-lg-1">
                         <input formaction="routes/delete" class="btn btn-default red" type="submit" value="Delete">
                     </div>
                 </div>
            </form>
            </div>
        @endforeach
        
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Add Route</h4></div>
        </div>
        
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

             <div class="row">
                 <div class="col-lg-2">
                     <input type="text" class="form-control" name="routeName" value="" required>
                 </div>

                 <div class="col-lg-2">
                     <input style='width: 100%' name="routeColor" class="jscolor colorpicker" value="pick color"> 
                 </div>

                 <div class='col-lg-2'>
                    <div class="input-group">
                       <span class="input-group-btn">
                           <span class="btn btn-default btn-file">
                               Upload…
                               <input class="fileInp" type="file" name="routePoints" id="fileInp" accept="text/xml">
                           </span>
                       </span>
                       <input type="text" class="form-control" readonly>
                   </div>
                 </div>

                 <div class="col-lg-2">
                 </div>

                 <div class="col-lg-1">

                 </div>

                 <div class="col-lg-1">
                     <input formaction="routes/new" class="btn btn-default green" type="submit" value="Save">
                 </div>

                 <div class="col-lg-2">

                 </div>
             </div>
        </form>
        
        <footer class="fixed-footer">
            <form enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div style="margin-top:10px;">
                    <input class="btn btn-default orange" formaction="/companies/{{$data['companyID']}}/download/routes" formmethod="get" type="submit" value="Save Routes">
                </div>
            </form>
        </footer>
    </div>
    @endsection
    
    <script type="text/javascript">
          
        $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function(event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {

                        var imageId = $(input).attr("data-img-id");

                        if (imageId != null){
                            $('#'+imageId).attr('src', e.target.result);
                        }else{
                            $('#img-upload').attr('src', e.target.result);
                        }
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(".fileInp").change(function(){
                readURL(this);
            }); 	
	});
    </script>
</body>
</html>



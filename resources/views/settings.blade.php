<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Manage Settings</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
        }
        .fixed-footer {
            position: fixed;
            left: 0;
            bottom: -10px;
            width: 100%;
            background-color: white;
            color: white;
            text-align: center;
        }
    </style>

</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')

<div class="container-fluid ">
    
    <div style="display:none;" id="complete-profile" class="warning row">
        <h4>Please Complete Your Profile By Adding These Fields</h4>
    </div>
    
    <form action="settings/save" method="post" enctype="multipart/form-data" autocomplete="off">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
    <div class="row">
	<div class="col-lg-8 col-bordered col-sm-4">
            
            <label>Update Company Profile Picture</label>
            <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
                        Browse… <input class="imgInp" type="file" name="image" id="imgInp">
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>

            @if ($data['profilePicture'] == null)
                <img class="img-upload" id="img-upload"/>
            @else
                <img src="{{$data['profilePicture']}}" class="img-upload" id="img-upload"/>
            @endif
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div id="map" style="height: 70%"></div>
            <button style="margin:10px" class="btn btn-default" onclick="saveLocation()" type="button">view location</button>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">address</label>
                <input type="text" class="form-control" name="address" value="{{$data['address']}}" >
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">hours</label>

                <table>
                    <tr>
                        <td>Monday</td>
                        <td><input type="time" name="monBegin" value = '{{ $data['hours']['monBegin'] }}' /></td>
                        <td> to</td>
                        <td> <input type="time" name="monEnd" value = '{{ $data['hours']['monEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td><input type="time" name="tueBegin" value = '{{ $data['hours']['tueBegin'] }}' /></td>
                        <td>to</td>
                        <td><input type="time" name="tueEnd" value = '{{ $data['hours']['tueEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Wednesday</td>
                        <td><input type="time" name="wedBegin" value = '{{ $data['hours']['wedBegin'] }}' /></td>
                        <td>to</td>
                        <td><input type="time" name="wedEnd" value = '{{ $data['hours']['wedEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Thursday</td>
                        <td><input type="time" name="thuBegin" value = '{{ $data['hours']['thuBegin'] }}' /></td>
                        <td>to</td>
                        <td><input type="time" name="thuEnd" value = '{{ $data['hours']['thuEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Friday</td>
                        <td><input type="time" name="friBegin" value = '{{ $data['hours']['friBegin'] }}' /></td>
                        <td>to</td>
                        <td><input type="time" name="friEnd" value = '{{ $data['hours']['friEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Saturday</td>
                        <td><input type="time" name="satBegin" value = '{{ $data['hours']['satBegin'] }}' /></td>
                        <td> to</td>
                        <td><input type="time" name="satEnd" value = '{{ $data['hours']['satEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Sunday</td>
                        <td><input type="time" name="sunBegin" value = '{{ $data['hours']['sunBegin'] }}'  /></td>
                        <td>to </td>
                        <td><input type="time" name="sunEnd"  value = '{{ $data['hours']['sunEnd'] }}'  /></td>
                    </tr>
                </table>
            </div>
        </div> 
        
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">Email</label>
                <input type="email" class="form-control" name="email" maxlength="1000" value="{{$data['email']}}" >
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">about</label>
                <input type="text" class="form-control" name="about" maxlength="1000" value="{{$data['about']}}" >
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">phone number</label>
                <input type="text" class="form-control" name="phoneNumber" value="{{$data['phoneNumber']}}" >
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-2 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">sector</label>
                <select name="sector" value="{{$data['sector']}}">
                    <option>{{$data['sector']}}</option>
                    <option value="Security">Security</option>
                    <option value="Transportation">Transportation</option>
                    <option value="Retail_Wholesale">Retail/Wholesale</option>
                    <option value="Legal_Services">Legal Services</option>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Real_Estate">Real Estate</option>
                    <option value="Personal_Services">Personal Services</option>
                    <option value="Environmental">Environmental</option>
                    <option value="Motor_Vehicles">Motor Vehicles</option>
                    <option value="Health_Services">Health Services</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Finance">Finance</option>
                    <option value="Education">Education</option>
                    <option value="Construction">Construction</option>
                    <option value="Technology">Technology</option>
                    <option value="Agriculture">Agriculture</option>
                    <option value="other">Other</option>
                </select>
            </div>
        </div>
        
        <div class="col-lg-2 col-bordered">
            <div>
                <div class="form-group" >
                    @if ($data['logTime'] == "true")
                        <input type="checkbox" name="logTimeValue" id="timelog-checkbox" checked/>
                    @else
                        <input type="checkbox" name="logTimeValue" id="timelog-checkbox"/>
                    @endif

                    <div class="[ btn-group ]">
                        <label for="timelog-checkbox" class="[ btn btn-default ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span> </span>
                        </label>
                        <label style="height: auto" for="timelog-checkbox" class="[ btn btn-default active]">
                            Log Driver Time
                        </label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-2 col-bordered">
            <div>
                <div class="form-group" >
                    @if ($data['discoverRoutes'] == "true")
                        <input type="checkbox" name="discoverRoutes" id="routes-checkbox" checked/>
                    @else
                        <input type="checkbox" name="discoverRoutes" id="routes-checkbox"/>
                    @endif

                    <div class="[ btn-group ]">
                        <label for="routes-checkbox" class="[ btn btn-default ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span> </span>
                        </label>
                        <label style="height: auto" for="routes-checkbox" class="[ btn btn-default active]">
                            Discover Routes
                        </label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-2 col-bordered">
            <div>
                <div class="form-group" >
                    @if ($data['routeClustering'])
                        <input type="checkbox" name="routeClustering" id="routeclustering-checkbox" checked/>
                    @else
                        <input type="checkbox" name="routeClustering" id="routeclustering-checkbox"/>
                    @endif

                    <div class="[ btn-group ]">
                        <label for="routeclustering-checkbox" class="[ btn btn-default ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span> </span>
                        </label>
                        <label style="height: auto" for="routeclustering-checkbox" class="[ btn btn-default active]">
                            Route Clustering
                        </label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-2 col-bordered">
            <div>
                <div class="form-group" >
                    @if ($data['deliveries'] == "true")
                        <input type="checkbox" name="deliveriesValue" id="deliveries-checkbox" checked/>
                    @else
                        <input type="checkbox" name="deliveriesValue" id="deliveries-checkbox"/>
                    @endif

                    <div class="[ btn-group ]">
                        <label for="deliveries-checkbox" class="[ btn btn-default ]">
                            <span class="[ glyphicon glyphicon-ok ]"></span>
                            <span> </span>
                        </label>
                        <label style="height: auto" for="deliveries-checkbox" class="[ btn btn-default active]">
                            Do Deliveries
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">type</label>
                <select name="type" value="{{$data['type']}}" selected="{{$data['sector']}}">
                    <option>{{$data['type']}}</option>
                    <option value="public">public</option>
                    <option value="private">private</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">country</label>
                <select name="countryCode" value="{{$data['countryCode']}}">
                    <option>{{$data['countryCode']}}</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Åland Islands</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia, Plurinational State of</option>
                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei Darussalam</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CD">Congo, the Democratic Republic of the</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Côte d'Ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curaçao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and McDonald Islands</option>
                    <option value="VA">Holy See (Vatican City State)</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran, Islamic Republic of</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KP">Korea, Democratic People's Republic of</option>
                    <option value="KR">Korea, Republic of</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao People's Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova, Republic of</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestinian Territory, Occupied</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Réunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Barthélemy</option>
                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin (French part)</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten (Dutch part)</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syrian Arab Republic</option>
                    <option value="TW">Taiwan, Province of China</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania, United Republic of</option>
                    <option value="TH">Thailand</option>
                    <option value="TL">Timor-Leste</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UM">United States Minor Outlying Islands</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                    <option value="VN">Viet Nam</option>
                    <option value="VG">Virgin Islands, British</option>
                    <option value="VI">Virgin Islands, U.S.</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                    <option value="ALL">All Countries</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
    
    <div id="location-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center">
                    <div class="[ form-group ]">
                        <h3>
                            Your Location
                        </h3>
                    </div>
                </div>
                
                <div id="location-modal-body" class="modal-body"  style="text-align: center">
                    <h4 id="show-address">{{$data['address']}}</h4>
                    <input id="location-address" name="location_address" type="hidden">
                    <input id="location-countrycode" name="location_countrycode" type="hidden">
                    <input id="location-latitude" name="location_latitude" type="hidden">
                    <input id="location-longitude" name="location_longitude" type="hidden">
                </div>

                <div class="modal-footer">
                    <div style="float: left">
                        <input class="btn btn-default orange" type="button" value="Done">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="fixed-footer">
        <div style="margin:10px;">
            <input class="btn btn-default orange" type="submit" value="Save Settings">
        </div>
    </footer>
    </form>
</div>

    <script type="text/javascript">
        var goo;
        var map;
        var companyPosition;
        var locationInfo = new Array();
        
        function initMap() {
            goo = google.maps;
            map = new goo.Map(document.getElementById("map"), {
                disableDefaultUI: false,
                mapTypeControl: false,
                center: {lat: -33.956809, lng: 18.460879},
                zoom: 17
            });
            
            var infoWindow = new google.maps.InfoWindow;
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                
                if (companyPosition){
                    pos = companyPosition;
                }
                /*infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map);*/
                map.setCenter(pos);
              }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }

            drawingManager = new google.maps.drawing.DrawingManager({
                drawingControl: false,
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                markerOptions: {icon: {!! json_encode(url('/')) !!}+"/img/map_location.png"}
            });
            
            drawingManager.setMap(map);
            
            google.maps.event.addListener(drawingManager, 'markercomplete', function(marker) {
                if (locationInfo["marker"] != null){
                    locationInfo["marker"].setMap(null);
                }
                locationInfo["marker"] = marker;
                getAddress(marker.getPosition());
            });
            
            load_current_location();
        };
        
        function saveLocation(){
            
            if (locationInfo["marker"] != null){
                
                $("#show-address").text(locationInfo["address"]);
                $("#location-address").val(locationInfo["address"]);
                $("#location-countrycode").val(locationInfo["countryCode"]);
                $("#location-latitude").val(locationInfo["marker"].getPosition().lat());
                $("#location-longitude").val(locationInfo["marker"].getPosition().lng());
                $("#location-modal").modal('show'); 
            }
        }
        
        function getAddress(latlng) {
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    
                    for (var i=0; i<results[0].address_components.length; i++) {
                        for (var b=0;b<results[0].address_components[i].types.length;b++) {

                            if (results[0].address_components[i].types[b] == "country") {
                                //this is the object you are looking for
                                country= results[0].address_components[i];  
                                locationInfo["countryCode"] = country.short_name;
                                break;
                            }
                        }
                    }
                     
                    if (results[1]) {
                        var address = results[1].formatted_address;
                        locationInfo["address"] = address;
                    }
                }
            });
        }
        
        function load_current_location(){
        
            @if ($data['location'] != null)
                
                var latitude = parseFloat("{{$data['location']->latitude}}");
                var longitude = parseFloat("{{$data['location']->longitude}}");
                var address = "{{$data['location']->address}}";
                var title;
                
                if (isNaN(latitude)){
                    return;
                }
                
                if (address){
                    title = address;
                }else{
                    title = "{{ $data['id'] }}";
                }
                
                companyPosition = new google.maps.LatLng(latitude, longitude);
                
                var marker = new google.maps.Marker( {
                    icon     : {
                        url     : {!! json_encode(url('/')) !!}+"/img/map_location.png",
                        size    : new google.maps.Size( 50, 50 ),
                        anchor  : new google.maps.Point( 25, 50 )
                    },
                    title    : title,
                    position : companyPosition,
                    map      : map
                });
                
                map.setCenter(new google.maps.LatLng(latitude, longitude));
                
                locationInfo["marker"] = marker;
                
            @endif
        }
        
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            if (!browserHasGeolocation){
                infoWindow.setContent('Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
        }
    </script>

    <script type="text/javascript">
        
        function completeProfile(){
            var completeProfile = document.getElementById('complete-profile');
            @foreach ($data as $key => $value )
                @if (!isset($value))
                    var div = document.createElement('div');
                    div.innerHTML = "{{$key}}";
                    div.setAttribute('class','info col-lg-1');
                    
                    completeProfile.style.display = "";
                    completeProfile.appendChild(div);
                @endIf
            @endforeach
        }
        
        $(document).ready( function() {
            completeProfile();
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
                });

                $('.btn-file :file').on('fileselect', function(event, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {

                            var imageId = $(input).attr("data-img-id");

                            if (imageId != null){
                                $('#'+imageId).attr('src', e.target.result);
                            }else{
                                $('#img-upload').attr('src', e.target.result);
                            }
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $(".imgInp").change(function(){
                    readURL(this);
            }); 	
	});
    </script>
    
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDtM_vJ0Vzzexq_xaT-TYbaV3w95pIu3U&libraries=drawing&callback=initMap">
    </script>
    <script src="{{url('/')}}/js/app.js"></script>

@endsection
</body>
</html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Manage Employees</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
        list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

    @foreach ($data as $indexKey => $employee )
        @if ($indexKey == 0)
            <div class="row">
                <div class="col-lg-2 col-bordered"><h4>Employees</h4></div>
            </div>
        
            <div class="row row-bordered">
                <div class="col-lg-1"><h4>id</h4></div>
                <div class="col-lg-1"><h4>name</h4></div>
                <div class="col-lg-2"><h4>phone</h4></div>
                <div class="col-lg-2"><h4>password</h4></div>
                <div class="col-lg-1"><h4>admin</h4></div>
                <div class="col-lg-1"><h4>super user</h4></div>
                <div class="col-lg-1"><h4>menu</h4></div>
                <div class="col-lg-1"><h4>save</h4></div>
                <div class="col-lg-1"><h4>delete</h4></div>
            </div>
        @endif
        
        <form method="post" autocomplete="off">
            <div class = "row">
                <div class="col-lg-1">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $employee->employeeId }}">
                    <input type="text" class="form-control" name="code" value="{{$employee->employeeCode}}" required>
                </div>
                
                <div class="col-lg-1">
                  <input type="text" class="form-control" name="name" value="{{$employee->name}}" required>
                </div>
                
                <div class="col-lg-2">
                  <input type="text" class="form-control" name="phoneNumber" value="{{$employee->phoneNumber}}" >
                </div>

                <div class="col-lg-2">
                  <input type="password" class="form-control" name="password" 
                         placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;" 
                         autocomplete="new-password">
                </div>
                 
                <div class="col-lg-1">
                    @if ($employee->privilege == "admin")
                        <input type="checkbox" name="admin" checked> Admin
                    @else
                        <input type="checkbox" name="admin"> Admin
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($employee->privilege == "superuser")
                        <input type="checkbox" name="superuser" checked> Super User
                    @else
                        <input type="checkbox" name="superuser"> Super User
                    @endif
                </div>
                
                <div class="col-lg-1">
                    <input onclick="expand('{{$employee->employeeId}}')" class="btn btn-default" type="button" value="Menu">
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/employees/edit" class="btn btn-default" type="submit" value="Save">
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/employees/delete" placeholder="+27...." class="btn btn-default" type="submit" value="Delete">
                </div>
             </div>
            
            <div id='{{$employee->employeeId}}' style="margin-bottom:40px;display:none;" class="col-lg-12">
                <label>Set user menu access</label>
                <div class="row">
                    <div class="col-lg-3">
                        @if (strpos( $employee->menu , 'settings' ) !== false)
                            <input type="checkbox" name="menu[]" value="settings" checked> Settings<br>
                        @else
                            <input type="checkbox" name="menu[]" value="settings"> Settings<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'employees' ) !== false)
                            <input type="checkbox" name="menu[]" value="employees" checked> Employees<br>
                        @else
                            <input type="checkbox" name="menu[]" value="employees"> Employees<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'routes' ) !== false)
                            <input type="checkbox" name="menu[]" value="routes" checked> Routes<br>
                        @else
                            <input type="checkbox" name="menu[]" value="routes"> Routes<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'track' ) !== false)
                            <input type="checkbox" name="menu[]" value="track" checked> Track<br>
                        @else
                            <input type="checkbox" name="menu[]" value="track"> Track<br>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        @if (strpos( $employee->menu , 'catalogue' ) !== false)
                            <input type="checkbox" name="menu[]" value="catalogue" checked> Catalogue<br>
                        @else
                            <input type="checkbox" name="menu[]" value="catalogue"> Catalogue<br>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        @if (strpos( $employee->menu , 'posts' ) !== false)
                            <input type="checkbox" name="menu[]" value="posts" checked> Posts<br>
                        @else
                            <input type="checkbox" name="menu[]" value="posts"> Posts<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'promotions' ) !== false)
                            <input type="checkbox" name="menu[]" value="promotions" checked> Promotions<br>
                        @else
                            <input type="checkbox" name="menu[]" value="promotions"> Promotions<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'followers' ) !== false)
                            <input type="checkbox" name="menu[]" value="followers" checked> Followers<br>
                        @else
                            <input type="checkbox" name="menu[]" value="followers"> Followers<br>
                        @endif
                    </div>
                    <div class="col-lg-2">
                        @if (strpos( $employee->menu , 'timelogs' ) !== false)
                            <input type="checkbox" name="menu[]" value="timelogs" checked> Time Logs<br>
                        @else
                            <input type="checkbox" name="menu[]" value="timelogs"> Time Logs<br>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        @if (strpos( $employee->menu , 'devices' ) !== false)
                            <input type="checkbox" name="menu[]" value="devices" checked> Devices<br>
                        @else
                            <input type="checkbox" name="menu[]" value="devices"> Devices<br>
                        @endif
                    </div>
                    <div class="col-lg-3">
                        @if (strpos( $employee->menu , 'deliveries' ) !== false)
                            <input type="checkbox" name="menu[]" value="deliveries" checked> Deliveries<br>
                        @else
                            <input type="checkbox" name="menu[]" value="deliveries"> Deliveries<br>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    @endforeach
    
    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>Add New Employee</h4></div>
    </div>
    
    <div class="row row-bordered">
        <div class="col-lg-2"><h4>id</h4></div>
          <div class="col-lg-2"><h4>name</h4></div>
          <div class="col-lg-2"><h4>phone number</h4></div>
          <div class="col-lg-1"><h4>password</h4></div>
          <div class="col-lg-1"><h4>admin</h4></div>
          <div class="col-lg-1"><h4>super user</h4></div>
          <div class="col-lg-1"><h4>menu</h4></div>
          <div class="col-lg-1"><h4>add</h4></div>
          <div class="col-lg-1"><h4>clear</h4></div>
    </div>
    
    <form method="post" action = "dashboard/employees" autocomplete="off">
        <div class = "row">
            <div class="col-lg-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" class="form-control" name="code" value="" required>
            </div>

            <div class="col-lg-2">
              <input type="text" class="form-control" name="name" value="" required>
            </div>

            <div class="col-lg-2">
              <input type="text" class="form-control" name="phoneNumber" placeholder="+27....">
            </div>

            <div class="col-lg-1">
              <input type="password" class="form-control" autocomplete="new-password" name="password" value="" required>
            </div>

            <div class="col-lg-1">
                 <input type="checkbox" name="admin"> Admin
            </div>
            
            <div class="col-lg-1">
                 <input type="checkbox" name="superuser"> Super User
            </div>

            <div class="col-lg-1">
                <input onclick="expand('add-menu')" class="btn btn-default" type="button" value="Menu">
            </div>
            
            <div class="col-lg-1">
                <input class="btn btn-default" type="submit" value="Add">
            </div>

            <div class="col-lg-1">
                <input class="btn btn-default" type="reset" value="Clear">
            </div>
         </div>
        
        <div id='add-menu' style="margin-bottom:40px;display:none;" class="col-lg-12">
            <label>Set user menu access</label>
            <div class="row">
                <div class="col-lg-3">
                    <input type="checkbox" name="menu[]" value="settings"> Settings<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="employees"> Employees<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="routes"> Routes<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="track"> Track<br>
                </div>
                <div class="col-lg-3">
                    <input type="checkbox" name="menu[]" value="catalogue"> Catalogue<br>
                </div>
                <div class="col-lg-3">
                    <input type="checkbox" name="menu[]" value="posts"> Posts<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="promotions"> Promotions<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="followers"> Followers<br>
                </div>
                <div class="col-lg-2">
                    <input type="checkbox" name="menu[]" value="timelogs"> Time Logs<br>
                </div>
                <div class="col-lg-3">
                    <input type="checkbox" name="menu[]" value="devices"> Devices<br>
                </div>
            </div>
        </div>
    </form>
    
    </div>

    <script type="text/javascript">
        
        function expand(id){
            if ($("#"+id).is(":visible")){
                $("#"+id).hide();
            }else{
                $("#"+id).show();
            }
        }
    </script>
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

 @endsection
</body>
</html>
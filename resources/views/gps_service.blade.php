
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>View GPS Service</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
            list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
        }
        .responstable{
            overflow-x: auto;
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>GPS Server Information</h4></div>
        </div>

        <div class="row row-bordered">
            <form method="post" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-lg-3">
                    <h4>Server Status</h4>
                    @if ($data["service"]->isServerRunning())
                        <input class="btn btn-default green" type="button" value="Running">
                        <input data-toggle="modal" data-target="#stop_server" class="btn btn-default" type="button" value="Stop Server">
                    @else
                        <input class="btn btn-default red" type="button" value="Down">
                        <input data-toggle="modal" data-target="#start_server" class="btn btn-default" type="button" value="Start Server">
                    @endif
                </div>

                <div class="col-lg-3">
                    <h4>---</h4>
                    <input formaction="/gps/service/restart" class="btn btn-default" type="submit" value="Restart Server">
                </div>
            
                <div class="col-lg-3">
                    <h4>---</h4>
                    <input formaction="/gps/service/clear/cache" class="btn btn-default orange" type="submit" value="Clear Cache">
                </div>
            </form>
        </div>

        <div id="stop_server" class="modal fade col-lg-12" role="dialog">
            <div class="modal-dialog" style="background: #fff;padding:10px;border-radius: 4px;">
                <!-- Modal content-->
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Switch Off GPS Server</h4>
                </div>
                <div>
                    <h5 class="modal-title">
                        To switch off the server please go to the command line and do the following:<br>
                        1. Find the process ID<br>
                            On Windows you run: netstat -ano | findstr 8081<br>
                            On Linux you run: sudo netstat -nlp | grep :8081<br>
                        2. Kill the process<br>
                            On Windows you run: taskkill /F /PID 4312<br>
                            On Linux you run: sudo kill -9 4312<br>
                    </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

        <div id="start_server" class="modal fade col-lg-12" role="dialog">
            <div class="modal-dialog" style="background: #fff;padding:10px;border-radius: 4px;">
                <!-- Modal content-->
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Switch On GPS Server</h4>
                </div>
                <div>
                    <h5 class="modal-title">
                        To start the server please go to the command line and do the following:<br>
                            1. On the command line start the script located in app/Scripts/GpsService.php<br>
                                Windows and Linux you run:<br>
                                nohup php app/Scripts/GpsService.php &lt;/dev/null >/dev/null 2>&1 &
                    </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            
        </div>
            <div class="row row-bordered">
            <div class="col-lg-2"><h4>Device Id</h4></div>
            <div class="col-lg-2"><h4>Employee</h4></div>
            <div class="col-lg-2"><h4>Company</h4></div>
            <div class="col-lg-1"><h4>Bus Id</h4></div>
            <div class="col-lg-2"><h4>Last Seen</h4></div>
            <div class="col-lg-2"><h4>Info</h4></div>
            <div class="col-lg-1"><h4>Speed</h4></div>
        </div>
        
        @foreach ($data["service"]->connectedDevices as $indexKey => $device)
            <div class = "row">
                <div class="col-lg-2">
                    <input type="text" class="form-control" name="deviceId" value="{{$device->deviceId}}" readonly>
                </div>
                
                <div class="col-lg-2">
                    @if ($device->employee != null)
                        <input type="text" class="form-control" name="currentEmployee" value="{{$device->employee->name}}" readonly>
                    @else
                        <input type="text" class="form-control" name="currentEmployee" value="" readonly>
                    @endif
                </div>
                
                <div class="col-lg-2">
                    <input type="text" class="form-control" name="currentEmployee" value="{{$device->getCompanyName()}}" readonly>
                </div>
                
                <div class="col-lg-1">
                    @if ($device->busId != null)
                        <input type="text" class="form-control" name="busId" value="{{$device->busId}}" readonly>
                    @else
                        <input type="text" class="form-control" name="busId" value="" readonly>
                    @endif
                </div>
                
                <div class="col-lg-2">
                    @if ($device->gpsTime != null)
                        <input data-toggle="modal" data-target="#modal-{{$device->deviceId}}" onclick="getAddress('{{$device->deviceId}}','{{$device->gpsLat}}','{{$device->gpsLng}}');" class="btn btn-default"  value="{{$device->gpsTime}}" readonly>
                    @else
                        <input type="text" class="btn btn-default" value="" readonly>
                    @endif
                </div>
                
                <div class="col-lg-2">
                    @if (strtolower($device->gpsInfo) == "location")
                        <input type="button" class="btn btn-default green"  value="{{$device->gpsInfo}}" readonly>
                    @else
                        <input type="button" class="btn btn-default orange"  value="{{$device->gpsInfo}}" readonly>
                    @endif
                </div>
                
                <div class="col-lg-1">
                    <input type="button" class="btn btn-default"  value="{{$device->gpsSpeed}}" readonly>
                </div>
                
                <div id="modal-{{$device->deviceId}}" class="modal fade col-lg-12" role="dialog">
                    <div class="modal-dialog" style="background: #fff;padding:10px;border-radius: 4px;">
                        <!-- Modal content-->
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Device Id: {{$device->deviceId}}</h4>
                          <h4 class="modal-title">Bus Id: {{$device->busId}}</h4>
                          <h4 class="modal-title">En Route: {{$device->getRouteName()}}</h4>
                          @if ($device->getStructuredCoordinates() !== null && count($device->getStructuredCoordinates()['latlng1']) > 0)
                            <h5 class="modal-title">Route Points: {{count($device->getStructuredCoordinates()['latlng1'])}}</h5>
                            <h5 class="modal-title">
                                First Point:<br>
                                {{$device->getStructuredCoordinates()['latlng1'][0]}}<br>
                                {{$device->getStructuredCoordinates()['time'][0]}}<br>
                                {{$device->getStructuredCoordinates()['repeat'][0]}}<br>
                            </h5>
                            <h5 class="modal-title">
                                Last Point:<br>
                                {{end($device->getStructuredCoordinates()['latlng1'])}}<br>
                                {{end($device->getStructuredCoordinates()['time'])}}<br>
                                {{end($device->getStructuredCoordinates()['repeat'])}}<br>
                            </h5>
                          @endif
                        </div>
                        <p id="location-{{$device->deviceId}}">
                        </p>         
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
             </div>
        @endforeach
    </div>

    <script type="text/javascript">
        var locationInfo = new Array();
        function getAddress(deviceId,lat,lng) {
            if (locationInfo[deviceId] != null){
                return;
            }
            
            var latlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng)); 
            //console.log(latlng);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var p = document.getElementById("location-"+deviceId);
                        var address = results[1].formatted_address;
                        locationInfo[deviceId] = address;
                        if (p != null){
                            p.innerHTML = address;
                        }
                    }
                }
            });
        }
    </script>
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDtM_vJ0Vzzexq_xaT-TYbaV3w95pIu3U&libraries=drawing&callback=initMap">
    </script>
 @endsection
</body>
</html>
<!DOCTYPE html>
<html>
  <head>
    <title>Drawing tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{url('/')}}/js/jquery.timepicker.min.js"></script>
    <link href="{{url('/')}}/css/jquery.timepicker.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    
    <script src="{{url('/')}}/js/jscolor.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  
<body id="app-layout"> 
    @extends('layouts.track_employees')
    <div id="toolbox">
        <div id="menu-buttons">
            <select style="float:left;margin:0px;width: 150px;" id="current-company" name="routeID" class="form-control" onchange="changeCompany()" >
                @foreach ( $data['companies'] as $company )
                    @if($company->companyId != $data['companyID'])
                        <option value="{{$company->companyId}}">{{$company->companyName}}</option>
                    @else        
                        <option selected value="{{$company->companyId}}">{{$company->companyName}}</option>
                    @endif   
                @endforeach
            </select>
            <input style="float:right;width:150px" type="text" placeholder="e.g. Forest Hill" class="form-control" id="usr" oninput="filter(this)">
            <a id="home-route" type="button" onclick="location.href = 'user/logout';" class="round-button orange">
                <img id="home-route-image" src="{{url('/')}}/img/home.png" />
            </a>
        </div>
    </div>
    
    <form method="post" accept-charset="utf-8" id="map_form">
        <input type="hidden" name="current" value="" id="current"  />
        <input type="hidden" name="old-current" value="" id="old-current" />
    </form>
    <div id="bus-stop-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="bus-times-modal-body" class="modal-body"  style="text-align: center">
                    <div>
                        <h4 id ="bus-stop-name"></h4>
                        <div style="padding-left:30px;padding-right: 30px;" id="bus-times-div">
                            <table style="width:100% !important;;" class="responstable" id="bus-stop-time-table"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </body>
</html>
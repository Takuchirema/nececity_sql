
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    
<body id="app-layout">
@extends('layouts.app')
@section('content')

<div class = "row">
@foreach ($data as $key=>$apk)
    @if ($apk !== '.' && $apk != '..')
        <div class="col-lg-4">
            <form action="delete/apk" method="post" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="fileName" value="{{$apk}}" readonly>
                <div class="row">
                    <div class="col-lg-6"><label> {{$apk}}</label></div>
                    <div class="col-lg-6"><input class="btn btn-default" type="submit" value="Delete"></div>
                 </div>
            </form>
        </div>
    @endif
@endforeach
</div>

<div class="col-lg-2">
    <form action="upload" method="post" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="postId" value="">
            <div class="input-group">
         
                <label class="control-label">Select File</label>
                <input id="apk" name="apk" type="file" class="file">

            </div>
        <div style="margin-top: 10px;" class="col-lg-1">
             <input class="btn btn-default" type="submit" value="upload">
         </div>
    </form>         
</div>

@endsection
</body>
    
</html>
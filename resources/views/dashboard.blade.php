<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        
        .col-lg-3 {
            padding: 10px;
        }
    </style>
</head>

<body id="app-layout">
@extends('layouts.app')
@section('content')
<div class="container-fluid vertical-center">

    <div class="row">
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'settings' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/settings';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span><br> <span class = "glyphicon-class">settings</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'employees' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/employees';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span><br> <span class = "glyphicon-class">employees</span>
            </button>
        </div>
        @endif
        
        @if (strtolower(Auth::user()->sector) == "transportation")
            @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'routes' ) !== false)
            <div class="col-lg-3">
                <button  onclick="location.href = '/edit_routes';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-road" aria-hidden="true"></span><br> <span class = "glyphicon-class">routes</span>
                </button>
            </div>
            @endif
            
            @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'track' ) !== false)
            <div class="col-lg-3">
                <button  onclick="location.href = '/track';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span><br> <span class = "glyphicon-class">track</span>
                </button>
            </div>
            @endif
            
            @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'dispatch' ) !== false)
            <div class="col-lg-3">
                <button  onclick="location.href = '/dispatcher';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-plane" aria-hidden="true"></span><br> <span class = "glyphicon-class">dispatch</span>
                </button>
            </div>
            @endif
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'shifts' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/shifts';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-time" aria-hidden="true"></span><br> <span class = "glyphicon-class">shifts</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'schedules' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/schedules';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><br> <span class = "glyphicon-class">schedules</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'catalogue' ) !== false)
        <div class="col-lg-3">   
            <button  onclick="location.href = '/catalogue';"  type="button" class="btn btn-default btn-lg btn-block " aria-label="Left Align">
                <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span><br> <span class = "glyphicon-class">catalogue</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'posts' ) !== false)
        <div class="col-lg-3">   
            <button  onclick="location.href = '/messages';"  type="button" class="btn btn-default btn-lg btn-block " aria-label="Left Align">
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><br> <span class = "glyphicon-class">posts</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'promotions' ) !== false)
        <div class="col-lg-3">   
            <button  onclick="location.href = '/promotions';"  type="button" class="btn btn-default btn-lg btn-block " aria-label="Left Align">
                <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span><br> <span class = "glyphicon-class">promotions</span>
            </button>
        </div>
        @endif

        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'followers' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/followers';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><br> <span class = "glyphicon-class">followers</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'devices' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/devices';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-phone" aria-hidden="true"></span><br> <span class = "glyphicon-class">devices</span>
            </button>
        </div>
        @endif
        
        @if (Auth::user()->menu === 'all' || strpos( Auth::user()->menu , 'deliveries' ) !== false)
        <div class="col-lg-3">
            <button  onclick="location.href = '/deliveries';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-gift" aria-hidden="true"></span><br> <span class = "glyphicon-class">deliveries</span>
            </button>
        </div>
        @endif
        
        @if (strtolower(Auth::user()->companyName) == "nececity")
            <div class="col-lg-3">
                <button  onclick="location.href = '/approve-companies';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span><br> <span class = "glyphicon-class">companies</span>
                </button>
            </div>
                
            <div class="col-lg-3">
            <button  onclick="location.href = '/upload';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                <span class="glyphicon glyphicon-upload" aria-hidden="true"></span><br> <span class = "glyphicon-class">uploads</span>
            </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/error/logs';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-fire" aria-hidden="true"></span><br> <span class = "glyphicon-class">logs</span>
                </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/php/info';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><br> <span class = "glyphicon-class">php</span>
                </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/cache';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span><br> <span class = "glyphicon-class">cache</span>
                </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/app_variables';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-expand" aria-hidden="true"></span><br> <span class = "glyphicon-class">variables</span>
                </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/gps/service';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span><br> <span class = "glyphicon-class">gps service</span>
                </button>
            </div>
        
            <div class="col-lg-3">
                <button  onclick="location.href = '/manage/devices';"  type="button" class="btn btn-default  btn-lg btn-block" aria-label="Left Align">
                    <span class="glyphicon glyphicon-camera" aria-hidden="true"></span><br> <span class = "glyphicon-class">manage devices</span>
                </button>
            </div>
        @endif

    </div>

</div>

@endsection
</body>
    
</html>


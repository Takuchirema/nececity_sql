
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="{{asset('js/bootstrap.js')}}"></script>
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
<div class="jumbotron vertical-center">   
    <div class="container-fluid ">
 <img src="{{URL::asset('/img/logo.png')}}" alt="profile Pic" height="200" width="200">



    <form method = "post" action = "authenticate">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">   
    <div class="row">

            <div class="col-md-4 col-md-offset-4"> 
            <div class="input-group">
            <span class="input-group-addon" id="basic-addon1"></span>
            <input type="text" class="form-control"  placeholder="Username" aria-describedby="basic-addon1" id="username">
            </div>

        </div>

    </div>
</div>
<div class="form-group">
<div class="row">
 
 <div class="col-md-4 col-md-offset-4"> 
     <div class="input-group">
    <span class="input-group-addon" id="basic-addon1"></span>
    <input type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1" id="password">
    </div>

  </div>

</div>
</div>
<div class="form-group">   
   <div class="row">
  
  <div class="col-md-4 col-md-offset-4"> 
     <button class="btn btn-default" type="submit">Login</button>

  </div>
  
</div>
</div>
</form>
    </div>
</div>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
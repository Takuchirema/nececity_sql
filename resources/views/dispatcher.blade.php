
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dispatch Employees</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
        list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>Dispatch Employees</h4></div>
    </div>

    <div class="row row-bordered">
        <div class="col-lg-2"><h4>id</h4></div>
        <div class="col-lg-2"><h4>name</h4></div>
        <div class="col-lg-2"><h4>bus</h4></div>
        <div class="col-lg-2"><h4>route</h4></div>
        <div class="col-lg-2"><h4>update</h4></div>
        <div class="col-lg-1"><h4>view</h4></div>
        <div class="col-lg-1"><h4>dispatch</h4></div>
    </div>
    @foreach ($data as $indexKey => $employee)
        <form method="post" autocomplete="off">
            <div class = "row">
                <div class="col-lg-2">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{$employee->employeeId}}">
                    <input type="text" class="form-control" name="code" value="{{$employee->employeeCode}}" required>
                </div>
                
                <div class="col-lg-2">
                  <input type="text" class="form-control" name="name" value="{{$employee->name}}" required>
                </div>
                
                <div class="col-lg-2">
                  @if ($employee->getDevice() != null)
                    <select id="{{$employee->employeeId}}_deviceId" class="form-control" name="deviceId" value="{{$employee->getDevice()->deviceId}}">
                        @foreach ($company->getDevices() as $indexKey => $device)
                            @if ($employee->getDevice()->deviceId === $device->deviceId)
                                <option value="{{$device->deviceId}}" selected>
                                    Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                </option>
                            @else
                                <option value="{{$device->deviceId}}">
                                    Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                </option>
                            @endif
                        @endforeach
                    </select>
                  @else
                    <select id="{{$employee->employeeId}}_deviceId" class="form-control" name="deviceId" value="" required>
                        <option>Select Bus</option>
                        @foreach ($company->getDevices() as $indexKey => $device)
                            <option value="{{$device->deviceId}}">
                                Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                            </option>
                        @endforeach
                    </select>
                  @endif
                </div>
                
                <div class="col-lg-2">
                  @if ($employee->route != null)
                    <select id="{{$employee->employeeId}}_routeId" class="form-control"  name="routeId" value="{{$employee->route->routeId}}">
                        <option value="{{$employee->route->routeId}}">{{$employee->route->routeName}}</option>
                        @foreach ($company->routes as $indexKey => $route)
                            <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                        @endforeach
                    </select>
                  @else
                    <select id="{{$employee->employeeId}}_routeId" class="form-control" name="routeId" value="">
                        <option >Select Route</option>
                        @foreach ($company->routes as $indexKey => $route)
                            <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                        @endforeach
                    </select>
                  @endif
                </div>
                
                <div class="col-lg-2">
                    <input onclick="updateShift('{{$employee->employeeId}}','{{$employee->shiftId}}', this.form);" class="btn btn-default" type="button" value="Update Shift">
                </div>
                
                <div class="col-lg-1">
                    @if ($employee->getShift() != null && !$employee->getShift()->isComplete())
                        <a target="_blank" rel="noopener noreferrer" href="/shifts/{{$employee->shiftId}}/view">
                           <input method="get" class="btn btn-default orange" type="button" value="View">
                        </a>
                    @else
                        <input method="get" class="btn btn-default orange" type="button" value="View" disabled>
                    @endif
                </div>

                <div class="col-lg-1">
                  @if ($employee->getShift() != null && !$employee->getShift()->isComplete())
                    <input onclick="stopShift('{{$employee->employeeId}}',this.form);" class="btn btn-default green" type="button" value="Stop Shift">
                  @else
                    <input onclick="startShift('{{$employee->employeeId}}',this.form);" class="btn btn-default orange" type="button" value="Start Shift">
                  @endif
                </div>
             </div>
        </form>
    @endforeach

    <script type="text/javascript">
        function stopShift(employeeId,form){
            document.getElementById(employeeId+"_deviceId").value = "";
            form.action = "shifts/stop";
            form.submit();
        }
        
        function updateShift(employeeId, shiftId, form){
            deviceId = document.getElementById(employeeId+"_deviceId").value;
            routeId = document.getElementById(employeeId+"_routeId").value;
            
            //alert(deviceId+"-"+routeId);
            if (deviceId === "Select Bus" || !deviceId || !routeId){
                alert("Please select bus and route to dispatch an employee");
                return;
            }
            form.method="GET";
            form.action = "shifts/"+shiftId+"/update";
            form.submit();
        }
        
        function startShift(employeeId,form){
            deviceId = document.getElementById(employeeId+"_deviceId").value;
            routeId = document.getElementById(employeeId+"_routeId").value;
            
            //alert(deviceId+"-"+routeId);
            if (deviceId === "Select Bus" || !deviceId || !routeId){
                alert("Please select bus and route to dispatch an employee");
                return;
            }
            form.action = "shifts/start";
            form.submit();
        }
        
        function expand(id){
            if ($("#"+id).is(":visible")){
                $("#"+id).hide();
            }else{
                $("#"+id).show();
            }
        }
    </script>
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

 @endsection
</body>
</html>
<!DOCTYPE html>
<html>
  <head>
    <title>Edit Route</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{url('/')}}/js/jquery.timepicker.min.js"></script>
    <script src="{{url('/')}}/js/side-navigation.js"></script>
    
    <link href="{{url('/')}}/css/jquery.timepicker.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    <link href="{{url('/')}}/css/side-navigation.css" rel="stylesheet">
    
    <script src="{{url('/')}}/js/jscolor.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      
      #menu-buttons input{
          height: 60px;
          width: 60px;
      }
    </style>
  </head>
  
<body id="app-layout"> 
    
    <div id="map">    
    </div>

    <div id="toolbox">
        <div id="menu-buttons">
            <button title="Home" id="home-route" type="button" onclick="location.href='{{url('/edit_routes')}}';" class="round-button orange">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
            </button>
            
            <button title="Draw Route" id="edit-route" type="button" onclick="editRoute()" class="round-button orange">
                 <span id='edit-route-span' class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </button>
            
            <button title="Save Route" id="show-modal" type="button" onclick="save();" class="round-button orange">
                <span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>
            </button>
            
            <input id="colorpicker" class="round-button jscolor" onchange="setRouteColor()">
        </div>
        
    </div>
    
    <form method="post" accept-charset="utf-8" id="map_form">
        <input type="hidden" name="current" value="" id="current"  />
        <input type="hidden" name="old-current" value="" id="old-current" />
    </form>
    
    <div id="bus-stop-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center">
                    <div class="[ form-group ]">
                        <input type="checkbox" name="fancy-checkbox-default" id="fancy-checkbox-default" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="fancy-checkbox-default" class="[ btn btn-default ]">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="fancy-checkbox-default" class="[ btn btn-default active ]">
                                Bus Stop
                            </label>
                        </div>
                    </div>
                    <div>
                        <input id="bus-stop-name" name="busStopName" placeholder="Bus Stop Name e.g. North Stop"/>
                    </div>
                </div>

                <div id="bus-times-modal-body" class="modal-body"  style="text-align: center">
                    <div>
                        <h4>Bus Stop Times</h4>
                        <label style="margin: 5px;" class="btn btn-default btn-file">
                            Browse<input id="upload-stop-times" type="file" accept="text/plain" hidden>
                        </label>
                        <button class="btn btn-default red" onclick="clearStopTimes();" type="submit">Clear</button>
                        </button>
                    </div>
                    
                    <div id="bus-times-div" style="vertical-align: middle;">
                        
                    </div>
                    
                    <input id="time-input" class="time-picker timepicker-without-dropdown text-center" />
                </div>

                <div class="modal-footer">
                    <div style="float: left">
                        <button class="btn btn btn-primary" data-dismiss="modal" onclick="saveBusStopTimes()" >Save</button>
                    </div>
                    
                    <div style="float: right">
                        <button class="btn btn btn" data-dismiss="modal" onclick="editBusStop()">Cancel</button>
                        <button class="btn btn btn" data-dismiss="modal" onclick="deleteVertex()">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="location-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="text-align: center">
                    <div class="[ form-group ]">
                        <h3>
                            Save Your Location
                        </h3>
                    </div>
                </div>
                
                <form action="settings/save" method="post" autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="location-modal-body" class="modal-body"  style="text-align: center">
                        <h4 id="show-address"></h4>
                        <input id="location-address" name="location_address" type="hidden">
                        <input id="location-latitude" name="location_latitude" type="hidden">
                        <input id="location-longitude" name="location_longitude" type="hidden">
                    </div>
                    
                    <div class="modal-footer">
                        <div style="float: left">
                            <input class="btn btn-default orange" type="submit" value="Save">
                        </div>
                    </div>
                    
                </form>
                
                
                
            </div>
        </div>
    </div>
    
    <script>
        // This example requires the Drawing library. Include the libraries=drawing
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
        var current_line;
        var previous_line;
        
        var current_map;
        var drawingManager;
        var drawing = true;
        var locationEditing = false;
        var editingBusStop = false;
        var timeIds = 1;
        var vertex_position;
        var vertex_index;
        var chosenRouteColor = "000";
        var bus_refresh_rate = 5000 ;
        var total_steps = 20;
        var goo;
        var map;
        var companyPosition;
        
        var vehicle_mode = false;
        //key value with vertices which are bustops with their values
        var busStops = new Array();
        var locationInfo = new Array();
        var vehicleMarkers= new Map();
        var employeeRoute = new Map();
        
        var vehicleDestination = new Map();
        
        function saveBusStopTimes(){
            
            var stopDiv = document.getElementById("bus-times-div");
            var stopTimes = stopDiv.getElementsByTagName('div');
            var busStopName = document.getElementById("bus-stop-name").value;
            
            if (!busStopName){
                alert ('Please Enter Bus Stop Name');
                return;
            }
            
            busStops[vertex_position] = new Array();
            
            busStops[vertex_position]["busStopName"] = busStopName;
            busStops[vertex_position]["busStopTimes"] = new Array();
            
            //create marker for the vertex position
            var marker = new google.maps.Marker( {
                icon     : {
                    url     : {!! json_encode(url('/')) !!}+"/img/bus_stop.png",
                    size    : new google.maps.Size( 50, 50 ),
                    anchor  : new google.maps.Point( 25, 50 )
                },
                title    : busStopName,
                position : vertex_position,
                map      : map
            });
            
            busStops[vertex_position]["busStopMarker"] = marker;
            
            console.log("editing vertex "+vertex_position);
            for (var i = 0; i < stopTimes.length; i++)
            {   
                var timeId = stopTimes[i].getAttribute("data-id");
                var timeInput = document.getElementById('time-'+timeId);
                if (timeInput === null){
                    continue;
                }
                var time = timeInput.value;
                busStops[vertex_position]["busStopTimes"].push(time);
            }
            console.log(busStops);
        }
        
        function editBusStop(){
            
            if (!editingBusStop){
                editingBusStop = true;
            }else{
                $("#bus-stop-modal").modal('hide'); 
                editingBusStop = false;
            }
        }
        
        function isValidTime(time){
            if (time.trim().length === 0){
                return false;
            }
            
            var re = new RegExp("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
            if (re.test(time)) {
                return true;
            }
            return false;
        }
        
        function fillBusStopTimes(busStop){
            var busStopTimes = busStop["busStopTimes"];
            var busStopTimeFrequency = busStop["busStopTimeFrequency"];
            
            for (var i=0;i<busStopTimes.length;i++){
                var time = busStopTimes[i];
                var frequency = busStopTimeFrequency[i];
                
                timeRow(time,frequency);
            }
        }
        
        function timeRow(time,frequency)
        {
            var div = document.createElement('div');
            div.className = 'time-container';
            div.style.width = '100%';
            div.id = 'div-'+timeIds;
            div.setAttribute('data-id',timeIds);

            var cancelBtn = document.createElement('button');
            cancelBtn.className = 'round-button red';
            cancelBtn.innerHTML = 'X';
            cancelBtn.setAttribute( 'onClick', "removeElement('div-"+timeIds+"')" );

            var frequencyBtn = document.createElement('button');
            frequencyBtn.className = 'round-button green';
            frequencyBtn.innerHTML = frequency;
            frequencyBtn.disabled = true;

            var input = document.createElement('input');
            input.className = 'timepicker timepicker-without-dropdown text-center';
            input.id = 'time-'+timeIds;

            $("#bus-times-div").append(div);

            if (time === null){
                time = $('#time-input').val();
                if (isValidTime(time)){
                    div.appendChild(input);
                    div.appendChild(cancelBtn);
                    div.appendChild(frequencyBtn);
                    $("#time-"+timeIds).val($('#time-input').val());
                }else{
                    alert("Please enter a valid time");
                }
            }else{
                if (isValidTime(time)){
                    div.appendChild(input);
                    div.appendChild(cancelBtn);
                    div.appendChild(frequencyBtn);
                    $("#time-"+timeIds).val(time);
                }
            }

            timeIds++;
        }
        
        function togglebus()
        {
            //remove all routes and bus stops
            clearRoutes();
            
            //To show what is currently displaying
            greyOutButtons();
            $("#show-buses").attr("class","round-button orange");
            
            vehicle_mode =  !vehicle_mode;
            if(vehicle_mode)
            {
                update_bus();
            }
            else
            {
                clearBusMarkers();
            }
        }
        
        function editRoute(){
            editLocation(false);
            clearBusMarkers();
            if (drawing){
                $("#edit-route-span").attr("class","glyphicon glyphicon-pencil")
                $("#edit-route-span").attr("title","Draw Route")
                drawingManager.setOptions({
                    drawingMode: null
                });
                drawing = false;
            }else{
                $("#edit-route-span").attr("class","glyphicon glyphicon-road")
                $("#edit-route-span").attr("title","Edit Route")
                drawingManager.setOptions({
                    drawingMode: google.maps.drawing.OverlayType.POLYLINE
                });
                drawing = true;
            }
        }
        
        function editLocation(edit){
            
            if (edit === null){
                edit = !locationEditing;
            }
            
            if (!edit){
                reverseGreyOutButtons();
                $("#edit-location").attr("class","round-button grey");
                $("#show-buses").attr("class","round-button grey");
                drawingManager.setOptions({
                    drawingMode: null
                });
                locationEditing = false;
            }else{
                greyOutButtons();
                $("#edit-location").attr("class","round-button red");
                drawingManager.setOptions({
                    drawingMode: google.maps.drawing.OverlayType.MARKER
                });
                locationEditing = true;
            }
        }
        
        function greyOutButtons(){ 
            $("#edit-location").attr("class","round-button grey");
            $("#home-route").attr("class","round-button grey");
            $("#edit-route").attr("class","round-button grey");
            $("#show-modal").attr("class","round-button grey");
            $("#show-buses").attr("class","round-button grey");   
        }
        
        function reverseGreyOutButtons(){
            $("#edit-location").attr("class","round-button orange");
            $("#home-route").attr("class","round-button orange");
            $("#edit-route").attr("class","round-button orange");
            $("#show-modal").attr("class","round-button orange");
            $("#show-buses").attr("class","round-button orange");
        }
        
        function uploadTimes(){
            var fileToLoad = document.getElementById("upload-stop-times").files[0];

            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                var timesFile = fileLoadedEvent.target.result;
                var times = timesFile.split('\n');
                for(var i = 0;i < times.length;i++){
                    time = times[i].trim();
                    
                    if (isValidTime(time)){
                        timeRow(time,"0");
                    }
                }
                document.getElementById('upload-stop-times').value= null;
            };
            fileReader.readAsText(fileToLoad, "UTF-8");
        }
        
        function CenterControl(controlDiv, map, drawingManager) {   
            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to recenter the map';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = 'Save';
            controlUI.appendChild(controlText);

            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function() {
                // google.maps.event.trigger(drawingManager, 'polylinecomplete') ;
                alert(  document.getElementById("vertices").value );    
            });
        }

        function mapchange(){
            var proceed = true;
            if( document.getElementById("current").value !=  document.getElementById("old-current").value)
            {
                proceed = confirm("you have made changes to the current map, if you change without clicking 'save' these changes will be lost") ;
            }

            if (proceed){
                
                var form = document.createElement("form");
                form.method = "get";
                form.action = "routes";

                var mapname = document.createElement("input");
                mapname.type = "hidden";
                mapname.name = "mapname";
                mapname.value = document.getElementById("current-map").value;

                var token = document.createElement("input");
                token.type = "hidden";
                token.name = "_token";
                token.value = "{{ csrf_token() }}";

                form.appendChild(mapname);
                form.appendChild(token);
                document.body.appendChild(form)

                form.submit();
            }
        }

        function deletes(){
            var form = document.createElement("form");
            
            //var element2 = document.createElement("input");
            form.method = "post";
            form.action = "routes/delete";
            var current_map_name =  document.getElementById("current-map").value;

            var current_path_string = document.getElementById("current").value.toString();
            var points = JSON.parse (current_path_string );

            var routeID = document.createElement("input");
            routeID.type = "hidden";
            routeID.name ="routeID";
            routeID.value = document.getElementById("current-map").value; ;

            form.appendChild(routeID);

            var token = document.createElement("input");

            token.type = "hidden";
            token.name = "_token";
            token.value = "{{ csrf_token() }}";

            form.appendChild(token);
            document.body.appendChild(form);

            form.submit();
        }
        
        function clearRoutes(){
            clearBusStops();
            if (current_line){
                current_line.setMap(null);
            }
        }
        
        function clearBusStops(){
            
            for (var key in busStops){
                var marker = busStops[key]['busStopMarker'];
                marker.setMap(null);
            }
        }
        
        function create(){
            var routeId =  document.getElementById("new-route-id").value;
            saveRoute(routeId);
            clearRoutes();
        }
        
        //save from the save route being edited
        function save(){
            var routeId =  "{{$data['route']->routeId}}";
            saveRoute(routeId);
            clearRoutes();
        }
        
        function saveRoute(routeIDString){
            var form = document.createElement("form");
            form.method = "post";
            form.action = "{{url('/routes/save')}}";

            var routeID = document.createElement("input");
            routeID.type = "hidden";
            routeID.name ="routeId";
            routeID.value = routeIDString;

            //alert("routeId "+routeIDString);
            var routeColor = document.createElement("input");
            routeColor.type = "hidden";
            routeColor.name ="routeColor";
            routeColor.value = chosenRouteColor;

            form.appendChild(routeID);

            form.appendChild(routeColor);

            var token = document.createElement("input");

            token.type = "hidden";
            token.name = "_token";
            token.value = "{{ csrf_token() }}";

            form.appendChild(token);

            var points_objects = new Array();
            
            for(i=0; i<current_line.getPath().getLength(); i++){
                var id = document.createElement("input");
                var position = current_line.getPath().getAt(i);
                
                id.type = "hidden";
                id.name ="point"+i+"[id]";
                id.value = i+1;

                var name = document.createElement("input");

                name.type = "hidden";
                name.name = "point"+i+"[name]";
                name.value = i;

                var busStop = document.createElement("input");

                busStop.type = "hidden";
                busStop.name ="point"+i+"[busStop]";
                busStop.value = false;
                
                if (busStops[position]){
                    //alert('busstop');
                    var busStopTimes = busStops[position]['busStopTimes'];
                    var busStopTimeFrequency = busStops[position]['busStopTimeFrequency'];
                    var busStopName = busStops[position]['busStopName'];
                    
                    busStop.value = true;
                    
                    name.value = busStopName;
                    
                    //put in the time tables
                    for (j=0; j<busStopTimes.length; j++){
                        var busStopTime = document.createElement("input");

                        busStopTime.type = "hidden";
                        busStopTime.name ="point"+i+"[timeTable]["+(j+1)+"]";
                        busStopTime.value = busStopTimes[j];
                        
                        form.appendChild(busStopTime);
                    }
                }

                var latitude = document.createElement("input");

                latitude.type = "hidden";
                latitude.name ="point"+i+"[latitude]";
                latitude.value = position.lat();

                var longitude = document.createElement("input");

                longitude.type = "hidden";
                longitude.name = "point"+i+"[longitude]";
                longitude.value = position.lng();

                form.appendChild(id);
                form.appendChild(name);
                form.appendChild(busStop);
                form.appendChild(latitude);
                form.appendChild(longitude);
            }

            document.body.appendChild(form);
            form.submit();
        }


        function save_current_map(){
            document.getElementById(map_name).value = current_line.getPath().getArray().toString();
        }
        
        document.getElementById("savebtn").addEventListener("onClick", save);
        
        function load_current_location(){
            @if ($data['location'] != null)
                var latitude = parseFloat("{{$data['location']->latitude}}");
                var longitude = parseFloat("{{$data['location']->longitude}}");
                var address = "{{$data['location']->address}}";
                var title;
                
                if (isNaN(latitude)){
                    return;
                }
                
                if (address){
                    title = address;
                }else{
                    title = "{{ $data['companyID'] }}";
                }
                
                companyPosition = new google.maps.LatLng(latitude, longitude);
                
                var marker = new google.maps.Marker( {
                    icon     : {
                        url     : {!! json_encode(url('/')) !!}+"/img/map_location.png",
                        size    : new google.maps.Size( 50, 50 ),
                        anchor  : new google.maps.Point( 25, 50 )
                    },
                    title    : title,
                    position : companyPosition,
                    map      : map
                });
                
                map.setCenter(new google.maps.LatLng(latitude, longitude));
                
                locationInfo["marker"] = marker;
                
            @endif
        }
        
        //puts a default polyline as well
        function load_current_route() {
            
            if (current_line){
                current_line.setMap(null);
            } 
            
            if (drawing){
                editRoute();
            }

            var current_path = new Array();
            
            var count = 0;
            @foreach ( $data['route']->points as $point)
                var position = new google.maps.LatLng({{$point->latitude}},{{$point->longitude}});
                if (count == 0){
                    map.setZoom(17);
                    map.panTo(position);
                }
                current_path[{{$point->id - 1}}] = position;
                
                @if ($point->busStop == "true")
                    busStops[position] = new Array();
                    var busStopName = "{{$point->name}}";
                    //alert('is bus stop '+busStopName);
                    busStops[position]["busStopName"] = busStopName;
                    busStops[position]["busStopTimes"] = new Array();
                    busStops[position]["busStopTimeFrequency"] = new Array();
                    
                    //create marker for the vertex position
                    var marker = new google.maps.Marker( {
                        icon     : {
                            url     : {!! json_encode(url('/')) !!}+"/img/bus_stop.png",
                            size    : new google.maps.Size( 50, 50 ),
                            anchor  : new google.maps.Point( 25, 50 )
                        },
                        title    : busStopName,
                        position : position,
                        map      : map
                    });
                    
                    busStops[position]["busStopMarker"] = marker;
                    @for ($i = 0; $i < count($point->timeTable); $i++)
                        busStops[position]["busStopTimes"].push("{{$point->timeTable[$i]}}"); 
                        busStops[position]["busStopTimeFrequency"].push("{{$point->getTimeDetails()[$i]->frequency}}");
                    @endfor
                @endif
                count++;
            @endforeach

            var current_path_string = JSON.stringify( current_path);
            document.getElementById("current").value = current_path_string ;
            document.getElementById("current").value = current_path_string ;
            document.getElementById("old-current").value = current_path_string;
            var current_path = JSON.parse(current_path_string);
            
            chosenRouteColor = "{{$data['route']->basicColor}}";
            //alert(chosenRouteColor);
            $('#colorpicker').val("{{$data['route']->basicColor}}");
            current_line = new google.maps.Polyline({
                path: current_path,
                geodesic: true,
                strokeColor: "{{$data['route']->basicColor}}",
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable:true,
                draggable:false
            });
            
            current_line.setPath(current_path);
            current_line.setMap(map);
            
            setGoogleMapListeners(current_line);

            return current_path;
        }

        function update_current_map(){
            document.getElementById("savebtn");
        }

        function getDrawingManager()
        {
          //alert(JSON.stringify (drawingManager));
        }

        function initMap() {
            goo = google.maps;
            map = new goo.Map(document.getElementById('map'), {
                disableDefaultUI: true,
                mapTypeControl: false,
                center: {lat: -33.956809, lng: 18.460879},
                zoom: 17
            });
            
            infoWindow = new google.maps.InfoWindow;
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                
                if (companyPosition){
                    pos = companyPosition;
                }
                /*infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                infoWindow.open(map);*/
                map.setCenter(pos);
              }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
              handleLocationError(false, infoWindow, map.getCenter());
            }

            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYLINE,
                drawingControl: false,
                markerOptions: {icon: {!! json_encode(url('/')) !!}+"/img/map_location.png"},
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 5,
                    clickable: false,
                    editable: true,
                    radius: 2000,
                    zIndex: 1
                },
                polylineOptions : {
                    editable:true,
                    draggable:false,
                    geodesic:true
                }
            });
            
            var form = document.getElementById('toolbox');
            
            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, map, drawingManager);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(form);
            
            drawingManager.setMap(map);
            
            //set the time picker for bus stop times
            $('.time-picker').timepicker({
                use24hours: true,
                timeFormat: 'HH:mm',
                interval: 60,
                defaultTime: '11',
                startTime: '10:00',
                dynamic: true,
                dropdown: false,
                scrollbar: false
            });
            
            $('#bus-stop-modal').on('keydown','#time-input',function(e) {   
                if (document.getElementById('fancy-checkbox-default').checked && e.which == 13 && editingBusStop) {
                    timeRow(null,"0");
                }else if (e.which == 13 && editingBusStop){
                    alert('Please tick as bus-stop');
                }
            });
            
            $("#time-input").bind('paste', function(e) {
                e.preventDefault();
                
                if (window.clipboardData === undefined){
                    timeRows = e.originalEvent.clipboardData.getData('Text') // use this method in Chrome to get clipboard data.
                }else{
                    timeRows = window.clipboardData.getData('Text') // use this method in IE/Edge to get clipboard data.
                }
                
                var rows = timeRows.split("\n");
                
                for(var y in rows) {
                    if (isValidTime(rows[y].trim())){
                        //copy and paste from pdf or text file
                        timeRow(rows[y].trim(),"0");
                    }else{
                        //copy and paste from excel sheet
                        var cells = rows[y].split("\t");
                        for(var x in cells) {
                            timeRow(cells[x].trim(),"0");
                        }
                    }
                }
            });
            
            google.maps.event.addListener(drawingManager, 'markercomplete', function(marker) {
                if (locationInfo["marker"] != null){
                    locationInfo["marker"].setMap(null);
                }
                
                google.maps.event.addListener(marker, 'click', function() {
                    
                    $("#show-address").text(locationInfo["address"]);
                    $("#location-address").val(locationInfo["address"]);
                    $("#location-latitude").val(locationInfo["marker"].getPosition().lat());
                    $("#location-longitude").val(locationInfo["marker"].getPosition().lng());
                    
                    $("#location-modal").modal('show'); 
                });
                
                locationInfo["marker"] = marker;
                getAddress(marker.getPosition());
            });
            
            //when the entire polyline is complete this is called
            google.maps.event.addListener(drawingManager, 'polylinecomplete', function(line) {
                
                document.getElementById("current").value = JSON.stringify(line.getPath().getArray());
                
                if(current_line) {
                    current_line.setMap(null);
                }
                
                //make sure only current_line object is the one under edit
                previous_line = current_line;
                current_line = line;
                
                if (previous_line.getPath().getLength() > 0 && current_line .getPath().getLength() > 0){
                    if (distanceBetween(previous_line,current_line) < 100){
                        if (previous_line.getPath().getLength() >= 2){
                            current_line = combinePolylines(previous_line,current_line);
                        }
                    }else{
                        var r = confirm("Would you like to continue with current polyline?");
                        if (r == true) {
                            current_line = combinePolylines(previous_line,current_line);
                        } else {
                            clearBusStops();
                        }
                    }
                }
                
                setGoogleMapListeners(current_line);
            });
            
            load_current_route();
            load_current_location();
        };
        
        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            if (!browserHasGeolocation){
                infoWindow.setContent('Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
        }
          
        function getAddress(latlng) {
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        var address = results[1].formatted_address;
                        locationInfo["address"] = address;
                    }
                }
            });
        }
        
        function getCurrentLocation(){
            jQuery.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", 
            function(success) {
                    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
            })
            .fail(function(err) {
                alert("API Geolocation error! \n\n"+err);
            });
        }
        
        function combinePolylines(line1,line2){
            //line1 is the previous line
            //line 2 is the line which is going to be shown and must be returned
            var p=line2.getPath().getArray();
            p.reverse();
            line2.setPath(p);
            
            for (i=line1.getPath().getLength()-1;i >= 0;i--){
                line2.getPath().push(line1.getPath().getAt(i));
            }
            
            //reverse again to get the positions correct for further editing
            p=line2.getPath().getArray();
            p.reverse();
            line2.setPath(p);
            
            return line2;
        }
        
        function distanceBetween(line1,line2){
            //line 1 is the old line
            //line 2 is the new line
            var position1 = line1.getPath().getAt(line1.getPath().getLength()-1);
            var position2 = line2.getPath().getAt(0);
            
            var distance = google.maps.geometry.spherical.computeDistanceBetween(position1, position2);
            //alert('distance '+distance);
            return distance;
        }
        
        function setGoogleMapListeners(line){
            
            google.maps.event.addListener(line, 'click', function(index, obj) { 
                str = JSON.stringify(index, null, 4);
                console.log(str+" position "+index['latLng']);
                vertex_position = index['latLng'];
                vertex_index = index['vertex'];
                        
                //This sequence matters
                document.getElementById("bus-stop-name").value = "";
                document.getElementById("bus-times-div").innerHTML = "";

                if (busStops[vertex_position]){

                    fillBusStopModal(vertex_position);
                }

                editBusStop();
                $("#bus-stop-modal").modal('show');     
            });

            //called when you move a point to a new position after drawing the polyline
            google.maps.event.addListener(line.getPath(), 'insert_at', function(index, obj) {
                //alert('insert at '+index['vertex']+" "+index['latLng']);
                current_line = line;
                document.getElementById("current").value =JSON.stringify(line.getPath().getArray());
            });

            //called when you undo a drag
            google.maps.event.addListener(line.getPath(), 'remove_at', function(index, obj) {
                current_line = line;
                console.log("remove at");
            });

            google.maps.event.addListener(line.getPath(), 'set_at', function(index, obj) {
                //obj is the old position
                //index is the vertex position which does not change with set_at
                //newPos is the new position vertex has been moved to
                current_line = line;
                newPos = line.getPath().getAt(index);

                //reposition the marker if the vertex is a busStop
                if (busStops[obj]){
                    busStops[newPos] = new Array();
                    busStops[newPos]["busStopTimes"] = new Array();

                    busStops[newPos]['busStopName'] = busStops[obj]['busStopName'];
                    busStops[newPos]['busStopTimes'] = busStops[obj]['busStopTimes'];

                    busStops[obj]['busStopMarker'].setPosition(newPos);
                    busStops[newPos]['busStopMarker'] = busStops[obj]['busStopMarker'];
                }

                document.getElementById("current").value = JSON.stringify(line.getPath().getArray());
            });
            
            google.maps.event.addListener(line, 'rightclick', function(e) {
            });
        }
        
        function fillBusStopModal(vertex_position){
            var busStopName = busStops[vertex_position]["busStopName"];
            
            document.getElementById("bus-stop-name").value = busStopName;
            fillBusStopTimes(busStops[vertex_position]);
        }
        
        function clearStopTimes(){
            document.getElementById("bus-times-div").innerHTML="";
        }
        
        function removeElement(id){
            var element = document.getElementById(id);
            element.parentNode.removeChild(element);
        }
        
        /*
         * Extends the line from the position clicked only if it is at the end
         */
        function extendPolyline(){
            //when the first vertex has been clicked reverse the path to
            //be able do push a vertex
            if(vertex_index===0){
              var p=current_line.getPath().getArray();
              p.reverse();
              current_line.setPath(p);
            }else if (vertex_index != current_line.getPath().getLength()-1){
                return;
            }
            
            editRoute();
            
            //push a vertex 
            current_line.getPath().push(vertex_position);
            //observe the mousemove of the map to set the latLng of the last vertex
            var move= goo.event.addListener(map,'mousemove',function(e){
                alert('mousing');
               current_line.getPath().setAt(current_line.getPath().getLength()-1,vertex_position); 
            });
        }
    
        /**
        * Deletes the vertex from the path.
        */
        function deleteVertex(){
            if (!current_line || vertex_index == undefined) {
                return;
            }
            
            current_line.getPath().removeAt(vertex_index);
        }
        
        function setRouteColor(){
            
            if (drawing){
                editRoute();
            }
            //
            chosenRouteColor = $('#colorpicker').val();
            //alert("chosen route color "+chosenRouteColor);
            if (current_line){
                current_line.setOptions({strokeColor: "#"+chosenRouteColor});
            }
        }
        
        window.onload = function(e){
            $("#upload-stop-times").on('change', uploadTimes);
        }
        
        function updateEmployee(employee, latlng)
        {
           
          if( !(vehicleMarkers.has(employee.employeeId)) )
          {
            //var latlng = new google.maps.LatLng(employee.latitude,employee.longitude);
               
            var marker = new google.maps.Marker( {
                icon     : {
                    url     : {!! json_encode(url('/')) !!}+"/img/bus_orange.png",
                    size    : new google.maps.Size( 100, 100 ),
                    anchor  : new google.maps.Point( 25, 50 )
                },
                title    : employee.name,
                position : latlng,
                map      : map
            });
            
            var content = '<div id="iw-container">' +
                    '<div class="iw-title">'+employee.route.routeId+'</div>' +
                    '<div class="iw-content">' +
                      '<div class="iw-subTitle">'+employee.name+'</div>' +
                      '<div class="iw-subTitle"> Last Seen: '+employee.lastSeenReadable+'</div>' +
                    '</div>' +
                  '</div>';
          
            var infoWindow = new google.maps.InfoWindow({
                content: content,
                maxWidth: 350
            });
            infoWindow.open(map,marker);
            
            marker.addListener('mouseover', function() {
                infoWindow.open(map, this);
            });

            // assuming you also want to hide the infowindow when user mouses-out
            marker.addListener('mouseout', function() {
                infoWindow.close();
            });

            employeeRoute.set(employee.employeeId,employee.route.routeId);
            marker.addListener('click', function() {
                
                //alert(employee.employeeId);
                document.getElementById("employee-name").innerHTML = employee.name;
                document.getElementById("employee-id").innerHTML = employee.employeeId;
                document.getElementById("employee-id").value = employee.employeeId;
                setCurrentBusMap( employeeRoute.get(employee.employeeId));
                //we are setting the selected route to the employee route
                //updateEmployeeRoute();
                plotEmployeeRoute(employee.route.points,employee.route.basicColor);
                  
            });
            vehicleMarkers.set(employee.employeeId,marker);
    
          }
          else
          {
             // var latlng = new google.maps.LatLng(employee.latitude,employee.longitude);
             //alert(JSON.stringify(latlng));
              vehicleMarkers.get(employee.employeeId).setPosition(latlng);
              
              move_marker(employee.employeeId,latlng);
              
          }
        }
        
        function move_marker(employeeID ,latlng)
        {
            vehicleMarkers.get(employeeID).setPosition(latlng);
        }
        
        function setCurrentBusMap(value)
        {
            var selection = document.getElementById('current-bus-map');
            var options = selection.options;
            for (var opt, j = 0; opt = options[j]; j++) {
                   if (opt.value == value) {
                       selection.selectedIndex = j;
                   break;
               }
           }
        }
        
        function updateVehicleDestination(employeesArray)
        {
             for (var i = 0; i < employeesArray.length; i++) {
               if(employeesArray[i].status == 'online')
               {
                   var currentEmployee =  employeesArray[i];
                   var destination_lat_lng =   new google.maps.LatLng(employeesArray[i].latitude, employeesArray[i].longitude);
                   
                   //currentEmployee.
                   //var latlng = new google.maps.LatLng($point.latitude,$point.longitude);
                   vehicleDestination.set(currentEmployee.employeeId , destination_lat_lng);
                   //updateEmployee(employeesArray[i],latlng);
               }
               
            }
        }
        function updateEmployees(employeesArray,current_step)
        {
           // updateEmployee(employeesArray[i]);
           for (var i = 0; i < employeesArray.length; i++) {
               //alert();
               var currentEmployee =  employeesArray[i];
               if(employeesArray[i].status == 'online')
               {
                   
                   //alert("Current Employee"+ JSON.stringify(currentEmployee));
                   if(!vehicleMarkers.has(currentEmployee.employeeId))
                   {
                        var latlng1 = new google.maps.LatLng(currentEmployee.latitude,currentEmployee.longitude);
                        updateEmployee(currentEmployee,latlng1);
                   }
                   
                   var EmployeeMarker = vehicleMarkers.get(currentEmployee.employeeId);
                   // alert(vehicleDestination.get(currentEmployee.employeeId).lat());
                   var latdif = (vehicleDestination.get(currentEmployee.employeeId).lat()-EmployeeMarker.getPosition().lat() ) ;
                   var lngdif = (vehicleDestination.get(currentEmployee.employeeId).lng()-EmployeeMarker.getPosition().lng() );
                   
                   var latoffset = (latdif/(total_steps-current_step));
                   var lngoffset = (lngdif/(total_steps-current_step));
                   
                  
                       
                    
                   
                    var latlng = new google.maps.LatLng(EmployeeMarker.getPosition().lat()+latoffset,EmployeeMarker.getPosition().lng()+lngoffset);
                   //vehicleDestination.set(employee.employeeId,employee.);
                  // alert(total_steps-current_step);
                   updateEmployee(currentEmployee,latlng);
                   
                   //alert(JSON.stringify(latlng));
                   //move_marker(currentEmployee.employeeId ,latlng);
               }
               else
               {
                  
                   
               }
            }
           if(current_step+1 < total_steps ){
                setTimeout(function(){ updateEmployees(employeesArray, current_step+1)},5000/total_steps);
           }
        }
        
        function update_bus() {
            $.ajax({
                type: "GET",
                url: {!! json_encode(url('/')) !!}+'/api/companies/{{$data["companyID"]}}/employees',
                success: function(data) {
                // do something with the return value here if you like
                    if(vehicle_mode)
                    {   
                        updateVehicleDestination(data["employees"]);    
                        updateEmployees(data["employees"], 1);
                    
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    console.log(xhr.responseText);
                 },
                beforeSend: function (xhr) {
                
                /* Authorization header */              
                xhr.setRequestHeader("companyID", "{{$data['companyID']}}");
                
            },

            });
            if (vehicle_mode)
            {
                setTimeout(update_bus, bus_refresh_rate); // you could choose not to continue on failure...
            }
            else
            {
                clearBusMarkers();
            }
    }
    
    function clearBusMarkers()
    {
        vehicleMarkers.forEach(function (item, key, mapObj) {  
            item.setMap(null);
            vehicleMarkers.delete(key);
        });  
    }
   
    function plotEmployeeRoute(points, colour){
        clearRoutes();
        //data["employees"][0]["route"]["points"]
        
        var current_path = new Array();
            
            var count = 0;
            //foreach ( points as $point)
            for(var i = 0; i < points.length; i++)
            {
                var $point = points[i] ;
                var position = new google.maps.LatLng($point.latitude,$point.longitude);
                if (count == 0){
                    //map.setZoom(17);
                    //map.panTo(position);
                }
                current_path[$point.id - 1] = position;
                
                if ($point.busStop === "true")
                {    busStops[position] = new Array();
                    var busStopName = $point.name;
                    
                    busStops[position]["busStopName"] = busStopName;
                    busStops[position]["busStopTimes"] = new Array();
                    busStops[position]["busStopTimeFrequency"] = new Array();
                    
                    //create marker for the vertex position
                    var marker = new google.maps.Marker( {
                        icon     : {
                            url     : {!! json_encode(url('/')) !!}+"/img/bus_stop.png",
                            size    : new google.maps.Size( 50, 50 ),
                            anchor  : new google.maps.Point( 25, 50 )
                        },
                        title    : busStopName,
                        position : position,
                        map      : map
                    });
                    
                    busStops[position]["busStopMarker"] = marker;
                    
                   // foreach ( $point.timeTable as $time)
                    for(var j = 0;j < $point.timeTable.length;j++){
                        busStops[position]["busStopTimes"].push($point.timeTable[j]);
                        busStops[position]["busStopTimeFrequency"].push($point.timeDetails[j].frequency);
                    }
                }
                count++;
            }
            
            var current_path_string = JSON.stringify( current_path);
            document.getElementById("current").value = current_path_string ;
            
            document.getElementById("old-current").value = current_path_string;
            
            var current_path = JSON.parse(current_path_string);
            
            chosenRouteColor = colour;
            $('#colorpicker').val(colour);
            current_line = new google.maps.Polyline({
                path: current_path,
                geodesic: true,
                strokeColor: colour,
                strokeOpacity: 1.0,
                strokeWeight: 2,
                editable:false,
                draggable:false
            });
            
            current_line.setPath(current_path);
            current_line.setMap(map); 
        
    }
    
    function updateEmployeeRoute(){
     
        document.getElementById('employeeID').value= document.getElementById('employee-id').value;
        document.getElementById('routeName').value=document.getElementById('current-bus-map').value;
        
        employeeRoute.set(document.getElementById('employee-id').value,document.getElementById('current-bus-map').value);
             $("#loading").val("loading");
            $.ajax({
                type: "post",
                url: {!! json_encode(url('/')) !!}+'/route/update',
                success: function(data) {
                    $("#loading").val("");
                  
                    plotEmployeeRoute(data["employees"][0]["route"]["points"],data["employees"][0]["route"].basicColor);
                },
                beforeSend: function (xhr) {
            },
            data: $("#update_employee_route").serialize()

            });
            
        }
    </script>
    
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDtM_vJ0Vzzexq_xaT-TYbaV3w95pIu3U&libraries=drawing&callback=initMap">
    </script>
    
    <script src="{{url('/')}}/js/app.js"></script>
  </body>
</html>
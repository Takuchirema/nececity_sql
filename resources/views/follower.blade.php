<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    
    <title>Manage Followers</title>

    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-lg-2 col-bordered">
                <h4>Followers</h4>
                <h4>{{count($data)}}</h4>
            </div>
        </div>

        <div class="row row-bordered">
            <div class="col-lg-4"><h4>profile</h4></div>
            <div class="col-lg-4"><h4>name</h4></div>
            <div class="col-lg-2"><h4>joined</h4></div>
            <div class="col-lg-2"><h4>access</h4></div>
        </div>
        
        @foreach ( $data as $indexKey => $follower )
        
            <form method="post" autocomplete="off">
                <div class = "row">
                    
                    <div class="col-lg-4">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @if ($follower->profilePicture == null)
                            <a href="#{{$follower->userId}}" onclick="profilePictureModal(this,'{{$follower->userId}}')" data-toggle="modal" data-target="#myModal" data-picture-url="{{url('/')}}/img/profile_picture.png">
                                <img style="width:20%" src="{{url('/')}}/img/profile_picture.png" alt="Cinque Terre">
                            </a>
                        @else
                            <a href="#{{$follower->userId}}" onclick="profilePictureModal(this,'{{$follower->userId}}')" data-picture-url="{{$follower->profilePicture}}">
                                <img style="width:20%;height:15%;" src="{{$follower->profilePicture}}" class="img-circle" alt="Cinque Terre">
                            </a>
                        @endif
                        
                        <div id="{{$follower->userId}}-modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">              
                              <div class="modal-content">

                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>                  
                                </div>

                                <div class="modal-body">
                                    <img style="width:100%"src="" class="img-responsive center-block" alt="Cinque Terre">
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        @if ($follower->name != null)
                            <h4>{{$follower->name}}</h4>
                        @elseif ($follower->fbName != null)
                            <h4>{{$follower->fbName}}</h4>
                        @else
                            <h4>{{$follower->userId}}</h4>
                        @endif
                    </div>

                    <div class="col-lg-2">
                        <h4>{{$follower->dayJoined}}</h4>
                    </div>
                    
                    <div class="col-lg-2">
                        @if ($follower->blocked == "true")
                            <input formaction="companies/{{ $follower->companyId }}/unblock/{{$follower->userId}}" class="btn btn-default green" type="submit" value="Unblock">
                        @else
                            <input formaction="companies/{{ $follower->companyId }}/block/{{$follower->userId}}" class="btn btn-default orange" type="submit" value="Block">                        
                        @endif
                    </div>
                    
                 </div>
            </form>
        @endforeach
    </div>
 @endsection

 <script type="text/javascript">
     
    function profilePictureModal(element,id){
        var pictureUrl = element.getAttribute('data-picture-url');
        $("#"+id+"-modal").find('img').attr("src",pictureUrl);
        $("#"+id+"-modal").modal('show');
    }
 </script>
 
</body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nececity</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/">Nececity</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                       
                        <a href="business" class="btn default-btn">Business</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
    <section style="background: #fff;height:100%;" class="contact bg-primary">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
              <li data-target="#myCarousel" data-slide-to="5"></li>
              <li data-target="#myCarousel" data-slide-to="6"></li>
              <li data-target="#myCarousel" data-slide-to="7"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                
              <div class="item active">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/follow_company.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Jammie Shuttles</h2>
                  <p style="font-size: 20px;">Important! To get real-time information for your Jammies, pleased
                  go to the menu, select companies and go to 'Manage'. Select Jammie Shuttle and scroll down to 'Follow'</p>
                </div>
              </div>
                
              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/search_shuttles.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Jammie Shuttles</h2>
                  <p style="font-size: 20px;">Drag down the top arrow and enter any route in the search box. It will appear if the shuttle is 
                  on the road and you can view its location, route and times.</p>
                </div>
              </div>

              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/next_time_arriving.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Jammie Shuttles</h2>
                  <p style="font-size: 20px;">Click on any bus-stop to view the time tables. The one highlighted in orange is the next expected
                  departure time from that stop. The eye on top is to set an alarm when the bus is within 200m. The
                  feature is under improvement.</p>
                </div>
              </div>
                
              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/company_posts.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Jammie Shuttles</h2>
                  <p style="font-size: 20px;">Get Notifications from Jammie Shuttle about expected delays or changes in time tables and routes
                  .</p>
                </div>
              </div>

              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/locate_friends.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Friends</h2>
                  <p style="font-size: 20px;">Pull up the button at the bottom to get your friends location and chat with them on whatsapp.</p>
                </div>
              </div>
              
              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/restaurants.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Restaurants</h2>
                  <p style="font-size: 20px;">Check out the restaurants at Rondebosch and neighbouring areas and the promotions
                  happening on the day. Just search "Rondebosch Promotions" for example, in companies manage view.
                  More restaurants coming soon!</p>
                </div>
              </div>
                
              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/user_settings.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Your Settings</h2>
                  <p style="font-size: 20px;">Important! Check the settings to make sure you have the preferred ones for frequency
                  of updates. Please take note of the battery and data usages. The round orange button on the map's
                  is for manual refresh, so you can also use that!</p>
                </div>
              </div>
                
              <div class="item">
                <img class="item-back-img" src="{{url('/')}}/img/tutorial/background.jpg" alt="Chania">
                <img class="mobile-tutorial" src="{{url('/')}}/img/tutorial/visibility_settings.png" alt="Chania">
                <img class="android-frame" src="{{url('/')}}/img/tutorial/android_frame.png" alt="Chania">
                <div class="carousel-caption right-caption text-right">
                  <h2>Your Settings</h2>
                  <p style="font-size: 20px;">Important! Visibility allows only friends in a certain locality to view your location! Please
                  set this to your preference. Also the location button on the map makes you invisible to everyone!</p>
                </div>
              </div>
                
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
    </section>

    <footer>
        <div class="container">
            <p style="font-size: 20px;"></p>
            <ul class="list-inline">
                <li>
                    <a href="privacy">Privacy</a>
                </li>
                <li>
                    <a href="terms">Terms</a>
                </li>
                <li>
                    <a href="faq">FAQ</a>
                </li>
            </ul>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('home/js/new-age.min.js')}}"></script>

</body>

</html>

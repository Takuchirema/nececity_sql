<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nececity</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('home/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset('home/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('home/vendor/device-mockups/device-mockups.min.css')}}">

    <!-- Theme CSS -->
    <link href="{{asset('home/css/new-age.min.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/">Nececity</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                       
                        <a href="business" class="btn default-btn">Business</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header style="margin-top:0px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="header-content">
                        <div class="header-content-inner">
                            <a href="#policy">
                                <h1>Nececity Privacy Policy</h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    <section style="background: #fff;text-align: left;" id="policy" class="contact bg-primary">
        <div class="container">
            <p style="color:#333;float:left;">
                
                Nececity cares about the privacy of our user’s information.
                Nececity aims to give our user’s the best experience in the present time with our services as well as work towards a better future using analytics. User information will be used primarily providing the services we render and as a source of data for future improvements to improve the Nececity Experience for you. This is our privacy policy so that Nececity user’s understand how we gather, store and use and most importantly protect user information.
                <br><br>Our policy, applies to you if you are the following:<br><br>
                An end-user of our app
                A company that has signed up to be on our platform
                Information we collect through the use of our services
                <br><br>Location Information : 
                <br><br>The Nececity app will collect location information when you request information about nearby nodes or points of inter. A company employee information is collected when the app is .This occurs when the app is in the foreground and or operating in the background.
                <br><br>Identification information : 
                <br><br>The nececity stores and keeps user information and changes.
                <br><br>Transaction Information : 
                <br><br>Company Banking details will be used by third party services to permit payments.
                <br><br>Log Information: 
                <br><br>Information of usage and requests made will be logged by the servers.
                <br><br>Use of Information
                <br><br>Render Area Specific Services: 
                <br><br>Nececity uses location information to customize information presented to the user that is location dependant. This includes nearby transporation units, setting location specific settings and nececity user population density.
                Setting or sending current location to other’s by the user’s own request.
                <br><br>Identification of users: 
                <br><br>Nececity will make use of user information to render services that require identification of user.
                Usage information will be used to perform analytics to detect trends an forms of service abuse.
                Faciliate communication between users
                Personalize user experience based on information gathered

                <br><br>Your Information Rights
                <br><br>All user information that is traceable to a user can be altered, changed or removed at the users request.
                Users can turn off location services on our app. Identification information is provided to use by the users on volition
                <br><br>Contact Us:
                <br><br>If you have any enquiries to Nececity’s privacy policy please contact us via email on support@nececity.com
                
            </p>
        </div>
    </section>

    <footer>
        <div class="container">
            <p></p>
            <ul class="list-inline">
                <li>
                    <a href="privacy">Privacy</a>
                </li>
                <li>
                    <a href="terms">Terms</a>
                </li>
                <li>
                    <a href="faq">FAQ</a>
                </li>
            </ul>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{asset('home/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('home/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('home/js/new-age.min.js')}}"></script>

</body>

</html>

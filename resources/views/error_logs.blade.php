
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
    
<body id="app-layout">
@extends('layouts.app')
@section('content')

<div style="margin: 10px;" class = "row">
    <span class="border border-warning rounded">
        <pre>{{$data}}</pre>
    </span>
</div>

<div class="col-lg-2">
    <form action="/clear/logs" method="post" autocomplete="off">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div style="margin-top: 10px;" class="col-lg-1">
             <input class="btn btn-default" type="submit" value="clear">
        </div>
    </form>         
</div>

@endsection
</body>
    
</html>
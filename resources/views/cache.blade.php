<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Cached Items</title>
    
    <style>
        
        /* Remove default bullets */
        ul, #myUL {
          list-style-type: none;
        }

        /* Remove margins and padding from the parent ul */
        #myUL {
          margin: 0;
          padding: 0;
        }

        /* Style the caret/arrow */
        .caret {
          cursor: pointer; 
          user-select: none; /* Prevent text selection */
        }

        /* Create the caret/arrow with a unicode, and style it */
        .caret::before {
          content: "\25B6";
          color: black;
          display: inline-block;
          margin-right: 6px;
        }

        /* Rotate the caret/arrow icon when clicked on (using JavaScript) */
        .caret-down::before {
          transform: rotate(90deg); 
        }

        /* Hide the nested list */
        .nested {
          display: none;
        }

        /* Show the nested list when the user clicks on the caret/arrow (with JavaScript) */
        .active {
          display: block;
        }
    </style>
</head>

<body id="app-layout">
      
    @php
        $storage = \Cache::getStore(); // will return instance of FileStore
        $filesystem = $storage->getFilesystem(); // will return instance of Filesystem
        $dir = (\Cache::getDirectory());
        $keys = [];
        foreach ($filesystem->allFiles($dir) as $file1) {
            if (is_dir($file1->getPath())) {
                foreach ($filesystem->allFiles($file1->getPath()) as $file2) {
                    $contents = \File::get($file2->getRealpath());
                    $expire = substr($contents, 0, 10);
                    $time = gmdate("Y-m-d H:i:s", $expire);
                    $keys = array_merge($keys, [$time => unserialize(substr($contents, 10))]);
                }
            }
            else {
                $contents = \File::get($file1->getRealpath());
                $expire = substr($contents, 0, 10);
                $time = gmdate("Y-m-d H:i:s", $expire);
                $keys = array_merge($keys, [$time => unserialize(substr($contents, 10))]);
            }
        }
    @endphp

    @foreach ( $keys as $expiry=>$cachetype)
        @if (is_array($cachetype))
            <p>Expiry Time: {{$expiry}}</p>
            @foreach ($cachetype as $cacheObject)
                @if (is_object($cacheObject))
                    <li><span class="caret">{{get_class($cacheObject)}}</span>
                        <ul class="nested">
                            @foreach (get_object_vars($cacheObject) as $name=>$var)
                                <li>
                                    '{{$name}} - {{var_dump($var)}}'
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
        @endif
    @endforeach

    
    <script type='text/javascript'>
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
          });
        }
    </script>
 
</body>

</html>

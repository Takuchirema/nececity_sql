@extends('layouts.appuser')

@section('content')
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '182966438884026',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10'
    });
    FB.AppEvents.logPageView();
    FB.getLoginStatus(function(response) {
        
        FB.api('/me', { locale: 'en_US', fields: 'name, email' },
            function(response) 
                {
                    if (typeof response.error == 'undefined') {
                        fbloginredirect(response.id, response.email, response.name);
                    }
                }
            );
            
        }, {scope: 'email,user_likes'});
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=182966438884026";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>
    
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form id="login-form" class="form-horizontal" role="form" method="POST" action="{{ url('user/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button onclick="hashPassword()" type="button" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('password/user/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                    <div  onlogin="refresh();" class="fb-login-button col-md-8 col-md-offset-4" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="true"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function hashPassword(){
        textPassword = $('#password').val();
        hashedPassword = textPassword;
        
        //alert('b4 hash: '+textPassword+' after hash: '+hashedPassword);
        $('#password').val(hashedPassword);
        document.getElementById("login-form").submit();
    };
    function refresh(){
        window.location = "{{url('user/login')}}";
    }
    function fbloginredirect( fbID, fbEmail,fbName) {
        
            method =  "post"; // Set method to post by default if not specified.

            // The rest of this code assumes you are not using a library.
            // It can be made less wordy if you use one.
            var form = document.createElement("form");
            form.setAttribute("method", method);
            form.setAttribute("action", "{{ url('user/login') }}");
                
            var csrf_field = document.createElement("input");
            csrf_field.setAttribute("type", "hidden");
            csrf_field.setAttribute("name", "_token");
            csrf_field.setAttribute("value", "{{csrf_token()}}");
            
            form.appendChild(csrf_field);
             
            var fbIDField = document.createElement("input");
            fbIDField.setAttribute("type", "hidden");
            fbIDField.setAttribute("name", "username");
            fbIDField.setAttribute("id", "username");
            fbIDField.setAttribute("value", fbID);
            
            form.appendChild(fbIDField);
            
            var fbEmailInput = document.createElement("input");
            fbEmailInput.setAttribute("type", "hidden");
            fbEmailInput.setAttribute("name", "password");
            fbEmailInput.setAttribute("id", "password");
            fbEmailInput.setAttribute("value", "");
            
            form.appendChild(fbEmailInput);
            
    document.body.appendChild(form);
  form.submit();
     
}
</script>
@endsection

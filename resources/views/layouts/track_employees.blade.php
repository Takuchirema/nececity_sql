<html lang="en">
<head>
    <title>Drawing tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{url('/')}}/js/jquery.timepicker.min.js"></script>
    <script src="{{url('/')}}/js/side-navigation.js"></script>
    
    <link href="{{url('/')}}/css/jquery.timepicker.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    <link href="{{url('/')}}/css/side-navigation.css" rel="stylesheet">
    
    <script src="{{url('/')}}/js/jscolor.js"></script>
    <style>
       /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
</head>
<body id="app-layout">
@extends('layouts.map')

<script>
    // This example requires the Drawing library. Include the libraries=drawing
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
    var current_line;
    var previous_line;

    var current_map;
    var drawingManager;
    var drawing = false;
    var locationEditing = false;
    var editingBusStop = false;
    var timeIds = 1;
    var vertex_position;
    var vertex_index;
    var chosenRouteColor = "#000";
    var bus_refresh_rate = 5000 ;
    var total_steps = 20;
    var goo;
    var map;

    var vehicle_mode = true;
    //key value with vertices which are bustops with their values
    var busStops = new Array();
    var locationInfo = new Array();
    var vehicleMarkers= new Map();
    var infoWindows = new Map();
    var employeeRoute = new Map();

    var vehicleDestination = new Map();

    function changeCompany(){
        var selection = document.getElementById('current-company');
        var selectedNode = selection.options[selection.selectedIndex];
        var company = selectedNode.value;

        location.href = location.href.substring(0, location.href.lastIndexOf('company_routes')+14) + "/"+company;
    }

    function isValidTime(time){
        if (time.trim().length === 0){
            return false;
        }

        var re = new RegExp("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
        if (re.test(time)) {
            return true;
        }
        return false;
    }

    function timeRow(time){
        var div = document.createElement('div');
        div.className = 'time-container';
        div.style.width = '100%';
        div.id = 'div-'+timeIds;
        div.setAttribute('data-id',timeIds);

        var cancelBtn = document.createElement('button');
        cancelBtn.className = 'round-button red';
        cancelBtn.innerHTML = 'X';
        cancelBtn.setAttribute( 'onClick', "removeElement('div-"+timeIds+"')" );

        var input = document.createElement('input');
        input.className = 'timepicker timepicker-without-dropdown text-center';
        input.id = 'time-'+timeIds;

        $("#bus-times-div").append(div);

        if (time === null){
            time = $('#time-input').val();
            if (isValidTime(time)){
                div.appendChild(input);
                div.appendChild(cancelBtn);
                $("#time-"+timeIds).val($('#time-input').val());
            }else{
                alert("Please enter a valid time");
            }
        }else{
            if (isValidTime(time)){
                div.appendChild(input);
                div.appendChild(cancelBtn);
                $("#time-"+timeIds).val(time);
            }
        }
        timeIds++;
    }

    function togglebus()
    {
        //remove all routes and bus stops
        clearRoutes();

        $("#show-buses").attr("class","round-button orange");

        if(vehicle_mode)
        {
            update_bus();
        }
        else
        {
            clearBusMarkers();
        }
    }

    function uploadTimes()
    {
        var fileToLoad = document.getElementById("upload-stop-times").files[0];

        var fileReader = new FileReader();
        fileReader.onload = function(fileLoadedEvent){
            var timesFile = fileLoadedEvent.target.result;
            var times = timesFile.split('\n');
            for(var i = 0;i < times.length;i++){
                time = times[i].trim();

                if (isValidTime(time)){
                    timeRow(time);
                }
            }
            document.getElementById('upload-stop-times').value= null;
        };
        fileReader.readAsText(fileToLoad, "UTF-8");
    }

    function CenterControl(controlDiv, map, drawingManager) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click to recenter the map';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'Save';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
            // google.maps.event.trigger(drawingManager, 'polylinecomplete') ;
            alert(  document.getElementById("vertices").value );    
        });
    }


    function clearRoutes(){
        clearBusStops();
        if (current_line){
            current_line.setMap(null);
        }
    }

    function clearBusStops(){
        for (var key in busStops){
            var marker = busStops[key]['busStopMarker'];
            marker.setMap(null);
        }
    }
    
    //save from the save route being edited
    function load_current_location(){
        @if ($data['location'] != null)
            var latitude = parseFloat("{{$data['location']->latitude}}");
            var longitude = parseFloat("{{$data['location']->longitude}}");
            var address = "{{$data['location']->address}}";
            var title;

            if (address){
                title = address;
            }else{
                title = "Jammie Shuttle";
            }

            var position = new google.maps.LatLng(latitude, longitude);

            var marker = new google.maps.Marker( {
                icon     : {
                    url     : {!! json_encode(url('/')) !!}+"/img/map_location.png",
                    size    : new google.maps.Size( 50, 50 ),
                    anchor  : new google.maps.Point( 25, 50 )
                },
                title    : title,
                position : position,
                map      : map
            });

            map.setCenter(new google.maps.LatLng(latitude, longitude));

            locationInfo["marker"] = marker;

        @endif

    }

    function initMap() {
        goo = google.maps;
        map = new goo.Map(document.getElementById('map'), {
            disableDefaultUI: true,
            mapTypeControl: false,
            center: {lat: -33.956809, lng: 18.460879},
            zoom: 17
        });

        drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: null,

            drawingControl: false,
            markerOptions: {icon: {!! json_encode(url('/')) !!}+"/img/map_location.png"},
            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: false,
                editable: false,
                radius: 2000,
                zIndex: 1
            },
            polylineOptions : {
                editable:false,
                draggable:false,
                geodesic:true
            }
        });

        var form = document.getElementById('toolbox');

        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map, drawingManager);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(form);

        drawingManager.setMap(map);

        //set the time picker for bus stop times
        $('.time-picker').timepicker({
            use24hours: true,
            timeFormat: 'HH:mm',
            interval: 60,
            defaultTime: '11',
            startTime: '10:00',
            dynamic: true,
            dropdown: false,
            scrollbar: false
        });

        $('#bus-stop-modal').on('keydown','#time-input',function(e) {   
            if (document.getElementById('fancy-checkbox-default').checked && e.which == 13 && editingBusStop) {
                timeRow(null);
            }else if (e.which == 13 && editingBusStop){
                alert('Please tick as bus-stop');
            }
        });

        $("#time-input").bind('paste', function(e) {
            e.preventDefault();

            if (window.clipboardData === undefined){
                timeRows = e.originalEvent.clipboardData.getData('Text') // use this method in Chrome to get clipboard data.
            }else{
                timeRows = window.clipboardData.getData('Text') // use this method in IE/Edge to get clipboard data.
            }

            var rows = timeRows.split("\n");

            for(var y in rows) {
                if (isValidTime(rows[y].trim())){
                    //copy and paste from pdf or text file
                    timeRow(rows[y].trim());
                }else{
                    //copy and paste from excel sheet
                    var cells = rows[y].split("\t");
                    for(var x in cells) {
                        timeRow(cells[x].trim());
                    }
                }
            }
        });

        google.maps.event.addListener(drawingManager, 'markercomplete', function(marker) {
            if (locationInfo["marker"] != null){
                locationInfo["marker"].setMap(null);
            }

            google.maps.event.addListener(marker, 'click', function() {

                $("#show-address").text(locationInfo["address"]);
                $("#location-address").val(locationInfo["address"]);
                $("#location-latitude").val(locationInfo["marker"].getPosition().lat());
                $("#location-longitude").val(locationInfo["marker"].getPosition().lng());

                $("#location-modal").modal('show'); 
            });

            locationInfo["marker"] = marker;
            getAddress(marker.getPosition());
        });

        //when the entire polyline is complete this is called
        google.maps.event.addListener(drawingManager, 'polylinecomplete', function(line) {

            document.getElementById("current").value = JSON.stringify(line.getPath().getArray());

            if(current_line) {
                current_line.setMap(null);
            }

            //make sure only current_line object is the one under edit
            previous_line = current_line;
            current_line = line;

            if (previous_line.getPath().getLength() > 0 && current_line .getPath().getLength() > 0){
                if (distanceBetween(previous_line,current_line) < 100){
                    if (previous_line.getPath().getLength() >= 2){
                        current_line = combinePolylines(previous_line,current_line);
                    }
                }else{
                    var r = confirm("Would you like to continue with current polyline?");
                    if (r == true) {
                        current_line = combinePolylines(previous_line,current_line);
                    } else {
                        clearBusStops();
                    }
                }
            }
            setGoogleMapListeners(current_line);
        });
        load_current_location();
    };

    function getAddress(latlng) {
        var geocoder = geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var address = results[1].formatted_address;
                    locationInfo["address"] = address;
                }
            }
        });
    }

    function distanceBetween(line1,line2){
        //line 1 is the old line
        //line 2 is the new line
        var position1 = line1.getPath().getAt(line1.getPath().getLength()-1);
        var position2 = line2.getPath().getAt(0);

        var distance = google.maps.geometry.spherical.computeDistanceBetween(position1, position2);
        //alert('distance '+distance);
        return distance;
    }

    function setGoogleMapListeners(line){

        google.maps.event.addListener(line, 'click', function(index, obj) { 
            str = JSON.stringify(index, null, 4);
            console.log(str+" position "+index['latLng']);
            vertex_position = index['latLng'];
            vertex_index = index['vertex'];

            //This sequence matters
            document.getElementById("bus-stop-name").value = "";
            document.getElementById("bus-times-div").innerHTML = "";

            if (busStops[vertex_position]){

                fillBusStopModal(vertex_position);
            }

            editBusStop();
            $("#bus-stop-modal").modal('show');     
        });

        //called when you move a point to a new position after drawing the polyline
        google.maps.event.addListener(line.getPath(), 'insert_at', function(index, obj) {
            //alert('insert at '+index['vertex']+" "+index['latLng']);
            current_line = line;
            document.getElementById("current").value =JSON.stringify(line.getPath().getArray());
        });

        //called when you undo a drag
        google.maps.event.addListener(line.getPath(), 'remove_at', function(index, obj) {
            current_line = line;
            console.log("remove at");
        });

        google.maps.event.addListener(line.getPath(), 'set_at', function(index, obj) {
            //obj is the old position
            //index is the vertex position which does not change with set_at
            //newPos is the new position vertex has been moved to
            current_line = line;
            newPos = line.getPath().getAt(index);

            //reposition the marker if the vertex is a busStop
            if (busStops[obj]){
                busStops[newPos] = new Array();
                busStops[newPos]["busStopTimes"] = new Array();

                busStops[newPos]['busStopName'] = busStops[obj]['busStopName'];
                busStops[newPos]['busStopTimes'] = busStops[obj]['busStopTimes'];

                busStops[obj]['busStopMarker'].setPosition(newPos);
                busStops[newPos]['busStopMarker'] = busStops[obj]['busStopMarker'];
            }

            document.getElementById("current").value = JSON.stringify(line.getPath().getArray());
        });

        google.maps.event.addListener(line, 'rightclick', function(e) {
        });
    }

    function fillBusStopModal(vertex_position){
        var busStopName = busStops[vertex_position]["busStopName"];
        var busStopTimes = busStops[vertex_position]["busStopTimes"];

        document.getElementById("bus-stop-name").value = busStopName;
        for (var i=0;i<busStopTimes.length;i++){
            var time = busStopTimes[i];
            timeRow(time);
        }
    }

    function clearStopTimes(){
        document.getElementById("bus-times-div").innerHTML="";
    }

    function removeElement(id){
        var element = document.getElementById(id);
        element.parentNode.removeChild(element);
    }

    function updateEmployee(employee, latlng)
    {
      //console.log(employee);
      if (employee.route !== null){
        employeeRoute.set(employee.employeeId,employee.route.routeName);
      }
      
      if(!(vehicleMarkers.has(employee.employeeId)))
      {
        //var latlng = new google.maps.LatLng(employee.latitude,employee.longitude);
        console.log("no employee");
        console.log(employee);
        var marker = new google.maps.Marker( {
            icon     : {
                url     : {!! json_encode(url('/')) !!}+"/img/bus_orange.png",
                size    : new google.maps.Size( 100, 100 ),
                anchor  : new google.maps.Point( 25, 50 )
            },
            title    : employee.name,
            position : latlng,
            map      : map
        });
        
        var routeName = "Not on Route";
        
        if (employee.route !== null){
            routeName = employee.route.routeName;
        }
        
        var content = '<div id="iw-container">' +
                '<div class="iw-title">'+routeName+'</div>' +
                '<div class="iw-content">' +
                  '<div class="iw-subTitle">'+employee.name+'</div>' +
                  '<div class="iw-subTitle"> Last Seen: '+employee.lastSeenReadable+'</div>' +
                '</div>' +
              '</div>';

        var infoWindow = new google.maps.InfoWindow({
            content: content,
            maxWidth: 350
        });

        marker.addListener('mouseover', function() {
            infoWindow.open(map, this);
            infoWindows.set(employee.employeeId,infoWindow);
        });

        // assuming you also want to hide the infowindow when user mouses-out
        marker.addListener('mouseout', function() {
            infoWindow.close();
            infoWindows.delete(employee.employeeId);
        });

        marker.addListener('click', function() {
            //we are setting the selected route to the employee route
            plotEmployeeRoute(employee.route);
        });
        vehicleMarkers.set(employee.employeeId,marker);
      }
      else
      {
          var routeName = "Not on Route";
        
          if (employee.route !== null){
              routeName = employee.route.routeName;
          }
          
          //console.log("has employee");
          //console.log(employee);
          var content = '<div id="iw-container">' +
                '<div class="iw-title">'+routeName+'</div>' +
                '<div class="iw-content">' +
                  '<div class="iw-subTitle">'+employee.name+'</div>' +
                  '<div class="iw-subTitle"> Last Seen: '+employee.lastSeenReadable+'</div>' +
                '</div>' +
              '</div>';

         //alert(JSON.stringify(latlng));
         vehicleMarkers.get(employee.employeeId).addListener('click', function() {
            //we are setting the selected route to the employee route
            plotEmployeeRoute(employee.route);
        });

        vehicleMarkers.get(employee.employeeId).infowindow = new google.maps.InfoWindow({
              content: content,
              maxWidth: 350
        });
        
        if (latlng != null){
            vehicleMarkers.get(employee.employeeId).setPosition(latlng);
            move_marker(employee.employeeId,latlng);
        }
      }
    }

    function move_marker(employeeID ,latlng)
    {
        vehicleMarkers.get(employeeID).setPosition(latlng);
    }

    function filter(searchInput)
    {
        searchString = searchInput.value;
        console.log(searchString+" "+vehicleMarkers.size);

        vehicleMarkers.forEach(function (marker, employeeId, mapObj) {
            marker = vehicleMarkers.get(employeeId);
            routeName = employeeRoute.get(employeeId);
            title = marker.getTitle();
            
            stringToSearch = title;
            if (routeName){
                stringToSearch = title +" "+ routeName;
            }
            
            //console.log("employee: "+employeeId+" route: "+routeName);
            if (searchString && stringToSearch.toUpperCase().includes(searchString.toUpperCase())){
                
                if (!infoWindows.has(employeeId)){
                    //alert("no info");
                    var infoWindow = new google.maps.InfoWindow();
                    infoWindow.setContent(marker.infowindow.content);
                    infoWindow.open(map, marker);
                    infoWindows.set(employeeId,infoWindow);
                }
            }else{
                if (infoWindows.has(employeeId)){
                    infoWindows.get(employeeId).close();
                    infoWindows.delete(employeeId);
                }
            }
        });
    }

    // The aim is to move each marker a step each in turn.
    // This makes it as if its asynchronous whereas we are time sharing.
    // We move each marker a small step at a time until we have done all the total_steps.
    function updateEmployees(employeesArray,current_step)
    {
       //console.log(employeesArray);
       for (var i = 0; i < employeesArray.length; i++) {
           var currentEmployee =  employeesArray[i];
           
           if (currentEmployee.latitude == null){
                continue;
           }
           
           //if(currentEmployee.status == 'online')
           {
               //console.log("Current Employee"+ JSON.stringify(currentEmployee));
               if(!vehicleMarkers.has(currentEmployee.employeeId))
               {
                    var latlng1 = new google.maps.LatLng(currentEmployee.latitude,currentEmployee.longitude);
                    updateEmployee(currentEmployee,latlng1);
               }
               
               var EmployeeMarker = vehicleMarkers.get(currentEmployee.employeeId);
               var destinationPos = vehicleDestination.get(currentEmployee.employeeId);
               var currentPos = EmployeeMarker.getPosition();

               //console.log(destinationPos.lat()+" "+currentPos.lat());
               if (destinationPos.lat() == currentPos.lat() && destinationPos.lng() == currentPos.lng())
               {
                   updateEmployee(currentEmployee,null);
                    continue;
               }

               var latdif = (destinationPos.lat()-currentPos.lat() ) ;
               var lngdif = (destinationPos.lng()-currentPos.lng() );

               var latoffset = (latdif/(total_steps-current_step));
               var lngoffset = (lngdif/(total_steps-current_step));

                var latlng = new google.maps.LatLng(currentPos.lat()+latoffset,currentPos.lng()+lngoffset);

              // alert(total_steps-current_step);
               updateEmployee(currentEmployee,latlng);
               //alert(JSON.stringify(latlng));
           }
       }
       if(current_step+1 < total_steps )
       {
            setTimeout(function(){ updateEmployees(employeesArray, current_step+1)},5000/total_steps);
       }
    }

    function update_bus() {
        $.ajax({
            type: "GET",
            url: {!! json_encode(url('/')) !!}+"/company_routes/{{$data['companyID']}}/update",
            success: function(data) {
                if(vehicle_mode)
                {
                    updateVehicleDestination(data["employees"]);    
                    //console.log(vehicleDestination);
                    updateEmployees(data["employees"], 1);
                }
            },
        });
        if (vehicle_mode)
        {
            setTimeout(update_bus, bus_refresh_rate); // you could choose not to continue on failure...
        }
        else
        {
            clearBusMarkers();
        }
    }

    function clearBusMarkers()
    {
        vehicleMarkers.forEach(function (item, key, mapObj) {  
            item.setMap(null);
            vehicleMarkers.delete(key);
        });  
    }

    //Where the marker is supposed to end up at for smooth transactioning
    function updateVehicleDestination(employeesArray)
    {
         for (var i = 0; i < employeesArray.length; i++) {
           //if(employeesArray[i].status == 'online')
           {
               var currentEmployee =  employeesArray[i];
               
               if (currentEmployee.latitude == null){
                   continue;
               }
               
               var destination_lat_lng =   new google.maps.LatLng(currentEmployee.latitude, currentEmployee.longitude);

               //var latlng = new google.maps.LatLng($point.latitude,$point.longitude);
               vehicleDestination.set(currentEmployee.employeeId , destination_lat_lng);
               //updateEmployee(employeesArray[i],latlng);
           }
        }
    }

    function plotEmployeeRoute(route){
        
        if (route == null){
            return;
        }
        
        var colour = route.basicColor;
        var points = route.points;
        
        //alert("plotting"+points.length);
        clearRoutes();
        //data["employees"][0]["route"]["points"]

        var current_path = new Array();

        var count = 0;
        //foreach ( points as $point)
        for(var i = 0; i < points.length; i++)
        {
            var $point = points[i] ;
            var position = new google.maps.LatLng($point.latitude,$point.longitude);
            if (count == 0){
                //map.setZoom(17);
                //map.panTo(position);
            }
            current_path[$point.id - 1] = position;

            if ($point.busStop === "true")
            {    busStops[position] = new Array();
                var busStopName = $point.name;

                busStops[position]["busStopName"] = busStopName;
                busStops[position]["busStopTimes"] = new Array();

                //create marker for the vertex position
                var marker = new google.maps.Marker( {
                    icon     : {
                        url     : {!! json_encode(url('/')) !!}+"/img/bus_stop.png",
                        size    : new google.maps.Size( 50, 50 ),
                        anchor  : new google.maps.Point( 25, 50 )
                    },
                    title    : busStopName,
                    position : position,
                    map      : map
                });


                busStops[position]["busStopMarker"] = marker;
                //busStops[position]["busStopMarker"].addListener('click', showBusTimes);

               // foreach ( $point.timeTable as $time)
                for(var j = 0;j < $point.timeTable.length;j++){
                    busStops[position]["busStopTimes"].push($point.timeTable[j]); 
                }
                addClickStopHandler(busStops[position]["busStopMarker"], busStops[position]["busStopTimes"], busStopName);
            }
            count++;
        }

        var current_path_string = JSON.stringify( current_path);
        document.getElementById("current").value = current_path_string ;

        document.getElementById("old-current").value = current_path_string;

        var current_path = JSON.parse(current_path_string);

        chosenRouteColor = colour;
        $('#colorpicker').val(colour);
        current_line = new google.maps.Polyline({
            path: current_path,
            geodesic: true,
            strokeColor: colour,
            strokeOpacity: 1.0,
            strokeWeight: 2,
            editable:false,
            draggable:false
        });

        current_line.setPath(current_path);
        current_line.setMap(map); 

    }

    function addClickStopHandler(stopMarker, times, stopname)
    {
        stopMarker.addListener( 'click', function(e){
            // Find a <table> element with id="myTable":
        var table = document.getElementById("bus-stop-time-table");
        table.innerHTML="<th>Departure</th>";

        var header = document.getElementById( "bus-stop-name");
        header.innerHTML=stopname;
        for( i = 0; i < times.length; i++)
        {
            table.innerHTML = table.innerHTML+"<tr><td>"+times[i]+"</tr></td>"
        }

        $("#bus-stop-modal").modal('show'); 
            //alert(JSON.stringify(times));
        })

    }

    $(document).ready(function() {
        togglebus();
    });

</script>

</body>
</html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Company Details</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>

 <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
      #target {
        width: 345px;
      }
    </style>

</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')

<div class="container-fluid ">
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">address</label>
                <input disabled type="text" class="form-control" name="value" value="{{$company->address}}" disabled>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">hours</label>
                <table>
                    <tr>
                        <td>Monday</td>
                        <td><input disabled type="time" name="monBegin" value = '{{ $data['hours']['monBegin'] }}' /></td>
                        <td> to</td>
                        <td> <input disabled type="time" name="monEnd" value = '{{ $data['hours']['monEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td><input disabled type="time" name="tueBegin" value = '{{ $data['hours']['tueBegin'] }}' /></td>
                        <td>to</td>
                        <td><input disabled type="time" name="tueEnd" value = '{{ $data['hours']['tueEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Wednesday</td>
                        <td><input disabled type="time" name="wedBegin" value = '{{ $data['hours']['wedBegin'] }}' /></td>
                        <td>to</td>
                        <td><input disabled type="time" name="wedEnd" value = '{{ $data['hours']['wedEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Thursday</td>
                        <td><input disabled type="time" name="thuBegin" value = '{{ $data['hours']['thuBegin'] }}' /></td>
                        <td>to</td>
                        <td><input disabled type="time" name="thuEnd" value = '{{ $data['hours']['thuEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Friday</td>
                        <td><input disabled type="time" name="friBegin" value = '{{ $data['hours']['friBegin'] }}' /></td>
                        <td>to</td>
                        <td><input disabled type="time" name="friEnd" value = '{{ $data['hours']['friEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Saturday</td>
                        <td><input disabled type="time" name="satBegin" value = '{{ $data['hours']['satBegin'] }}' /></td>
                        <td> to</td>
                        <td><input disabled type="time" name="satEnd" value = '{{ $data['hours']['satEnd'] }}'  /></td>
                    </tr>
                    <tr>
                        <td>Sunday</td>
                        <td><input disabled type="time" name="sunBegin" value = '{{ $data['hours']['sunBegin'] }}'  /></td>
                        <td>to </td>
                        <td><input disabled type="time" name="sunEnd"  value = '{{ $data['hours']['sunEnd'] }}'  /></td>
                    </tr>
                </table>
            </div>
        </div> 
        
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <input disabled type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="recipient-name" class="form-control-label">about</label>
                <input disabled type="text" class="form-control" name="value" value="{{$data['about']}}" >
                <input disabled type="hidden" name="property" value="about">
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <input disabled type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="recipient-name" class="form-control-label">phone number</label>
                <input disabled type="text" class="form-control" name="value" value="{{$data['phoneNumber']}}" >
                <input disabled type="hidden" name="property" value="phoneNumber">
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <input disabled type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="recipient-name" class="form-control-label">sector</label>
                <select name="value" value="{{$data['sector']}}">
                    <option>{{$data['sector']}}</option>
                    <option value="Security">Security</option>
                    <option value="Transportation">Transportation</option>
                    <option value="Retail_Wholesale">Retail/Wholesale</option>
                    <option value="Legal_Services">Legal Services</option>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Real_Estate">Real Estate</option>
                    <option value="Personal_Services">Personal Services</option>
                    <option value="Environmental">Environmental</option>
                    <option value="Motor_Vehicles">Motor Vehicles</option>
                    <option value="Health_Services">Health Services</option>
                    <option value="Hospitality">Hospitality</option>
                    <option value="Finance">Finance</option>
                    <option value="Education">Education</option>
                    <option value="Construction">Construction</option>
                    <option value="Technology">Technology</option>
                    <option value="Agriculture">Agriculture</option>
                    <option value="other">Other</option>
                </select>
                <input disabled type="hidden" name="property" value="sector">
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8 col-bordered">
            <div class="form-group" >
                <label for="recipient-name" class="form-control-label">type</label>
                <select name="value" value="{{$data['type']}}" selected="{{$data['sector']}}">
                    <option>{{$data['type']}}</option>
                    <option value="public">public</option>
                    <option value="private">private</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <!--  <h3>location</h3> <br>{{$data["location"]}}<br><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target=".location">edit</button><br><br> -->
</div>
    @endsection
<!-- Large modal -->

<div class="modal fade location" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id = "mapModal">
<input disabled id="pac-input" class="controls" type="text" placeholder="Search Box">

</div>

<script type="text/javascript">
    
</script>

<script 
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJIh263gju6FwHOd15fLEW7veRcdAQ6Xc&libraries=places&callback=initAutocomplete">
</script>

</body>
</html>
<!DOCTYPE html>
<html>
  <head>
    <title>Drawing tools</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="{{url('/')}}/js/jquery-3.2.1.min.js"></script>
    <script src="{{url('/')}}/js/jquery.timepicker.min.js"></script>
    <script src="{{url('/')}}/js/side-navigation.js"></script>
    
    <link href="{{url('/')}}/css/jquery.timepicker.min.css" rel="stylesheet">
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    <link href="{{url('/')}}/css/side-navigation.css" rel="stylesheet">
    
    <script src="{{url('/')}}/js/jscolor.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  
  <body id="app-layout"> 
    
    <div id="map">    
        
    </div>
    
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDtM_vJ0Vzzexq_xaT-TYbaV3w95pIu3U&libraries=drawing&callback=initMap">
    </script>
    
    <script src="{{url('/')}}/js/app.js"></script>
  </body>
</html>
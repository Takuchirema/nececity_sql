<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'NeceCity') }}</title>

    <!-- Styles -->
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
    <link href="{{url('/')}}/css/notifications.css" rel="stylesheet">
    <link href="{{url('/')}}/css/bootstrap-multiselect.css" rel="stylesheet">
    <link href="{{url('/')}}/css/bootstrap-datetimepicker-1.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="{{url('/')}}/js/jquery-ui.min.js"></script>
    <script src="{{url('/')}}/js/popper.min.js"></script>
    <script src="{{url('/')}}/js/sha512.js"></script>
    <script src="{{url('/')}}/js/moment.js"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
            
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    @if (Auth::guard('web_company')->check())
                        <a class="navbar-brand" href="{{ url('/dashboard/company') }}">
                            {{ config('app.name', 'NeceCity') }}
                        </a>
                    @elseif (Auth::guard('web_employee')->check())
                        <a class="navbar-brand" href="{{ url('/dashboard/employee') }}">
                            {{ config('app.name', 'NeceCity') }}
                        </a>
                    @else
                        <a class="navbar-brand" href="{{ url('./') }}">
                            {{ config('app.name', 'NeceCity') }}
                        </a>
                    @endif
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guard('web_company')->check())
                            <li>
                                @if (count(Auth::guard('web_company')->user()->getNotifications()) > 0)
                                <div class="notifications">
                                    <i class="fa fa-bell"></i>
                                    <span class="num">{{count(Auth::user()->getNotifications())}}</span>
                                    <ul>
                                        @foreach (Auth::guard('web_company')->user()->getNotifications() as $notification)
                                        <li class="icon">
                                            <form method="post">
                                                <input name="notificationId" value="{{$notification->notificationId}}" hidden>
                                                <input name="companyId" value="{{$notification->companyId}}" hidden>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="row notification">
                                                    <h5 class="col-lg-12">{{$notification->getCompanyName()}}</h5>
                                                    <p class="col-lg-12">{{$notification->notification}}</p>
                                                    <input class="col-lg-6" placeholder="reply" type="text" class="form-control" name="reply">
                                                    <button class="btn btn-default green col-lg-2" formaction="/notifications/reply" title="Send" type="submit">
                                                        <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                                    </button>
                                                    <button class="btn btn-default orange col-lg-2" formaction="/notifications/delete" title="Delete" type="submit">
                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </form>
                                        </li>
                                        @endforeach
                                    </ul>
                                    
                                </div>
                                @endif
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::guard('web_company')->user()->companyName }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @elseif (Auth::guard('web_employee')->check())
                            <li>
                                @if (count(Auth::guard('web_employee')->user()->getNotifications()) > 0)
                                <div class="notifications">
                                    <i class="fa fa-bell"></i>
                                    <span class="num">{{count(Auth::user()->getNotifications())}}</span>
                                    <ul>
                                        @foreach (Auth::guard('web_employee')->user()->getNotifications() as $notification)
                                        <li class="icon">
                                            <form method="post">
                                                <input name="notificationId" value="{{$notification->notificationId}}" hidden>
                                                <input name="companyId" value="{{$notification->companyId}}" hidden>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="row notification">
                                                    <h5 class="col-lg-12">{{$notification->companyId}}</h5>
                                                    <p class="col-lg-12">{{$notification->notification}}</p>
                                                    <input class="col-lg-6" placeholder="reply" type="text" class="form-control" name="reply">
                                                    <button class="btn btn-default green col-lg-2" formaction="notifications/reply" title="Send" type="submit">
                                                        <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                                    </button>
                                                    <button class="btn btn-default orange col-lg-2" formaction="notifications/delete" title="Delete" type="submit">
                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </form>
                                        </li>
                                        @endforeach
                                    </ul>
                                    
                                </div>
                                @endif
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::guard('web_employee')->user()->companyName }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    
    <!-- Scripts -->
    <script src="{{url('/')}}/js/app.js"></script>
    <script src="{{url('/')}}/js/imageuploadify.min.js"></script>
    <script src="{{url('/')}}/js/export_files/libs/FileSaver/FileSaver.min.js"></script>
    <script src="{{url('/')}}/js/export_files/libs/jsPDF/jspdf.min.js"></script>
    <script src="{{url('/')}}/js/export_files/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>
    <script src="{{url('/')}}/js/export_files/tableExport.js"></script>
    <script src="{{url('/')}}/js/bootstrap-multiselect.js"></script>
    <script src="{{url('/')}}/js/bootstrap-datetimepicker-1.min.js"></script>
    
</body>
</html>

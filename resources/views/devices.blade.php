
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Manage Devices</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
    list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>Devices</h4></div>
    </div>

    <div class="row row-bordered">
        <div class="col-lg-1"><h4>Device Id</h4></div>
        <div class="col-lg-1"><h4>Employee Id</h4></div>
        <div class="col-lg-1"><h4>Employee Name</h4></div>
        <div class="col-lg-2"><h4>Phone</h4></div>
        <div class="col-lg-1"><h4>Bus Id</h4></div>
        <div class="col-lg-2"><h4>Last Seen</h4></div>
        <div class="col-lg-1"><h4>Status</h4></div>
        <div class="col-lg-1"><h4>Logout</h4></div>
        <div class="col-lg-1"><h4>Save</h4></div>
        <div class="col-lg-1"><h4>Delete</h4></div>
    </div>
        
    @foreach ($data as $indexKey => $device)
        
        <form method="post" autocomplete="off">
            <div class = "row">
                <div class="col-lg-1">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" class="form-control" name="deviceId" value="{{$device->deviceId}}" readonly>
                </div>
                
                <div class="col-lg-1">
                    @if ($device->employeeId != null)
                    <input type="text" class="form-control" name="currentEmployee" value="{{$device->getEmployee()->employeeCode}}">
                    @else
                        <input type="text" class="form-control" name="currentEmployee" value="">
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($device->employee != null)
                        <input type="text" class="form-control" name="currentEmployee" value="{{$device->getEmployee()->name}}">
                    @else
                        <input type="text" class="form-control" name="currentEmployee" value="">
                    @endif
                </div>
                
                <div class="col-lg-2">
                    @if ($device->phoneNumber != null)
                        <input type="text" class="form-control" name="phoneNumber" value="{{$device->phoneNumber}}">
                    @else
                        <input type="text" class="form-control" name="phoneNumber" value="">
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($device->busId != null)
                        <input type="text" class="form-control" name="busId" value="{{$device->busId}}">
                    @else
                        <input type="text" class="form-control" name="busId" value="">
                    @endif
                </div>
                
                <div class="col-lg-2">
                    @if ($device->lastLocationUpdate != null)
                        @if ($device->isActive())
                            <input type="text" class="btn btn-default green"  value="{{$device->lastLocationUpdate}}" readonly>
                        @else
                            <input type="text" class="btn btn-default red"  value="{{$device->lastLocationUpdate}}" readonly>
                        @endif
                    @else
                        <input type="text" class="form-control" value="" readonly>
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($device->employee != null)
                        @if (strtolower($device->getEmployee()->status) == "online")
                            <input type="button" class="btn btn-default green"  value="{{$device->getEmployee()->status}}">
                        @else
                            <input type="button" class="btn btn-default orange"  value="{{$device->getEmployee()->status}}">
                        @endif
                    @else
                        <input type="button" class="btn btn-default orange"  value="Offline">
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($device->logout != null && $device->logout == "false" && $device->getEmployee() != null)
                        @if (strtolower($device->getEmployee()->status) == "online")
                            <input formaction="dashboard/devices/logout" class="btn btn-default orange" type="submit" value="Logout">
                        @else
                            <input class="btn btn-default" type="button" value="Logout">
                        @endif
                    @else
                        <input class="btn btn-default" type="button" value="Logout">
                    @endif
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/devices/edit" class="btn btn-default" type="submit" value="Save">
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/devices/delete" placeholder="+27...." class="btn btn-default" type="submit" value="Delete">
                </div>
             </div>
        </form>
    @endforeach
    
    </div>

    <script type="text/javascript">
        function saveRoute(route,form){
            if (route != null && route && route !== "Select Route"){
                form.action = "dashboard/devices/edit";
                form.submit();
            }
        }
    </script>
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

 @endsection
</body>
</html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>Manage Promotions</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link href="{{url('/')}}/css/imageuploadify.min.css" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        li{
            list-style-type: none;
        }

        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }
    </style>

</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid">
    
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Promotions</h4></div>
        </div>
        
        <div class="row row-bordered">
            <div class="col-lg-2"><h4>title</h4></div>
            <div class="col-lg-2"><h4>likes</h4></div>
            <div class="col-lg-2"><h4>comments</h4></div>
            <div class="col-lg-2"><h4>edit</h4></div>
            <div class="col-lg-2"><h4>delete</h4></div>
        </div>

        @foreach ($data as $promotion )
            <div class="bordered">
            <form action="promotion/delete" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="promotionId" value="{{$promotion->promotionId}}">
                
                <div class="row">

                    <div style="margin-bottom:40px;" class="col-lg-2">
                        <input type="text" class="form-control" name="title" value="{{$promotion->title}}">
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-2">
                        <h4>{{$promotion->likes}}</h4>
                    </div>
                    
                    <div class="col-lg-2">
                         <button class="btn btn-default orange" type="button" data-toggle="modal" data-target="#comment{{$promotion->promotionId}}">
                             {{sizeof($promotion->comments)}}
                         </button>
                     </div>

                    <div style="margin-bottom:40px;" class="col-lg-2" onclick="expand('{{$promotion->promotionId}}')">
                        <div><h5 style="margin: 0px" class="btn btn-primary">Edit</h5></div>
                    </div>
                    
                    <div class="col-lg-2">
                         <input formaction="promotion/delete" class="btn btn-default red" type="submit" value="Delete">
                     </div>
                </div>
            </form>
                
            <form action="promotion/edit" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="promotionId" value="{{$promotion->promotionId}}">
                
                <div id="{{$promotion->promotionId}}" style="display:none" class="row">
                    
                    <div style="margin-bottom:40px;" class="col-lg-8">
                        <input type="text" class="form-control" name="title" value="{{$promotion->title}}">
                    </div>
                    
                    <div class="col-lg-8">
                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Duration</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4>From</h4>
                                    <input name="from" type='text' class="form-control" value="{{$promotion->from}}"/>
                                </div>

                                <div class="col-lg-6">
                                    <h4>To</h4>
                                    <input name="to" type='text' class="form-control" value="{{$promotion->to}}"/>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Promotion Days</label>
                            <h5>{{$promotion->days}}</h5>
                            <label>Edit Days</label> 
                            <div class="row">
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="mon"> Monday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="tue"> Tuesday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="wed"> Wednesday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="thu"> Thursday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="fri"> Friday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="sat"> Saturday<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="days[]" value="sun"> Sunday<br>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Price</label>
                            <div class="row">
                                <div class="col-lg-5">
                                    <input placeholder="e.g. R40" type="text" name="price" class="form-control" value="{{$promotion->price}}">
                                </div>

                                <div class="col-lg-1">
                                    <label> for </label>
                                </div>

                                <div class="col-lg-5">
                                    <input placeholder="e.g. 1 Plate" type="text" name="units" class="form-control" value="{{$promotion->units}}">
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div id="{{$promotion->promotionId.'_img_container'}}" class="col-lg-12 input-group">
                                    <input name="images[]" type="file" accept="image/*" multiple>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label for="lblmessage">Description</label>
                            <textarea name="promotion" class="form-control" rows="3" value="{{$promotion->promotion}}">{{$promotion->promotion}}</textarea>
                            <input type = "hidden" name="type" value="company">
                            <button style="margin-top:10px;" type="submit" class="btn btn-primary">edit</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        
            <div id="comment{{$promotion->promotionId}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Comments</h4>
                        </div>
                        
                        <div class="modal-body">
                            @foreach($promotion->comments as $comment)
                            @if ($comment != null)
                                  <article class="row">
                                      <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                                        <figure class="thumbnail">
                                          <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                          <figcaption class="text-center">{{$comment->userId}}</figcaption>
                                        </figure>
                                      </div>
                                      <div class="col-md-9 col-sm-9">
                                        <div class="panel panel-default arrow left">
                                          <div class="panel-body">
                                            <header class="text-left">
                                              <div style="color:#fff;background-color:{{$comment->color}}" class="comment-user form-control"><i class="fa fa-user"></i> {{$comment->userId}} </div>
                                              <time style="color:{{$comment->color}}" class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{$comment->formattedTime}} </time>
                                            </header>
                                            <div class="comment-post">
                                              <p>
                                                  {{$comment->comment}}
                                              </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </article>
                            @endif
                            @endforeach
                            <form id = '{{$promotion->promotionId}}' method='post' action='{{url('promotions/'.$promotion->promotionId)}}'>
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   <input type ='text' form_id ='{{$promotion->promotionId}}' class="form-control" name = 'comment'>

                                    <button class="btn btn-default green" type="submit">post</button>
                              </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        
        <form method = "post" action="promotion/new" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-lg-8">
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Duration</label>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>From</h4>
                                <div class='input-group date' id='from_datetimepicker'>
                                    <input name="from" type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <h4>To</h4>
                                <div class='input-group date' id='to_datetimepicker'>
                                    <input name="to" type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Promotion Days</label>
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="mon"> Monday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="tue"> Tuesday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="wed"> Wednesday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="thu"> Thursday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="fri"> Friday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="sat"> Saturday<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="days[]" value="sun"> Sunday<br>
                            </div>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Price</label>
                        <div class="row">
                            <div class="col-lg-5">
                                <input placeholder="e.g. R40" type="text" name="price" class="form-control">
                            </div>
                            
                            <div class="col-lg-1">
                                <label> for </label>
                            </div>
                            
                            <div class="col-lg-5">
                                <input placeholder="e.g. 1 Plate" type="text" name="units" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Upload Image</label>
                        <div class="row">
                            <div  class="col-lg-12 input-group">
                                <input name="images[]" type="file" accept="image/*" multiple>
                            </div>
                        </div>
                    </div>

                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label for="lblmessage">Description</label>
                        <textarea name="promotion" class="form-control" rows="3"></textarea>
                        <input type = "hidden" name="type" value="company">
                        <button style="margin-top:10px;" type="submit" class="btn btn-primary">Post</button>
                    </div>
                </div>
            </div>
      </form>

    </div>
    
    <script type="text/javascript">
        
        $("#from_datetimepicker").datetimepicker(
            {
                    format: "yyyy-mm-dd"
            }
        );
        
        function expand(id){
            if ($("#"+id).is(":visible")){
                $("#"+id).hide();
            }else{
                $("#"+id).show();
            }
        }
        
        var stringToColour = function(str) {
            var hash = 0;
            for (var i = 0; i < str.length; i++) {
              hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
            var colour = '#';
            for (var i = 0; i < 3; i++) {
              var value = (hash >> (i * 8)) & 0xFF;
              colour += ('00' + value.toString(16)).substr(-2);
            }
            return colour;
        };
        
        $(document).ready( function() {
            $('input[type="file"]').imageuploadify();
            fillImages();	
	});
        
        function fillImages(){
            @foreach ($data as $promotion )
                //get the prepared image container
                
                @if ($promotion->pictureUrl == null)
                    @continue;
                @endif
                
                //alert("{{count($promotion->pictureUrls)}}");
                @foreach ($promotion->pictureUrls as $pictureUrl)
                    var pictureUrl = "{{$pictureUrl}}";
                    var promotionId = "{{$promotion->promotionId}}";
                    
                    toDataUrl(pictureUrl, promotionId, function(myBase64, pictureUrl, promotionId) {
                        var filename = pictureUrl.substring(pictureUrl.lastIndexOf('/')+1);
                        //alert(pictureUrl+" "+filename);
                        var block = myBase64.split(";");
                        // Get the content type of the image
                        var contentType = block[0].split(":")[1];// In this case "image/gif"
                        // get the real base64 content of the file
                        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
                        // Convert it to a blob to upload
                        //var blob = b64toBlob(realData, contentType);

                        var dragBox = $("#"+promotionId+"_img_container").find('#dragbox');
                        dragBox.attr("data",myBase64);
                        dragBox.attr("data-name",filename);
                        dragBox.trigger('click');
                        dragBox.attr("data","");
                        dragBox.attr("data-name","");
                    });
                @endforeach
            @endforeach
        }
        
        function toDataUrl(url, postId, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result, url, postId);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }
        
        $("#to_datetimepicker").datetimepicker(
            {
                format: "yyyy-mm-dd"
            }
        );
        
    </script>
    @endsection
</body>
</html>
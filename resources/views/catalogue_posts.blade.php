
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>Catalogue Posts</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link href="{{url('/')}}/css/imageuploadify.min.css" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        li{
            list-style-type: none;
        }

        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }
    </style>

</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid">
    
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Catalogue/Menu</h4></div>
        </div>
        
        <div class="row row-bordered">
            <div class="col-lg-2"><h4>time</h4></div>
            <div class="col-lg-2"><h4>title</h4></div>
            <div class="col-lg-2"><h4>likes</h4></div>
            <div class="col-lg-2"><h4>comments</h4></div>
            <div class="col-lg-2"><h4>edit</h4></div>
            <div class="col-lg-2"><h4>delete</h4></div>
        </div>

        @foreach ($data as $post )
            <div class="bordered">
            <form  action="catalogue/delete" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="postId" value="{{$post->postId}}">
                <div class="row">
                    <div style="margin-bottom:40px;" class="col-lg-2">
                        <h5>{{$post->day}}</h5>
                    </div>

                    <div style="margin-bottom:40px;" class="col-lg-2">
                        <input type="text" class="form-control" name="title" value="{{$post->title}}">
                    </div>

                    <div style="margin-bottom:40px;" class="col-lg-2">
                        <h4>{{$post->likes}}</h4>
                    </div>

                    <div class="col-lg-2">
                         <button class="btn btn-default orange" type="button" data-toggle="modal" data-target="#comment{{$post->postId}}">
                             {{sizeof($post->comments)}}
                         </button>
                     </div>

                    <div style="margin-bottom:40px;" class="col-lg-2" onclick="expand('{{$post->postId}}')">
                        <div><h5 style="margin: 0px" class="btn btn-primary">Edit</h5></div>
                    </div>

                    <div class="col-lg-2">
                         <input class="btn btn-default red" type="submit" value="Delete">
                     </div>
                </div>
            </form>
        
            <form action="catalogue/edit" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="postId" value="{{$post->postId}}">
                
                <div id="{{$post->postId}}" style="display:none" class="row">
                    
                    <div style="margin-bottom:40px;" class="col-lg-8">
                        <input type="text" class="form-control" name="title" value="{{$post->title}}">
                    </div>
                    
                    <div class="col-lg-8">
                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Duration</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h4>From</h4>
                                    <input name="from" type='text' class="form-control" value="{{$post->from}}"/>
                                </div>

                                <div class="col-lg-6">
                                    <h4>To</h4>
                                    <input name="to" type='text' class="form-control" value="{{$post->to}}"/>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Current Amenities</label>
                            <h5>{{$post->amenities}}</h5>
                            <label>Edit Amenities</label> 
                            <div class="row">
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="pool"> Pool<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="restaurant"> Restaurant<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="wifi"> WiFi<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="pets"> Pets<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="parking"> Parking<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="spa"> Spa<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="ac"> Air Conditioning<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="gym"> Gym<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="security"> Security<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="cafe"> Cafe<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="laundry"> Laundry Service<br>
                                </div>
                                <div class="col-lg-3">
                                    <input type="checkbox" name="amenities[]" value="atm"> ATM<br>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label>Price</label>
                            <div class="row">
                                <div class="col-lg-5">
                                    <input placeholder="e.g. R40" type="text" name="price" class="form-control" value="{{$post->price}}">
                                </div>

                                <div class="col-lg-1">
                                    <label> for </label>
                                </div>

                                <div class="col-lg-5">
                                    <input placeholder="e.g. 1 Plate" type="text" name="units" class="form-control" value="{{$post->units}}">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <label>PDF File</label>
                            <div style="margin-bottom:40px;" class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse… <input class="imgInp" type="file" accept="application/pdf" name="file" id="imgInp">
                                    </span>
                                </span>
                                <input type="text" class="form-control" value="{{$post->fileUrl}}" readonly>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div id="{{$post->postId.'_img_container'}}" class="col-lg-12 input-group">
                                    <input name="images[]" type="file" accept="image/*" multiple>
                                </div>
                            </div>
                        </div>

                        <div style="margin-bottom:40px;" class="col-lg-12">
                            <label for="lblmessage">Description</label>
                            <textarea name="post" class="form-control" rows="3" value="{{$post->post}}">{{$post->post}}</textarea>
                            <input type = "hidden" name="type" value="company">
                            <button formaction="catalogue/edit" style="margin-top:10px;" type="submit" class="btn btn-primary">edit</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        
            <div id="comment{{$post->postId}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Comments</h4>
                        </div>
                        
                        <div class="modal-body">
                            @foreach($post->comments as $comment)
                            @if ($comment != null)
                                  <article class="row">
                                      <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                                        <figure class="thumbnail">
                                          <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                                          <figcaption class="text-center">{{$comment->userId}}</figcaption>
                                        </figure>
                                      </div>
                                      <div class="col-md-9 col-sm-9">
                                        <div class="panel panel-default arrow left">
                                          <div class="panel-body">
                                            <header class="text-left">
                                              <div style="color:#fff;background-color:{{$comment->color}}" class="comment-user form-control"><i class="fa fa-user"></i> {{$comment->userId}} </div>
                                              <time style="color:{{$comment->color}}" class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{$comment->formattedTime}} </time>
                                            </header>
                                            <div class="comment-post">
                                              <p>
                                                  {{$comment->comment}}
                                              </p>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </article>
                            @endif
                            @endforeach
                              <form id = '{{$post->postId}}' method='post' action='{{url('catalogue/'.$post->postId)}}'>
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   <input type ='text' form_id ='{{$post->postId}}' class="form-control" name = 'comment'>

                                    <button class="btn btn-default green" type="submit">post</button>
                              </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        
        <form method = "post" action="catalogue/new" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-lg-8">
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Duration</label>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>From</h4>
                                <div class='input-group date' id='from_datetimepicker'>
                                    <input name="from" type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <h4>To</h4>
                                <div class='input-group date' id='to_datetimepicker'>
                                    <input name="to" type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Amenities</label>
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="pool"> Pool<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="restaurant"> Restaurant<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="wifi"> WiFi<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="pets"> Pets<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="parking"> Parking<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="spa"> Spa<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="ac"> Air Conditioning<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="gym"> Gym<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="security"> Security<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="cafe"> Cafe<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="laundry"> Laundry Service<br>
                            </div>
                            <div class="col-lg-3">
                                <input type="checkbox" name="amenities[]" value="atm"> ATM<br>
                            </div>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Price</label>
                        <div class="row">
                            <div class="col-lg-5">
                                <input placeholder="e.g. R40" type="text" name="price" class="form-control">
                            </div>
                            
                            <div class="col-lg-1">
                                <label> for </label>
                            </div>
                            
                            <div class="col-lg-5">
                                <input placeholder="e.g. 1 Plate" type="text" name="units" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-12">
                        <label>PDF File</label>
                        <div style="margin-bottom:40px;" class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    Browse… <input class="imgInp" type="file" name="file" accept="application/pdf" id="imgInp">
                                </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                        </div>
                    </div>
                    
                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label>Upload Image</label>
                        <div class="row">
                            <div  class="col-lg-12 input-group">
                                <input name="images[]" type="file" accept="image/*" multiple>
                            </div>
                        </div>
                    </div>

                    <div style="margin-bottom:40px;" class="col-lg-12">
                        <label for="lblmessage">Description</label>
                        <textarea name="post" class="form-control" rows="3"></textarea>
                        <input type = "hidden" name="type" value="company">
                        <button style="margin-top:10px;" type="submit" class="btn btn-primary">post</button>
                    </div>
                </div>
            </div>
      </form>

    </div>
    
    <script type="text/javascript">
        
        $("#from_datetimepicker").datetimepicker(
            {
                    format: "yyyy-mm-dd"
            }
        );
        
        function expand(id){
            if ($("#"+id).is(":visible")){
                $("#"+id).hide();
            }else{
                $("#"+id).show();
            }
        }
        
        var stringToColour = function(str) {
            var hash = 0;
            for (var i = 0; i < str.length; i++) {
              hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
            var colour = '#';
            for (var i = 0; i < 3; i++) {
              var value = (hash >> (i * 8)) & 0xFF;
              colour += ('00' + value.toString(16)).substr(-2);
            }
            return colour;
        };
        
        $(document).ready( function() {
            $('input[type="file"]').imageuploadify();
            fillImages();
	});
        
        function fillImages(){
            @foreach ($data as $post )
                //get the prepared image container
                
                @if ($post->pictureUrl == null)
                    @continue;
                @endif
                
                //alert("{{count($post->pictureUrls)}}");
                @foreach ($post->pictureUrls as $pictureUrl)
                    var pictureUrl = "{{$pictureUrl}}";
                    var postId = "{{$post->postId}}";
                    
                    toDataUrl(pictureUrl, postId, function(myBase64, pictureUrl, postId) {
                        var filename = pictureUrl.substring(pictureUrl.lastIndexOf('/')+1);
                        //alert(pictureUrl+" "+filename);
                        var block = myBase64.split(";");
                        // Get the content type of the image
                        var contentType = block[0].split(":")[1];// In this case "image/gif"
                        // get the real base64 content of the file
                        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
                        // Convert it to a blob to upload
                        //var blob = b64toBlob(realData, contentType);

                        var dragBox = $("#"+postId+"_img_container").find('#dragbox');
                        dragBox.attr("data",myBase64);
                        dragBox.attr("data-name",filename);
                        dragBox.trigger('click');
                        dragBox.attr("data","");
                        dragBox.attr("data-name","");
                    });
                @endforeach
            @endforeach
        }
        
        function toDataUrl(url, postId, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function() {
                var reader = new FileReader();
                reader.onloadend = function() {
                    callback(reader.result, url, postId);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.responseType = 'blob';
            xhr.send();
        }
        
        $("#to_datetimepicker").datetimepicker(
            {
                format: "yyyy-mm-dd"
            }
        );
        
    </script>
    
    <script type="text/javascript">
        
        $(document).ready( function() {
            $(document).on('change', '.btn-file :file', function() {
                var input = $(this),
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
                });

                $('.btn-file :file').on('fileselect', function(event, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {

                            var imageId = $(input).attr("data-img-id");

                            if (imageId != null){
                                $('#'+imageId).attr('src', e.target.result);
                            }else{
                                $('#img-upload').attr('src', e.target.result);
                            }
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $(".imgInp").change(function(){
                    readURL(this);
            }); 	
	});
    </script>
                
    @endsection
</body>
</html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>Manage Deliveries</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link href="{{url('/')}}/css/imageuploadify.min.css" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <style>
        li{
            list-style-type: none;
        }

        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>Deliveries</h4></div>
    </div>

    <div class="row row-bordered">
        <div class="col-lg-1"><h4>Id</h4></div>
        <div class="col-lg-1"><h4>Employee</h4></div>
        <div class="col-lg-1"><h4>User</h4></div>
        <div class="col-lg-2"><h4>Address</h4></div>
        <div class="col-lg-2"><h4>Phone</h4></div>
        <div class="col-lg-1"><h4>Status</h4></div>
        <div class="col-lg-1"><h4>Time</h4></div>
        <div class="col-lg-1"><h4>Items</h4></div>
        <div class="col-lg-1"><h4>Save</h4></div>
        <div class="col-lg-1"><h4>Delete</h4></div>
    </div>
        
    @foreach ($data['deliveries'] as $indexKey => $delivery)       
        <form method="post" autocomplete="off">
            <div class = "row">
                <div class="col-lg-1">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" class="form-control" name="deliveryId" value="{{$delivery->deliveryId}}" readonly>
                </div>
                
                <div class="col-lg-1">
                    @if ($delivery->employee != null)
                        <input type="text" class="form-control" name="currentEmployee" value="{{$delivery->employee->employeeCode}}">
                    @else
                        <input type="text" class="form-control" name="currentEmployee" value="">
                    @endif
                </div>
                
                <div class="col-lg-1">
                    <select class="form-control" onchange="saveUser(value,this.form);" name="route" value="{{$delivery->userId}}">
                        @foreach ($data['followers'] as $indexKey => $follower)
                            <option value="{{$follower->userId}}">{{$follower->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="col-lg-2">
                    @if ($delivery->address != null)
                        <input type="text" class="form-control" name="address" value="{{$delivery->address}}">
                    @else
                        <input type="text" class="form-control" name="address" value="">
                    @endif
                </div>
                
                <div class="col-lg-2">
                    @if ($delivery->phoneNumber != null)
                        <input type="text" class="form-control" name="phoneNumber" value="{{$delivery->phoneNumber}}">
                    @else
                        <input type="text" class="form-control" name="phoneNumber" value="">
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($delivery->status != null)
                        <select class="form-control" name="status" value="{{$delivery->status}}">
                            <option value="Pending">Pending</option>
                            <option value="In transit">In Transit</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Cancelled">Cancelled</option>
                        </select>
                    @else
                        <select class="form-control" name="status" value="">
                            <option value="Pending">Pending</option>
                            <option value="In transit">In Transit</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Cancelled">Cancelled</option>
                        </select>
                    @endif
                </div>
                
                <div class="col-lg-1">
                    @if ($delivery->time != null)
                        <input type="text" class="btn btn-default"  value="{{$delivery->time}}" readonly>
                    @else
                        <input type="text" class="form-control" value="" readonly>
                    @endif
                </div>
                
                
                <div class="col-lg-1">
                    <input data-toggle="modal" data-target="#items-{{$delivery->deliveryId}}" class="btn btn-default orange" type="button" value="Items">
                </div>
                
                <div class="col-lg-1">
                    <input formaction="deliveries/edit" class="btn btn-default green" type="submit" value="Save">
                </div>

                <div class="col-lg-1">
                    <input formaction="deliveries/delete" class="btn btn-default red" type="submit" value="Delete">
                </div>
             </div>
        </form>
    
        <form method="post" autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="deliveryId" value="{{$delivery->deliveryId}}">
            <div id="items-{{$delivery->deliveryId}}" class="modal fade col-lg-12" role="dialog">
                <div class="modal-dialog" style="background: #fff;padding:10px;border-radius: 4px;">
                    <!-- Modal content-->
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Delivery Items</h4>
                        </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div id="items">
                            @foreach($delivery->deliveryItems as $item)
                                @if ($item != null)
                                  <article class="row">
                                    <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
                                      <figure class="thumbnail">
                                          <img class="img-responsive" src="{{$item->url}}" />
                                        <figcaption class="text-center">{{$item->name}}</figcaption>
                                      </figure>
                                    </div>
                                    <div class="col-md-9 col-sm-9">
                                      <div class="panel panel-default arrow left">
                                        <div class="panel-body">
                                            <div style="color:#fff;background-color:#411c0e" class="comment-user form-control"><i class="fa fa-tag"></i> {{$item->price}} </div>                 
                                          <div class="comment-post">
                                            <p>
                                                "{{$item->description}}"
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </article>
                                @endif
                            @endforeach
                            </div>

                            <label>Name</label>
                            <input type ='text' placeholder="e.g. Bokomo Rice" class="form-control" id='name'>
                            <label>Image Url</label>
                            <input type ='url' class="form-control" id='url'>
                            <label>Price</label>
                            <input type ='text' placeholder="e.g. $50" class="form-control" id='price'>
                            <label>Description</label>
                            <input type ='text' placeholder="e.g. 2kg White Bokomo Rice" class="form-control" id="description">

                            <button onclick="addItem();" class="btn btn-default green" type="button">Add</button>
                            <button onclick="clearItems();" class="btn btn-default orange" type="button">Clear</button>

                        <div class="modal-footer">
                            <button formaction="deliveries/edit" class="btn btn-default orange" type="submit">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                </div>
            </div>
        </form>
        
    @endforeach
    
    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>Add New Delivery</h4></div>
    </div>
    
    <div class="row row-bordered">
          <div class="col-lg-2"><h4>User</h4></div>
          <div class="col-lg-3"><h4>Address</h4></div>
          <div class="col-lg-2"><h4>Phone Number</h4></div>
          <div class="col-lg-1"><h4></h4></div>
          <div class="col-lg-1"><h4></h4></div>
          <div class="col-lg-1"><h4></h4></div>
          <div class="col-lg-2"><h4>Create</h4></div>
    </div>

        <form method = "post" action="deliveries/new" enctype="multipart/form-data">                
                
            <div class="col-lg-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <select class="form-control" name="userId">
                    @foreach ($data['followers'] as $indexKey => $follower)
                        <option value="{{$follower->userId}}">{{$follower->name}}</option>
                    @endforeach
                </select>
            </div>
                
            <div class="col-lg-3">
                    <input type="text" class="form-control" name="address" value="">
            </div>
                
            <div class="col-lg-2">
                    <input type="text" class="form-control" name="phoneNumber" value="">
            </div>
                
            <div class="col-lg-1">
                
            </div>
                
            <div class="col-lg-1">
                
            </div>

            <div class="col-lg-1">
                
            </div>

            <div class="col-lg-2">
                <input class="btn btn-default" type="submit" value="New Delivery">
            </div>
        </form>
    
    </div>

    <script type="text/javascript">
        function saveUser(userId,form){
            if (userId != null && userId && userId !== "Select User"){
                form.action = "deliveries/edit";
                form.submit();
            }
        }
        
        function clearItems(){
            var items = document.getElementById("items");
            items.innerHTML = "";
        }
        
        function addItem(){
            var name = document.getElementById("name").value; 
            var price = document.getElementById("price").value; 
            var description = document.getElementById("description").value; 
            var url = document.getElementById("url").value;
            var items = document.getElementById("items");
	    var count = getCount(items, false);
            if (!name || !price || ! description){
                alert("Please fill in item name, price and description");
		return;
            }
            
            var html = "<article class='row'>"+
                        "<div class='col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs'>"+
                        "<figure class='thumbnail'>"+
                        "<img class='img-responsive' src='"+url+"' />"+
                        "<figcaption class='text-center'>"+name+"</figcaption>"+
                        "</figure>"+
                        "</div>"+
                        "<div class='col-md-9 col-sm-9'>"+
                        "<div class='panel panel-default arrow left'>"+
                        "<div class='panel-body'>"+
                        "<div style='color:#fff;background-color:#411c0e' class='comment-user form-control'><i class='fa fa-tag'></i>"+price+"</div>"+                 
                        "<div class='comment-post'>"+
                        "<p>"+description+"</p>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "</div>"+
                        "<input name='items["+count+"][id]' value='"+(count+1)+"' hidden>"+
			"<input name='items["+count+"][name]' value='"+name+"' hidden>"+
                        "<input name='items["+count+"][price]' value='"+price+"' hidden>"+
                        "<input name='items["+count+"][description]' value='"+description+"' hidden>"+
                        "<input name='items["+count+"][url]' value='"+url+"' hidden>"+
			
                        "</article>";
                
            items.innerHTML = items.innerHTML + html;  
        }
        function getCount(parent, getChildrensChildren){
            var relevantChildren = 0;
            var children = parent.childNodes.length;
            for(var i=0; i < children; i++){
                if(parent.childNodes[i].nodeType != 3){
                    if(getChildrensChildren)
                        relevantChildren += getCount(parent.childNodes[i],true);
                    relevantChildren++;
        }
    }
    return relevantChildren;
}
    </script>
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

 @endsection
</body>
</html>






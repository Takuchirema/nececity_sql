
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>View Time Logs</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
            list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
        }
        .responstable{
            overflow-x: auto;
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Employee Time Logs</h4></div>
        </div>

        <form method="get" autocomplete="off">   
            <div class="row row-bordered">

                <div class="col-lg-4">
                    <h4>Employee Name</h4>
                    <select class="form-control" id="employeeId" name="employeeId">
                        <option value="all">All</option>
                        @foreach ($data as $indexKey => $employee)
                            <option value="{{$employee->employeeId}}">{{$employee->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-4">
                    <h4>Route Name</h4>
                    <select id="routeId" class="form-control" name="routeId">
                        <option value="all">All</option>
                        @foreach ($company->routes as $indexKey => $route)
                            <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-4">
                    <h4>Date</h4>
                    <div class='input-group date' id='datetimepicker'>
                        <input name="dateString" type='text' class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>

            </div>
           
            <div class="row">
                <div style="margin-left: 30px" class="col-lg-2">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-default" formaction="time-logs" type="button"
                            onclick="showTimeLog('')">Show Time Logs</button>
                </div>
            </div>
        </form>
        
        <div class="row row-bordered">
            <div class="col-lg-3"><h4>id</h4></div>
            <div class="col-lg-3"><h4>route</h4></div>
            <div class="col-lg-2"><h4>date</h4></div>
            <div class="col-lg-2"><h4>view</h4></div>
            <div class="col-lg-2"><h4>delete</h4></div>
        </div>
        
        <div id="time-log-container" class="time-log-container">
            <div>
                <form>
                    <div class="row">
                        <div class="col-lg-3">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" class="form-control" name="employeeId" value="" readonly>
                        </div>

                        <div class="col-lg-3">
                          <input type="text" class="form-control" name="routeId" value="" readonly>
                        </div>

                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="date" value="" readonly>
                        </div>

                        <div class="col-lg-2">
                            <input onclick="" class="btn btn-default" type="button" value="View">
                        </div>

                        <div class="col-lg-2">
                            <input formaction="../../dashboard/time-logs/delete" class="btn btn-default" type="submit" value="Delete">
                        </div>
                    </div>
                </form>
                <div id="time-logs">
                    
                </div>
            </div>
        </div>
        
    </div>

    <script type="text/javascript">
        
        $("#datetimepicker").datetimepicker(
            {
                format: "yyyy-mm-dd"
            }
        );
        
        $(document).ready(function() {
            var url = {!! json_encode(url('/')) !!}+"/api/companies/{{$company->companyId}}/logs/"
            showTimeLog(url);
        });
        
        function showLogs(divId){
            
            if($("[id='"+divId+"']").is(":visible")){
                $("[id='"+divId+"']").fadeOut(500);
            }else{
                $("[id='"+divId+"']").fadeIn(500);
            }
        }

        function showTimeLog(url){
            var employeeId = $("#employeeId").val();
            var routeId = $("#routeId").val();
            var date = $("#datetimepicker").find("input").val();
            var defaultUrl = {!! json_encode(url('/')) !!}+"/api/employees/logs/route/date/";
                
            if (employeeId.trim() != ""){
                defaultUrl = defaultUrl+employeeId+"/";
            }
            
            defaultUrl = defaultUrl+"{{$company->companyId}}/";

            if (routeId != ""){
                defaultUrl = defaultUrl+routeId+"/";
            }
            
            if (date != ""){
                defaultUrl = defaultUrl+date+"/";
            }
            
            //alert("logs for: "+defaultUrl);
            
            $.ajax({
                type: "GET",
                url: (url === "")? defaultUrl:url,
                success: function(data) {
                    //alert("data returned");
                    var success = data['success'];
                    var message = data['message'];
                    
                    if (success === 1){
                        //console.log(data["logs"]);
                        populateLogs(data["logs"],"time-log-container");
                    }else{
                        alert(message);
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("token", "{{$company->token}}");
                    xhr.setRequestHeader("companyID", "{{$company->companyId}}");
                },

            });
        }
        
        function populateLogs(logs,containerId){
            console.log( logs );
            
            document.getElementById(containerId).innerHTML="";
            
            for(var date in logs){
                employees = logs[date];
                for (var employeeId in employees){
                    routes = employees[employeeId];
                    for (var routeId in routes){
                        var stopTimeLogs = new Array();
                    
                        var div = document.createElement("DIV");
                        div.setAttribute("class", "row");
                        
                        var form = createTimeLogForm(date,employeeId,routeId);
                        var timeLogsDiv = document.createElement("DIV");
                        timeLogsDiv.setAttribute("id", date+"_"+employeeId+"_"+routeId);
                        timeLogsDiv.style.display='none';
                        timeLogsDiv.setAttribute("class", "col-lg-12");
                        
                        timeLogs = routes[routeId];
                        for (var i=0;i<timeLogs.length;i++){
                            logArray = timeLogs[i];
                        
                            routeName = logArray["routeId"];
                            routePointName = logArray["routePointId"];
                            arrivalTime = logArray["arrivalTime"];
                            departureTime = logArray["departureTime"];
                            
                            if (!routePointName){
                                continue;
                            }

                            if (!(routePointName in stopTimeLogs)){
                                stopTimeLogs[routePointName] = new Array();
                                stopTimeLogs[routePointName].push(arrivalTime+","+departureTime);
                            }else{
                                stopTimeLogs[routePointName].push(arrivalTime+","+departureTime);
                            }
                        }
                        
                        var tablesDiv = createTimeLogTables(timeLogsDiv,stopTimeLogs);
                        
                        div.appendChild(form);
                        div.appendChild(timeLogsDiv);
                        
                        document.getElementById(containerId).appendChild(div);
                    }
                }
            }
            
            function createTimeLogForm(date, employeeId, routeId){
                var form = document.createElement("FORM");
                form.setAttribute("method","post");
                
                var divRow = document.createElement("DIV");
                divRow.setAttribute("class", "row");
                
                var input = document.createElement("INPUT");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", "_token");
                input.setAttribute("value", $('meta[name="_token"]').attr('content'));
                divRow.appendChild(input);
                
                var div1 = document.createElement("DIV");
                div1.setAttribute("class","col-lg-3");
                
                var input1 = document.createElement("INPUT");
                input1.setAttribute("class","form-control");
                input1.setAttribute("name","employeeId");
                input1.setAttribute("value",employeeId);
                input1.setAttribute("type","text");
                div1.appendChild(input1);
                
                divRow.appendChild(div1);
                
                var div2 = document.createElement("DIV");
                div2.setAttribute("class","col-lg-3");
                
                var input2 = document.createElement("INPUT");
                input2.setAttribute("class","form-control");
                input2.setAttribute("name","routeId");
                input2.setAttribute("value",routeId);
                input2.setAttribute("type","text");
                div2.appendChild(input2);
                
                divRow.appendChild(div2);
                
                var div3 = document.createElement("DIV");
                div3.setAttribute("class","col-lg-2");
                
                var input3 = document.createElement("INPUT");
                input3.setAttribute("class","form-control");
                input3.setAttribute("name","date");
                input3.setAttribute("value",date);
                input3.setAttribute("type","text");
                div3.appendChild(input3);
                
                divRow.appendChild(div3);
                
                var div4 = document.createElement("DIV");
                div4.setAttribute("class","col-lg-2");
                
                var input4 = document.createElement("INPUT");
                input4.setAttribute("class","btn btn-default");
                input4.setAttribute("value","View");
                input4.setAttribute("onclick","showLogs('"+date+"_"+employeeId+"_"+routeId+"')");
                input4.setAttribute("type","button");
                div4.appendChild(input4);
                
                divRow.appendChild(div4);
                
                var div5 = document.createElement("DIV");
                div5.setAttribute("class","col-lg-2");
                
                var input5 = document.createElement("INPUT");
                input5.setAttribute("class","btn btn-default");
                input5.setAttribute("value","Delete");
                input5.setAttribute("type","submit");
                input5.setAttribute("formaction","../../dashboard/time-logs/delete");
                div5.appendChild(input5);
                
                divRow.appendChild(div5);
                
                form.appendChild(divRow);
                
                return form;
            }
            
            function createTimeLogTables(div,stopTimeLogs){
                for (var stopName in stopTimeLogs) {
                    console.log(stopTimeLogs);
                    var table = document.createElement("TABLE");
                    table.className = "responstable";

                    var stopColumn = "<tr><th colspan='2'>"+stopName+"</th></tr>"+
                                     "<tr><th>Arrival</th><th>Departure</th></tr>";

                    //alert("stop name: "+stopName);
                    stopTimes = stopTimeLogs[stopName];

                    for (var i = 0;i < stopTimes.length;i++){
                        times = stopTimes[i];

                        timesArray = times.split(",");
                        arrivalTime = timesArray[0];
                        departureTime = timesArray[1];
                        //alert("stop "+stopName+" "+arrivalTime+" "+departureTime);

                        stopColumn = stopColumn + "<tr><td>"+arrivalTime+"</td><td>"+departureTime+"</td></tr>";
                    }

                    table.innerHTML = stopColumn;
                    div.appendChild(table);
                }
                return div;
            }
        }
    </script>

 @endsection
</body>
</html>
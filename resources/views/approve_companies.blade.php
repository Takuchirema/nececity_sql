<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>Approve Companies</title>
    <link href="{{url('/')}}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link href="{{url('/')}}/css/imageuploadify.min.css" rel="stylesheet">
    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
      
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Followers</h4></div>
        </div>

        <div class="row row-bordered">
            <div class="col-lg-2"><h4>profile</h4></div>
            <div class="col-lg-1"><h4>name</h4></div>
            <div class="col-lg-1"><h4>more</h4></div>
            <div class="col-lg-1"><h4>notifications</h4></div>
            <div class="col-lg-3"><h4>notify</h4></div>
            <div class="col-lg-2"><h4>access</h4></div>
            <div class="col-lg-2"><h4>delete</h4></div>
        </div>
        
        @foreach ( $data as $indexKey => $company )
        @if ($company != null)
            <form method="post" autocomplete="off">
                
                <div class = "row notifications-stack">
                    
                    <div class="col-lg-2">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $company->companyId }}">
                        
                        @if ($company->profilePicture == null || $company->hasProfilePicture != "true")
                            <a href="#" onclick="profilePictureModal(this,'{{$company->companyId}}')" data-toggle="modal" data-target="#myModal" data-picture-url="{{url('/')}}/img/profile_picture.png">
                                <img style="width:40%" src="{{url('/')}}/img/profile_picture.png" alt="Cinque Terre">
                            </a>
                        @else
                            <a href="#" onclick="profilePictureModal(this,'{{$company->companyId}}')" data-picture-url="{{$company->profilePicture}}">
                                <img style="width:40%;height:15%;" src="{{$company->profilePicture}}" class="img-circle" alt="Cinque Terre">
                            </a>
                        @endif
                        
                        <div id="{{$company->companyId}}-modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">              
                              <div class="modal-content">

                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>                  
                                </div>

                                <div class="modal-body">
                                    <img style="width:100%"src="" class="img-responsive center-block" alt="Cinque Terre">
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-1">
                        <h4>{{$company->companyName}}</h4>
                    </div>
                    
                    <div class="col-lg-1">
                        <a class="btn btn-default blue" data-toggle="collapse" href="#collapse-{{$company->noSpaceId}}">More</a>
                    </div>
                    
                    <div style="z-index:1" class="col-lg-1">
                        @if (count($company->getNotifications()) > 0)
                            <div style="z-index:1" class="notifications">
                                <i class="fa fa-bell"></i>
                                <span class="num">{{count($company->getNotifications())}}</span>
                                <ul>
                                    @foreach ($company->getNotifications() as $notification)
                                        <li class="icon">
                                            <input name="notificationId" value="{{$notification->notificationId}}" hidden>
                                            <div style="z-index:1" class="row notification">
                                                <h5 class="col-lg-12">{{$company->companyName}}</h5>
                                                <p class="col-lg-12">{{$notification->notification}}</p>
                                                <input class="col-lg-6" placeholder="reply" type="text" class="form-control" name="reply" readonly>
                                                <button class="btn btn-default green col-lg-2" formaction="notifications/reply" title="Send" type="submit">
                                                    <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                                </button>
                                                <button class="btn btn-default orange col-lg-2" formaction="notifications/delete" title="Delete" type="submit">
                                                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                    </div>
                    
                    <div class="col-lg-2">
                        <input name="companyId" value="{{$company->companyId}}" hidden>
                        <input placeholder="Notify" type="text" name="reply" class="form-control">
                    </div>
                    
                    <div class="col-lg-1">
                        <input formaction="notifications/reply" class="btn btn-default orange" type="submit" value="Notify">                        
                    </div>
                    
                    <div class="col-lg-2">
                        @if ($company->approved == null || $company->approved != "true")
                            <input formaction="dashboard/company/approve" class="btn btn-default green" type="submit" value="Approve">
                        @else
                            <input formaction="dashboard/company/disapprove" class="btn btn-default orange" type="submit" value="Disapprove">                       
                        @endif
                    </div>
                    
                    <div class="col-lg-2">
                            <input formaction="dashboard/company/delete" class="btn btn-default red" type="submit" value="Delete">
                    </div>
                    
                 </div>
            </form>
        
            <div id="collapse-{{$company->noSpaceId}}" class="container-fluid collapse" >
                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">address</label>
                                    <input disabled type="text" class="form-control" name="value" value="{{$company->address}}" disabled>
                                </div>
                            </div>
                            
                            <div class="col-lg-4"></div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">Business Hours</label>
                                    <table>
                                        <tr>
                                            <td>Monday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->mon != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tuesday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->tue != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Wednesday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->wed != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Thursday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->thu != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Friday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->fri != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Saturday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->sat != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sunday</td>
                                            <td>
                                                @if ($company->businessHours != null && $company->businessHours->sun != null)
                                                    <input disabled name="monBegin" value = '{{ $company->businessHours->mon}}' />
                                                @else
                                                    <input disabled name="monBegin" value='' />
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div> 

                            <div class="col-lg-4">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">about</label>
                                    <input disabled type="text" class="form-control" name="value" value="{{$company->about}}" >
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">phone number</label>
                                    <input disabled type="text" class="form-control" name="value" value="{{$company->phoneNumber}}" >
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">email</label>
                                    <input disabled type="text" class="form-control" name="value" value="{{$company->email}}" >
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">Sector</label>
                                    <br>
                                    <input disabled value="{{$company->sector}}">
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">Type</label>
                                    <br>
                                    <input disabled value="{{$company->type}}">
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>
                
                        <div class="row">
                            <div class="col-lg-8 col-bordered">
                                <div class="form-group" >
                                    <label for="recipient-name" class="form-control-label">Country Code</label>
                                    <br>
                                    <input disabled value="{{$company->countryCode}}">
                                </div>
                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>
                    </div>
        @endif @endforeach
 
        <script type="text/javascript">

           function profilePictureModal(element,id){
               var pictureUrl = element.getAttribute('data-picture-url');
               $("#"+id+"-modal").find('img').attr("src",pictureUrl);
               $("#"+id+"-modal").modal('show');
           }
        </script>
 @endsection
</body>
</html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>View Shift</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <!-- <script type="text/javascript" src="{{url('/')}}/js/html2canvas.js"></script>
    <script type="text/javascript" src="{{url('/')}}/js/jspdf.min.js"></script> -->
    
    <style>
         li{
        list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */

            display: flex;
            align-items: center;
        }
        #map{
            height: 100%;
            width: 100%;
            left: 0;
            position: absolute;
            top: 0; 
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
        <div id="mySidebar" class="sidebar">
            <div>
                <button data-toggle="tooltip" data-placement="right" title="employee" class="glyphicon glyphicon-user round-button orange" aria-hidden="true"></button>
                <label id="employee-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="mileage" class="glyphicon glyphicon-road round-button orange" aria-hidden="true"></button>
                <label id="mileage-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="speed" class="glyphicon glyphicon-dashboard round-button orange" aria-hidden="true"></button>
                <label id="speed-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="last seen" class="glyphicon glyphicon-time round-button orange" aria-hidden="true"></button>
                <label id="last-seen-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="status" class="glyphicon glyphicon-info-sign round-button orange" aria-hidden="true"></button>
                <label id="status-label"> --- </label>
            </div>
            
            <div class="bordered"></div>
            
            <div>
                <button data-toggle="modal" data-target="#shift-log-modal" class="glyphicon glyphicon-eye-open round-button orange" aria-hidden="true"></button>
                <label id="shift-log-label"> View Logs </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="top speed" class="glyphicon glyphicon-random round-button orange" aria-hidden="true"></button>
                <label id="top-speed-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="started" class="glyphicon glyphicon-play round-button orange" aria-hidden="true"></button>
                <label id="start-label"> --- </label>
            </div>
            
            <div>
                <button data-toggle="tooltip" data-placement="right" title="completed" class="glyphicon glyphicon-stop round-button orange" aria-hidden="true"></button>
                <label id="end-label"> --- </label>
            </div>
        </div>

        <input id="current" hidden>
        <input id="old-current" hidden>
        
        <div id="stop-modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center">
                        <div class="form-group" >
                            <input type="checkbox" name="fancy-checkbox-default" id="is-bus-stop" disabled/>
                            <div class="btn-group">
                                <label for="is-bus-stop" class="btn btn-default">
                                    <span class="glyphicon glyphicon-ok"></span>
                                    <span> </span>
                                </label>
                                <label id="stop-name" for="fancy-checkbox-default" class="btn btn-default">
                                    Bus Stop
                                </label>
                            </div>
                        </div>
                        <div>
                            <div style="display: inline-block;">
                                <button data-toggle="tooltip" data-placement="right" title="time" class="glyphicon glyphicon-time round-button orange" aria-hidden="true"></button>
                                <label id="point-time-label"> --- </label>
                            </div>

                            <div style="display: inline-block;">
                                <button data-toggle="tooltip" data-placement="right" title="speed" class="glyphicon glyphicon-dashboard round-button orange" aria-hidden="true"></button>
                                <label id="point-speed-label"> --- </label>
                            </div>
                            
                            <div style="display: inline-block;">
                                <button data-toggle="tooltip" data-placement="right" title="bearing" class="glyphicon glyphicon-scale round-button orange" aria-hidden="true"></button>
                                <label id="point-direction-label"> --- </label>
                            </div>
                        </div>
                    </div>
                    
                    <div id="bus-times-modal-body" class="modal-body"  style="text-align: center">
                        <div>
                            <h4 id ="bus-stop-name"></h4>
                            <div style="padding-left:30px;padding-right: 30px;" id="bus-times-div">
                                <table style="width:100% !important;;" class="responstable" id="bus-stop-time-table"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="shift-log-modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="text-align: center">
                        <button  onclick="saveAsPdf();"  type="button" class="btn btn-default btn-lg btn-block " aria-label="Left Align">
                            <span class="glyphicon glyphicon-download" aria-hidden="true"></span><br> <span class = "glyphicon-class">Download</span>
                        </button>
                    </div>
                    <div id="shift-log-body" class="modal-body"  style="text-align: center">
                    </div>
                </div>
            </div>
        </div>
        
        <div id="main">
            <button class="openbtn" onclick="toggleNav()">&#9776; </button>
            <div id="map"></div>
        </div>
    </div>

    <script type="text/javascript">
        var open = false;
        
        var current_line;
        var drawingManager;
        var goo;
        var map;

        // Key Value with vertices which are bustops with their values
        var busStops = new Array();
        var infoWindows = new Map();
        
        // This is the last point drawn.
        // Next point starts from here.
        var currentPoint = 0;
        var marker;
        
        $(document).ready(function(){
            getShift();
        });
        
        function toggleNav(){
            if (!open){
                openNav();
                open=true;
            }else{
                closeNav();
                open=false;
            }
        }
        
        /* Set the width of the sidebar to 250px and the left margin of the page content to 250px */
        function openNav() {
            document.getElementById("mySidebar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }
        
        /* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
        function closeNav() {
            document.getElementById("mySidebar").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }
        
        function initMap() {
            goo = google.maps;
            map = new goo.Map(document.getElementById('map'),{
                disableDefaultUI: true,
                mapTypeControl: false,
                center: {lat: -33.956809, lng: 18.460879},
                zoom: 17
            });
            
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: null,
                drawingControl: false,
                markerOptions: {icon: {!! json_encode(url('/')) !!}+"/img/map_location.png"},
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 3,
                    clickable: false,
                    editable: false,
                    radius: 2000,
                    zIndex: 1
                },
                polylineOptions : {
                    editable:false,
                    draggable:false,
                    geodesic:true
                }
            });
            
            drawingManager.setMap(map);
        }
        
        function getShift() {
            $.ajax({
                type: "GET",
                url: {!! json_encode(url('/')) !!}+"/shifts/{{$shift->shiftId}}/info",
                success: function(data) {
                    if (!data.end){
                        setTimeout(getShift, 5000);
                    }
                    updateShift(data);
                },
            });
        }
        
        function updateShift(shift){
            
            if (shift.speed){
                var speedLabel = document.getElementById("speed-label");
                speedLabel.innerHTML = shift.speed + " km/hr";
            }
            
            var employeeLabel = document.getElementById("employee-label");
            employeeLabel.innerHTML = shift.employeeName;
            
            if (shift.mileage){
                var mileageLabel = document.getElementById("mileage-label");
                mileageLabel.innerHTML = shift.mileage + " km";
            }
            
            if (shift.lastSeen){
                var statusLabel = document.getElementById("last-seen-label");
                statusLabel.innerHTML = shift.lastSeen;
            }
            
            var statusLabel = document.getElementById("status-label");
            statusLabel.innerHTML = shift.status;
            
            var startLabel = document.getElementById("start-label");
            startLabel.innerHTML = shift.start;
            
            if (shift.end){
                var endLabel = document.getElementById("end-label");
                endLabel.innerHTML = shift.end;
            }
            
            if (shift.topSpeed){
                var topSpeedLabel = document.getElementById("top-speed-label");
                topSpeedLabel.innerHTML = shift.topSpeed + " km/hr";
            }
            
            plotRoute(shift);
            createShiftLogTables(shift);
        }
        
        function plotRoute(shift){

            var colour = "#8B0000";
            var points = shift.points;
            
            if (!points){
                return;
            }
            
            var current_path = new Array();
            
            var count = 0;
            if (currentPoint >= points.length){
                currentPoint=0;
                clearRoutes();
            }
            
            for(var i = currentPoint; i < points.length; i++)
            {
                var $point = points[i] ;
                var position = new google.maps.LatLng($point.latitude,$point.longitude);
                
                current_path[$point.id - 1] = position;
                if ($point.busStop === "true")
                {    
                    busStops[position] = new Array();
                    var busStopName = $point.name;

                    busStops[position]["busStopName"] = busStopName;
                    busStops[position]["busStopTimes"] = new Array();

                    //create marker for the vertex position
                    var marker = new google.maps.Marker( {
                        icon     : {
                            url     : {!! json_encode(url('/')) !!}+"/img/bus_stop.png",
                            size    : new google.maps.Size( 50, 50 ),
                            anchor  : new google.maps.Point( 25, 50 )
                        },
                        title    : busStopName,
                        position : position,
                        map      : map
                    });

                    busStops[position]["busStopMarker"] = marker;

                   // foreach ( $point.timeTable as $time)
                    for(var j = 0;j < $point.timeTable.length;j++){
                        busStops[position]["busStopTimes"].push($point.timeTable[j]); 
                    }
                    addClickStopHandler(busStops[position]["busStopMarker"], $point);
                }else{
                    var marker = new google.maps.Marker( {
                        icon     : {
                            url     : {!! json_encode(url('/')) !!}+"/img/route_point.png",
                            size    : new google.maps.Size( 50, 50 ),
                            anchor  : new google.maps.Point( 5, 5 )
                        },
                        title    : busStopName,
                        position : position,
                        map      : map
                    });
                    addClickStopHandler(marker, $point);
                }
                
                count++;
            }

            var current_path_string = JSON.stringify(current_path);
            document.getElementById("current").value = current_path_string ;

            document.getElementById("old-current").value = current_path_string;

            var current_path = JSON.parse(current_path_string);

            $('#colorpicker').val(colour);
            current_line = new google.maps.Polyline({
                path: current_path,
                geodesic: true,
                strokeColor: colour,
                strokeOpacity: 1.0,
                strokeWeight: 3,
                editable:false,
                draggable:false
            });
            
            current_line.setPath(current_path);
            current_line.setMap(map);
            
            var latlng = current_path.slice(-1)[0];
            moveMarker(shift,latlng);
        }
        
        function moveMarker(shift, latlng){
            
            if (marker){
                marker.setPosition(latlng);
            }else{
                marker = new google.maps.Marker( {
                    icon     : {
                        url     : {!! json_encode(url('/')) !!}+"/img/bus_orange.png",
                        size    : new google.maps.Size( 100, 100 ),
                        anchor  : new google.maps.Point( 25, 50 )
                    },
                    title    : shift.employeeName,
                    position : latlng,
                    map      : map
                });
                
                map.panTo(marker.getPosition());
                
                var routeName = "No Route";

                if (shift.routeName){
                    routeName = shift.routeName;
                }

                var content = '<div id="iw-container">' +
                        '<div class="iw-title">'+routeName+'</div>' +
                        '<div class="iw-content">' +
                          '<div class="iw-subTitle">'+shift.employeeName+'</div>'
                        '</div>' +
                      '</div>';

                var infoWindow = new google.maps.InfoWindow({
                    content: content,
                    maxWidth: 350
                });

                marker.addListener('mouseover', function() {
                    infoWindow.open(map, this);
                    infoWindows.set(shift.employeeId,infoWindow);
                });

                // assuming you also want to hide the infowindow when user mouses-out
                marker.addListener('mouseout', function() {
                    infoWindow.close();
                    infoWindows.delete(shift.employeeId);
                });
            }
        }
        
        function createShiftLogTables(shift){
            
            var div = document.getElementById("shift-log-body");
            var shiftLog = shift.shiftLog;
            var shiftCycles = shift.shiftCycles;
            console.log(shiftLog);
            console.log(shiftCycles);
            var table = document.createElement("TABLE");
            table.className = "responstable";
            
            var tableInfo = "<tr><th>Name</th><th colspan='100%'>"+shift.employeeName+"</th></tr>"
                         +"<tr><th>Route</th><th colspan='100%'>"+shift.routeName+"</th></tr>"
                         +"<tr><th>Mileage</th><th colspan='100%'>"+shift.mileage+"</th></tr>"
                         +"<tr><th>Top Speed</th><th colspan='100%'>"+shift.topSpeed+"</th></tr>"
                         +"<tr><th>Start</th><th colspan='100%'>"+shift.start+"</th></tr>"
                         +"<tr><th>End</th><th colspan='100%'>"+shift.end+"</th></tr>";
                 
            for (var stopName in shiftLog) {
                var times = shiftLog[stopName];
                tableInfo = tableInfo + "<tr><td>"+stopName+"</td>";
                
                for (i = 0; i <= shiftCycles; i++) {
                    if (times[i]){
                        tableInfo = tableInfo + "<td>"+times[i]+"</td>";
                    }else{
                        tableInfo = tableInfo + "<td>--</td>";
                    }
                }
                tableInfo = tableInfo + "</tr>";
            }

            table.innerHTML = tableInfo;
            div.appendChild(table);
            
            return div;
        }
        
        function saveAsPdf() {
            $("#shift-log-body").tableExport({type:'pdf',
                jspdf: {orientation: 'p',
                        margins: {right: 20, left: 20, top: 30, bottom: 30},
                        autotable: {styles: {fillColor: 'inherit',
                                             textColor: 'inherit',
                                             fontStyle: 'inherit'},
                                    tableWidth: 'wrap'}}});
        }
        
        function clearRoutes(){
            clearBusStops();
            if (current_line){
                current_line.setMap(null);
            }
        }

        function clearBusStops(){
            for (var key in busStops){
                var marker = busStops[key]['busStopMarker'];
                marker.setMap(null);
            }
        }
        
        function addClickStopHandler(stopMarker, point)
        {
            stopMarker.addListener( 'click', function(e){
                if (point.busStop === "true")
                {
                    var pointName = point.name;
                    var times = point.timeTable;
                }else{
                    var pointName = "Point "+point.id;
                }
                
                if (!pointName){
                    pointName = "Bus Stop";
                }
                
                var stopName = document.getElementById( "stop-name");
                stopName.innerHTML=pointName;
                
                var pointDate = document.getElementById( "point-time-label");
                pointDate.innerHTML=point.date;
                
                if (point.speed){
                    var pointSpeed = document.getElementById( "point-speed-label");
                    pointSpeed.innerHTML=point.speed+" km/hr";
                }
                
                if (point.bearing){
                    var pointDirection = document.getElementById( "point-direction-label");
                    pointDirection.innerHTML=point.bearing;
                }
                
                if (times){
                    var table = document.getElementById("bus-stop-time-table");
                    table.innerHTML="<th>Time Table</th>";
                
                    var checkBox = document.getElementById("is-bus-stop");
                    checkBox.checked = true;
                    
                    for( i = 0; i < times.length; i++)
                    {
                        table.innerHTML = table.innerHTML+"<tr><td>"+times[i]+"</tr></td>"
                    }
                }else{
                    var checkBox = document.getElementById("is-bus-stop");
                    checkBox.checked = false;
                }

                $("#stop-modal").modal('show');
            })
        }
    
    </script>
    
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDtM_vJ0Vzzexq_xaT-TYbaV3w95pIu3U&libraries=drawing&callback=initMap">
    </script>
@endsection
</body>
</html>
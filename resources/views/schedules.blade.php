
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    
    <title>View Employee Schedules</title>
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        li{
            list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
        }
        .responstable{
            overflow-x: auto;
        }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

        @if($errors->any())
            <div id="complete-profile" class="warning row">
                <h4>{{$errors->first()}}</h4>
            </div>
        @endif
        
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Employee Schedules</h4></div>
        </div>

        <form action="/schedules/view" method="get" autocomplete="off">   
            <div class="row row-bordered">

                <div class="col-lg-1">
                    <h4>Employee</h4>
                    <select class="form-control" id="employeeId" name="employeeId">
                        <option value="">All</option>
                        @foreach ($company->getEmployees() as $indexKey => $employee)
                            @if ($parameters['employeeId'] == $employee->employeeId)
                                <option value="{{$employee->employeeId}}" selected>{{$employee->name}}</option>
                            @else
                                <option value="{{$employee->employeeId}}">{{$employee->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                
                <div class="col-lg-2">
                    <h4>Bus</h4>
                    <select class="form-control" name="deviceId" id="deviceId">
                        <option value="">All</option>
                        @foreach ($company->getDevices() as $indexKey => $device)
                            @if ($parameters['deviceId'] == $device->deviceId)
                                <option value="{{$device->deviceId}}" selected>
                                    Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                </option>
                            @else
                                <option value="{{$device->deviceId}}">
                                   Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-2">
                    <h4>Route</h4>
                    <select class="form-control" id="routeId" name="routeId">
                        <option value="">All</option>
                        @foreach ($company->routes as $indexKey => $route)
                            @if ($parameters['routeId'] == $route->routeId)
                                <option value="{{$route->routeId}}" selected>{{$route->routeName}}</option>
                            @else
                                <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                
                <div class="col-lg-2">
                    <h4>Days</h4>
                    <select name="days[]" class="form-control days-update-picker" multiple>
                        @if ($parameters['days'] === "")
                            <option value="" selected>All</option>
                        @else
                             <option value="">All</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '1') !== false)
                            <option value="1" selected>Monday</option>
                        @else
                            <option value="1" selected>Monday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '2') !== false)
                            <option value="2" selected>Tuesday</option>
                        @else
                            <option value="2">Tuesday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '3') !== false)
                            <option value="3" selected>Wednesday</option>
                        @else
                            <option value="3">Wednesday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '4') !== false)
                            <option value="4" selected>Thursday</option>
                        @else
                            <option value="4">Thursday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '5') !== false)
                            <option value="5" selected>Friday</option>
                        @else
                            <option value="5">Friday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '6') !== false)
                            <option value="6" selected>Saturday</option>
                        @else
                            <option value="6">Saturday</option>
                        @endif
                        
                        @if (strpos($parameters['days'], '0') !== false)
                            <option value="0" selected>Sunday</option>
                        @else
                            <option value="0">Sunday</option>
                        @endif
                    </select>
                </div>

                <div class="col-lg-2">
                    <h4>Start</h4>
                    <div class='input-group date' id='start_date'>
                        @if (isset($parameters['start']))
                            <input name="start" type='text' value="{{$parameters['start']}}" class="form-control" />
                        @else
                            <input name="start" type='text' class="form-control" />
                        @endif
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
                
                <div class="col-lg-2">
                    <h4>End</h4>
                    <div class='input-group date' id='end_date'>
                        @if (isset($parameters['end']))
                            <input name="end" type='text' value="{{$parameters['end']}}" class="form-control" />
                        @else
                            <input name="end" type='text' class="form-control" />
                        @endif
                        
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>
            </div>
           
            <div class="row">
                <div style="margin-left: 30px" class="col-lg-2">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-default" type="submit">Show Schedules</button>
                </div>
            </div>
        </form>
        
        @if (count($data) > 0)
            <div class="row row-bordered">
                <div class="col-lg-1"><h4>employee</h4></div>
                <div class="col-lg-2"><h4>bus</h4></div>
                <div class="col-lg-1"><h4>route</h4></div>
                <div class="col-lg-2"><h4>days</h4></div>
                <div class="col-lg-2"><h4>start</h4></div>
                <div class="col-lg-2"><h4>end</h4></div>
                <div class="col-lg-1"><h4>update</h4></div>
                <div class="col-lg-1"><h4>delete</h4></div>
            </div>
        @endif
        
        <div id="time-log-container" class="time-log-container">
            @foreach ($data as $indexKey => $schedule)
                <form autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="_token" value="{{csrf_token() }}">
                        <input type="hidden" class="form-control" name="scheduleId" value="{{$schedule->scheduleId}}">

                        <div class="col-lg-1">
                            <select class="form-control" id="employeeId" name="employeeId">
                                <option value="">All</option>
                                @foreach ($company->getEmployees() as $indexKey => $employee)
                                    @if ($schedule->employeeId == $employee->employeeId)
                                        <option value="{{$employee->employeeId}}" selected>{{$employee->name}}</option>
                                    @else
                                        <option value="{{$employee->employeeId}}">{{$employee->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <select class="form-control" name="deviceId" id="deviceId">
                                @foreach ($company->getDevices() as $indexKey => $device)
                                    @if ($schedule->deviceId == $device->deviceId)
                                        <option value="{{$device->deviceId}}" selected>
                                            Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                        </option>
                                    @else
                                        <option value="{{$device->deviceId}}">
                                           Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="col-lg-1">
                            <select class="form-control" name="routeId">
                                @foreach ($company->routes as $indexKey => $route)
                                    @if ($schedule->routeId == $route->routeId)
                                        <option value="{{$route->routeId}}" selected>{{$route->routeName}}</option>
                                    @else
                                        <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <select  name="days[]" class="form-control days-update-picker" multiple>
                                @if (strpos($schedule->days, '1') !== false)
                                    <option value="1" selected>Monday</option>
                                @else
                                    <option value="1" selected>Monday</option>
                                @endif

                                @if (strpos($schedule->days, '2') !== false)
                                    <option value="2" selected>Tuesday</option>
                                @else
                                    <option value="2">Tuesday</option>
                                @endif

                                @if (strpos($schedule->days, '3') !== false)
                                    <option value="3" selected>Wednesday</option>
                                @else
                                    <option value="3">Wednesday</option>
                                @endif

                                @if (strpos($schedule->days, '4') !== false)
                                    <option value="4" selected>Thursday</option>
                                @else
                                    <option value="4">Thursday</option>
                                @endif

                                @if (strpos($schedule->days, '5') !== false)
                                    <option value="5" selected>Friday</option>
                                @else
                                    <option value="5">Friday</option>
                                @endif

                                @if (strpos($schedule->days, '6') !== false)
                                    <option value="6" selected>Saturday</option>
                                @else
                                    <option value="6">Saturday</option>
                                @endif

                                @if (strpos($schedule->days, '0') !== false)
                                    <option value="0" selected>Sunday</option>
                                @else
                                    <option value="0">Sunday</option>
                                @endif
                            </select>
                        </div>
                        
                        <div class="col-lg-2">
                            <div class='input-group date' id='update_start'>
                                <input name="start" type='text' value="{{sprintf('%04d',$schedule->start)}}" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class='input-group date' id='update_end'>
                                <input name="end" type='text' value="{{sprintf('%04d',$schedule->end)}}" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-lg-1">
                            <input formmethod="post" formaction="/schedules/{{$schedule->scheduleId}}/update" class="btn btn-default green" type="submit" value="Update">
                        </div>
                        
                        <div class="col-lg-1">
                            <input formmethod="post" formaction="/schedules/{{$schedule->scheduleId}}/delete" class="btn btn-default red" type="submit" value="Delete">
                        </div>
                    </div>
                </form>
            @endforeach
            
            <div class="row">
                <div class="col-lg-2 col-bordered"><h4>Create New Schedule</h4></div>
            </div>
            
            <div class="row row-bordered">
                <div class="col-lg-1"><h4>employee</h4></div>
                <div class="col-lg-2"><h4>bus</h4></div>
                <div class="col-lg-2"><h4>route</h4></div>
                <div class="col-lg-2"><h4>days</h4></div>
                <div class="col-lg-2"><h4>start</h4></div>
                <div class="col-lg-2"><h4>end</h4></div>
                <div class="col-lg-1"><h4>add</h4></div>
            </div>
            
            <form method="post" action="/schedules/create" autocomplete="off">   
                <div class="row row-bordered">
                    <input type="hidden" name="_token" value="{{csrf_token() }}">
                    <div class="col-lg-1">
                        <select class="form-control" id="employeeId" name="employeeId" id="employeeId">
                            @foreach ($company->getEmployees() as $indexKey => $employee)
                                @if ($parameters['employeeId'] == $employee->employeeId)
                                    <option value="{{$employee->employeeId}}" selected>{{$employee->name}}</option>
                                @else
                                    <option value="{{$employee->employeeId}}">{{$employee->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="col-lg-2">
                        <select class="form-control" name="deviceId" id="deviceId">
                            @foreach ($company->getDevices() as $indexKey => $device)
                                <option value="{{$device->deviceId}}">
                                    Bus: {{$device->busId}} - Device: {{$device->deviceId}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="col-lg-2">
                        <select class="form-control" id="routeId" name="routeId">
                            @foreach ($company->routes as $indexKey => $route)
                                <option value="{{$route->routeId}}">{{$route->routeName}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <select id="days" name="days[]" class="days-picker" multiple>
                            <option value="1">Monday</option>
                            <option value="2">Tuesday</option>
                            <option value="3">Wednesday</option>
                            <option value="4">Thursday</option>
                            <option value="5">Friday</option>
                            <option value="6">Saturday</option>
                            <option value="0">Sunday</option>
                        </select>
                    </div>

                    <div class="col-lg-2">
                        <div class="input-group date" name="add_start" id="add_start">
                            <input name="start" type="text" class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class='input-group date' id='add_end' name='add_end'>
                            <input name="end" type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-lg-1">
                        <button onclick="createSchedule(this.form);" class="btn btn-default" type="button">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.days-picker').multiselect();
            
            $('.days-update-picker').multiselect();
            
            $("#start_date").datetimepicker({
                format: "HH:mm"
            });
            
            $("#end_date").datetimepicker({
                format: "HH:mm"
            });
            
            $("#add_start").datetimepicker({
                format: "HH:mm"
            });
            
            $("#add_end").datetimepicker({
                format: "HH:mm"
            });
            
            $("#update_start").datetimepicker({
                format: "HH:mm"
            });
            
            $("#update_end").datetimepicker({
                format: "HH:mm"
            });
        });
        
        function createSchedule(form){
            deviceId = form.elements["deviceId"].value;
            routeId = form.elements["routeId"].value;
            days = form.elements["days"].value;
            employeeId = form.elements["employeeId"].value;
            start = form.elements["start"].value;
            end = form.elements["end"].value;
            
            //alert(deviceId+" "+routeId+" "+days+" "+employeeId+" "+start+" "+end);
            if (!deviceId || !routeId || !days || !employeeId || !start || !end){
                alert("Please fill in all items");
                return;
            }
            form.submit();
        }
        
    </script>

 @endsection
</body>
</html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=variable-width, initial-scale=1">

    <title>Manage App Variables</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
         li{
    list-style-type: none;
        }
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
        .vertical-center {
            min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
            min-height: 100vh; /* These two lines are counted as one :-)       */
            display: flex;
            align-items: center;
    }
    </style>
</head>
<body id="app-layout">
@extends('layouts.app')
@section('content')
    <div class="container-fluid ">

    <div class="row">
        <div class="col-lg-2 col-bordered"><h4>App Variables</h4></div>
    </div>

    <div class="row row-bordered">
        <div class="col-lg-3"><h4>Code</h4></div>
        <div class="col-lg-3"><h4>Value</h4></div>
        <div class="col-lg-4"><h4>Description</h4></div>
        <div class="col-lg-1"><h4>Save</h4></div>
        <div class="col-lg-1"><h4>Delete</h4></div>
    </div>
        
    @foreach ($data as $indexKey => $variable)
        @if (!isset($variable))
            @continue;
        @endif
        <form method="post" autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="variableId" value="{{$variable->id}}" readonly>    
            <div class = "row">
                <div class="col-lg-3">
                    @if ($variable->code != null)
                        <input type="text" class="form-control" name="code" value="{{$variable->code}}">
                    @else
                        <input type="text" class="form-control" name="code" value="">
                    @endif
                </div>
                
                <div class="col-lg-3">
                    @if ($variable->value != null)
                        <input type="text" class="form-control" name="value" value="{{$variable->value}}">
                    @else
                        <input type="text" class="form-control" name="value" value="">
                    @endif
                </div>
                
                <div class="col-lg-3">
                    @if ($variable->description != null)
                        <input type="text" class="form-control" value="{{$variable->description}}" readonly="true">
                    @else
                        <input type="text" class="form-control" value="">
                    @endif
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/app_variables/edit" class="btn btn-default" type="submit" value="Save">
                </div>

                <div class="col-lg-1">
                    <input formaction="dashboard/app_variables/delete" placeholder="+27...." class="btn btn-default" type="submit" value="Delete">
                </div>
             </div>
        </form>
    @endforeach
    
        <div class="row">
            <div class="col-lg-2 col-bordered"><h4>Add Variable</h4></div>
        </div>
    
        <form method="post" autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class = "row">
                <div class="col-lg-3">
                    <input type="text" class="form-control" name="code" value="">
                </div>

                <div class="col-lg-3">
                    <input type="text" class="form-control" name="value" value="">
                </div>

                <div class="col-lg-3">
                </div>

                <div class="col-lg-3">
                    <input formaction="dashboard/app_variables/new" class="btn btn-default" type="submit" value="Save">
                </div>
             </div>
        </form>
    
    </div>

    <script type="text/javascript">
        function saveRoute(route,form){
            if (route != null && route && route !== "Select Route"){
                form.action = "dashboard/variables/edit";
                form.submit();
            }
        }
    </script>
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

 @endsection
</body>
</html>
<?php


/**
 * Description of MigrationsForTest
 *
 * @author Takunda Chirema
 */
class MigrationsForTest extends TestCase{
    
    public function testCreateUsersMigration()
    {
        $this->post('/users', ['id' => 'taku','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/users', ['id' => 'ben','email' => 'ben@example.com',
            'phoneNumber' => '077982323','password' => 'ben','salt' => '','privilege' => 'employee']);
        
        $this->post('/users', ['id' => 'jon','email' => 'jon@example.com',
            'phoneNumber' => '077982323','password' => 'jon','salt' => '','privilege' => 'employee']);
        
        $this->post('/users', ['id' => 'dan','email' => 'dan@example.com',
            'phoneNumber' => '077982323','password' => 'dan','salt' => '','privilege' => 'employee']);
        
        $this->post('/users', ['id' => 'sim','email' => 'sim@example.com',
            'phoneNumber' => '077982323','password' => 'sim','salt' => '','privilege' => 'employee']);
        
    }
    
    public function testCreateCompaniesMigration()
    {
        $this->post('/companies', ['id' => 'frigo','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'077 777 7777', 'about'=>'Get some coffee on campus', 'sector'=>'Restaurant',
            'email'=>'friigocoffee@frigo.co.za', 'address'=>'Rondebosch, Cape Town, 7700','type'=>'private','password'=>'frigo'])
                ->seeJson([
                 'success' => 1,
             ]);
        
        $this->post('/companies', ['id' => 'Jammie Shuttle','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'077 777 7777', 'about'=>'University of Cape Town Shuttle Services. Transport to campus and from'
            . '', 'sector'=>'Transportation','email'=>'jshuttle@uct.ac.za', 
            'address'=>'University of Cape Town, Baxter Road, Rondebosch', 'type'=>'public','password'=>'jammie'])
                ->seeJson([
                 'success' => 1,
             ]); 
        
        $this->post('/companies', ['id' => 'CPS','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'021 650 2121', 'about'=>'Security on Campus UCT', 'sector'=>'Security', 
            'email'=>'cps@uct.ac.za', 'address'=>'Robert Leslie Social Science Building, University Avenue.','type'=>'public','password'=>'cps'])
                ->seeJson([
                 'success' => 1,
             ]); 
        
        $this->post('/companies', ['id' => 'MyCiti','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'077 777 7777', 'about'=>'We are a transport company based in Cape Town. We offer the best of '
            . 'services including a real time tracking of buses for commuters. Feel free to get in contact with us if you have'
            . 'any queries. Enjoy our rides!!', 'sector'=>'Transportation', 'email'=>'transport.info@capetown.gov.za', 
             'address'=>'civic center Cape Town CBD','type'=>'public','password'=>'myciti'])
                ->seeJson([
                 'success' => 1,
             ]); 
        
        $this->post('/companies', ['id' => 'OpenBus','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'0861 733 287', 'about'=>"A ticket on the City Sightseeing bus gives you "
            . "access to all our main tours and takes you directly to Cape Town's top attractions – "
            . "including the Table Mountain Cableway, V&A Waterfront and Camps Bay"
            , 'sector'=>'Transportation', 'email'=>'info@citysightseeing.co.za', 
             'address'=>' V&A Waterfront and Long Street','type'=>'public','password'=>'openbus'])
                ->seeJson([
                 'success' => 1,
             ]);
        
        $this->post('/companies', ['id' => 'Metrorail','monday' => '8:00,17:00',
            'tuesday' => '8:00,17:00','wednesday' => '8:00,17:00','thursday' => '8:00,17:00',
            'friday' => '8:00,17:00','saturday' => '8:00,17:00','sunday' => '8:00,17:00','address'=>"test road",
            'phoneNumber'=>'0861 733 287', 'about'=>"Explore Cape Town on our trains. Northen, Southern and Central lines"
            . "will take you through the heart and resorts of Cape Town, from the CBD to the Beach. Travel to and from"
            . "work at a cheap rate with a reliable service. Enjoy Metrorail!"
            , 'sector'=>'Transportation', 'email'=>'receptionist@intersite.co.za', 
             'address'=>'Propnet Building, 1 Adderley Street, Cape Town 8000','type'=>'public','password'=>'metrorail'])
                ->seeJson([
                 'success' => 1,
             ]);
        
    }
    
    public function testCreateCompanyEmployeesMigration()
    {
        $this->post('/employees', ['id' => 'tsitsi','company' => 'frigo','email' => 'tsitsi@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tsitsi','company' => 'Jammie Shuttle','email' => 'tsitsi@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tsitsi','company' => 'CPS','email' => 'tsitsi@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tsitsi','company' => 'MyCiti','email' => 'tsitsi@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tsitsi','company' => 'OpenBus','email' => 'tsitsi@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => '','privilege' => 'employee']);
        
        
        $this->post('/employees', ['id' => 'taku','company' => 'frigo','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'taku','company' => 'Jammie Shuttle','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'taku','company' => 'CPS','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'taku','company' => 'MyCiti','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'taku','company' => 'OpenBus','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'taku','company' => 'Metrorail','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => '','privilege' => 'employee']);
        
        
        $this->post('/employees', ['id' => 'tin','company' => 'frigo','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tin','company' => 'Jammie Shuttle','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tin','company' => 'CPS','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tin','company' => 'MyCiti','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tin','company' => 'OpenBus','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'tin','company' => 'Metrorail','email' => 'tin@example.com',
            'phoneNumber' => '077982323','password' => 'tin','salt' => '','privilege' => 'employee']);
        
        /** admin users */
        $this->post('/employees', ['id' => 'admin','company' => 'frigo','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'admin','company' => 'Jammie Shuttle','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'admin','company' => 'CPS','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'admin','company' => 'MyCiti','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'admin','company' => 'OpenBus','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
        $this->post('/employees', ['id' => 'admin','company' => 'Metrorail','email' => 'admin@example.com',
            'phoneNumber' => '077982323','password' => 'admin','salt' => '','privilege' => 'employee']);
        
    }
    
    public function testCreateCompanyFollowersMigration(){
        
        $this->post('/user/ben/follow/frigo');
        
        $this->post('/user/ben/follow/Jammie Shuttle');
        
        $this->post('/user/ben/follow/CPS');
        
    }
    
    public function testCreateUserMessagesMigration(){
        
        $this->post('/users/ben/messages',['friendID'=>'tsitsi','message'=>'hey how are you?']);
        
        $this->post('/users/taku/messages',['friendID'=>'tsitsi','message'=>'hey how are you?']);
        
        $this->post('/users/ben/messages',['friendID'=>'taku','message'=>'hey how are you?']);
        
    }
    
    public function testUserRequestMigration(){
        
        $this->post('user/taku/request/tsitsi');
        
        $this->post('user/ben/request/tsitsi');
        
        $this->post('user/dan/request/tsitsi');
        
        $this->post('user/tsitsi/request/ben');
        
    }
    
    public function testCreateCompanyPostMigration()
    {
          $this->post('companies/frigo/posts/company', ['post'=>'this is a post','message'=>'hey how are you?']);
          $this->post('companies/frigo/posts/company', ['post'=>'post','message'=>'wooop?']);
          $this->post('companies/frigo/posts/admin', ['post'=>'this is a post','message'=>'lets go']);
          $this->post('companies/frigo/posts/admin', ['post'=>'poker night','message'=>'oh guys company poker night yay']);
          $this->post('companies/frigo/posts/admin', ['post'=>'poker night','message'=>'no, the company will not provide the money, bring your own']);
          $this->post('companies/frigo/posts/admin', ['post'=>'poker night','message'=>'...poker night cancelled due to lack of attendance']);
    }
}

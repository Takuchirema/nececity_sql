<?php

/**
 * Description of UserControllerTest
 *
 * @author Takunda Chirema
 */

class UserControllerTest extends TestCase{
    
    public function testNewUser()
    {
        $this->post('/users', ['userID' => 'tsitsi','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'tsitsi','salt' => ''])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testEditUserProfile()
    {
        $this->patch('/users', ['userID' => 'tsitsi','email' => 'tsitsi@example.com'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetUserDetails()
    {
        $this->get('/api/users/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    /**
     * Needs to be tested again when companies are inserted
     */
    public function testGetUserCompanies()
    {
        $this->get('/users/tsitsi/companies')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    
    public function testGetUserFriends()
    {
        $this->get('/users/tsitsi/friends')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    /**
     * must be tested when friend requests test user
     */
    public function testGetUserRequests()
    {
        $this->get('/users/tsitsi/requests/received')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    /**
     * must be tested with user requesting test user
     */
    public function testGetUserRequested()
    {
        $this->get('/users/tsitsi/requests/sent')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetAllUsers()
    {
        $this->get('/users/tsitsi/all')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetUserMessages()
    {
        $this->get('/users/tsitsi/messages')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testSendMessage()
    {
        $this->post('/users/tsitsi/messages',['friendID'=>'taku','message'=>'hey how are you?'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testSetUserAlert()
    {
        $this->post('/users/tsitsi/alert/on')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testCancelUserAlert()
    {
        $this->delete('/users/tsitsi/alert/off')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testEditUserSettings()
    {
        $this->patch('/users/tsitsi/settings',['usersRefreshInterval' => '5 min'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testHideUserLocation()
    {
        
    }
    
    public function testUnfriend()
    {
        $this->delete('/users/tsitsi/unfriend/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testAcceptRequest()
    {
        $this->post('/users/tsitsi/accept/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testRejectRequest()
    {
        $this->delete('/users/tsitsi/reject/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testSendRequest()
    {
        $this->post('/users/tsitsi/request/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testCancelRequest()
    {
        $this->delete('/users/tsitsi/cancel/taku')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testFollowCompany()
    {
        $this->post('/users/tsitsi/follow/frigo')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testUnfollowCompany()
    {
        $this->delete('/users/tsitsi/unfollow/frigo')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testPostComment()
    {
        $this->post('/users/tsitsi/comment/1/from/frigo',['comment' => 'nice picture!',
            'time' => '2016-12-17_10:40'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
}

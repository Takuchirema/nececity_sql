<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/testUser','UserController@newUser');

Route::get('/testGetUsers','UserController@getAllUsers');

Route::get('/testSendMessage','UserController@sendMessage');

Route::get('/testGetMessages','UserController@getUserMessages');

Route::get('/', function()
{
	return View::make('hello');
});
//1
Route::post('/companies', 'CompanyController@newCompany');
//2
Route::patch('/companies', 'CompanyController@editProfile');
//3
Route::get('/companies/{companyName}', 'CompanyController@getDetails()');
//4
Route::get('companies', 'CompanyController@getFollowers');
//5
Route::get('companies/{companyName}/posts/{type}', 'CompanyController@getPosts');
//6
Route::get('Companies/{companyName}/Employees', 'CompanyController@getEmployees');
//7
Route::post('Companies/{companyName}/Posts/{type}', 'CompanyController@createPost');

Route::get('companies/{companyName}/user/promotions/{userID}', 'CompanyController@getCompanyUserPromotions');

//8
Route::patch('Companies/{companyName}/location', 'CompanyController@setPlace');
//9
Route::patch('Companies/{companyName}/followers', 'CompanyController@blockFollower');
//10
Route::post('users/', 'UserController@newUser');
//11
Route::patch('users/', 'UserController@editProfile');
//12
Route::get('users/', 'UserController@getDetails');
//13
Route::get('users/{username}/{companyName}/', 'UserController@getCompanies');
//14
Route::get('Users/{username}/friends/', 'UserController@getFriends');
//15
Route::get('Users/{username}/requests/received', 'UserController@getRequests');
//16
Route::get('users/{username}/requests/sent', 'UserController@getRequested');
//17
Route::get('users/{username}/friends/missing', 'UserController@getAllUsers');
//18
Route::get('users/{username}/messages', 'UserController@getMessages');
//19
Route::post('users/{username}/alert/on', 'UserController@setAlert');
//20
Route::delete('users/{username}/alert/off', 'UserController@cancelAlert');
//21
Route::patch('users/{username}/settings', 'UserController@editSettings');
//22
Route::patch('user/{username}/location/visibility', 'UserController@setLocationVisibility');
//23
Route::delete('user/{username}/friends/{friendId}', 'UserController@unfriend');
//24
Route::post('user/{username}/friends/{friendID}','UserController@acceptRequest');
//25
Route::delete('user/{username}/requests/received', 'UserController@rejectRequest');
//26
Route::post('user/{username}/requests/sent', 'UserController@sendRequest');
//27
Route::delete('user/{username}/requests/sent', 'UserController@cancelRequest');
//28
Route::post('user/companies/{companyname}','UserController@followCompany');
//29
Route::delete('user/companies/{companyname}', 'UserController@unfollowCompany');
//30
Route::post('companies/{companyname}/Posts/{PostID}/comments/','UserController@postComment');
//31
Route::post('companies/{companyname}/employees/', 'EmployeeController@new_employee');
//32
Route::patch('companies/{companyname}/employees/{employeeID}', 'EmployeeController@edit_profile');
//33
Route::get('employees/{employeename}', 'EmployeeController@get_details');
//34
Route::get('employees/{employeename}/companies','EmployeeController@get_company');
//35
Route::get('employees/{employeename}/friends', 'EmployeeController@get_friends');
//37
Route::patch('employees/{employeename}/messages', 'EmployeeController@get_messages');
//38
Route::patch('employees/{employeename}/alert','EmployeeController@cancel_alert');
//39
Route::patch('employees/{employeename}/settings', 'EmployeeController@edit_settings');
//40
Route::post('companies/{companyname}/posts/{postId}/comments', 'EmployeeController@post_comment');

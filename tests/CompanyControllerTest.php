<?php

class CompanyControllerTest extends TestCase{
    
    public function testNewCompany()
    {
        $this->post(
                '/companies', 
                [
                    'id' => 'yes',
                    'monday' => '8:00,17:00',
                    'tuesday' => '8:00,17:00',
                    'wednesday' => '8:00,17:00',
                    'thursday' => '8:00,17:00',
                    'friday' => '8:00,17:00',
                    'saterday' => '8:00,17:00',
                    'sunday' => '8:00,17:00',
                    'address'=>"test road",
                    'phoneNumber'=>'077 777 7777',
                    'about'=>'this is about testing',
                    'sector'=>'testing',
                    'type'=>'private'
                ])
                ->seeJson(
                [
                    'success' => 1,
                ]);             
    }
    
    public function testEditCompanyProfile(){

    	$this->patch(
                '/companies',
                [
                    'id'=>'yes',
                    'property'=>'about',
                    'value'=>'we sell pizza'
                ])
                ->seeJson(
                [
                    'success' => 1,
                ]);
    }
    
    public function testGetCompanyDetails(){
    	$this->get(
                '/companiess/yes'
                )
                ->seeJson(
                [
                    'success' => 1,
                ]);

    }
    
    public function testGetCompanyFollowers(){
    	$this->get(
                '/companies/yes/followers'
                )
                ->seeJson(
                [
                    'success' => 1,
                ]);
    }
    
    public function testGetCompanyPosts(){
    	$this->get(
                'companies/{testco}/posts/{public}'
                )
                ->seeJson(
                [
                    'success'=>1
                ]);
    }
    
    public function testGetCompanyEmployees(){
    	$this->get(
                'companies/frigo/employees'
                )
                ->seeJson(
                [
                    'success'=>1
                ]);
    }
    
    public function testCreateCompanyPost(){
    	$this->post(
                'companies/frigo/posts/company',
                [
                    'post'=>'Welcome to Frigo everybody!!!'
                ])
                ->seeJson(
                [
                    'success'=>1
                ]);
    }
    
    public function testSetCompanyLocation(){
    	$this->patch('companies/frigo/location', ['latitude'=>'12.638383','longitude'=>'63.4566'])
    	->seeJson(['success'=>1]);
    }
    
    public function testBlockCompanyFollower(){
    	$this->patch('companies/frigo/block/ben')
    	->seeJson(['success'=>1]);

    }
    
    public function testUnblockCompanyFollower(){
    	$this->patch('companies/frigo/unblock/ben')
    	->seeJson(['success'=>1]);
    }
}
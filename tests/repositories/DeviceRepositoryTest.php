<?php

use App\Repositories\DeviceRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\RouteRepository;
use App\Models\Device;
use App\Http\Helpers\Constants;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeviceRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create(){
        $this->createCompany("test");
        $companyRep = CompanyRepository::getRepository(null, "test");
        
        $repository = new DeviceRepository('1',$companyRep->companyId);
        $repository->create();
        
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        $repository->update(array('discoverRoutes' => 'true'));
        
        return $repository;
    }

    public function testCanCreateDevice()
    {
        $repository = $this->create();
        
        $device = Device::query()
                ->where('companyId','=',$repository->companyId)
                ->where('deviceId','=','1')
                ->first();
        $this->assertEquals('1', $device->deviceId);
    }
    
    public function testGetDevice()
    {
        $this->create();
        
        $deviceRepository = DeviceRepository::getRepository('1');
        $this->assertEquals('1', $deviceRepository->deviceId);
    }
    
    public function testGetDevices()
    {
        $repository = $this->create();
        
        $devices1 = DeviceRepository::getDevices($repository->companyId)['devices'];
        $this->assertEquals(1, sizeof($devices1));
    }
    
    public function testUpdateDevice()
    {
        $repository = $this->create();
        $repository->update(array('employeeId' => '2'));
        
        $device = Device::query()
                ->where('companyId','=',$repository->companyId)
                ->where('deviceId','=','1')
                ->first();
        $this->assertEquals('2', $device->employeeId);
    }
    
    public function testCreateUnknownRoute(){
        $repository = $this->create();
        $routePoints = $this->createRoutePoints();
        $time = DateTime::createFromFormat('d/m/Y', '29/06/2019');
        
        $deviceRep = DeviceRepository::getRepository('1');
        Constants::deviceUpdateInterval;
        foreach ($routePoints as $point){
            $time->add(new DateInterval('PT300S')); 
            
            $lat = explode(",", $point)[0];
            $lng = explode(",", $point)[1];
            
            $timeStr = $time->format('Y-m-d H:i:s');
            
            if ($point === $routePoints[0]){
                $deviceRep->setLatLng($lat, $lng, $timeStr, "0", "100");
            }else{
                $deviceRep->setLatLng($lat, $lng, $timeStr, "60", "100");
            }
        }
        
        $routeDetails = RouteRepository::getRoutesDetails($repository->companyId)['routes'];
        $this->assertEquals('Rondebosch - Rondebosch', $routeDetails[0]->routeName);
    }
    
     public function testCreateUnknownEmployee(){
         $repository = $this->create();
         $repository->setRouteId('1');
         DeviceRepository::updateCacheRepository($repository);
         
         //echo "\n company id: ".$repository->companyId." ".$repository->getRouteId();
         $employees = DeviceRepository::createUnknownEmployees($repository->companyId);
         $this->assertEquals('Unknown', $employees[0]->name);
     }
    
    function createRoutePoints(){
        $routePoints=array("-33.95596731014625,18.45878078323699",
                    "-33.95675933670842,18.458169239581594",
                    "-33.95756915399711,18.45779373031951",
                    "-33.95827217594244,18.4575040517459",
                    "-33.95845015526027,18.4590168176303",
                    "-33.95916206880829,18.45875932556487",
                    "-33.96042570068434,18.45949961525298",
                    "-33.96073715636207,18.459918039859303",
                    "-33.96042570068434,18.460422295154103",
                    "-33.96026552303463,18.46025063377715",
                    "-33.9584145594265,18.4610338388095",
                    "-33.95707080580857,18.461377161563405",
                    "-33.95495279344261,18.46146299225188",
                    "-33.954694712702086,18.460325735629567",
                    "-33.95488159883375,18.4600253282199",
                    "-33.95572703096754,18.460078972400197",
                    "-33.956999612971906,18.459606903613576",
                    "-33.95859253844646,18.458930986941823",
                    "-33.95923325983545,18.45875932556487",
                    "-33.96052358687735,18.459553259433278",
                    "-33.96074605507896,18.45998241287566",
                    "-33.96037230816794,18.460443752826222",
                    "-33.96021213041771,18.46026136261321",
                    "-33.958224044631194,18.461073033797447",
                    "-33.956855339246886,18.461405627715294",
                    "-33.954888610246456,18.461448543059532",
                    "-33.954666126728156,18.4602361845848",
                    "-33.95500430144607,18.460075252043907",
                    "-33.95584973236068,18.460053794371788",
                    "-33.95835037884169,18.45903455494613",
                    "-33.95905339433161,18.4587770628807",
                    "-33.95596731014625,18.45878078323699"
            );
        
        return $routePoints;
    }       
}


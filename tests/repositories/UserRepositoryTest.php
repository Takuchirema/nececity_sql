<?php

use App\Repositories\UserRepository;
use App\Repositories\AlertRepository;
use App\Repositories\RelationshipRepository;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create($userId)
    {
        $repository = new UserRepository($userId);
        $repository->userId=$userId;
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='user';
        $repository->salt='salt';
        $repository->email='user@yahoo.com';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateUser()
    {
        $this->create('1');
        
        $user = User::query()
                    ->where('userId','=','1')
                    ->first();
        $this->assertEquals('user', $user->name);
    }
    
    public function testGetUsers()
    {
        $this->create('1');
        
        $users = UserRepository::getUsers()['users'];
        $this->assertEquals(1, sizeof($users));
    }
    
    public function testDeleteUser()
    {
        $repository = $this->create('1');
        
        //$repository->delete();
        //$users = UserRepository::getUsers()['users'];
        //$this->assertEquals(0, sizeof($users));
    }
    
    public function testUpdateProperty()
    {
        $repository = $this->create('1');
        
        $repository->update(array('name' => 'user1'));
        $user1 = User::query()
                    ->where('userId','=','1')
                    ->first();
        
        $this->assertEquals('user1', $user1->name);
    }
    
    public function testGetFriends()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('1', '2');
        UserRepository::updateFriendship('1', '2', 'state', 'accepted');
        
        $friends = UserRepository::getFriends('2')['users'];
        
        $this->assertEquals(1, sizeof($friends));
    }
    
    public function testGetFriendsOfFriends()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('1', '2');
        UserRepository::updateFriendship('1', '2', 'state', 'accepted');
        
        UserRepository::sendRequest('2', '3');
        UserRepository::updateFriendship('2', '3', 'state', 'accepted');
        
        $friends = UserRepository::getFriendsOfFriends('1',2)['users'];
        $uniqueFriends = $this->removeDuplicates($friends,'1');
        
        $this->assertEquals(2, sizeof($uniqueFriends));
    }
    
    function removeDuplicates($users,$userId)
    {
        $uniqueUsers = array();
        
        foreach ($users as $user){
            if ($user->userId != $userId){
                $uniqueUsers[$user->userId] = $user;
            }
        }
        return array_values ( $uniqueUsers );
    }
    
    public function testGetAlertingUsers()
    {
        $this->create('1');
        $this->create('2');
        
        $alert = new AlertRepository('1');
        $alert->setUserId('1');
        $alert->setLatitude('35');
        $alert->setLongitude('35');
        $alert->setLocation('location');
        $alert->setTime('2017-12-22_17:00');
        $alert->create();
        
        $alert = new AlertRepository('2');
        $alert->setUserId('2');
        $alert->setLatitude('35');
        $alert->setLongitude('35');
        $alert->setLocation('location');
        $alert->setTime('2017-12-22_17:00');
        $alert->create();

        $friends = UserRepository::getAlertingUsers('1','35','35')['users'];
        
        $this->assertEquals(2, sizeof($friends));
    }
    
    public function testGetRequestingUsers()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('2', '1');
        
        UserRepository::sendRequest('3', '1');
        
        $friends = UserRepository::getRequestingUsers('1')['users'];
        
        $this->assertEquals(2, sizeof($friends));
    }
    
    public function testGetRequestedUsers()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('1', '2');
        
        UserRepository::sendRequest('1', '3');
        
        $friends = UserRepository::getRequestedUsers('1')['users'];
        
        $this->assertEquals(2, sizeof($friends));
    }
    
    public function testUpdateVisibility()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('1', '2');
        UserRepository::updateFriendship('1', '2', 'state', 'accepted');
        
        UserRepository::updateVisibility('1', '2', 'no');
        
        $friendRelation = UserRepository::getFriendRelation('1', '2');
        $relationshipRep = new RelationshipRepository;
        $relationshipRep->relationshipId = $friendRelation->relationshipId;
        
        $this->assertEquals('no', $relationshipRep->getProperty('visibility/visible'));
    }
    
     public function testUnfriend()
    {
        $this->create('1');
        $this->create('2');
        
        UserRepository::sendRequest('1', '2');
        UserRepository::updateFriendship('1', '2', 'state', 'accepted');
        
        UserRepository::unfriend('1', '2');
        
        $friendRelation = UserRepository::getFriendRelation('1', '2');
        
        $this->assertEquals(null, $friendRelation);
    }
    
    public function testGetRandomUsers()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        UserRepository::sendRequest('1', '2');
        UserRepository::updateFriendship('1', '2', 'state', 'accepted');
        
        $users = UserRepository::getRandomUsers('1', 100)["users"];
        
        $this->assertEquals('3', $users[0]->userId);
    }
    
    public function testSearchForUsers()
    {
        $this->create('ronny');
        $this->create('flap');
        $this->create('jacko');
        
        UserRepository::sendRequest('flap', 'jacko');
        UserRepository::updateFriendship('flap', 'jacko', 'state', 'accepted');
        
        $users = UserRepository::searchForUsers('jacko', 'on')["users"];
        
        $this->assertEquals('ronny', $users[0]->userId);
    }
}


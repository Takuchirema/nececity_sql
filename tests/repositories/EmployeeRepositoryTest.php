<?php

use App\Repositories\EmployeeRepository;
use App\Models\Employee;
use App\Models\TimeLog;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmployeeRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create($code)
    {
        $repository = new EmployeeRepository(null,'1');
        $repository->employeeCode=$code;
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='employee';
        $repository->privilege='admin';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateEmployee()
    {
        $repository = $this->create('1');
        $employeeId = $repository->employeeId;
        
        $employee = Employee::query()
                    ->where('companyId','=','1')
                    ->where('employeeCode','=','1')
                    ->first();
        $this->assertEquals('employee', $employee->name);
    }
    
    public function testGetEmployees()
    {
        $repository = $this->create('1');
        
        $employees = EmployeeRepository::getEmployees('1')['employees'];
        $this->assertEquals(1, sizeof($employees));
    }
    
    public function testDeleteEmployee()
    {
        $repository = $this->create('1');
        
        $repository->delete();
        $employees = EmployeeRepository::getEmployees('1')['employees'];
        $this->assertEquals(0, sizeof($employees));
    }
    
    public function testUpdateProperty()
    {
        $repository = $this->create('1');
        
        $repository->update(array('name' => 'employee1'));
        $employee1 = Employee::query()
                    ->where('employeeCode','=','1')
                    ->first();
        
        $this->assertEquals('employee1', $employee1->name);
    }
}


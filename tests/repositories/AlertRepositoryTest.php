<?php

use App\Repositories\AlertRepository;
use App\Models\Alert;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AlertRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function testCanCreateAlert()
    {
        $repository = new AlertRepository;
        $repository->companyId='1';
        $repository->employeeId='1';
        $repository->latitude='35';
        $repository->longitude='35';
        $repository->time='2017-12-22_17:00';
        $repository->create();
        
        $alert = Alert::query()->where('companyId','=','1')->first();
        $this->assertEquals('35', $alert->latitude);
    }
    
    public function testGetAlert()
    {
        $repository = new AlertRepository;
        $repository->companyId='1';
        $repository->employeeId='1';
        $repository->latitude='35';
        $repository->longitude='35';
        $repository->time='2017-12-22_17:00';
        $repository->create();
        
        $alertRepository = AlertRepository::getAlert('1','1');
        $this->assertEquals('2017-12-22_17:00', $alertRepository->time);
    }
    
    public function testGetAlerts()
    {
        $repository = new AlertRepository;
        $repository->companyId='1';
        $repository->employeeId='1';
        $repository->latitude='35';
        $repository->longitude='35';
        $repository->time='2017-12-22_17:00';
        $repository->create();
        
        $alerts1 = AlertRepository::getAlerts('2017-12-22')['alerts'];
        $this->assertEquals(1, sizeof($alerts1));
        
        $alerts2 = AlertRepository::getAlerts('2017-12-23')['alerts'];
        $this->assertEquals(0, sizeof($alerts2));
    }
}


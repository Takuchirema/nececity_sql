<?php

use App\Repositories\DeviceRepository;
use App\Repositories\ShiftRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\RouteRepository;
use App\Models\Device;
use App\Models\Route;
use App\Http\Helpers\Constants;
use Illuminate\Foundation\Testing\DatabaseTransactions;


/**
 * Description of ShiftRepositoryTest
 *
 * @author Takunda
 */
class ShiftRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function createEmployee($employeeCode){
        $this->createCompany("test");
        $companyRep = CompanyRepository::getRepository(null, "test");
        
        $repository = new EmployeeRepository(null,$companyRep->companyId);
        $repository->setEmployeeCode($employeeCode);
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='employee';
        $repository->privilege='admin';
        $repository->create();
        
        return $repository;
    }
    
    function createDevice(){
        $this->createCompany("test");
        $companyRep = CompanyRepository::getRepository(null, "test");
        
        $repository = new DeviceRepository('1',$companyRep->companyId);
        $repository->create();
        
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        $repository->update(array('discoverRoutes' => 'true'));
        
        return $repository;
    }
    
    public function testCreateShift()
    {
        $deviceRep = $this->createDevice();
        $employeeRep = $this->createEmployee('1');
        
        $shiftRep = new ShiftRepository($employeeRep->employeeId, $deviceRep->companyId, '1', $deviceRep->deviceId);
        $shiftRep->create();
        
        $createdRep = ShiftRepository::getRepository($shiftRep->shiftId);
        $this->assertEquals($employeeRep->employeeId, $createdRep->employeeId);
    }
    
    public function testProcessShift(){
        $deviceRep = $this->createDevice();
        $deviceRep->structureRouteCoordinates(null);
        $employeeRep = $this->createEmployee('1');
        
        $shiftRep = new ShiftRepository($employeeRep->employeeId, $deviceRep->companyId, '1', $deviceRep->deviceId);
        $shiftRep->create();
        
        $employeeRep->shiftId = $shiftRep->shiftId;
                
        $routePoints = $this->createRoutePoints();
        $time = DateTime::createFromFormat('d/m/Y', '29/06/2019');
        Constants::deviceUpdateInterval;
        
        //The shift will be 2 rounds of the route
        for ($i=0;$i<2;$i++){
            foreach ($routePoints as $point){
                $time->add(new DateInterval('PT30S')); 

                $lat = explode(",", $point)[0];
                $lng = explode(",", $point)[1];

                $timeStr = $time->format('Y-m-d H:i:s');

                if ($point === $routePoints[0]){
                    $deviceRep->setLatLng($lat, $lng, $timeStr, "0", "100");
                }else{
                    $deviceRep->setLatLng($lat, $lng, $timeStr, "60", "100");
                }

                $shiftRep->updateCoordinates($employeeRep, $deviceRep, $shiftRep);
                //echo "*** shift coordinates size: ".count($shiftRep->getStructuredRouteCoordinates()['latlng1']);
                if ($point != $routePoints[0]){
                    continue;
                }
                
                //repeat the first point so that it is our bus stop
                for ($j=0;$j<5;$j++){
                    $time->add(new DateInterval('PT10S'));
                    $deviceRep->setLatLng($lat, $lng, $timeStr, "0", "100");
                    $shiftRep->updateCoordinates($employeeRep, $deviceRep, $shiftRep);
                }
            }
        }
        
        $routeRep = RouteRepository::getRoutes($deviceRep->companyId, false)['routes'][0];
        $shiftRep->routeId = $routeRep->routeId;
        $shiftRep->endShift();
        
        $newRouteRep = RouteRepository::getRepository($routeRep->routeId, $routeRep->companyId, false);
        //echo "** route name: ".$newRouteRep->routeName." size: ".$newRouteRep->routePoints;
        $this->assertEquals(3, count($newRouteRep->getBusStopPoints()['points']));
        
        $newShiftRep = ShiftRepository::getRepository($shiftRep->shiftId);
        //var_dump($newShiftRep->createShiftLog());
        $this->assertEquals('60', $newShiftRep->topSpeed);
        $this->assertEquals(5, count($newShiftRep->busStopPoints));
        //$shiftLog = $newShiftRep->createShiftLog();
        //var_dump($newShiftRep->shiftSummary);
        $this->assertEquals(1, count($newShiftRep->createShiftLog()["1 - Bus Stop 1"]));
    }
    
    public function testRefreshTimeTables(){
        $deviceRep = $this->createDevice();
        $deviceRep->structureRouteCoordinates(null);
        $employeeRep = $this->createEmployee('1');
        
        $shiftRep = new ShiftRepository($employeeRep->employeeId, $deviceRep->companyId, '1', $deviceRep->deviceId);
        $shiftRep->create();
        
        $routeRep = $this->createRoute($deviceRep->companyId);
        
        $shiftRep->update(array("routePoints" => $this->getShiftPoints(),"routeId" => $routeRep->routeId));
        $shiftRep = ShiftRepository::getRepository($shiftRep->shiftId,false,false);
        $shiftRep->points = RouteRepository::decodeFromXml($shiftRep->routePoints);
        //var_dump($shiftRep->routePoints);
        $shiftRep->processRouteCoordinates();
        //var_dump($shiftRep->busStopPoints);
        $shiftRep->endShift();
        
        $shiftRep->update(array("routePoints" => $this->getShiftPoints()));
        $shiftRep = ShiftRepository::getRepository($shiftRep->shiftId,false,false);
        $shiftRep->points = RouteRepository::decodeFromXml($shiftRep->routePoints);
        $shiftRep->processRouteCoordinates();
        //var_dump($shiftRep->busStopPoints);
        
        for($i=0;$i<10;$i++){
            if ($i == 9){
                // Update the time so that route refreshes now.
                $currentDate = new DateTime();
                $date = $currentDate->sub(DateInterval::createFromDateString('30 days'));
                $routeRep->update(array("lastTimetableRefresh" => $date));
            }
            
            $shiftRep->updateRouteTimeTables(); 
        }
        
        $routeRep = RouteRepository::getRepository($routeRep->routeId, $routeRep->companyId);
        //var_dump($routeRep->points[2]);
        
        $routeRep = RouteRepository::getRepository($routeRep->routeId, $routeRep->companyId);
        $busStops = $routeRep->getBusStopPoints()['points'];
        //var_dump($busStops);
        // Point 3 must now not be a bus stop.
        $this->assertEquals(1, count($busStops));
        
        // The remaining bus stop times should now have frequency of 1.
        // This is because of the reset after refresh.
        $timeDetails = reset($busStops)->getTimeDetails();
        $this->assertEquals(1, $timeDetails[0]->frequency);
        
        // The 12:00 time should also have been removed
        $this->assertEquals(4, count($timeDetails));
    }
    
    function getShiftPoints(){
        return '<?xml version="1.0" encoding="UTF-8"?>
        <xml>
           <point pointId="1" name="" busStop="false" latitude="35.5678" longitude="35.5678" />
           <point pointId="2" name="North Stop" busStop="true" latitude="36.5678" longitude="36.5678">
              <timetable>
                 <time frequency="1" direction="0">8:00</time>
                 <time frequency="1" direction="0">9:00</time>
                 <time frequency="1" direction="0">10:00</time>
                 <time frequency="1" direction="0">11:00</time>
              </timetable>
           </point>
           <point pointId="3" name="" busStop="false" latitude="37.5678" longitude="37.5678" />
        </xml>';
    }
    
    function createRoute($companyId)
    {
        $repository = new RouteRepository(null);
        $repository->setRouteName("claremont");
        $repository->companyId=$companyId;
        $repository->color='#fff';
        $repository->create();
        
        //$routeRep = RouteRepository::getRepository($repository->routeId, $repository->companyId);
        //echo "** last calc ".$routeRep->lastTimetableRefresh;
        
        $points = array (
            "point1"  => array(
                        "id" => "1",
                        "name" => "",
                        "busStop" => "false",
                        "latitude" => "35.5678",
                        "longitude" => "35.5678"),
            "point2" => array(
                        "id" => "2",
                        "name" => "North Stop",
                        "busStop" => "true",
                        "latitude" => "36.5678",
                        "longitude" => "36.5678",
                        "timeTable" => array('8:00','9:00','10:00','11:00','12:00')),
            "point3"   => array(
                        "id" => "3",
                        "name" => "South Stop",
                        "busStop" => "true",
                        "latitude" => "37.5678",
                        "longitude" => "37.5678"),
            "point4"   => array(
                        "id" => "4",
                        "name" => "West Stop",
                        "busStop" => "true",
                        "latitude" => "36.56788",
                        "longitude" => "36.56788"),
        );
        
        $xml = $repository->encodeToXml($points);
        
        return $repository;
    }
    
    function initialiseCoordinatesArray(){
        $structuredRouteCoordinates = array();
        $structuredRouteCoordinates["latlng1"] = array();
        $structuredRouteCoordinates["latlng2"] = array();
        $structuredRouteCoordinates["latlng3"] = array();
        $structuredRouteCoordinates["time"] = array();
        $structuredRouteCoordinates["time2"] = array();
        $structuredRouteCoordinates["repeat"] = array();
        $structuredRouteCoordinates["speed"] = array();
        $structuredRouteCoordinates["bearing"] = array();
        
        return $structuredRouteCoordinates;
    }
    
    function createRoutePoints(){
        $routePoints=array(
                    "-33.95845015526027,18.4590168176303",
                    "-33.95916206880829,18.45875932556487",
                    "-33.96042570068434,18.45949961525298",
                    "-33.96073715636207,18.459918039859303",
                    "-33.96042570068434,18.460422295154103",
                    "-33.96026552303463,18.46025063377715",
                    "-33.9584145594265,18.4610338388095",
                    "-33.95707080580857,18.461377161563405",
                    "-33.95495279344261,18.46146299225188",
                    "-33.954694712702086,18.460325735629567",
                    "-33.95488159883375,18.4600253282199",
                    "-33.95572703096754,18.460078972400197",
                    "-33.956999612971906,18.459606903613576",
                    "-33.95859253844646,18.458930986941823",
                    "-33.95923325983545,18.45875932556487",
                    "-33.96052358687735,18.459553259433278",
                    "-33.96074605507896,18.45998241287566",
                    "-33.96037230816794,18.460443752826222",
                    "-33.96021213041771,18.46026136261321",
                    "-33.958224044631194,18.461073033797447",
                    "0,0",
                    "-33.956855339246886,18.461405627715294",
                    "-33.954888610246456,18.461448543059532",
                    "-33.954666126728156,18.4602361845848",
                    "-33.95500430144607,18.460075252043907",
                    "-33.95584973236068,18.460053794371788",
                    "-33.95835037884169,18.45903455494613",
                    "-33.95905339433161,18.4587770628807"
            );
        
        return $routePoints;
    }
    
    function createLongRoutePoints(){
        $routePoints = array (
            "-33.954972813970905,18.461482991059256",
            "-33.95502176021842,18.461804856141043",
            "-33.955115202976415,18.462046254952384",
            "-33.95525314209827,18.46214281447692",
            "-33.9552753903228,18.462207187493277",
            "-33.95544892627439,18.462250102837515",
            "-33.95559131448316,18.462341297944022",
            "-33.95562691149812,18.462545145829154",
            "-33.95551122114505,18.462748993714285",
            "-33.95532878603791,18.46286164649291",
            "-33.95451449774165,18.46314059623046",
            "-33.95414517315881,18.463221062500907",
            "-33.95335757200751,18.463339079697562",
            "-33.95297489311044,18.46339808829589",
            "-33.952814701433255,18.463526834328604",
            "-33.95273905525864,18.463789690812064",
            "-33.95278355301655,18.463998903115225",
            "-33.95289034754062,18.46411692031188",
            "-33.95313953424221,18.46417592891021",
            "-33.95334867251782,18.464272488434744",
            "-33.95344211711313,18.464433420975638",
            "-33.95343321763229,18.464814294655753",
            "-33.953246328319516,18.465077151139212",
            "-33.95277465346684,18.46542047389312",
            "-33.95008355343583,18.46675423022066",
            "-33.94950506241443,18.46694734926973",
            "-33.94830356849791,18.46672204371248",
            "-33.94712719704055,18.466666077748187",
            "-33.946432983229364,18.466676806584246",
            "-33.94589006848279,18.466644620076067",
            "-33.945507356006736,18.46631202615822",
            "-33.94470632432415,18.465711211338885",
            "-33.944181199463856,18.465206956044085",
            "-33.94362046934172,18.464466666355975",
            "-33.94300633211276,18.46411261476601",
            "-33.94261470605904,18.463640545979388",
            "-33.942610314152596,18.463455480195762",
            "-33.942596963231956,18.463401836015464",
            "-33.942850630366074,18.46302632675338",
            "-33.94289068300236,18.46277419910598",
            "-33.942940568267964,18.46218429507985",
            "-33.9429650448554,18.46157006921544",
            "-33.94297172028712,18.461143597982073",
            "-33.942942793412556,18.46086733045354",
            "-33.94292276554646,18.460617894432744",
            "-33.94294279184999,18.460414046547612",
            "-33.94303402272868,18.46000098635932",
            "-33.94307852556085,18.459713989994725",
            "-33.943005095875286,18.459515506527623",
            "-33.94284043513801,18.459295565388402",
            "-33.9426105002651,18.459174271013012",
            "-33.94245696455166,18.459225232984295",
            "-33.94229230275414,18.459246690656414",
            "-33.94198077956323,18.45908575811552",
            "-33.94151267065043,18.45887654581236",
            "-33.94117889270267,18.458796079541912",
            "-33.94116999193948,18.45909112253355",
            "-33.94164173110607,18.459235961820355",
            "-33.94175298996252,18.458973105336895",
            "-33.94198440791814,18.45909112253355",
            "-33.94230483174124,18.459257419492474",
            "-33.94246059288628,18.459209139730206",
            "-33.94261190343998,18.459166224385967",
            "-33.94285240713127,18.45929421392475",
            "-33.94301261756001,18.459519519482",
            "-33.94309272266131,18.459718002949103",
            "-33.9430348689846,18.46001841035877",
            "-33.94294141296224,18.460426106129034",
            "-33.94293251238333,18.460624589596137",
            "-33.942945863251346,18.460876717243536",
            "-33.94296366440543,18.461155666981085",
            "-33.942977015268575,18.46157409158741",
            "-33.94294141296224,18.462190999660834",
            "-33.942905810641015,18.46276499239002",
            "-33.942807904180896,18.46304394212757",
            "-33.942545336300064,18.463360442791327",
            "-33.942402926263256,18.463451637897833",
            "-33.9423851249919,18.463617934856757",
            "-33.9420958538107,18.464143647823676",
            "-33.94190003891477,18.4647605558971",
            "-33.94171312427574,18.46533251706228",
            "-33.94157071284676,18.465890416537377",
            "-33.94123248474813,18.46654487553701",
            "-33.941050019031636,18.466947206889245",
            "-33.94148615594807,18.46732271615133",
            "-33.94294746358489,18.46851018159225",
            "-33.943214480500465,18.467700154469753",
            "-33.943321287032184,18.467437297986294",
            "-33.9439754741148,18.467828900502468",
            "-33.94383306647047,18.468370706723476",
            "-33.94367730783671,18.46875694482162",
            "-33.94271159794614,18.468091756985928",
            "-33.94159577306397,18.467165986199234",
            "-33.94087926215825,18.466575900215958",
            "-33.93950391173208,18.46543859337862",
            "-33.9385248029398,18.464558828821737",
            "-33.93670365358788,18.462626244702506",
            "-33.93700404528863,18.462277670790172",
            "-33.93734006489482,18.461896797110057",
            "-33.9374580052369,18.46171708910606",
            "-33.93753589027889,18.46167953817985",
            "-33.93738902128306,18.46142204611442",
            "-33.93761154996574,18.461167236258007",
            "-33.93751808798986,18.461046536852336",
            "-33.93737181106251,18.46093388407371",
            "-33.93730505233081,18.461067994524456",
            "-33.93732953053851,18.461215516020275",
            "-33.937327305247194,18.461354389324697",
            "-33.93738738809238,18.461424126759084",
            "-33.93754093294857,18.46168966545156",
            "-33.93700908798983,18.46228243364385",
            "-33.936599631867075,18.46273840917638",
            "-33.93714365296651,18.46338371696436",
            "-33.93763321671152,18.463941616439456",
            "-33.938020415134474,18.4643278545376",
            "-33.938805348028886,18.465016477204927",
            "-33.940176092668565,18.46618592033542",
            "-33.941057273997195,18.46694766769565",
            "-33.94150638497551,18.46733099637231",
            "-33.942974992507324,18.468532626010983",
            "-33.94354462739713,18.46887594876489",
            "-33.9443545704429,18.469272915699094",
            "-33.945098760990504,18.46963354658442",
            "-33.946282499691605,18.470298734420112",
            "-33.94727041938811,18.47081371855097",
            "-33.94767982418999,18.471028295272163",
            "-33.94711642466146,18.472524832822955",
            "-33.9469517718771,18.473029088117755",
            "-33.94783541047307,18.47348811038114",
            "-33.94813801220341,18.473632949667945",
            "-33.94824036254519,18.47353102572538",
            "-33.9482670626141,18.473627585249915",
            "-33.94834271276388,18.473675865012183",
            "-33.94907251075545,18.473563212233557",
            "-33.949099210563304,18.4737509668646",
            "-33.94910366053047,18.47397090800382",
            "-33.948698712565935,18.47403528102018",
            "-33.94816916232046,18.474067467528357",
            "-33.948062361871685,18.474180120306983",
            "-33.947964461342586,18.475086706954016",
            "-33.947978820766515,18.476145073706448",
            "-33.94830812204302,18.47605924301797",
            "-33.94830367203426,18.476295277411282",
            "-33.94901122050412,18.476198717886746",
            "-33.94900232055989,18.475968047911465",
            "-33.94864187203672,18.475994870001614",
            "-33.948027771036884,18.47608606510812",
            "-33.948014420965954,18.47507755451852",
            "-33.94813012151124,18.474187061125576",
            "-33.94823692187497,18.47413878136331",
            "-33.949149169520574,18.47405295067483",
            "-33.949171419340985,18.473908111388027",
            "-33.949135819625525,18.473441407019436",
            "-33.949153619485124,18.473178550535977",
            "-33.94916623197696,18.472761003865344",
            "-33.94926413112368,18.47258397807036",
            "-33.94965127664613,18.471999256505114",
            "-33.94972692556552,18.471752493275744",
            "-33.949788604965754,18.470961592551703",
            "-33.94986425376307,18.46999599730634",
            "-33.94997105195071,18.469593665954108",
            "-33.950300345518876,18.469100139495367",
            "-33.94988205347032,18.468917749282355",
            "-33.94935696054162,18.468665621634955",
            "-33.948775974730744,18.468436676014562",
            "-33.94844667526443,18.468334752071996",
            "-33.94853567524582,18.467975336064",
            "-33.948775974730744,18.468442040432592",
            "-33.94937227052181,18.468672710407873",
            "-33.950242721603665,18.46907185905434",
            "-33.949922327661476,18.46956002109505",
            "-33.94981997934242,18.469908708266985",
            "-33.94977103010284,18.470718735389482",
            "-33.949704281094384,18.471646779708635",
            "-33.94960638245398,18.471952551536333",
            "-33.9500914246146,18.4720008312986",
            "-33.952241847920476,18.472100932966214",
            "-33.95329199493791,18.47217603481863",
            "-33.95385266134397,18.472079475294095",
            "-33.95440442467659,18.472015102277737",
            "-33.95535778292798,18.471758291336073",
            "-33.95657697555673,18.471436426254286",
            "-33.957351198302476,18.471200391860975",
            "-33.957894039934075,18.47105018815614",
            "-33.95858841256046,18.470970178169864",
            "-33.95922023510495,18.47083070330109",
            "-33.960288093526955,18.470154786629337",
            "-33.96054170758464,18.470098460332565",
            "-33.960775299479074,18.47006359152283",
            "-33.96084949323892,18.47005963665606",
            "-33.96097963670889,18.47002342683436",
            "-33.96102635482904,18.4700126979983",
            "-33.961071960588235,18.470005992475762",
            "-33.96111756632299,18.470000628057733",
            "-33.96141344683753,18.469999286953225",
            "-33.96145460306818,18.470150831762567",
            "-33.96145190298643,18.470196715916586",
            "-33.96144450343924,18.470274111181197",
            "-33.96144450343924,18.47049941673845",
            "-33.96091058316168,18.47069253578752",
            "-33.96044784954381,18.470885654836593",
            "-33.960207583018715,18.471346994787154",
            "-33.96000291099915,18.471368452459274",
            "-33.95974484557697,18.470338484197555",
            "-33.95921981350852,18.47069253578752",
            "-33.95891516331335,18.47081618637685",
            "-33.95840347456784,18.470912745901387",
            "-33.957927378797315,18.470923474737447",
            "-33.957066468532666,18.471193935198926",
            "-33.956345638272964,18.471424605174207",
            "-33.95528218001073,18.471704877127195",
            "-33.95449013969829,18.471903360594297",
            "-33.95396952491546,18.471999920118833",
            "-33.95389832948389,18.4715439445863",
            "-33.9537840542778,18.4709823895746",
            "-33.954033238362285,18.470848279123857",
            "-33.95432691866792,18.470842914705827",
            "-33.95461614828127,18.470714168673112",
            "-33.95486310509611,18.470649795656755",
            "-33.95507223913557,18.470601515894487",
            "-33.955425471794506,18.470459358816697",
            "-33.95575474425643,18.47041107905443",
            "-33.95608401544413,18.47038425696428",
            "-33.955983899339444,18.4702796508127",
            "-33.95583483736531,18.470030205374314",
            "-33.95576141868505,18.46978612435396",
            "-33.95568069636734,18.469427115102576",
            "-33.95558725423037,18.46904087700443",
            "-33.95548491272453,18.468877262254523",
            "-33.95543596674344,18.468619770189093",
            "-33.955306927203765,18.467987671801666",
            "-33.95521348465632,18.467553153941253",
            "-33.955217934303775,18.467247382113555",
            "-33.95524908182935,18.466764584490875",
            "-33.9551778874684,18.46635688872061",
            "-33.9550354985678,18.466072574565032",
            "-33.9546572768935,18.465313929284775",
            "-33.95452823617291,18.465029615129197",
            "-33.95439474556638,18.464772123063767",
            "-33.95423455656211,18.4644180714738",
            "-33.95396757421818,18.46420349475261",
            "-33.953551868967594,18.464155214990342",
            "-33.95313136802298,18.46418203708049",
            "-33.952888855951414,18.464112299646104",
            "-33.95278206142548,18.463999646867478",
            "-33.95273533877825,18.463782387937272",
            "-33.95281543472924,18.463522213662827",
            "-33.95297785128731,18.463393467630112",
            "-33.953369429659524,18.463339823449815",
            "-33.95415703070106,18.463219124044144",
            "-33.95452190554883,18.463141339982712",
            "-33.95534268974004,18.46285888569696",
            "-33.95551177592253,18.46274355070932",
            "-33.95562524146187,18.462534338406158",
            "-33.955589644446206,18.462330490521026",
            "-33.955442806599194,18.46224872896221",
            "-33.95526927063512,18.462208495826985",
            "-33.95525147205469,18.462138758392598",
            "-33.95510685845068,18.46204756328609",
            "-33.9550200901703,18.461803482265736"
        );
        
        return $routePoints;
    }
}

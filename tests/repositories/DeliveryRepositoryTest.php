<?php

use App\Repositories\DeliveryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use App\Models\Delivery;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Description of DeliveryRepositoryTest
 *
 * @author Takunda
 */
class DeliveryRepositoryTest extends TestCase{
    
    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create($userId,$companyId)
    {
        $repository = new DeliveryRepository($userId,$companyId);
        $repository->address='15 Hermitage Flats, Rosebank, Capetown.';
        $repository->latitude='-18.9';
        $repository->longitude='33.8';
        $repository->phoneNumber='0623456789';
        $repository->create();
        
        return $repository;
    }
    
    function createUser($id){
        $repository = new UserRepository($id);
        $repository->userId=$id;
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='user';
        $repository->salt='salt';
        $repository->email='user@yahoo.com';
        $repository->create();
        
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        
        return $repository;
    }
    
    public function testCanCreateDelivery()
    {
        $this->createCompany('1');
        $companyRep = CompanyRepository::getRepository(null,'1');
        
        $user = $this->createUser('1');
        
        $this->create($user->userId,$companyRep->companyId);
        
        $deliveryNode = Delivery::query()->where('userId','=',$user->userId)->first();
        $this->assertEquals($companyRep->companyId, $deliveryNode->companyId);
    }
    
    public function testDeleteDelivery()
    {
        $company = $this->createCompany('1');
        $user = $this->createUser('1');
        
        $delivery = $this->create($user->userId,$company->companyId);
        
        DeliveryRepository::delete($company->companyId,$delivery->deliveryId);
        $deliveryRep = DeliveryRepository::getRepository($delivery->deliveryId,false);
        $this->assertEquals(null, $deliveryRep);
    }
    
    public function testGetDelivery()
    {
        $company = $this->createCompany('1');
        $user = $this->createUser('1');
        
        $delivery = $this->create($user->userId,$company->companyId);
        
        $deliveryRepository = DeliveryRepository::getRepository($delivery->deliveryId,false);
        $this->assertEquals($delivery->deliveryId, $deliveryRepository->deliveryId);
    }
    
    public function testEncodeToXml()
    {
        $company = $this->createCompany('1');
        $user = $this->createUser('1');
        
        $deliveryRep = $this->create($user->userId,$company->companyId);
        
        $items = array (
            "item1"  => array(
                        "id" => "1",
                        "name" => "Kelloggs Cornflakes",
                        "price" => "R50",
                        "description" => "Cornflakes 750g from P n Pay",
                        "url" => "http://"),
            "item2" => array(
                        "id" => "2",
                        "name" => "White Paddy Rice",
                        "price" => "R60",
                        "description" => "Rice 2kg from Pick n Pay",
                        "url" => "http://"),
            "item3"   => array(
                        "id" => "3",
                        "name" => "Iwisa Meali Meal",
                        "price" => "R40",
                        "description" => "Iwisa 2kg from Pick n Pay",
                        "url" => "http://"),
        );
        
        $expectedXml = '<?xml version="1.0"?> <xml><item id="1" price="R50" name="Kelloggs Cornflakes" '
                . 'description="Cornflakes 750g from P n Pay" url="http://"/><item id="2" price="R60" '
                . 'name="White Paddy Rice" description="Rice 2kg from Pick n Pay" url="http://"/><item id="3" '
                . 'price="R40" name="Iwisa Meali Meal" description="Iwisa 2kg from Pick n Pay" url="http://"/>'
                . '</xml> ';
        $xml = $deliveryRep->encodeToXml($items);
        $this->assertEquals($expectedXml, $xml);
    }
    
    public function testDecodeFromXml()
    {
        $company = $this->createCompany('1');
        $user = $this->createUser('1');
        
        $deliveryRep = $this->create($user->userId,$company->companyId);
        
        $XML = '<?xml version="1.0"?> <xml><item id="1" price="R50" name="Kelloggs Cornflakes" '
                . 'description="Cornflakes 750g from P n Pay" url="http://"/><item id="2" price="R60" '
                . 'name="White Paddy Rice" description="Rice 2kg from Pick n Pay" url="http://"/><item id="3" '
                . 'price="R40" name="Iwisa Meali Meal" description="Iwisa 2kg from Pick n Pay" url="http://"/>'
                . '</xml>';
        $items = $deliveryRep->decodeFromXml($XML);
        $this->assertEquals(3, sizeof($items));
    }
}

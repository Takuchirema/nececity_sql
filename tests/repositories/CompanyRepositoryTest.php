<?php

use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use App\Repositories\FollowerRepository;
use App\Models\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CompanyRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        
        return $repository;
    }
    
    function createUser($userId)
    {
        $repository = new UserRepository($userId);
        $repository->userId=$userId;
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='user';
        $repository->salt='salt';
        $repository->email='user@yahoo.com';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateCompany()
    {
        $this->create('1');
        
        $company = Company::query()->where('companyName','=','1')->first();
        $this->assertEquals('rosebank', $company->address);
    }
    
    public function testDeleteCompany()
    {
        $this->create('1');
        $companyRepository = CompanyRepository::getRepository(null,'1');
        
        CompanyRepository::delete($companyRepository->companyId);
        $company = CompanyRepository::getRepository(null,'1');
        $this->assertEquals(null, $company);
    }
    
    public function testGetCompany()
    {
        $this->create('1');
        
        $companyRepository = CompanyRepository::getRepository(null,'1');
        $this->assertEquals('rosebank', $companyRepository->address);
    }
    
    public function testGetCompanies()
    {
        $this->create('1');
        
        $companyRep = CompanyRepository::getRepository(null,'1');
        $this->assertEquals(1, $companyRep->companyName);
    }
    
    public function testUpdateProperty()
    {
        $repository = $this->create('1');
        
        $repository->update(array('address' => 'claremont'));
        
        $companyRepository = CompanyRepository::getRepository(null,'1');
        $this->assertEquals('claremont', $companyRepository->address);
    }
    
    public function testGetFollowedCompanies()
    {
        $this->create('1');
        $this->create('2');
        $this->create('3');
        
        $this->createUser('1');
        
        $companyRep = CompanyRepository::getRepository(null,'1');
        
        $repository = new FollowerRepository('1');
        $repository->companyId=$companyRep->companyId;
        $repository->create();
        //var_dump($companyRep);
        $companies = CompanyRepository::getFollowedCompanies('1')['companies'];
        $this->assertEquals(1, $companies[0]->companyName);
    }
    
    public function testGetUnfollowedCompanies()
    {
        $this->create('1');
        $this->create('2');
        
        $this->createUser('1');
        
        $companyRep = CompanyRepository::getRepository(null,'1');
        
        $repository = new FollowerRepository('1');
        $repository->companyId=$companyRep->companyId;
        $repository->create();
        
        $companies = CompanyRepository::getUnfollowedCompanies('1')['companies'];
        $this->assertNotEquals('1', $companies[0]->companyName);
    }
}


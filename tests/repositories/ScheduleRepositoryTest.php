<?php

use App\Repositories\DeviceRepository;
use App\Repositories\ShiftRepository;
use App\Repositories\ScheduleRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\RouteRepository;
use App\Models\Device;
use App\Models\Route;
use App\Http\Helpers\Constants;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Description of ScheduleRepositoryTest
 *
 * @author Takunda
 */
class ScheduleRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function createEmployee($code, $companyId){
        $repository = new EmployeeRepository(null,$companyId);
        $repository->setEmployeeCode($code);
        $repository->setPassword('password');
        $repository->lastSeen='2017-12-22_17:00';
        $repository->name='employee';
        $repository->privilege='admin';
        $repository->create();
        
        return $repository;
    }
    
    function createDevice($companyId){
        $repository = new DeviceRepository('1',$companyId);
        $repository->create();
        
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        $repository->update(array('discoverRoutes' => 'true'));
        
        return $repository;
    }
    
    function createRoute($companyId)
    {
        $repository = new RouteRepository(null);
        $repository->setRouteName("claremont");
        $repository->companyId=$companyId;
        $repository->color='#fff';
        $repository->create();
        
        $points = array (
            "point1"  => array(
                        "id" => "1",
                        "name" => "",
                        "busStop" => "false",
                        "latitude" => "35.5678",
                        "longitude" => "35.5678"),
            "point2" => array(
                        "id" => "2",
                        "name" => "North Stop",
                        "busStop" => "true",
                        "latitude" => "36.5678",
                        "longitude" => "36.5678",
                        "timeTable" => array('8:00','9:00','10:00','11:00')),
            "point3"   => array(
                        "id" => "3",
                        "name" => "",
                        "busStop" => "false",
                        "latitude" => "37.5678",
                        "longitude" => "37.5678"),
        );
        $repository->encodeToXml($points);
        return $repository;
    }
    
    public function createSchedule($st=null,$et=null){
        $this->createCompany("test");
        $companyRep = CompanyRepository::getRepository(null, "test");
        $companyId = $companyRep->companyId;
        
        $deviceRep = $this->createDevice($companyId);
        $employeeRep = $this->createEmployee('1',$companyId);
        $routeRep = $this->createRoute($companyId);
        
        $time = new DateTime();
        if (!isset($st)){
            $st = (int)$time->format('Hi');
        }

        $time->add(new DateInterval('PT5S'));
        if (!isset($et)){
            $et = (int)$time->format('Hi');
        }
        
        $scheduleRep = new ScheduleRepository($companyId, $employeeRep->employeeId, $deviceRep->deviceId);
        $scheduleRep->setDays("0123456");
        $scheduleRep->setStart($st);
        $scheduleRep->setEnd($et);
        $scheduleRep->setRouteId($routeRep->routeId);
        $scheduleRep->create();
        
        return $scheduleRep;
    }
    
    public function testCreateSchedule()
    {
        $scheduleRep = $this->createSchedule();
        
        $createdRep = ScheduleRepository::getRepository($scheduleRep->scheduleId);
        $this->assertEquals('1', $createdRep->getEmployee()->employeeCode);
    }
    
    public function testGetSchedule(){
        $scheduleRep = $this->createSchedule(600,1200);
        $companyId = $scheduleRep->companyId;
        $employeeId = $scheduleRep->employeeId;
        
        $createdReps = ScheduleRepository::getSchedules($companyId, null, null, null, null, null, null)["schedules"];
        $this->assertEquals(1, count($createdReps));
        
        $createdReps = ScheduleRepository::getSchedules(null, $employeeId, null, null, null, null, null)["schedules"];
        $this->assertEquals(1, count($createdReps));
        
        $createdReps = ScheduleRepository::getSchedules(null, null, null, null, "0", 600, 1300)["schedules"];
        $this->assertEquals(1, count($createdReps));
        
        $createdReps = ScheduleRepository::getSchedules(null, null, null, null, "0", 1200, 1600)["schedules"];
        $this->assertEquals(0, count($createdReps));
        
        $createdReps = ScheduleRepository::getSchedules(null, null, null, null, "7", null, null)["schedules"];
        $this->assertEquals(0, count($createdReps));
    }
    
    public function testCreateShiftFromSchedule(){
        $scheduleRep = $this->createSchedule();
        $deviceRep = DeviceRepository::getRepository($scheduleRep->deviceId);
        $employeeId = $scheduleRep->employeeId;
        $companyId = $scheduleRep->companyId;
        
        $routePoints = $this->createRoutePoints();
        $time = DateTime::createFromFormat('d/m/Y', '29/06/2019');
        Constants::deviceUpdateInterval;
        
        $count = count($routePoints);
        foreach ($routePoints as $point){
            $time->add(new DateInterval('PT10S'));
            
            $lat = explode(",", $point)[0];
            $lng = explode(",", $point)[1];

            $timeStr = $time->format('Y-m-d H:i:s');
            if ($point === $routePoints[0]){
                $deviceRep->setLatLng($lat, $lng, $timeStr, "0", "100");
            }else if ($point === $routePoints[$count-1]){
                $time->add(new DateInterval('PT2M'));
                $timeStr = $time->format('Y-m-d H:i:s');
                $deviceRep->setLatLng($lat, $lng, $timeStr, "0", "100");
            }else{
                $deviceRep->setLatLng($lat, $lng, $timeStr, "60", "100");
            }
        }
        $this->assertEquals($employeeId, $deviceRep->employeeId);
        
        $shiftReps = ShiftRepository::getShifts($companyId, $employeeId, null, null, null)["shifts"];
        $this->assertEquals(1, count($shiftReps));
        $this->assertEquals(true, isset($shiftReps[0]->end));
    }
    
    function createRoutePoints(){
        $routePoints=array(
                    "-33.95845015526027,18.4590168176303",
                    "-33.95916206880829,18.45875932556487",
                    "-33.96042570068434,18.45949961525298",
                    "-33.96073715636207,18.459918039859303",
                    "-33.96042570068434,18.460422295154103",
                    "-33.96026552303463,18.46025063377715",
                    "-33.9584145594265,18.4610338388095",
                    "-33.95707080580857,18.461377161563405",
                    "-33.95495279344261,18.46146299225188",
                    "-33.954694712702086,18.460325735629567",
                    "-33.95488159883375,18.4600253282199",
                    "-33.95572703096754,18.460078972400197",
                    "-33.956999612971906,18.459606903613576"
            );
        return $routePoints;
    }
}

<?php

use App\Repositories\CataloguePostRepository;
use App\Models\CataloguePost;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CataloguePostRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $repository = new CataloguePostRepository('1');
        $repository->title="test catalogue";
        $repository->post='Hi All';
        $repository->time='2017-12-22_17:00';
        $repository->companyId='1';
        $repository->duration='2017-12-22,2017-12-30';
        $repository->amenities='pool,braai,ac';
        $repository->price='R30';
        $repository->units='for 1 plate';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreatePost()
    {
        $repository = $this->create();
        
        $post = CataloguePost::query()
                    ->where('postId','=',$repository->postId)
                    ->where('companyId','=','1')
                    ->first();
        $this->assertEquals('Hi All', $post->post);
    }
    
    public function testGetPosts()
    {
        $this->create();
        
        $posts = CataloguePostRepository::getPosts('1','company')['posts'];
        $this->assertEquals(1, sizeof($posts));
    }
    
    public function testDeletePost()
    {
        $repository = $this->create();
        
        $repository->delete($repository->postId,'1','company');
        $posts = CataloguePostRepository::getPosts('1','company')['posts'];
        $this->assertEquals(0, sizeof($posts));
    }
    
    public function testLikePost()
    {
        $repository = $this->create();
        CataloguePostRepository::likePost($repository->postId, '1');
        
        $post = CataloguePostRepository::getNode($repository->postId,'1');
        $this->assertEquals('1', $post->likes);
    }
}


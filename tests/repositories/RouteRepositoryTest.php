<?php

use App\Repositories\RouteRepository;
use App\Repositories\CompanyRepository;
use App\Models\Route;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RouteRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $companyRep = $this->createCompany('test_route');
        
        $repository = new RouteRepository(null);
        $repository->setRouteName("claremont");
        $repository->companyId=$companyRep->companyId;
        $repository->color='#fff';
        $repository->create();
        
        $points = array (
            "point1"  => array(
                        "id" => "1",
                        "name" => "",
                        "busStop" => "false",
                        "latitude" => "35.5678",
                        "longitude" => "35.5678"),
            "point2" => array(
                        "id" => "2",
                        "name" => "North Stop",
                        "busStop" => "true",
                        "latitude" => "36.5678",
                        "longitude" => "36.5678",
                        "timeTable" => array('8:00','9:00','10:00','11:00')),
            "point3"   => array(
                        "id" => "3",
                        "name" => "",
                        "busStop" => "false",
                        "latitude" => "37.5678",
                        "longitude" => "37.5678"),
        );
        $repository->encodeToXml($points);
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        $repository->update(array('approved' => 'true'));
        $repository->update(array('discoverRoutes' => 'true'));
        
        return $repository;
    }

    public function testCanCreateRoute()
    {
        $this->create();
        
        $route = Route::query()->where('routeName','=','claremont')->first();
        $this->assertEquals('#fff', $route->color);
    }
    
    public function testDeleteRoute()
    {
        $repository = $this->create();
        
        $repository->delete('1',$repository->routeId);
        $route = RouteRepository::getRepository($repository->routeId,'1');
        $this->assertEquals(null, $route);
    }
    
    public function testGetRoute()
    {
        $repository = $this->create();
        
        $routeRepository = RouteRepository::getRepository($repository->routeId,'1');
        $this->assertEquals('claremont', $routeRepository->routeName);
    }
    
    public function testGetRouteNames()
    {
        $routeRep = $this->create();
        
        $routeDetails = RouteRepository::getRoutesDetails($routeRep->companyId)['routes'];
        $this->assertEquals('claremont', $routeDetails[0]->routeName);
    }
    
    public function testEncodeToXml()
    {
        $repository = $this->create();
        $expectedXml = '<?xml version="1.0"?> <xml><point pointId="1" name="" busStop="false" latitude="35.5678" longitude="35.5678" distance="0" bearing="0"/><point pointId="2" name="North Stop" busStop="true" latitude="36.5678" longitude="36.5678" distance="142976.69707162" bearing="0"><timetable><time frequency="1" bearing="0">8:00</time><time frequency="1" bearing="0">9:00</time><time frequency="1" bearing="0">10:00</time><time frequency="1" bearing="0">11:00</time></timetable></point><point pointId="3" name="" busStop="false" latitude="37.5678" longitude="37.5678" distance="285229.34920033" bearing="0"/></xml> ';
        $routeRepository = RouteRepository::getRepository($repository->routeId,'1');
        $this->assertEquals($expectedXml, $routeRepository->routePoints);
    }
    
    public function testDecodeFromXml()
    {
        $repository = $this->create();
        
        $XML = '<?xml version="1.0"?> <xml><point pointId="1" name="" busStop="false" latitude="35.5678" longitude="35.5678"/><point pointId="2" name="North Stop" busStop="true" latitude="35.5678" longitude="35.5678"><timetable><time>8:00</time><time>9:00</time><time>10:00</time><time>11:00</time></timetable></point><point pointId="3" name="" busStop="false" latitude="35.5678" longitude="35.5678"/></xml> ';
        $points = $repository->decodeFromXml($XML);
        $this->assertEquals(3, sizeof($points));
    }
    
    public function testReversePoints()
    {
        $repository = $this->create();
        
        $expectedXml = '<?xml version="1.0"?> <xml><point pointId="1" name="1" busStop="false" latitude="37.5678" longitude="37.5678" distance="0" bearing="0" speed="0" stopFrequency="0"/><point pointId="2" name="North Stop" busStop="true" latitude="36.5678" longitude="36.5678" distance="142252.65212872" bearing="0" speed="0" stopFrequency="0"><timetable><time frequency="1" bearing="0">8:00</time><time frequency="1" bearing="0">9:00</time><time frequency="1" bearing="0">10:00</time><time frequency="1" bearing="0">11:00</time></timetable></point><point pointId="3" name="3" busStop="false" latitude="35.5678" longitude="35.5678" distance="285229.34920033" bearing="0" speed="0" stopFrequency="0"/></xml> ';
        RouteRepository::reversePoints($repository->companyId, $repository->routeId);
        
        $newRepository = RouteRepository::getRepository($repository->routeId,$repository->companyId);
        $this->assertEquals($expectedXml, $newRepository->routePoints);
    }
    
    public function testStructuredCoordinates(){
        $repository = $this->create();
        
        $routeRepository = RouteRepository::getRepository($repository->routeId,'1');
        $structuredCoordinates = $routeRepository->getStructuredRouteCoordinates();
        
        $this->assertEquals('35.5678,35.5678', $structuredCoordinates["latlng1"][0]);
        $this->assertEquals('35.56,35.56', $structuredCoordinates["latlng2"][0]);
        $this->assertEquals('35.567,35.567', $structuredCoordinates["latlng3"][0]);
    }
}


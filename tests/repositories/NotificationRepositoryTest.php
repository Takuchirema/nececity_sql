<?php

use App\Repositories\NotificationRepository;
use App\Models\Notification;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $repository = new NotificationRepository('1','2');
        $repository->notification='Please set location';
        $repository->time='2017-12-22_17:00';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateNotification()
    {
        $repository = $this->create();
        
        $notification = Notification::query()
                    ->where('notificationId','=',$repository->notificationId)
                    ->first();
        $this->assertEquals('Please set location', $notification->notification);
    }
    
    public function testGetNotifications()
    {
        $this->create();
        
        $notifications = NotificationRepository::getNotifications('2')['notifications'];
        $this->assertEquals(1, sizeof($notifications));
    }
    
    public function testDeleteNotification()
    {
        $repository = $this->create();
        
        $repository->delete($repository->notificationId);
        $notifications = NotificationRepository::getNotifications('2')['notifications'];
        $this->assertEquals(0, sizeof($notifications));
    }
    
    public function testSeenNotification()
    {
        $repository = $this->create();
        NotificationRepository::seenNotification($repository->notificationId);
        
        $notification = NotificationRepository::getNode($repository->notificationId);
        $this->assertEquals(true, $notification->seen);
    }
}

<?php

use App\Repositories\SettingsRepository;
use App\Models\Settings;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SettingsRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function testCanCreateSettings()
    {
        $repository = new SettingsRepository('1');
        $repository->postsRefreshInterval='5 sec';
        $repository->companiesRefreshInterval='5 sec';
        $repository->messagesRefreshInterval='5 sec';
        $repository->usersRefreshInterval='5 sec';
        $repository->visibilityRegion='capetown';
        $repository->create();
        
        $settings = Settings::query()->where('userId','=','1')->first();
        $this->assertEquals('5 sec', $settings->postsRefreshInterval);
    }
    
    public function testGetSettings()
    {
        $repository = new SettingsRepository('1');
        $repository->postsRefreshInterval='5 sec';
        $repository->companiesRefreshInterval='5 sec';
        $repository->messagesRefreshInterval='5 sec';
        $repository->usersRefreshInterval='5 sec';
        $repository->visibilityRegion='capetown';
        $repository->create();
        
        $settingsRepository = SettingsRepository::getSettings('1',null);
        $this->assertEquals('5 sec', $settingsRepository->postsRefreshInterval);
    }
    
    public function testEditSettings()
    {
        $repository = new SettingsRepository('1');
        $repository->postsRefreshInterval='5 sec';
        $repository->companiesRefreshInterval='5 sec';
        $repository->messagesRefreshInterval='5 sec';
        $repository->usersRefreshInterval='5 sec';
        $repository->visibilityRegion='capetown';
        $repository->create();
        
        $repository->update(array('postsRefreshInterval' => '30 sec'));
        
        $settingsRepository = SettingsRepository::getSettings('1',null);
        $this->assertEquals('30 sec', $settingsRepository->postsRefreshInterval);
    }
}


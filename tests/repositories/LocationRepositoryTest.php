<?php

use App\Repositories\LocationRepository;
use App\Models\Location;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LocationRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function testCanCreateLocation()
    {
        $repository = new LocationRepository('1');
        $repository->latitude='35';
        $repository->longitude='35';
        $repository->create();
        
        $location = Location::query()
                    ->where('companyId','=','1')
                    ->first();
        
        $this->assertEquals('35', $location->longitude);
    }
    
    public function testGetLocation()
    {
        $repository = new LocationRepository('1');
        $repository->latitude='35';
        $repository->longitude='35';
        $repository->create();
        
        $location = LocationRepository::getLocation('1');
        $this->assertEquals('35', $location->longitude);
    }
}


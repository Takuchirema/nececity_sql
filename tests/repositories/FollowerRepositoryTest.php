<?php

use App\Repositories\FollowerRepository;
use App\Repositories\UserRepository;
use App\Models\Follower;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FollowerRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $userRepository = new UserRepository('1');
        $userRepository->userId='1';
        $userRepository->setPassword('password');
        $userRepository->lastSeen='2017-12-22_17:00';
        $userRepository->name='user';
        $userRepository->salt='salt';
        $userRepository->email='user@yahoo.com';
        $userRepository->create();
        
        $repository = new FollowerRepository('1');
        $repository->companyId='1';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateFollower()
    {
        $this->create();
        
        $follower = Follower::query()->where('userId','=','1')->first();
        $this->assertEquals('1', $follower->companyId);
    }
    
    public function testGetFollowers()
    {
        $this->create();
        
        $followers = FollowerRepository::getFollowers('1')['followers'];
        $this->assertEquals(1, sizeof($followers));
    }
}


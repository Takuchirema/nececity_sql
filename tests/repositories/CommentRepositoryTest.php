<?php

use App\Repositories\CommentRepository;
use App\Models\Comment;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create(){
        $repository = new CommentRepository('taku','Hi All!');
        $repository->time='2017-12-22_17:00';
        $repository->companyId='1';
        $repository->postId='1';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateComment()
    {
        $this->create();
        $comment = Comment::query()->where('postId','=','1')->first();
        $this->assertEquals('Hi All!', $comment->comment);
    }
    
    public function testGetComments()
    {
        $this->create();
        
        $comments = CommentRepository::getComments('1',null,null)['comments'];
        $this->assertEquals(1, sizeof($comments));
    }
}


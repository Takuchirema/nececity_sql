<?php

use App\Repositories\UserRepository;
use App\Repositories\RouteRepository;
use App\Repositories\CompanyRepository;
use App\Models\Device;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Description of ClusterRepositoryTest
 *
 * @author Takunda
 */
class ClusterRepositoryTest extends TestCase{
    
    use DatabaseTransactions;
    
    private $users = array();
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function createUser($userId)
    {
        $repository = new UserRepository($userId);
        $repository->userId=$userId;
        $repository->setPassword('password');
        
        $currentDate = date('Y-m-d_H:i:s');
        $repository->lastSeen=$currentDate;
        
        $repository->name='user';
        $repository->salt='salt';
        $repository->email='user@yahoo.com';
        $repository->create();
        
        return $repository;
    }
    
    function createRoute($companyId,$routeName)
    {
        $repository = new RouteRepository(null);
        $repository->routeName=$routeName;
        $repository->companyId=$companyId;
        $repository->color='#000000';
        $repository->create();
        
        $routePoints = file_get_contents(__DIR__ . '/routepoints.xml');
        $repository->update(array('routePoints' => $routePoints));
         
        return $repository;
    }
    
    function createCompany($companyName){
        $repository = new CompanyRepository($companyName);
        $repository->address='rosebank';
        $repository->phoneNumber='062';
        $repository->sector='transport';
        $repository->about='us';
        $repository->type='public';
        $repository->setPassword('company');
        $repository->email='co@yahoo.com';
        $repository->create();
        
        return $repository;
    }
    
    /*
     * This test aims to test entire process of clustering.
     * Creates 100 users and 1 company.
     * Creates 1 route for that company.
     * Puts users in 2 clusters along the route.
     * Moves the clusters along the route to simulate people on a bus.
     */
    public function testClusteringProcess()
    {
        for ($i = 1; $i <= 50; $i++) {
            $rep = $this->createUser($i);
            $this->users[$i] = $rep;
        }
        fwrite(STDOUT, "1\n");
        /**********/
        $users = UserRepository::getUsers()['users'];
        $this->assertGreaterThanOrEqual(50, sizeof($users));
        /*********/
        fwrite(STDOUT, "2\n");
        $this->createCompany("TestCompany");
        fwrite(STDOUT, "3\n");
        $companyRep = CompanyRepository::getRepository(null,"TestCompany");
        fwrite(STDOUT, "Company Rep ".$companyRep->companyId."\n");
        
        $rep = $this->createRoute($companyRep->companyId,"TestRoute");
        
        $routeRep = RouteRepository::getRepository($rep->routeId, $companyRep->companyId);
        $points = $routeRep->points;
        
        // Move user clusters
        // Move 25 one direction and 25 the other direction
        $pointsCount = count($points);
        fwrite(STDOUT, "Points Count ".$pointsCount."\n");
        for ($i = 0; $i < $pointsCount; $i++) {
            $currentDate = date('Y-m-d_H:i:s');
            
            $latitude1 = $points[$i]->latitude;
            $longitude1 = $points[$i]->longitude;
            
            for ($j = 1; $j <= 25; $j++) {
                $routeRep->setUserClusterLocation($j, "180", $currentDate, $latitude1, $longitude1);
            }
            
            $latitude2 = $points[$pointsCount - $i - 1]->latitude;
            $longitude2 = $points[$pointsCount - $i - 1]->longitude;
            for ($k = 26; $k <= 50; $k++) {
                $routeRep->setUserClusterLocation($k, "0", $currentDate, $latitude2, $longitude2);
            }
            
            $routeRep->findClusters();
            fwrite(STDOUT, "Found clusters: ".count($routeRep->locationClusters)."\n");
            
            $this->displayClusters($routeRep->locationClusters);
            sleep(2);
        }
    }
    
    public function displayClusters($clusters){
        
        foreach ($clusters as $cluster){
            fwrite(STDOUT, "**** Cluster ".$cluster->id." ****\n");
            fwrite(STDOUT, "Cluster id: ".$cluster->id."\n");
            fwrite(STDOUT, "Cluster lat: ".$cluster->latitude."\n");
            fwrite(STDOUT, "Cluster lng: ".$cluster->longitude."\n");
            fwrite(STDOUT, "Cluster cluster size: ".$cluster->clusterSize."\n");
        }
    }
}

<?php

use App\Repositories\PostRepository;
use App\Models\Post;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $repository = new PostRepository('1');
        $repository->post='Hi All';
        $repository->time='2017-12-22_17:00';
        $repository->companyId='1';
        $repository->type='company';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreatePost()
    {
        $repository = $this->create();
        
        $post = Post::query()
                    ->where('postId','=',$repository->postId)
                    ->where('companyId','=','1')
                    ->first();
        $this->assertEquals('Hi All', $post->post);
    }
    
    public function testGetPosts()
    {
        $this->create();
        
        $posts = PostRepository::getPosts('1','company')['posts'];
        $this->assertEquals(1, sizeof($posts));
    }
    
    public function testDeletePost()
    {
        $repository = $this->create();
        
        $repository->delete($repository->postId,'1','company');
        $posts = PostRepository::getPosts('1','company')['posts'];
        $this->assertEquals(0, sizeof($posts));
    }
    
    public function testLikePost()
    {
        $repository = $this->create();
        PostRepository::likePost($repository->postId, '1');
        
        $post = PostRepository::getNode($repository->postId,'1');
        $this->assertEquals('1', $post->likes);
    }
}


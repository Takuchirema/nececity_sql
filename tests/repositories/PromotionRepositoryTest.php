<?php

use App\Repositories\PromotionRepository;
use App\Models\Promotion;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PromotionRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create()
    {
        $repository = new PromotionRepository('1');
        $repository->promotion='Hi All';
        $repository->time='2017-12-22_17:00';
        $repository->companyId='1';
        $repository->weekly='true';
        $repository->onceoff='false';
        $repository->days='mon,tue';
        $repository->create();
        
        return $repository;
    }

    public function testCanCreatePromotion()
    {
        $repository = $this->create();
        
        $promotion = Promotion::query()
                    ->where('promotionId','=',$repository->promotionId)
                    ->where('companyId','=','1')
                    ->first();
        $this->assertEquals('Hi All', $promotion->promotion);
    }
    
    public function testGetPromotions()
    {
        $this->create();
        
        $promotions = PromotionRepository::getPromotions('1')['promotions'];
        $this->assertEquals(1, sizeof($promotions));
    }
    
    public function testDeletePromotion()
    {
        $repository = $this->create();
        
        PromotionRepository::delete($repository->promotionId,'1');
        $promotions = PromotionRepository::getPromotions('1')['promotions'];
        $this->assertEquals(0, sizeof($promotions));
    }
    
    public function testLikePromotion()
    {
        $repository = $this->create();
        PromotionRepository::likePromotion($repository->promotionId, '1');
        
        $promotion = PromotionRepository::getNode($repository->promotionId,'1');
        $this->assertEquals('1', $promotion->likes);
    }
}


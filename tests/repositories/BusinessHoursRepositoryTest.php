<?php

use App\Repositories\BusinessHoursRepository;
use App\Models\BusinessHours;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BusinessHoursRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function create(){
        $repository = new BusinessHoursRepository('1');
        $repository->mon='8,5';
        $repository->tue='8,5';
        $repository->wed='8,5';
        $repository->thu='8,5';
        $repository->fri='8,5';
        $repository->sat='8,5';
        $repository->sun='8,5';
        $repository->create();
    }
    
    public function testCanCreateBusinessHours()
    {
        $this->create();
        
        $businessHours = BusinessHours::query()->where('companyId','=','1')->first();
        //var_dump($businessHours);
        $this->assertEquals('8,5', $businessHours->mon);
    }
    
    public function testGetBusinessHours()
    {
        $this->create();
        
        $businessHoursRepository = BusinessHoursRepository::getBusinessHours('1');
        $this->assertEquals('8,5', $businessHoursRepository->mon);
    }
    
    public function testGetBusinessHoursNode()
    {
        $this->create();
        
        $businessHours = BusinessHoursRepository::getBusinessHoursNode('1');
        $this->assertEquals('8,5', $businessHours->mon);
    }
}


<?php

use App\Repositories\RelationshipRepository;
use App\Models\Relationship;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RelationshipRepositoryTest extends TestCase{

    use DatabaseTransactions;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    function create(){
        $repository = new RelationshipRepository;
        $repository->entityId='1';
        $repository->relatedEntityId='2';
        $repository->type = RelationshipRepository::$userSeenPost;
        $repository->create();
        
        return $repository;
    }

    public function testCanCreateRelationship()
    {
        $repository = $this->create();
        $relationshipId = $repository->relationshipId;
        
        $relationship = Relationship::query()->where('relationshipId','=',$relationshipId)->first();
        $this->assertEquals($relationshipId, $relationship->relationshipId);
    }
    
    public function testGetRelationship()
    {
        $repository = $this->create();
        
        $relationshipRepository = RelationshipRepository::getRelationship($repository->relationshipId);
        $this->assertEquals($repository->type, $relationshipRepository->type);
    }
    
    public function testInsertProperty()
    {
        $repository = $this->create();
        $repository->insertProperty('accepted');
        $repository->updateProperty('accepted', 'false');
        
        $this->assertEquals('false', $repository->getProperty('accepted'));
    }
    
    public function testUpdateProperty()
    {
        $repository = $this->create();
        $repository->insertProperty('accepted');
        $repository->updateProperty('accepted', 'true');
        
        $this->assertEquals('true', $repository->getProperty('accepted'));
    }
}
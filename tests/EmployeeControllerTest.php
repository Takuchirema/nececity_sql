<?php

/**
 * Description of EmployeeControllerTest
 *
 * @author Takunda Chirema
 */
class EmployeeControllerTest extends TestCase{
    
    public function testNewEmployee()
    {
        $this->post('/employees', ['id' => 'tsitsi','company' => 'frigo','email' => 'taku@example.com',
            'phoneNumber' => '077982323','password' => 'taku','salt' => ''])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testEditEmployeeProfile()
    {
        $this->patch('/employees', ['id' => 'tsitsi','company' => 'frigo','email' => 'tsitsi@example.com'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetEmployeeDetails()
    {
        $this->get('/employees/tsitsi/company/frigo')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetEmployeeFriends()
    {
        $this->get('/employees/tsitsi/company/frigo/friends')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testGetEmployeeMessages()
    {
        $this->get('/employees/tsitsi/company/frigo/messages')
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testSetEmployeeAlert()
    {
        $this->post('/employees/tsitsi/alert/on',['company' => 'frigo'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testCancelEmployeeAlert()
    {
        $this->delete('/employees/tsitsi/alert/off',['company' => 'frigo'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testEditEmployeeSettings()
    {
        $this->patch('/employees/tsitsi/settings',['company' => 'frigo','employeesRefreshInterval' => '5 min'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
    public function testPostEmployeeComment()
    {
        $this->post('/employees/tsitsi/comment/1',['company' => 'frigo','comment' => 'nice picture!',
            'time' => '2016-12-17_10:40'])
             ->seeJson([
                 'success' => 1,
             ]);
    }
    
}

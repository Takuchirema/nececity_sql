<?php namespace App\Auth;

//use Illuminate\Contracts\Auth\UserRepository as UserContract;
//use App\UserRepository;
use App\Repositories\UserRepository;
use  Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;


class WebUserProvider implements UserProvider {

    protected $model;

    public function __construct(UserRepository $model)
    {
        $this->model = $model;
    }

    public function retrieveById($identifier)
    {
        $user = UserRepository::getUser($identifier);
        if ($user)
         {
            return $user; 
         }
         else
         {
            return null;
         }
    }

    public function retrieveByToken($identifier, $token)
    {
        $user = UserRepository::getUser($identifier);
        if ($user && $user->token === $token)
         {
            return $user;
         }
         else
         {
            null;
         }
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->update(array("token" => $token));

    }
    public function retrieveByCredentials(array $credentials)
    {   
        $user = UserRepository::getUser($credentials["username"]);
         if ($user)
         {
            return $user;
         }
         else
         {
            return null;
         }
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    { 
        if (isset($user->fbId)){
            return true;
        }
        
        // remember bcrypt is suppose to happen clientside using javascript
        return password_verify($credentials['password'], $user->getPassword());
    }
}
<?php namespace App\Auth;

use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class CompanyProvider implements UserProvider {

    protected $model;

    public function __construct(CompanyRepository $model)
    {
        $this->model = $model;
    }

    public function retrieveById($identifier)
    {
        $companyRep = CompanyRepository::getRepository($identifier);
        if ($companyRep)
         {
            return $companyRep; 
         }
         else
         {
            return null;
         }
    }

    public function retrieveByToken($identifier, $token)
    {
        $companyRep = CompanyRepository::getRepository($identifier);
        
        if ($companyRep && $companyRep->token === $token)
        {
            return $companyRep;
        }
        else
        {
            return null;
        }  
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->update(array("token" => $token));
    }
    
    public function retrieveByCredentials(array $credentials)
    {
        $companyname = $credentials["companyname"];
        $companyRep = CompanyRepository::getRepository(null,$companyname);
        
        if ($companyRep)
        {
           return $companyRep; 
        }
        else
        {
           return null;
        }
    }

    public function validateCredentials(Authenticatable $company, array $credentials)
    {
        // remember bcrypt is suppose to happen clientside using javascript
        return $credentials['password'] == $company->getPassword();
    }
}
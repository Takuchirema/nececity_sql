<?php namespace App\Auth;

use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class EmployeeProvider implements UserProvider {

    protected $model;

    public function __construct(CompanyRepository $model)
    {
        $this->model = $model;
    }

    public function retrieveById($identifier)
    {
        $id = explode("_", $identifier);
        $companyRep = CompanyRepository::getRepository($id[1]);
        
        $employeeRep = EmployeeRepository::getRepository($id[0], $id[1]);
        if ($employeeRep->privilege !== "superuser"){
            $companyRep->setMenu($employeeRep->menu);
        }
        
        if ($companyRep)
         {
            return $companyRep; 
         }
         else
         {
            return null;
         }
    }

    public function retrieveByToken($identifier, $token)
    {
        $id = explode("_", $identifier);
        $companyRep = CompanyRepository::getRepository($id[1]);
        
        $employeeRep = EmployeeRepository::getRepository($id[0], $id[1]);
        if ($employeeRep->privilege !== "superuser"){
            $companyRep->setMenu($employeeRep->menu);
        }
        
        if ($companyRep && $companyRep->token === $token)
         {
            return $companyRep;
         }
         else
         {
            null;
         }
    }

    public function updateRememberToken(Authenticatable $company, $token)
    {
        $company->update(array("token" => $token));
    }
    
    public function retrieveByCredentials(array $credentials)
    {   
        $username = $credentials["username"];
        $companyname = $credentials["companyname"];
        //dd($username." ".$companyname);
        $employeeRep = EmployeeRepository::getRepository(null,null,$companyname,$username);
        if ($employeeRep)
        {
           return $employeeRep; 
        }
        else
        {
            return null;
        }
    }

    public function validateCredentials(Authenticatable $employee, array $credentials)
    {
        return password_verify($credentials['password'], $employee->getPassword());
    }
}
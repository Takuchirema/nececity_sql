<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'routes';
    protected $primaryKey = 'routeId';
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeLog extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'timelogs';
    protected $primaryKey = 'timeLogId';
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'relationships';
    protected $primaryKey = 'relationshipId';
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'alerts';
    protected $primaryKey = 'alertId';
    /**
    * One to One relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user() 
    {
        return $this->belongsTo('App\Models\User','userId');
    }
    
    /**
    * One to One relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function employee() 
    {
        return $this->belongsTo('App\Models\Employee','employeeId','companyId');                        ;
    }
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'comments';
    protected $primaryKey = 'commentId';
    
    /**
    * One to One relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function post() 
    {
        return $this->belongsTo('App\Models\Post','postId');
    }
}

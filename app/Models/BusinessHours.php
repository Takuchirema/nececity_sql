<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessHours extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'businesshours';
    protected $primaryKey = 'businessHoursId';
    
    /**
    * One to One relation
    *
    * @return Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function company() 
    {
        return $this->belongsTo('App\Models\Company','companyId');
    }
}

<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Delivery
 *
 * @author Takunda
 */
class Delivery extends Model {
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'deliveries';
    protected $primaryKey = 'deliveryId';
}

<?php namespace App\Models;

/**
 * Description of CataloguePost
 *
 * @author Takunda
 */

use Illuminate\Database\Eloquent\Model;

class CataloguePost extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'catalogue_posts';
    protected $primaryKey = 'postId';
}

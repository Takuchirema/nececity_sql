<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'followers';
    protected $primaryKey = 'userId';
    public $incrementing = false;
}

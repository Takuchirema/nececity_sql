<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppVariable
 *
 * @author Takunda
 */
class AppVariable extends Model {
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'app_variables';
    protected $primaryKey = 'id';
}

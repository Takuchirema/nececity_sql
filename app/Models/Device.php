<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'devices';
    protected $primaryKey = 'deviceId';
    protected $casts = [
        'deviceId' => 'string'
    ];
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ShiftRepository;
use App\Repositories\ScheduleRepository;
use App\Repositories\CompanyRepository;
use Response;

class ScheduleController extends BaseController
{
    public function createSchedule(Request $request){
        $employeeId = $request->input('employeeId');
        $companyId = $this->getCompanyId();
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        $startinput = $request->input('start');
        $endinput = $request->input('end');
        $dayinputs = $request->input('days');
        
        $days = implode("", $dayinputs);
        
        $start = preg_replace("/[^0-9]/", "", $startinput);
        $end = preg_replace("/[^0-9]/", "", $endinput);
        
        $scheduleRep = new ScheduleRepository($companyId, $employeeId, $deviceId);
        $scheduleRep->setDays($days);
        $scheduleRep->setStart((int)$start);
        $scheduleRep->setEnd((int)$end);
        $scheduleRep->setRouteId($routeId);
        $result = $scheduleRep->create();
        
        if ($result["success"] == 0){
            return back()->withErrors([$result["message"]]);
        }
        
        return back();
    }
    
    public function updateSchedule(Request $request, $scheduleId){
        if (!isset($scheduleId)){
            $scheduleId = $request->input('scheduleId');
        }
        $companyId = $this->getCompanyId();
        $employeeId = $request->input('employeeId');
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        $startinput = $request->input('start');
        $endinput = $request->input('end');
        $dayinputs = $request->input('days');
        
        $days = implode("", $dayinputs);
        
        $start = preg_replace("/[^0-9]/", "", $startinput);
        $end = preg_replace("/[^0-9]/", "", $endinput);
        
        $scheduleProperties = array();  
        if (isset($employeeId)){
            $scheduleProperties["employeeId"] = $employeeId;
        }
        
        if (isset($routeId)){
            $scheduleProperties["routeId"] = $routeId;
        }
        
        if (isset($deviceId)){
            $scheduleProperties["deviceId"] = $deviceId;
        }
        
        if (isset($start)){
            $scheduleProperties["start"] = $start;
        }
        
        if (isset($end)){
            $scheduleProperties["end"] = $end;
        }
        
        if (isset($days)){
            $scheduleProperties["days"] = $days;
        }
        
        $scheduleRep = new ScheduleRepository($companyId, $employeeId, $deviceId);
        $scheduleRep->setScheduleId($scheduleId);
        $scheduleRep->update($scheduleProperties);
        
        return back();
    }
    
    public function deleteSchedule(Request $request, $scheduleId){
        if (!isset($scheduleId)){
            $scheduleId = $request->input('scheduleId');
        }
        ScheduleRepository::delete($scheduleId);
        return back();
    }
    
    public function schedules(){
        if (!$this->authorizedMenu('schedules')){
            return back();
        }
        
        $parameters = array("employeeId"=>"","routeId"=>"","deviceId"=>"","start"=>"","end"=>"","days"=>"","status"=>"");
        
        $id = $this->getCompanyId();
        $company = CompanyRepository::getRepository($id);
        return view('schedules')->with(['data'=>array()])->with(['company'=>$company])->with(['parameters'=>$parameters]);
    }
    
    public function getSchedules(Request $request){
        $companyId = $this->getCompanyId();
        $employeeId = $request->input('employeeId');
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        $startinput = $request->input('start');
        $endinput = $request->input('end');
        $dayinputs = $request->input('days');
        $status = $request->input('status');

        //dd($dayinputs);
        $days = implode("", $dayinputs);
        
        $start = preg_replace("/[^0-9]/", "", $startinput);
        $end = preg_replace("/[^0-9]/", "", $endinput);
        
        $parameters = array();
        $parameters["employeeId"] = $employeeId;
        $parameters["routeId"] = $routeId;
        $parameters["deviceId"] = $deviceId;
        $parameters["start"] = $startinput;
        $parameters["end"] = $endinput;
        $parameters["days"] = $days;
        $parameters["status"] = $status;
        
        $result = ScheduleRepository::getSchedules($companyId, $employeeId, $deviceId, $routeId, $days, $start, $end);
        $data = $result["schedules"];
        //dd($companyId." - ".$employeeId." - ". $deviceId." - ". $routeId." - ". $days." - ". $start." - ". $end);
        //dd($result["schedules"]);
        
        $company = CompanyRepository::getRepository($companyId);
        return view('schedules')->with(['data'=>$data])->with(['company'=>$company])->with(['parameters'=>$parameters]);
    }
}

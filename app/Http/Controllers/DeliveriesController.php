<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\DeliveryRepository;
use App\Repositories\FollowerRepository;

class DeliveriesController extends BaseController
{
    public function createDelivery(Request $request){
        $companyId = $this->getCompanyId();
        $userId = $request->input('userId');
        $address = $request->input('address');
        $phoneNumber = $request->input('phoneNumber');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        
        $repository = new DeliveryRepository($userId,$companyId);
        $repository->address=$address;
        $repository->phoneNumber=$phoneNumber;
        
        if (isset($latitude)){
            $repository->latitude=$latitude;
        }
        
        if (isset($longitude)){
            $repository->longitude=$longitude;
        }
        
        $repository->create();
        
        return redirect('deliveries');
    }
    
    public function editDelivery(Request $request){
        $companyId = $this->getCompanyId();
        
        $deliveryId = $request->input('deliveryId');
        $userId = $request->input('userId');
        $address = $request->input('address');
        $phoneNumber = $request->input('phoneNumber');
        $status = $request->input('status');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $items = $request->input('items');
        
        $deliveryRep = new DeliveryRepository($userId,$companyId);
        $deliveryRep->setDeliveryId($deliveryId);
        $deliveryRep->setStatus($status);
        
        $properties = array();
        if (isset($latitude)){
            $properties['latitude'] = $latitude;
        }
        
        if (isset($longitude)){
            $properties['longitude'] = $longitude;
        }
        
        if (isset($userId)){
            $properties['userId'] = $userId;
        }
        
        if (isset($address)){
            $properties['address'] = $address;
        }
        
        if (isset($phoneNumber)){
            $properties['phoneNumber'] = $phoneNumber;
        }
        
        if (isset($items)){
            $deliveryRep->encodeToXml($items);
        }
        
        $deliveryRep->update($properties);
        
        return redirect('deliveries');
    }
    
    public function deleteDelivery(Request $request){
        $companyId = $this->getCompanyId();
        
        $deliveryId = $request->input('deliveryId');
        DeliveryRepository::delete($companyId, $deliveryId);
        
        return redirect('deliveries');
    }
    
    public function getDeliveries(Request $request){
        $companyId = $this->getCompanyId();
        
        $deliveries = DeliveryRepository::getDeliveries($companyId)['deliveries'];
        $followers = FollowerRepository::getFollowers($companyId)['followers'];
        
        $result['deliveries'] = $deliveries;
        $result['followers'] = $followers;
        //dd($result);
        return view('deliveries')->with(['data'=>$result]);
    }
}

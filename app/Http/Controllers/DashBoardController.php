<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\AppVariableRepository;

class DashBoardController extends BaseController
{
    public function dashboard(){
        return view('dashboard');
    }
    
    public function appVariables(){
        $data = array();
        $result = AppVariableRepository::getVariables();
        $data = $result["variables"];
        
        return view('app_variables')->with(['data'=>$data]);
    }
    
    public function newVariable(Request $request){
        $variable = new AppVariableRepository();
        $variable->code = $request->input('code');
        $variable->value = $request->input('value');
        $variable->create();
        return redirect('app_variables');
    }
    
    public function editVariable(Request $request)
    {
        $variable = new AppVariableRepository();
        $variable->code = $request->input('code');
        $variable->value = $request->input('value');
        $variable->update(array('value' => $request->input('value')));
        
        return redirect('app_variables');
    }
    
    public function deleteVariable(Request $request)
    {
        AppVariableRepository::delete($request->input('code'));
        return redirect('app_variables');
    }
}
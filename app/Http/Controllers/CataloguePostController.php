<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\CataloguePostRepository;
use App\Repositories\CommentRepository;

class CataloguePostController extends BaseController
{
    public function newCatalogue(Request $request)
    {
        date_default_timezone_set('Africa/Johannesburg');
        $id = $this->getCompanyId();
        $files = $request->file('images');
        $pdfFile = $request->file('file');
        
        $amenities = "";
        if ($request->input('amenities')!==null){
            foreach($request->input('amenities') as $index => $value) {
                $amenities = $amenities.",".$value;
            }
        }
        
        $postClass = new CataloguePostRepository($id);
        $postClass->setPost($request->input('post'));
        $postClass->setTime(date("Y-m-d_H:i"));
        $postClass->setTitle($request->input('title'));
        $postClass->setDuration($request->input('from').",".$request->input('to'));
        $postClass->setPrice($request->input('price'));
        $postClass->setUnits($request->input('units'));
        $postClass->setAmenities($amenities);
        
        $result = $postClass->create();
        
        CataloguePostRepository::uploadPictures($files,$id,$result["postId"]);
        
        if (isset($pdfFile)){
            CataloguePostRepository::uploadFile($pdfFile, $id, $result["postId"]);
        }
        
        return redirect('catalogue');
    }
    
    public function editCatalogue(Request $request){
        $companyId = $this->getCompanyId();
        $postId = $request->input('postId');
        $files = $request->file('images');
        $pdfFile = $request->file('file');
        
        //var_dump($request);
        $postRep = new CataloguePostRepository($companyId);
        $postRep->postId = $postId;
        
        $amenities = "";
        $properties = array();
        if ($request->input('amenities') != null){
            foreach($request->input('amenities') as $index => $value) {
                $amenities = $amenities.",".$value;
            }
            $properties['amenities'] = $amenities;
        }
        
        if ($request->input('title') != null){
            $properties['title'] = $request->input('title');
        }
        
        if ($request->input('from') != null){
            $properties['duration'] = $request->input('from').",".$request->input('to');
        }
        
        if ($request->input('price') != null){
            $properties['price'] = $request->input('price');
        }
        
        if ($request->input('units') != null){
            $properties['units'] = $request->input('units');
        }
        
        if ($postRep != null){
            $properties['post'] = $request->input('post');
            CataloguePostRepository::uploadPictures($files,$companyId,$postId);    
        }
        $postRep->update($properties);
        
        if (isset($pdfFile)){
            CataloguePostRepository::uploadFile($pdfFile, $companyId, $postId);
        }
        
        return redirect('catalogue');
    }
    
    public function deleteCatalogue(Request $request){
        
        $companyId = $this->getCompanyId();
        $postId = $request->input('postId');
        
        $result = CataloguePostRepository::delete($postId, $companyId,null);
        
        return redirect('catalogue');
    }
    
    public function catalogue()
    {
        if (!$this->authorizedMenu('catalogue')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = CataloguePostRepository::getPosts($id, PostType::Company);
        $data = $result["posts"];

        return view('catalogue_posts')->with(['data'=>$data]);
    }
    
    public function newCatalogueComment(Request $request,$postID){
        
        $result = array();
        
        $comment = $request->input('comment');
        $time =  date("Y-m-d_H:i");
        $companyID = $this->getCompanyId();
        $userName='admin';
        
        $postNode = CataloguePostRepository::getNode($postID,$companyID);
        
        if (!is_null($postNode)){
            $commentClass = new CommentRepository($userName, $comment);
            $commentClass->setCataloguePostId($postID);
            $commentClass->setCompanyId($companyID);
            $commentClass->setTime($time);
            
            $result = $commentClass->create();
        }else{
            $result["success"] = 0;
            $result["message"] = "No such post exists for ".$companyID;
        }
        
        return redirect('catalogue');
    }
}

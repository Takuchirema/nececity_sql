<?php namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;

class BaseController extends Controller 
{

    public function getCompanyId(){
        if (Auth::guard('web_company')->check()){
            //dd(Auth::guard('web_company')->id());
            return Auth::guard('web_company')->id();
        }elseif (Auth::guard('web_employee')->check()){
            //dd(Auth::guard('web_employee')->id());
            return Auth::guard('web_employee')->id();
        }
    }
    
    public function getCompanyName(){
        return Auth::user()->companyName;
    }
    
    public function getEmployeeId(){
        if (Auth::guard('web_employee')->check()){
            $ids = explode("_", Auth::id());
            return $ids[0];
        }
    }
    
    public function authorizedMenu($authorize){
        if (Auth::guard('web_company')->check()){
            //dd(Auth::guard('web_company')->id());
            $menu = Auth::guard('web_company')->user()->menu;
        }elseif (Auth::guard('web_employee')->check()){
            //dd(Auth::guard('web_employee')->id());
            $menu = Auth::guard('web_employee')->user()->menu;
        }
        if ($menu === 'all' || strpos( $menu , $authorize ) !== false){
            return true;
        }
        return false;
    }
}

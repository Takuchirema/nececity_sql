<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\Statistic;

class HomeController extends BaseController
{
    public function requiresAuth($request){
        foreach ($this->noAuthRequestPaths as $path){
            if ($request->is($path)){
                return false;
            }
        }
        return true;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function passwords(){
        return view('forgot_password');
    }
    
    public function privacy()
    {
        return view('privacy');
    }
    
    public function terms()
    {
        return view('terms');
    }
    
    public function tutorial()
    {
        return view('tutorial');
    }
    
    public function faq()
    {
        return view('faq');
    }
    
    public function statistics()
    {
        $statistics = new Statistic();
        $data = array(
        "Registered Users" => $statistics->totalRegisteredUsers,
        "Companies" => $statistics->totalCompany,
        "Total Online" => $statistics->totalOnlineUser,
        );

        return view('statistics')->with(['data'=>$data]);
    }
    
    public function upload(){
        $path = public_path('apk/');
        
        $apks = scandir($path);
        
        return view('upload')->with(['data'=>$apks]);    
    }
    
    public function errorLogs(){
        $path = public_path('logs/error_log.txt');
        
        if (!is_file($path)){
            if (!file_exists(public_path('logs'))){
                mkdir(public_path('logs'), 0777, true);
            }
            file_put_contents($path, '');
        }
        
        $logs = file_get_contents($path);
 
        return view('error_logs')->with(['data'=>$logs]);    
    }
    
    public function clearLogs(){
        $path = public_path('logs/error_log.txt');
        
        file_put_contents($path, "");
        
        return redirect('error/logs');
    }
    
    public function apkupload(Request $request){
        $file = Input::file('apk');
        $path = public_path('apk/');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        move_uploaded_file($file,$path."/".$file->getClientOriginalName());

        return redirect('upload');
    }
    
    public function deleteApk(Request $request){
        $file = public_path('apk/').$request->input("fileName");
        unlink($file);
        return redirect('upload');
    }
    
    public function apkdownload(Request $request){
        
        $user = Auth::user();
        if($user->sector==='Transportation')
        {
            return view('download');
        }
    }
    
    public function phpInfo(){
        return view('php_info');
    }
    
    public function cacheInfo(){
        return view('cache');
    }
}

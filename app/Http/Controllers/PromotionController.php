<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\CommentRepository;
use App\Repositories\PromotionRepository;

class PromotionController extends BaseController
{
    public function newPromotion(Request $request)
    {
        date_default_timezone_set('Africa/Johannesburg');
        $id = $this->getCompanyId();
        $files = $request->images;
        
        $days = "";
        if ($request->input('days')!==null){
            foreach($request->input('days') as $index => $value) {
                $days = $days.",".$value;
            }
        }
        
        $promotionClass = new PromotionRepository($id);
        $promotionClass->setPromotion($request->input('promotion'));
        $promotionClass->setTime(date("Y-m-d_H:i"));
        $promotionClass->setTitle($request->input('title'));
        $promotionClass->setDuration($request->input('from').",".$request->input('to'));
        $promotionClass->setPrice($request->input('price'));
        $promotionClass->setUnits($request->input('units'));
        $promotionClass->setDays($days);
        
        $result = $promotionClass->create();
        
        PromotionRepository::uploadPictures($files,$id,$result["promotionId"]);
        
        return redirect('promotions');
    }
    
    public function editPromotion(Request $request){
        $companyId = $this->getCompanyId();
        $promotionId = $request->input('promotionId');
        $files = $request->images;
        
        $promotionRep = new PromotionRepository($companyId);
        $promotionRep->promotionId = $promotionId;
        
        $days = "";
        $properties = array();
        if ($request->input('days') != null){
            foreach($request->input('days') as $index => $value) {
                $days = $days.",".$value;
            }
            $properties['days'] = $days;
        }
        
        if ($request->input('title') != null){
            $properties['title'] = $request->input('title');
        }
        
        if ($request->input('from') != null){
            $properties['duration'] = $request->input('from').",".$request->input('to');
        }
        
        if ($request->input('price') != null){
            $properties['price'] = $request->input('price');
        }
        
        if ($request->input('units') != null){
            $properties['units'] = $request->input('units');
        }
        
        if ($promotionRep != null){
            $properties['promotion'] = $request->input('promotion');
            PromotionRepository::uploadPictures($files,$companyId,$promotionId);
        }
        $promotionRep->update($properties);
        
        return redirect('promotions');
    }
    
    public function deletePromotion(Request $request){
        
        $companyId = $this->getCompanyId();
        $promotionId = $request->input('promotionId');
        
        $result = PromotionRepository::delete($promotionId, $companyId);
        
        return redirect('promotions');
    }
    
    
    
    public function promotions()
    {
        if (!$this->authorizedMenu('promotions')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = PromotionRepository::getPromotions($id);
        $data = $result["promotions"];

        return view('promotions')->with(['data'=>$data]);
    }
    
    public function newPromotionComment(Request $request,$promotionID){
        
        $result = array();
        
        $comment = $request->input('comment');
        $time =  date("Y-m-d_H:i");
        $companyID = $this->getCompanyId();
        $userName='admin';
        
        $promotionNode = PromotionRepository::getNode($promotionID,$companyID);
        
        if (!is_null($promotionNode)){
            $commentClass = new CommentRepository($userName, $comment);
            $commentClass->setPromotionId($promotionID);
            $commentClass->setCompanyId($companyID);
            $commentClass->setTime($time);
            
            $result = $commentClass->create();
        }else{
            $result["success"] = 0;
            $result["message"] = "No such promotion exists for ".$companyID;
        }
        
        return redirect('promotions');
    }
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\GpsServiceRepository;
use App\Repositories\DeviceRepository;

class GpsServiceController extends BaseController
{   
    public function recordInfo(Request $request){
        $deviceId = $request->input('id');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $time = $request->input('time');
        $speed = $request->input('speed');
        $bearing = $request->input('bearing');
        $info = $request->input('info');
        
        GpsServiceRepository::recordInfo($request);
        if (isset($info) && $info == "location"){
            $deviceRep = DeviceRepository::getOrCreateRepository($deviceId);
            $deviceRep->setLatLng($latitude, $longitude, $time, $speed, $bearing);
        }
    }
    
    public function getServiceDetails(){
        $service = GpsServiceRepository::getRepository();
        
        $result['service'] = $service;
        return view('gps_service')->with(['data'=>$result]);
    }
    
    public function restartService(){
        $service = GpsServiceRepository::getRepository();
        $service->restartServer();
        return back();
    }
    
    public function startService(Request $request){
        $service = GpsServiceRepository::getRepository();
        $service->startServer();
        return back();
    }
    
    public function stopService(Request $request){
        $service = GpsServiceRepository::getRepository();
        $service->stopServer();
        return back();
    }
    
    public function clearCache(Request $request){
        GpsServiceRepository::clearCache();
        return redirect('gps/service');
    }
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\CompanyRepository;
use App\Repositories\DeviceRepository;

class DeviceController extends BaseController
{
    public function recordLocation(Request $request){
        
    }
    
    public function devices()
    {
        if (!$this->authorizedMenu('devices')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = DeviceRepository::getDevices($id);
        $data = $result["devices"];
        
        $company = CompanyRepository::getRepository($id);
        
        return view('devices')->with(['data'=>$data])->with(['company'=>$company]);
    }
    
    public function manageDevices()
    {
        if (!$this->authorizedMenu('devices')){
            return back();
        }
        
        $devices = DeviceRepository::getDevices(null)["devices"];
        $companies = CompanyRepository::getCompanies()["companies"];
        
        $data['devices'] = $devices;
        $data['companies'] = $companies;
        
        return view('manage_devices')->with(['data'=>$data]);
    }
    
    public function editDevice(Request $request)
    {
        //dd($request);
        $companyId = $request->input('companyId');
        $deviceId = $request->input('deviceId');
        $phoneNumber = $request->input('phoneNumber');
        $busId = $request->input('busId');
        $employeeId = $request->input('currentEmployee');
        
        $deviceRep = new DeviceRepository($deviceId,null);
        $properties = array();
        
        if (isset($phoneNumber)){
            $properties["phoneNumber"] = $phoneNumber;
        }
        
        if (isset($busId)){
            $properties["busId"] = $busId;
        }
        
        if (isset($employeeId)){
            $properties["employeeId"] = $employeeId;
        }
        
        if(isset($companyId)){
            $properties["companyId"] = $companyId;
        }
        $deviceRep->update($properties);
        
        return back();
    }
    
    public function deleteDevice(Request $request)
    {
        $id = $this->getCompanyId();
        $deviceId =  $request->input('deviceId') ;
        
        DeviceRepository::delete($deviceId,$id);
        
        return back();
    }
    
    public function logoutDevice(Request $request)
    {
        $id = $this->getCompanyId();
        $deviceId =  $request->input('deviceId') ;
        
        DeviceRepository::logoutDevice($deviceId,$id);
        
        return back();
    }
    
    public function loginDevice(Request $request)
    {
        $id = $this->getCompanyId();
        $deviceId =  $request->input('deviceId') ;
        
        DeviceRepository::loginDevice($deviceId,$id);
        
        return back();
    }
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\UserRepository;
use App\Repositories\RouteRepository;


class RouteController extends BaseController
{
    public function routes(Request $request)
    {
        if (!$this->authorizedMenu('routes')){
            return back();
        }
        
        $companyID = $this->getCompanyId();
     
        $routeID = $request->input('mapname') ;
        $result = array();
        
        $result['success'] = 1;
        $result['route'] = RouteRepository::getRepository($routeID, $companyID);
        
        $result['routeDetails'] = RouteRepository::getRoutesDetails($companyID)['routes'];
        $result['location'] = $this->getCompanyLocation();
        $result['companyID'] =  $companyID ;
     
        return view('routes')->with(['data'=>$result]);
    }
    
    public function newRoute(Request $request){
        $companyId = $this->getCompanyId();
        $routeName = $request->input('routeName') ;
        $routeColor = $request->input('routeColor') ;
        
        $file = Input::file('routePoints');
        
        if (isset($file)){
            $routePoints = file_get_contents($file);
        }
        
        $routeRep = new RouteRepository(null);
        $routeRep->setRouteName($routeName);
        $routeRep->setCompanyId($companyId);
        
        if (isset($routeColor)){
            $routeRep->setColor('#'.$routeColor);
        }
        
        if (isset($routePoints)){
            $routeRep->setRoutePoints($routePoints);
        } 
        
        $routeRep->create();
        
        $result = array();
        $result['routes'] = RouteRepository::getRoutes($companyId, false)['routes'];
     
        //return view('edit_routes')->with(['data'=>$result]);
        return redirect('edit_routes');
    }
    
    public function saveRoute(Request $request){
        $companyId = $this->getCompanyId();
        $routeId = $request->input('routeId') ;
        $routeColor = $request->input('routeColor') ;
        
        $file = Input::file('routePoints');
        
        if (isset($file)){
            $routePoints = file_get_contents($file);
        }
        
        $routeRep = new RouteRepository($routeId);
        $routeRep->setCompanyId($companyId);
        
        $properties = array();
        if (isset($routeColor)){
            $properties['color'] = '#'.$routeColor;
        }
        
        if (isset($routePoints)){
            $properties['routePoints'] = $routePoints;
        } 
        
        $points = array();
        foreach($request->input() as $name => $value){
            if (strpos($name, 'point') !== false){
                array_push($points,$value);
            }
        }
        
        if (count($points) > 0){
            $pointsXML = $routeRep->encodeToXml($points);
            $properties['routePoints'] = $pointsXML;
        }
     
        $routeRep->update($properties);
        
        if (count($points) > 0){
            return $this->editRoute($routeId);
        }else{
            return redirect('edit_routes');
        }
    }
    
    public function reverseRoute(Request $request){
        $companyId = $this->getCompanyId();
        $routeId = $request->input('routeId') ;
        
        RouteRepository::reversePoints($companyId, $routeId);
        return redirect('edit_routes');   
    }
    
    public function editRoute($routeId){
        $companyId = $this->getCompanyId();
        
        $result = array();
        $result['route'] = RouteRepository::getRepository($routeId,$companyId);
        $result['location'] = $this->getCompanyLocation();
        $result['companyID'] =  $companyId;
     
        return view('edit_route')->with(['data'=>$result]);
    }
    
    public function editRoutes(Request $request){
        $companyID = $this->getCompanyId();
     
        $routeID = $request->input('mapname') ;
        $result = array();
        
        $result['success'] = 1;
        $result['route'] = RouteRepository::getRepository($routeID, $companyID);
        
        $result['routes'] = RouteRepository::getRoutes($companyID, false)['routes'];
        
        $result['location'] = $this->getCompanyLocation();
        $result['companyID'] =  $companyID ;
     
        return view('edit_routes')->with(['data'=>$result]);
    }
    
    public function deleteRoute(Request $request)
    {
        $id = $this->getCompanyId();
        $mapname =  $request->input('routeId') ;

        RouteRepository::delete($id,$mapname);
        //return redirect('routes');
        return redirect('edit_routes');
    }
    
    public function downloadRoute(Request $request, $companyId, $routeId){
        $routeRep = RouteRepository::getRepository($routeId, $companyId);
        $companyRep = CompanyRepository::getRepository($companyId);
        $filename = $companyRep->companyName.'_'.$routeRep->routeName.'.xml';
        
        Storage::put($filename, $routeRep->routePoints);
        $filename = Storage::disk()->path($filename);
        return Response::download($filename)->deleteFileAfterSend(true);
    }
    
    public function downloadRoutes(Request $request, $companyId){
        $routes = RouteRepository::getRoutes($companyId, false)['routes'];
        $companyRep = CompanyRepository::getRepository($companyId);
        $filename = $companyRep->companyName.'_Routes.xml';
        
        foreach ($routes as $route){
            Storage::append($filename, $route->routeName);
            Storage::append($filename, $route->routePoints);
        }
        
        $filename = Storage::disk()->path($filename);
        return Response::download($filename)->deleteFileAfterSend(true);
    }
    
    public function companyRoutes(Request $request,$companyID=null)
    {
        $loggedIn = Auth::guard('web_students')->check(); 
        //$loggedIn = true;
        if(!$loggedIn)
        {
            return view('auth_user.loginuser');
        }
        
        $userID = Auth::guard('web_students')->getUser()->userId;
        $companies = CompanyRepository::getFollowedCompanies($userID)["companies"];
        
        if (!isset($companyID)){
            $companyID = $companies[0]->companyId;
        }
     
        $routeID = $request->input('mapname') ;
        $result = array();

        $result['success'] = 1;
        $result['route'] = RouteRepository::getRepository($routeID, $companyID);
        
        $result['routeDetails'] = RouteRepository::getRoutesDetails($companyID)["routes"];
        
        $company = CompanyRepository::getRepository($companyID);
        $location = null;
        if (isset($company)){
            $location = $company->location;
        }
        
        $result['location'] = $location;
        $result['companyID'] =  $companyID ;
        $result['companies'] = $companies;
        $result['token'] = "sdfsf";
        
        return view('companies')->with(['data'=>$result]);
    }
    
     public function saveCompanyRoute(Request $request){
        
        $companyID = $this->getCompanyId();
        $routeID = $request->input('routeID');
        $routeColor = $request->input('routeColor');
        
        //echo print_r($request->all());
        
        $route = new RouteRepository($routeID);
        $route->setCompanyId($companyID);
        $route->setColor($routeColor);
        
        $route->create();

        $points = array();
        
        foreach($request->input() as $name => $value){
            if (strpos($name, 'point') !== false){
                array_push($points,$value);
            }
        }
        $route->encodeToXml($points);
        
        //return redirect('routes?mapname='.$routeID.'&_token=jx0Qz0coboPMM4fyGeT61GOU7GQAbljux6S5bY88');
        return redirect('edit_routes');
    }
    
    public function company_routes_fb(Request $request)
    {
        //store fb details
        $fbId = $request->input('fbID');
        $fbEmail = $request->input('fbEmail');
        $fbName = $request->input('fbName');
        
        $user = UserRepository::getUser($fbId);
        
        if (!$user){
            //create new user
            $user = new UserRepository($fbId);
            $user->setName($fbName);
            $user->setFbId($fbId);
            $user->setEmail($fbEmail);
            $user->setFbEmail($fbEmail);
            $user->setFbName($fbName);
            $user->setStatus($request->input('offline'));
            $user->create();
            
            $this->followDefaultCompanies($user->userId);
            
            $user->update(array("status" => "online"));
            array_push($result["users"], $user);
        }else{
            $user->update(array("status" => "online"));
        }
        
        $userID = $user->userId;
        $companies = CompanyRepository::getFollowedCompanies($userID)["companies"];
        
        $companyID = $companies[0]->companyId;
     
        $routeID = $request->input('mapname') ;
        $result = array();

        $result['success'] = 1;
        $result['route'] = RouteRepository::getRepository($routeID, $companyID);
        
        $result['routeDetails'] = RouteRepository::getRoutesDetails($companyID)["routes"];
        
        $company = CompanyRepository::getRepository($companyID);
        $location = null;
        if (isset($company)){
            $location = $company->location;
        }
        
        $result['location'] = $location;
        $result['companyID'] =  $companyID ;
        $result['companies'] = $companies;
        $result['token'] = "sdfsf";
        
        return view('companies')->with(['data'=>$result]);
    }
    
    public function track(Request $request)
    {
        if (!$this->authorizedMenu('track')){
            return back();
        }
        
        $companyId = $this->getCompanyId();
        
        $result = array();
        $result['success'] = 1;
        
        $result['routeDetails'] = RouteRepository::getRoutesDetails($companyId)["routes"];
        
        $company = CompanyRepository::getRepository($companyId);
        $location = null;
        if (isset($company)){
            $location = $company->location;
        }
        
        $result['location'] = $location;
        $result['companyID'] =  $companyId ;
        $result['token'] = "sdfsf";
        
        return view('track')->with(['data'=>$result]);
    }
    
    public function updateEmployeeRoute(Request $request)
    {
        $routeName =  $request->input('routeName');
        $employeeId =  $request->input('employeeID');
        
        $company = $this->getCompanyId();
        
        $employeeRep = new EmployeeRepository($employeeId,$company);
        $result = $employeeRep->update(array('routeId' => $routeName));
        
        $employee = EmployeeRepository::getRepository($employeeId,$company);
        return $employee;
        //return redirect('routes');
    }
    
    public function getCompanyLocation(){
        $companyID = $this->getCompanyId();
        
        $company = CompanyRepository::getRepository($companyID);
        
        if (isset($company)){
            return $company->location;
        }
        return null;
    }
}

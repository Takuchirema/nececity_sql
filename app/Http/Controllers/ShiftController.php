<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\BaseRepository;
use App\Repositories\CommentRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\ScheduleRepository;
use App\Repositories\TimeLogRepository;
use App\Repositories\RouteRepository;
use App\Repositories\ShiftRepository;

class ShiftController extends BaseController
{
    public function startShift(Request $request){
        $employeeId = $request->input('id');
        $companyId = $this->getCompanyId();
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        
        $employeeRep = new EmployeeRepository($employeeId,$companyId);
        
        $properties = array();
        //dd($request->input('deviceId'));        
        if (isset($routeId)){
            $properties["routeId"] = $routeId;
        }else if (isset($employeeRep->routeId)){
            $routeId = $employeeRep->routeId;
        }
        
        if (isset($deviceId)){
            $properties["deviceId"] = $deviceId;
            $properties["status"] = "online";
        }else{
            $properties["deviceId"] = "";
            $properties["status"] = "offline";
        }
        $employeeRep->update($properties);
        
        //Create the shift
        $shiftRep = new ShiftRepository($employeeId, $companyId, $routeId, $deviceId);
        $shiftRep->create();
        
        return redirect('dispatcher');
    }
    
    public function updateShift(Request $request, $shiftId){
        $employeeId = $request->input('id');
        $companyId = $this->getCompanyId();
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        
        $employeeRep = EmployeeRepository::getRepository($employeeId,$companyId);
        
        $employeeProperties = array();
        $shiftProperties = array();
        //dd($request->input('deviceId'));        
        if (isset($routeId)){
            $employeeProperties["routeId"] = $routeId;
            $shiftProperties["routeId"] = $routeId;
        }
        
        if (isset($deviceId)){
            $employeeProperties["deviceId"] = $deviceId;
            $shiftProperties["deviceId"] = $deviceId;
            $employeeProperties["status"] = "online";
        }else{
            $employeeProperties["deviceId"] = "";
            $employeeProperties["status"] = "offline";
        }
        
        if (isset($employeeRep)){
            $employeeRep->update($employeeProperties);
        }
        
        if (isset($shiftId)){
            $shiftRep = ShiftRepository::getRepository($shiftId);
        }else{
            $shiftRep = ShiftRepository::getRepository($employeeRep->shiftId);
        }
        
        if(!isset($shiftRep)){
            $shiftRep = new ShiftRepository($employeeId, $companyId, $routeId, $deviceId);
            $shiftRep->create();
        }else{
            $shiftRep->update($shiftProperties);
        }
        
        if ($shiftRep->isComplete() && isset($routeId)){
            //dd($routeId." 00");
            $shiftRep->routeId = $routeId;
            $shiftRep->updateRouteTimeTables();
        }
        
        return back();
    }
    
    public function createRoute(Request $request, $shiftId){
        $shiftRep = ShiftRepository::getRepository($shiftId);
        
        // Haven't figured out creating routes from shifts yet.
        if (isset($shiftRep)){
            //$shiftRep->createUnknownRoute();
        }
        return back();
    }
    
    public function stopShift(Request $request){
        $employeeId = $request->input('id');
        $companyId = $this->getCompanyId();
        
        $employeeRep = EmployeeRepository::getRepository($employeeId, $companyId);
        
        // check if shift was scheduled and end by schedule instead
        $scheduleRep = ScheduleRepository::getRepository($employeeRep->shiftId);
        if (isset($scheduleRep)){
            ScheduleRepository::endShift($scheduleRep->deviceId);
        }else{
            $properties = array();
            $properties["deviceId"] = "";
            $properties["shiftId"] = "";
            $properties["status"] = "offline";

            $employeeRep->update($properties);

            //end the shift
            $shiftRep = ShiftRepository::getRepository($employeeRep->shiftId);
            if (isset($shiftRep)){
                $shiftRep->endShift();
            }
        }
        
        return back();
    }
    
    public function deleteShift(Request $request, $shiftId){
        ShiftRepository::delete($shiftId);
        return back();
    }
    
    public function dispatcher(Request $request){
        if (!$this->authorizedMenu('dispatch')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = EmployeeRepository::getEmployees($id);
        $data = $result["employees"];
 
        $company = CompanyRepository::getRepository($id,null,false);
        
        return view('dispatcher')->with(['data'=>$data])->with(['company'=>$company]);
    }
    
    public function viewShift(Request $request, $shiftId){
        if (!$this->authorizedMenu('shifts')){
            return back();
        }
        
        $id = $this->getCompanyId();
        $shift = ShiftRepository::getRepository($shiftId);
        
        $company = CompanyRepository::getRepository($id);
        
        return view('view_shift')->with(['shift'=>$shift])->with(['company'=>$company]);
    }
    
    public function shifts(){
        if (!$this->authorizedMenu('shifts')){
            return back();
        }
        
        $parameters = array("from"=>"","to"=>"","employeeId"=>"","routeId"=>"");
        
        $id = $this->getCompanyId();
        $company = CompanyRepository::getRepository($id);
        return view('shifts')->with(['data'=>array()])->with(['company'=>$company])->with(['parameters'=>$parameters]);
    }
    
    public function getShifts(Request $request){
        if (!$this->authorizedMenu('shifts')){
            return back();
        }
        
        $parameters = array();
        
        $id = $this->getCompanyId();
        $from = $request->input('from');
        $to = $request->input('to');
        $employeeId = $request->input('employeeId');
        $routeId = $request->input('routeId');
        
        $parameters["from"] = $from;
        $parameters["to"] = $to;
        $parameters["employeeId"] = $employeeId;
        $parameters["routeId"] = $routeId;
        
        //dd($parameters);
        $result = ShiftRepository::getShifts($id, $employeeId, $routeId, $from, $to);
        $data = $result["shifts"];
        
        $company = CompanyRepository::getRepository($id);
        return view('shifts')->with(['data'=>$data])->with(['company'=>$company])->with(['parameters'=>$parameters]);
    }
    
    public function getShiftInfo(Request $request, $shiftId){
        $shiftRep = ShiftRepository::getRepository($shiftId);
        $shiftRep->prepareInfo();
        if ($shiftRep->isComplete()){
            $shiftRep->createShiftLog();
        }
        return Response::json($shiftRep);
    }
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use App\Http\Helpers\PostType;
use Response;
use Log;

use App\Repositories\CommentRepository;
use App\Repositories\PostRepository;

class PostController extends BaseController
{
    public function messages()
    {
        if (!$this->authorizedMenu('posts')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = PostRepository::getPosts($id, PostType::Company);
        $data = $result["posts"];

        return view('message')->with(['data'=>$data]);
    }
    
    public function newPostComment(Request $request,$postID){
        
        $result = array();
        
        $comment = $request->input('comment');
        $time =  date("Y-m-d_H:i");
        $companyID = $this->getCompanyId();
        $userName='admin';
        
        $postNode = PostRepository::getNode($postID,$companyID);
        
        if (!is_null($postNode)){
            $commentClass = new CommentRepository($userName, $comment);
            $commentClass->setPostId($postID);
            $commentClass->setCompanyId($companyID);
            $commentClass->setTime($time);
            
            $result = $commentClass->create();
        }else{
            $result["success"] = 0;
            $result["message"] = "No such post exists for ".$companyID;
        }
        
        return redirect('messages');
    }
    
    public function newPost(Request $request)
    {
        date_default_timezone_set('Africa/Johannesburg');
        $id = $this->getCompanyId();
        $files = $request->images;
        
        $postClass = new PostRepository($id);
        $postClass->setPost($request->input('post'));
        $postClass->setTime(date("Y-m-d_H:i"));
        $postType = $request->input('type');
        
        if ($postType === PostType::Company){
            $postClass->setType(PostType::Company);
        }else if ($postType === PostType::Admin){
            $postClass->setType(PostType::Admin);
        }else{
            $result = array();
            $result['success'] = 0;
            $result['message'] = "post type not recognized";
            return Response::json($result);
        }
        
        $result = $postClass->create();
        
        PostRepository::uploadPictures($files,$id,$result["postId"]);
        
        return redirect('messages');
    }
    
    public function editPost(Request $request){
        $companyId = $this->getCompanyId();
        $postId = $request->input('postId');
        $files = $request->images;
        
        $postRep = new PostRepository($companyId);
        $postRep->postId = $postId;
        
        if ($postRep != null){
            $postRep->update(array('post' => $request->input('post')));
            
            PostRepository::uploadPictures($files,$companyId,$postId);
        }
        return redirect('messages');
    }
    
    public function deletePost(Request $request){
        
        $companyId = $this->getCompanyId();
        $postId = $request->input('postId');
        $postType = $request->input('postType');
        
        $result = PostRepository::delete($postId, $companyId,$postType);
        
        return redirect('messages');
    }
}

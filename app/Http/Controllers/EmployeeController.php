<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\CommentRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\TimeLogRepository;
use App\Repositories\RouteRepository;
use App\Repositories\ShiftRepository;
/**
 * Description of EmployeeController
 *
 * @author Takunda Chirema
 */
class EmployeeController extends BaseController
{
    public function newEmployee(Request $request){
        $hashPassword = password_hash($request->input("password"), PASSWORD_BCRYPT);
        
        $employee = new EmployeeRepository(null,$request->input('company'));
        
        $employee->setEmployeeCode($request->input('code'));
        $employee->setPhoneNumber($request->input('phoneNumber'));
        $employee->setName($request->input('firstname').' '.$request->input('lastname'));
        $employee->setPassword($request->input($hashPassword));
        $employee->setStatus($request->input('offline'));
        
        $result = $employee->create();
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function editEmployeeProfile(Request $request){
        $result = array();
        
        $companyId = $request->input('companyID');
        $employeeId = $request->input('employeeID');
        $status = $request->input('status');
        
        $password = $request->input('password');
        $newPassword = $request->input('newPassword');
        $lastSeen = $request->input('lastSeen');
        
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        
        $location = $request->input('location');
        $firebasetoken = $request->input('firebasetoken');
        
        $employeeRep = EmployeeRepository::getRepository($employeeId,$companyId);
        
        if ($employeeRep){
            if (isset($password) && !password_verify($password,$employeeRep->getPassword())){
                $result["success"] = 0;
                $result["message"] = "invalid credentials ".$employeeRep->getPassword();
                return Response::json($result);
            }
        }
        
        $properties = array();
        if (isset($newPassword)){
            $hashPassword = password_hash($newPassword, PASSWORD_BCRYPT);
            $properties["password"] = $hashPassword;
        }
        
        if (isset($lastSeen)){
            $properties["lastSeen"] = $lastSeen;
        }
        
        if (isset($latitude)){
            $properties["latitude"] = $latitude;
        }
        
        if (isset($longitude)){
            $properties["longitude"] = $longitude;
        }
        
        if (isset($location)){
            $properties["location"] = $location;
        }
        
        if (isset($status)){
            $properties["status"] = $status;
        }
        
        if (isset($firebasetoken)){
            $properties["firebasetoken"] = $firebasetoken;
        }
        
        $employeeRep->update($properties);
        
        //also put user as online
        //$employee->update(array("status" => "online"));
        //echo print_r($request->input());
        
        $result["success"] = 1;
        $result["message"] = "Successfully updated!";
        $result["versions"] = array();
        $result["logout"] = $employeeRep->getLogout();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function createDevice($deviceId,$companyId,$employeeId){
        $device = new DeviceRepository($deviceId,$companyId);
        $device->create();
        
        $deviceNode = DeviceRepository::getNode($deviceId,$companyId);
        
        if ($deviceNode !== null){
            $deviceNode->employeeId = $employeeId;
            $deviceNode->save();
        }
        
        return $deviceNode;
    }
    
    public function setCompanyRoute(Request $request){
        
        $companyID = $request->input('companyID');
        $employeeID = $request->input('employeeID');
        $routeID = $request->input('routeID');
        
        $employee = new EmployeeRepository($employeeID,$companyID);
        $employee->update(array("routeId" => $routeID));
        
        $route = RouteRepository::getRepository($routeID, $companyID);
        
        $response = array();
        $response['success'] = 1;
        $response['message'] = "Route successfully set";
        $response['route']=$route;
        
        return Response::json($response);
    }
    
    public function getEmployeeSalt($employeeID,$companyID){
        
        $employee = EmployeeRepository::getNode($employeeID, $companyID);
         
        if ($employee){
            $newResult["versions"] = array();
            $newResult["versions"] = Constants::getVersions();
            $newResult["admin_versions"] = Constants::getAdminVersions();
            $newResult["success"] = 1;
            $newResult["salt"] = $employee->salt;
        }else{
            $newResult["success"] = 0;
            $newResult["message"] = "employee not Found";
        }
        
        return Response::json($newResult);
    }
    
    public function login(Request $request,$employeeID,$companyName){        
        $password = $request->input("password");
        $version = $request->input("version");
        
        $companyRep = CompanyRepository::getRepository(null, $companyName);
        
        if (!isset($companyRep)){
            $result["success"] = 0;
            $result["message"] = "company not found ";
            return Response::json($result);
        }
        
        $companyID = $companyRep->companyId;
        
        $employeeRep = EmployeeRepository::getRepository($employeeID, $companyID);
        
        $properties = array();
        if ($employeeRep){
            if (!password_verify($password, $employeeRep->getPassword())){
                
                if ($password == $employeeRep->getPassword()){
                    $hashPassword = password_hash($password, PASSWORD_BCRYPT);
                    $properties["password"] = $hashPassword;
                }else{
                    $result["success"] = 0;
                    $result["message"] = "invalid credentials";
                    return Response::json($result);
                }
            }
        }else{
            $result["success"] = 0;
            $result["message"] = "employee not found ";
            return Response::json($result);
        }
        
        $deviceId = $request->input("deviceId");
        if (isset($deviceId)){
            $deviceNode = $this->createDevice($deviceId,$companyID,$employeeID);
            $deviceNode->appVersion = $version;
            $deviceNode->save();
        }
        
        $device = DeviceRepository::getRepository($deviceId);
        
        $token = bin2hex(random_bytes(16));
        $properties["token"] = $token;
        $properties["logout"] = "false";
        $properties["status"] = "online";
        
        $employeeRep->update($properties);
        
        $result["employees"] = array();
        
        $result["success"] = 1;
        $result["message"] = "You are logged in";
        $result["token"] = $token;
        $result["latestVersion"] = $device->hasLatestVersion();
        $result["updateUrl"] = $device->getUpdateUrl();
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        array_push($result["employees"], $employeeRep);
        
        return Response::json($result);
    }
    
    public function deviceLogin(Request $request){
        $deviceId = $request->input("deviceId");
        $version = $request->input("version");
        
        $deviceRep = DeviceRepository::getRepository($deviceId);
        
        if (!isset($deviceRep)){
            $result = array();
            $result["success"] = 0;
            $result["message"] = "This device is not yet registered. Please use normal login first.";
            return Response::json($result);
        }
        
        if (isset($version)){
            $deviceNode = $this->createDevice($deviceId,$deviceRep->companyId,$deviceRep->employeeId);
            $deviceNode->appVersion = $version;
            $deviceNode->save();
            
            $deviceRep->setAppVersion($version);
        }
        
        $employeeRep = $deviceRep->employee;
        $properties = array();
        if ($employeeRep->getToken() !== null){
            $token = bin2hex(random_bytes(16));
            $properties["token"] = $token;
        }
        $properties["logout"] = "false";
        $employeeRep->update($properties);
        
        $result["employees"] = array();
        
        $result["success"] = 1;
        $result["message"] = "Device login successfull";
        $result["token"] = $token;
        $result["latestVersion"] = $deviceRep->hasLatestVersion();
        $result["updateUrl"] = $deviceRep->getUpdateUrl();
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        array_push($result["employees"], $employeeRep);
        
        return Response::json($result);
    }
    
    public function changeEmployee(Request $request,$newEmployeeID,$companyID){
        $employeeID = $request->input("employeeID");
        
        $employeeRep = EmployeeRepository::getRepository($employeeID, $companyID);
        $newEmployeeRep = EmployeeRepository::getRepository($newEmployeeID, $companyID);
        
        $properties = array();
        if (!$employeeRep || !$newEmployeeRep){
            $result["success"] = 0;
            $result["message"] = "employees not found";
            return Response::json($result);
        }
        
        $deviceId = $request->input("deviceId");
        if (isset($deviceId)){
            $this->createDevice($deviceId,$companyID,$newEmployeeID);
        }
        
        $device = DeviceRepository::getRepository($deviceId);
        
        $token = bin2hex(random_bytes(16));
        $properties["token"] = $token;
        $properties["logout"] = "false";
        $newEmployeeRep->update($properties);
        
        $result["employees"] = array();
        
        $result["success"] = 1;
        $result["message"] = "Employee successfully changed to ".$newEmployeeRep->employeeId;
        $result["token"] = $token;
        $result["latestVersion"] = $device->hasLatestVersion();
        $result["updateUrl"] = $device->getUpdateUrl();
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        array_push($result["employees"], $newEmployeeRep);
        
        return Response::json($result);
    }
    
    public function deleteEmployees(Request $request)
    {
        $company = $this->getCompanyId();
        
        $employeeRep = new EmployeeRepository($request->input('id'),$company);
        $employeeRep->delete();
        return redirect('employees');
    }
    
    public function getEmployeeDetails($employeeID,$companyID){
        
        $employeeRep = EmployeeRepository::getRepository($employeeID, $companyID);
        
        $result['success'] = 1;
        $result['message'] = "Retrieved Employee Details";
        $result['employees'] = $employeeRep;
        
        return Response::json($result);
    }
    
    public function uploadEmployeeProfilePicture(Request $request,$employeeID,$companyID){
        
        try{
            $employee = new EmployeeRepository($employeeID,$companyID);
            
            $file = $request->file('picture');
            
            $result = $employee->uploadPicture($file);
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
    }
    
    public function uploadEmployeeLogs(Request $request,$employeeID,$companyID){
        
        try{
            $employee = new EmployeeRepository($employeeID,$companyID);
            
            $file = $request->file('logs');
            
            $result = $employee->recordTimeLog($file);
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
    }
    
    public function getEmployeeLogs($employeeID,$companyID,$routeId,$date=null)
    {
        if ($employeeID === "all"){
            $employeeID = null;
        }
        
        if ($routeId === "all"){
            $routeId = null;
        }
        
        try{
            
            $employee = new EmployeeRepository($employeeID,$companyID);
            
            $logs = $employee->getRouteDateTimeLogs($routeId, $date)["timeLogs"];
            
            if ($logs == null){
                $result = array();
                $result['success'] = 0;
                $result['message'] = "There is no log for ".$date;
                return Response::json($result);
            }
            
            $result = array();
            $result['logs'] = $logs;
            $result['success'] = 1;
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "Could not get logs";
            
            return Response::json($result);
        }
    }
    
    public function getEmployeeFriends($employeeID,$companyID){
        $result = EmployeeRepository::getEmployees($companyID);
        
        echo json_encode($result);
        return Response::json($result);
    }
    
    public function getEmployeeMessages($employeeID,$companyID){
        $result = array();
        $result['success'] = 1;
        $result['messages'] = array();
        return Response::json($result);
    }
    
    public function seenEmployeeMessage(Request $request,$userID){
        $result = array();
        $result['success'] = 1;
        return Response::json($result);
    }
    
    public function sendEmployeeMessage(Request $request,$userID){
        $result = array();
        $result['success'] = 1;
        $result['message'] = "messages not sent";
        return Response::json($result);
    }
    
    public function setEmployeeAlert(Request $request,$employeeID){
        
        $alert = new AlertRepository($employeeID);
        $alert->setLatitude($request->input('latitude'));
        $alert->setLongitude($request->input('longitude'));
        $alert->setLocation($request->input('location'));
        $alert->setTime($request->input('time'));
        $alert->setCompanyId($request->input('company'));
        
        $result = $alert->create();
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function cancelEmployeeAlert($employeeID,$companyID){
        $alert = new AlertRepository;
        $alert->setUserId($employeeID);
        $alert->setCompanyId($companyID);
        $alert->removeAlert();
        
        $result['success'] = 1;
        $result['message'] = 'Alert removed';      
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function seenEmployeeAlert(Request $request,$userID,$friendID){
        
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $friendID;
        $relationship->type = RelationshipRepository::$employeeSeenAlert;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "AlertRepository Seen";
        return Response::json($result);
    }
    
    public function seenUserAlert($employeeID,$companyID,$userID){
        
        $relationship = new RelationshipRepository;
        $relationship->entityId = $employeeID;
        $relationship->relatedEntityId = $userID;
        $relationship->type = RelationshipRepository::$employeeSeenAlert;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "AlertRepository Seen";
        return Response::json($result);
    }
    
    public function editEmployeeSettings(Request $request,$employeeID){
        
        $settings = new SettingsRepository($employeeID);
        $settings->setCompanyId($request->input('company'));
        
        $properties = array();
        foreach($request->input() as $name => $value){
            if ($name != 'company'){
                $properties[$name] = $value;
            }
        }
        
        $result = $settings->update($properties);
        
        echo json_encode($result);
        return Response::json($result);
    }
    
    public function postEmployeeComment(Request $request,$employeeID,$postID){
        
        $comment = $request->input('comment');
        $time = $request->input('time');
        $companyID = $request->input('companyID');
        
        $repository = new CommentRepository($employeeID,$comment);
        $repository->time=$time;
        $repository->companyId=$companyID;
        $repository->postId=$postID;
        $result = $repository->create();
        
        return Response::json($result);
    }
    
    public function postPromotionComment(Request $request,$employeeID,$promotionID){
        $comment = $request->input('comment');
        $time = $request->input('time');
        $companyID = $request->input('companyID');
        
        $repository = new CommentRepository($employeeID,$comment);
        $repository->time=$time;
        $repository->companyId=$companyID;
        $repository->promotionId=$promotionID;
        $result = $repository->create();
        
        return Response::json($result);
    }
    
    /*** Web ***/
    
    public function newEmployees(Request $request)
    {
        $company = $this->getCompanyId();
        $admin = $request->input('admin');
        $superuser = $request->input('superuser');
        
        $menu = "";
        if ($request->input('menu')!==null){
            foreach($request->input('menu') as $index => $value) {
                $menu = $menu.",".$value;
            }
        }
        
        $employee = new EmployeeRepository(null,$company);
        
        $employee->setEmployeeCode($request->input('code'));
        $employee->setPhoneNumber($request->input('phoneNumber'));
        $employee->setName($request->input('name'));
        $employee->setPassword($request->input('password'));
        $employee->setSalt($request->input('salt'));
        $employee->setStatus($request->input('offline'));
        $employee->setMenu($menu);
        
        if ($superuser == "on"){
            $employee->setPrivilege('superuser');
        }else if ($admin == "on"){
            $employee->setPrivilege('admin');
        }else{
            $employee->setPrivilege('employee');
        }
        
        $employee->create();
        return redirect('employees');
    }
    
    public function editEmployees(Request $request)
    {
        $employeeId = $request->input('id');
        $companyId = $this->getCompanyId();
        $admin = $request->input('admin');
        $superuser = $request->input('superuser');
        $phoneNumber = $request->input('phoneNumber');
        $name = $request->input('name');
        $routeId = $request->input('routeId');
        $deviceId = $request->input('deviceId');
        
        $employeeRep = new EmployeeRepository($employeeId,$companyId);
        
        $menu = "";
        $properties = array();
        if ($request->input('menu') !== null){
            foreach($request->input('menu') as $index => $value) {
                $menu = $menu.",".$value;
            }
        }
        $properties["menu"] = $menu;
        
        //dd($request->input('deviceId'));
        if (isset($phoneNumber)){
            $properties["phoneNumber"] = $phoneNumber;
        }
        
        if (isset($name)){
            $properties["name"] = $name;
        }
        
        if (isset($routeId)){
            $properties["routeId"] = $routeId;
        }
        
        if (isset($deviceId)){
            $properties["deviceId"] = $deviceId;
        }else{
            $properties["deviceId"] = "";
        }
        
        if ($superuser == "on"){
            $properties["privilege"] = 'superuser';
        }else if ($admin == "on"){
            $properties["privilege"] = 'admin';
        }else{
            $properties["privilege"] = 'employee';
        }
        
        if ($request->input('password') != ""){
            $hashPassword = password_hash($request->input("password"), PASSWORD_BCRYPT);
            $properties["password"] = $hashPassword;
        }
        $employeeRep->update($properties);
        
        return redirect('employees');
    }
    
    public function employees()
    {
        if (!$this->authorizedMenu('employees')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = EmployeeRepository::getEmployees($id);
        $data = $result["employees"];
 
        $company = CompanyRepository::getRepository($id);
        
        return view('employee')->with(['data'=>$data])->with(['company'=>$company]);
    }
    
    public function timeLogs(){
        
        if (!$this->authorizedMenu('timelogs')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = EmployeeRepository::getEmployees($id);
        $data = $result["employees"];
        
        $company = CompanyRepository::getRepository($id);
        
        return view('time_logs')->with(['data'=>$data])->with(['company'=>$company]);
    }
    
    public function deleteTimeLog(Request $request){
        $id = $this->getCompanyId();
        
        $dateStr = $request->input('date');
        $employeeId = $request->input('employeeId');
        $routeId = $request->input('routeId');
        
        TimeLogRepository::deteleTimeLogs($id, $dateStr, $employeeId, $routeId);
        
        return $this->timeLogs();
    }
}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\AlertRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\CommentRepository;
use App\Repositories\PostRepository;
use App\Repositories\RouteRepository;
use App\Repositories\RelationshipRepository;
use App\Repositories\FollowerRepository;
use App\Models\Company;
use App\Models\Relationship;
use App\Http\Helpers\Constants;
use Datetime;
use DateTimeZone;
use Response;

/**
 * Description of UserController
 *
 * @author Takunda Chirema
 */

class UserController extends BaseController 
{
    public function newUser(Request $request){
        
        $user = new UserRepository($request->input('userID'));
        $user->setName($request->input('userID'));
        $user->setEmail($request->input('email'));
        $user->setPhoneNumber($request->input('phoneNumber'));
        $user->setPassword($request->input('password'));
        $user->setSalt($request->input('salt'));
        $user->setStatus($request->input('offline'));
        
        $result = $user->create();
        
        $settings = new SettingsRepository($user->userId);
        $settings->create();
        
        $this->followDefaultCompanies($user->userId);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function registerUser(Request $request){
        $hashPassword = password_hash($request->input("password"), PASSWORD_BCRYPT);
        
        $user = new UserRepository($request->input('userID'));
        $user->setEmail($request->input('email'));
        $user->setPhoneNumber($request->input('phoneNumber'));
        $user->setPassword($hashPassword);
        $user->setStatus('offline');
        
        $result = $user->create();
        
        $settings = new SettingsRepository($user->userId);
        $settings->create();
        
        $this->followDefaultCompanies($user->userId);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function editUserProfile(Request $request){
        $result = array();
        
        $userId = $request->input('userID');
        $password = $request->input('password');
        $newPassword = $request->input('newPassword');
        
        $date = new DateTime('now');
        $systemLastSeen = $date->format('Y-m-d_H:i:s');
        
        $lastSeen = $request->input('lastSeen');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        //$country = $request->input('country');
        $location = $request->input('location');
        
        // These are for crowdsourcing
        $companyId = $request->input('companyId');
        $routeId = $request->input('routeId');
        $direction = $request->input('direction');
        $firebasetoken = $request->input('firebasetoken');
        
        //echo "route thingies routeId: ".$routeId." lastSeen: ".$lastSeen." direction: ".$direction." coId: ".$companyId;
        if (isset($routeId)){
            $routeRep = RouteRepository::getRepository($routeId, $companyId);
            if (isset($routeRep)){
                $result["message"] = "route set ".$routeId." ".$systemLastSeen." ".$direction;
                $routeRep->setUserClusterLocation($userId, $direction, $systemLastSeen, $latitude, $longitude);
            }
        }
        
        $user = new UserRepository($userId);
        
        if ($user){
            if (isset($password) && !password_verify($password,$user->getPassword())){
                $result["success"] = 0;
                $result["message"] = "invalid credentials";
                return Response::json($result);
            }
        }
        
        $properties = array();
        if (isset($newPassword)){
            $hashPassword = password_hash($newPassword, PASSWORD_BCRYPT);
            $properties["password"] = $hashPassword;
        }
        
        if (isset($lastSeen)){
            $properties["lastSeen"] = $lastSeen;
        }
        
        if (isset($latitude)){
            $properties["latitude"] = $latitude;
        }
        
        if (isset($longitude)){
            $properties["longitude"] = $longitude;
        }
        
        if (isset($location)){
            $properties["location"] = $location;
        }
        
        if (isset($firebasetoken)){
            $properties["firebasetoken"] = $firebasetoken;
        }
        $user->update($properties);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function facebookLogin(Request $request){
        $result = array();
        $result["users"] = array();
        
        $fbId = $request->input('fbID');
        $fbEmail = $request->input('fbEmail');
        $fbName = $request->input('fbName');
        
        $fbUser = UserRepository::getUserNode($fbId, null);
        $properties = array();
        
        if (!$fbUser){
            //create new user
            $user = new UserRepository($fbId);
            $user->setName($fbName);
            $user->setFbId($fbId);
            $user->setEmail($fbEmail);
            $user->setFbEmail($fbEmail);
            $user->setFbName($fbName);
            $user->setStatus($request->input('offline'));
            $user->create();
            
            $settings = new SettingsRepository($user->userId);
            $settings->create();
        
            $this->followDefaultCompanies($user->userId);
            $properties["status"] = "online";
            array_push($result["users"], $user);
        }else{
            $user = UserRepository::getUser($fbId, null);
            $properties["status"] = "online";
        }
        $token = bin2hex(random_bytes(16));
        $properties["token"] = $token;
        
        $user->update($properties);
        
        array_push($result["users"], $user);
        $result["success"] = 1;
        $result["message"] = "Retrieved Users";
        $result["token"] = $token;
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getUserSalt($userID){
        
        $userNode = UserRepository::getUserNode($userID, null);
         
        if ($userNode){
            //echo json_encode($result);
            $newResult["versions"] = array();
            $newResult["versions"] = Constants::getVersions();
            $newResult["admin_versions"] = Constants::getAdminVersions();
            $newResult["success"] = 1;
            $newResult["salt"] = $userNode->salt;
        }else{
            $newResult["success"] = 0;
            $newResult["message"] = "User not Found. NB, Case sensitive";
        }
        return Response::json($newResult);
    }
    
    public function login(Request $request,$userID){        
        $password = $request->input("password");
        
        $user = UserRepository::getUser($userID, null);
        
        if ($user){
            
            if ($password != $user->getPassword()){
                $result["success"] = 0;
                $result["message"] = "invalid credentials";
                return Response::json($result);
            }
        }else{
            $result["success"] = 0;
            $result["message"] = "user not found";
            return Response::json($result);
        }
        
        $token = bin2hex(random_bytes(16));
        $user->update(array("token" => $token));
        
        $usersResult["success"] = 1;
        $usersResult["message"] = "Successfully logged in";
        $usersResult["token"] = $token;
        $usersResult["versions"] = array();
        $usersResult["versions"] = Constants::getVersions();
        $usersResult["admin_versions"] = Constants::getAdminVersions();
        return Response::json($usersResult);
    }
    
    public function newLogin(Request $request,$userID){
        $password = $request->input("password");
        
        $userRep = UserRepository::getUser($userID);
        
        $properties = array();
        if ($userRep){
            if (!password_verify($password, $userRep->getPassword())){
                $userSalt = $userRep->salt;
                
                if (isset($userSalt) && !empty($userSalt) && $userSalt != "null"){
                    $hashPassword = password_hash($password, PASSWORD_BCRYPT);
                    $properties["password"] = $hashPassword;
                    $properties["salt"] = "";
                }else{
                    $result["success"] = 0;
                    $result["message"] = "Invalid Credentials";
                    return Response::json($result);
                }
            }
        }else{
            $result["success"] = 0;
            $result["message"] = "user not found";
            return Response::json($result);
        }
        
        $token = bin2hex(random_bytes(16));
        $properties["token"] = $token;
        
        $userRep->update($properties);
        
        $result["users"] = array();
        array_push($result["users"], $userRep);
        $result["success"] = 1;
        $result["message"] = "Successfully logged in";
        $result["token"] = $token;
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getUserDetails(Request $request,$userID){  
        $user = UserRepository::getUser($userID);
        
        //also put user as online
        $user->update(array("status" => "online"));
        
        //echo json_encode($result);
        $result['users'] = $user;
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function uploadUserProfilePicture(Request $request,$userID){
        
        try{
            $user = new UserRepository($userID);
            
            $file = $request->file('picture');
            
            $result = $user->uploadProfilePicture($file);
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
    }
    
    public function getAllCompanies(Request $request,$userID){
        
        $newResult = array();
        
        $countryCode = $request->header('countryCode');

        $followingResult = CompanyRepository::getFollowedCompanies($userID, $countryCode);
        $newResult['following'] = $followingResult['companies'];
        
        $notFollowingResult = CompanyRepository::getUnfollowedCompanies($userID, $countryCode);
        $newResult['notFollowing'] = $notFollowingResult['companies'];
        
        $newResult['success'] = 1;
        $newResult['message'] = "companies retrieved";
        
        $newResult["versions"] = array();
        $newResult["versions"] = Constants::getVersions();
        $newResult["admin_versions"] = Constants::getAdminVersions();
        return Response::json($newResult);
    }
    
    public function getUserCompanies(Request $request,$userID){
        
        $result = CompanyRepository::getFollowedCompanies($userID);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getUserFriends(Request $request,$userID){
        
        $result = UserRepository::getFriends($userID);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getAlertingUsers($latitude,$longitude,$employeeID,$companyID){
        
        $zerodplat = substr($latitude,0,strpos($latitude,".") );
	$zerodplng = substr($longitude,0,strpos($longitude,".") );
        
        $result = UserRepository::getAlertingUsers($employeeID,$zerodplat,$zerodplng);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function getUserRequests($userID){

        $result = UserRepository::getRequestingUsers($userID);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getUserRequested($userID){
        
        $result = UserRepository::getRequestedUsers($userID);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function getAllUsers($userID){
        
        $friendsOfFriends = UserRepository::getFriendsOfFriends($userID,2)['users'];
        $result['users'] = $this->removeDuplicates($friendsOfFriends,$userID);
        
        $randomUsers = UserRepository::getRandomUsers($userID, 100);
        $result['users'] = array_merge($result['users'], $randomUsers["users"]);
        
        $result['success'] = 1;
        $result['message'] = 'Retrieved Friends of Friends';
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function searchForUsers($userID,$searchString)
    {
        $result = UserRepository::searchForUsers($userID, $searchString);
        $result['users'] = $result["users"];
                
        $response['success'] = 1;
        $response['message'] = 'Retrieved Friends of Friends';
        //echo json_encode($result);
        $response["versions"] = array();
        $response["versions"] = Constants::getVersions();
        $response["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    function removeDuplicates($users,$userId)
    {
        $uniqueUsers = array();
        
        foreach ($users as $user){
            if ($user->userId != $userId){
                $uniqueUsers[$user->userId] = $user;
            }
        }
        return array_values ( $uniqueUsers );
    }
    
    public function getUserMessages($userID){
        $result = array();
        
        //echo json_encode($result);
        $result['success'] =0;
        $result['message'] = 'Not Supported';
        return Response::json($result);
    }
    
    public function sendMessage(Request $request,$userID){
        $result = array();
        
        //echo json_encode($result);
        $result['success'] =0;
        $result['message'] = 'Not Supported';
        return Response::json($result);
    }
    
    public function seenUserMessage(Request $request,$userID){
        $result = array();
        
        //echo json_encode($result);
        $result['success'] =0;
        $result['message'] = 'Not Supported';
        return Response::json($result);
    }
    
    public function setUserAlert(Request $request,$userID){
        $alert = new AlertRepository($userID);
        $alert->setUserId($userID);
        $alert->setLatitude($request->input('latitude'));
        $alert->setLongitude($request->input('longitude'));
        $alert->setLocation($request->input('location'));
        $alert->setTime($request->input('time'));
        $result = $alert->create();
        
        //remove all alerts seen from other users
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function getLocationAlerts($latitude,$longitude){
        
        $zerodplat = substr($latitude,0,strpos($latitude,".") );
	$zerodplng = substr($longitude,0,strpos($longitude,".") );
        
        $result = AlertRepository::getLocationAlerts($zerodplat,$zerodplng);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function getAlerts(){
        $date = date("d.m.Y", strtotime("-3 Months"));
        
        $result = AlertRepository::getAlerts($date);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function seenUserAlert($userID,$friendID){
        
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $friendID;
        $relationship->type = RelationshipRepository::$userSeenAlert;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "AlertRepository Seen";
        return Response::json($result);
    }
    
    public function cancelUserAlert($userID){
        $alert = new AlertRepository;
        $alert->setUserId($userID);
        $alert->removeAlert();
        
        $result = array();
        $result['success'] = 1;
        $result['message'] = 'Alert Cancelled';
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function editUserSettings(Request $request,$userID)
    {
        $settings = new SettingsRepository($userID);
        
        $properties = array();
        foreach($request->input() as $name => $value){
            if ($name != 'id' && $name != 'userID'){
                $properties[$name] = $value;
            }
        }
        $result = $settings->update($properties);
        
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function hideUserLocation ($userID,$friendID)
    {
        $result = array();
        
        UserRepository::updateVisibility($userID, $friendID, 'no');
        
        $result["success"] = 1;
        $result["message"] = "Location Hid!";
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function showUserLocation ($userID,$friendID){
        
        $result = array();
        
        UserRepository::updateVisibility($userID, $friendID, 'yes');
        
        $result["success"] = 1;
        $result["message"] = "Location Hid!";
        //echo json_encode($result);
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function unfriend($userID,$friendID)
    {
        $result = array();
        
        UserRepository::unfriend($userID, $friendID);
        
        $result["success"] = 1;
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function acceptRequest ($userID,$friendID){
        $result = array();
        
        UserRepository::updateFriendship($userID, $friendID, 'state', 'accepted');
        
        $result["success"] = 1;
        $result["message"] = "Request accepted";
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function rejectRequest($userID,$friendID)
    {
        $result = array();
        
        UserRepository::unfriend($userID, $friendID);
        
        $result["success"] = 1;
        $result["message"] = "Request accepted";
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function sendRequest($userID,$friendID)
    {
        $result = UserRepository::sendRequest($userID, $friendID);
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function cancelRequest($userID, $friendID){
        $response = $this->rejectRequest($userID, $friendID);
        return $response;
    }
    
    public function followDefaultCompanies($userID){
        
        $companies = Company::query()
                    ->where('sector','=','Transportation')
                    ->where('type','=','public')
                    ->get();
        
        foreach ($companies as $company){
            $this->followCompany($userID, $company->companyId);
        }
    }
    
    public function followCompany($userID,$companyID){
        $result = array();
        
        $follower = new FollowerRepository($userID);
        $follower->companyId = $companyID;
        $follower->create();  
        
        $result["success"] = 1;
        $result["message"] = "You are now following ".$companyID;
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function unfollowCompany($userID,$companyID)
    {
        FollowerRepository::deleteFollower($userID, $companyID);
        
        $result["success"] = 1;
        $result["message"] = "You have unfollowed ".$companyID;
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
    
    public function postComment(Request $request,$userName,$postID,$companyID)
    {
        $comment = $request->input('comment');
        $time = $request->input('time');
        
        $repository = new CommentRepository($userName,$comment);
        $repository->time=$time;
        $repository->companyId=$companyID;
        $repository->postId=$postID;
        $result = $repository->create();
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function postPromotionComment(Request $request,$userID,$promotionID,$companyID)
    {
        $comment = $request->input('comment');
        $time = $request->input('time');
        
        $repository = new CommentRepository($userID,$comment);
        $repository->time=$time;
        $repository->companyId=$companyID;
        $repository->promotionId=$promotionID;
        $result = $repository->create();
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function postCatalogueComment(Request $request,$userID,$postID,$companyID)
    {
        $comment = $request->input('comment');
        $time = $request->input('time');
        
        $repository = new CommentRepository($userID,$comment);
        $repository->time=$time;
        $repository->companyId=$companyID;
        $repository->cataloguePostId=$postID;
        $result = $repository->create();
        
        //echo json_encode($result);
        return Response::json($result);
    }
}

<?php namespace App\Http\Controllers;

/**
 * Description of CompanyController
 *
 * @author Takunda Chirema
 */

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Constants;
use Response;
use Log;

use App\Repositories\EmployeeRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\UserRepository;
use App\Repositories\LocationRepository;
use App\Repositories\BusinessHoursRepository;
use App\Repositories\PostRepository;
use App\Repositories\CataloguePostRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\RouteRepository;
use App\Repositories\FollowerRepository;
use App\Repositories\RelationshipRepository;
use App\Repositories\NotificationRepository;
use App\Models\Relationship;
use App\Http\Helpers\PostType;
use App\Http\Helpers\DeleteFolder;

class CompanyController extends BaseController 
{
    public function newCompany(Request $request){
        
        $response = array();

        $company = new CompanyRepository($request->input('id'));
        $company->setAddress($request->input('address'));
        $company->setPhoneNumber($request->input('phoneNumber'));
        $company->setAbout($request->input('about'));
        $company->setSector($request->input('sector'));
        $company->setEmail($request->input('email'));
        $company->setType($request->input('type'));

        $response['company'] = $company->create();
        
        //create the business hours node for the company
        $businessHours = new BusinessHoursRepository($request->input('id'));
        $businessHours->setMon($request->input('monday'));
        $businessHours->setTue($request->input('tuesday'));
        $businessHours->setWed($request->input('wendesday'));
        $businessHours->setThu($request->input('thursday'));
        $businessHours->setFri($request->input('friday'));
        $businessHours->setSat($request->input('saterday'));
        $businessHours->setSun($request->input('sunday'));
        $response['businessHours'] = $businessHours->create();

        //echo( json_encode($response));
        return Response::json($response);
        //put all the other details
    }
    
     public function uploadDeviceLog(Request $request,$deviceId){
        
        try{
            $file = $request->file('logs');
            $txt = file_get_contents($file);
                    
            $path = public_path('logs/');
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            
            file_put_contents($path."error_log.txt", $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
        
        $result = array();
        $result['success'] = 1;
        $result['message'] = "Device log uploaded";
        $result['fileName'] = $file->getClientOriginalName();
        
        return Response::json($result);
    }
    
    public function editProfile(Request $request){
        
        $companyRep = CompanyRepository::getRepository($request->input('id'));
        $result = $companyRep->update(array('about' => 'we sell pizza'));
        
        return Response::json($result);
        
    }
    
    public function saveCompanyRoute(Request $request){
        
        $companyID = $request->input('companyID');
        $routeName = $request->input('routeName');
        $routeColor = $request->input('routeColor');
        
        $route = new RouteRepository(null);
        $route->setRouteName($routeName);
        $route->setCompanyId($companyID);
        $route->setColor($routeColor);
        
        $result = $route->create();
        
        $points = array();
        foreach($request->input() as $name => $value){
            if ($name != 'companyID' && $name != 'routeID' && $name != 'routeColor'){
                array_push($points,$value);
            }
        }
        
        $route->encodeToXml($points);
        
        $routeRep = RouteRepository::getRepository($routeID, $companyID);
        
        $result['route'] = $routeRep;
        
        return Response::json($result);
    }
    
    public function getRoute($companyID,$routeID){
        $result = array();
        
        $routeRep = RouteRepository::getRepository($routeID, $companyID);
        
        $result['success'] = 1;
        $result['message'] = 'Route Succefully Obtained';
        $result['route'] = $routeRep;
        
        return Response::json($result);
    }
    
    public function getRoutes(Request $request){
        $result = array();
        $routes = array();
        
        if ($request->input('companies') !== null){
            foreach($request->input('companies') as $companyId => $routeId) {
                $routeRep = RouteRepository::getRepository($routeId, $companyId);
                array_push($routes,$routeRep);
            }
        }
        
        $result['success'] = 1;
        $result['message'] = 'Routes Succefully Obtained';
        $result['routes'] = $routes;
        
        return Response::json($result);
    }
    
    public function getAllCompanies(){
        
        $result = CompanyRepository::getCompanies();
        
        //echo json_encode(result);
        return Response::json($result);
    }
    
    public function getCompanyDetails(Request $request, $companyID){
        
        $result['companies'] = array();
        
        $company = CompanyRepository::getRepository($companyID);
        array_push($result['companies'], $company);
        
        $result["success"] = 1;
        $result["message"] = "Retrieved Companies";
        
        //echo json_encode($result);
        return Response::json($result);
    }
    
    public function getCompanyFollowers($id){
        $result = FollowerRepository::getFollowers($id);
        return Response::json($result);
    }
    
    public function getCompanyPosts($companyID, $type){   
        
        $result = PostRepository::getPosts($companyID,$type);
        
        return Response::json($result);
    }
    
    public function getEmployeeCompanyPosts($companyID, $employeeId){   
        
        $result = PostRepository::getEmployeePosts($companyID,$employeeId);

        return Response::json($result);
    }
    
    public function getCompanyPromotions($companyID){
        
        $result = PromotionRepository::getPromotions($companyID); 
        
        return Response::json($result);
    }
    
    public function getEmployeeCompanyPromotions($companyID, $employeeId){
        
        $result = PromotionRepository::getEmployeePromotions($companyID,$employeeId); 

        return Response::json($result);
    }
    
    public function getCompanyCatalogue($companyID){
        
        $result = CataloguePostRepository::getPosts($companyID,null); 
        
        return Response::json($result);
    }
    
    public function getEmployeeCompanyCatalogue($companyID, $employeeId){
        
        $result = CataloguePostRepository::getEmployeePosts($companyID,$employeeId);

        return Response::json($result);
    }
    
    public function deletePost($companyID, $postID, $type)
    {
        $result = PostRepository::delete($postID, $companyID, $type);
        return Response::json($result);
    }

    public function getCompanyUserPromotions($companyID,$userID){     
        $result = PromotionRepository::getUserPromotions($companyID,$userID);
        return Response::json($result);
    }
    
    public function getCompanyUserPosts($companyID,$userID){
        $result = PostRepository::getUserPosts($companyID,$userID);
        return Response::json($result);
    }
    
    public function getUserPosts(Request $request, $userID){
        $result = array();
        $posts = array();
        
        $countryCode = $request->header('countryCode');
        
        if (isset($countryCode)){
            $followingResult = CompanyRepository::getFollowedCompanies($userID,$countryCode);
        }else{
            $followingResult = CompanyRepository::getFollowedCompanies($userID);
        }
        
        foreach($followingResult['companies'] as $companyRep){
            $companyPosts = PostRepository::getUserPosts($companyRep->companyId,$userID)['posts'];
            $posts = array_merge($posts,$companyPosts);
        }
        
        $result['posts'] = $posts;
        $result['success'] = 1;
        $result['message'] = "retrieved posts";
        return Response::json($result);
    }
    
    public function getUserPromotions(Request $request, $userID){
        $result = array();
        $promotions = array();
        
        $countryCode = $request->header('countryCode');
        
        if (isset($countryCode)){
            $followingResult = CompanyRepository::getFollowedCompanies($userID, $countryCode);
        }else{
            $followingResult = CompanyRepository::getFollowedCompanies($userID);
        }
        
        foreach($followingResult['companies'] as $companyRep){
            $companyPromotions = PromotionRepository::getUserPromotions($companyRep->companyId,$userID)['promotions'];
            $promotions = array_merge($promotions,$companyPromotions);
        }
        
        $result['promotions'] = $promotions;
        $result['success'] = 1;
        $result['message'] = "retrieved promotions";
        return Response::json($result);
    }
    
    public function seenCompanyUserPosts($companyID,$userID,$postID)
    {
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $postID;
        $relationship->type = RelationshipRepository::$userSeenPost;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "PostRepository Seen";
        return Response::json($result);
    }
    
    public function seenCompanyUserPromotions($companyID,$userID,$promotionID){
        
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $promotionID;
        $relationship->type = RelationshipRepository::$userSeenPromotion;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "Promotion Repository Seen";
        return Response::json($result);
    }
    
    public function likeCompanyUserPosts($companyID,$userID,$postID){
        
        $relationshipNode = Relationship::query()
                            ->where('entityId','=',$userID)
                            ->where('relatedEntityId','=',$postID)
                            ->where('type','=', RelationshipRepository::$userLikedPost)
                            ->first();
         
        if (!is_null($relationshipNode)){
            $result["success"] = 0;
            $result["message"] = "Already Liked This post";
            return Response::json($result);
        }
        
        PostRepository::likePost($postID, $companyID);
            
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $postID;
        $relationship->type = RelationshipRepository::$userLikedPost;
        $relationship->create();

        $result["success"] = 1;
        $result["message"] = "Post Liked!";
        
        return Response::json($result);
    }
    
    public function likeCompanyUserPromotions($companyID,$userID,$promotionID){
        
        $relationshipNode = Relationship::query()
                            ->where('entityId','=',$userID)
                            ->where('relatedEntityId','=',$promotionID)
                            ->where('type','=', RelationshipRepository::$userLikedPromotion)
                            ->first();
         
        if (!is_null($relationshipNode)){
            $result["success"] = 0;
            $result["message"] = "Already Liked This Promotion";
            return Response::json($result);
        }
        
        PromotionRepository::likePromotion($promotionID, $companyID);
            
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $promotionID;
        $relationship->type = RelationshipRepository::$userLikedPromotion;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "Promotion Liked!";
        
        return Response::json($result);
    }
    
    public function likeCompanyCataloguePost($companyID,$userID,$postID){
        
        $relationshipNode = Relationship::query()
                            ->where('entityId','=',$userID)
                            ->where('relatedEntityId','=',$postID)
                            ->where('type','=', RelationshipRepository::$userLikedCataloguePost)
                            ->first();
         
        if (!is_null($relationshipNode)){
            $result["success"] = 0;
            $result["message"] = "Already Liked This post";
            return Response::json($result);
        }
        
        CataloguePostRepository::likePost($postID, $companyID);
            
        $relationship = new RelationshipRepository;
        $relationship->entityId = $userID;
        $relationship->relatedEntityId = $postID;
        $relationship->type = RelationshipRepository::$userLikedCataloguePost;
        $relationship->create();

        $result["success"] = 1;
        $result["message"] = "Catalogue post liked!";
        
        return Response::json($result);
    }
    
    public function getCompanyEmployeePosts($companyID,$employeeID){
        $result = PostRepository::getEmployeePosts($companyID, $employeeID);
        return Response::json($result);
    }
    
    public function seenCompanyEmployeePosts($companyID,$employeeID,$postID){
        
        $relationship = new RelationshipRepository;
        $relationship->entityId = $employeeID;
        $relationship->relatedEntityId = $postID;
        $relationship->type = RelationshipRepository::$employeeSeenPost;
        $relationship->create();
        
        $result["success"] = 1;
        $result["message"] = "Post Seen";
        return Response::json($result);
    }
    
    public function getCompanyEmployees($companyId){
        $result = EmployeeRepository::getEmployees($companyId);
        return Response::json($result);
    }
    
    public function getUserEmployees(Request $request, $userID){
        $result = array();
        $employees = array();
        
        $countryCode = $request->header('countryCode');
        
        if ($request->input('companies') !== null){
            foreach($request->input('companies') as $companyId) {
                $companyEmployees = EmployeeRepository::getEmployees($companyId)['employees'];
                $deviceEmployees = DeviceRepository::createUnknownEmployees($companyId);
                
                $employees = array_merge($employees,$companyEmployees);
                $employees = array_merge($employees,$deviceEmployees);
            }
            
            $result['message'] = "retrieved employees from companies request";
        }else{
            if (isset($countryCode)){
                $followingResult = CompanyRepository::getFollowedCompanies($userID,$countryCode);
            }else{
                $followingResult = CompanyRepository::getFollowedCompanies($userID);
            }
            
            foreach($followingResult['companies'] as $companyRep){
                $companyEmployees = EmployeeRepository::getEmployees($companyRep->companyId)['employees'];
                $deviceEmployees = DeviceRepository::createUnknownEmployees($companyRep->companyId);
                
                $employees = array_merge($employees,$companyEmployees);
                $employees = array_merge($employees,$deviceEmployees);
            }
            
            $result['message'] = "retrieved employees from following";
        }
        
        $result['employees'] = $employees;
        $result['success'] = 1;
        return Response::json($result);
    }
    
    public function getEmployeeLogs($companyId){
        $logs = CompanyRepository::getEmployeeLogs($companyId)["timeLogs"];
        
        $result = array();
        $result['logs'] = $logs;
        $result['success'] = 1;
        return Response::json($result);
    }
    
    public function createCompanyPost(Request $request,$companyID,$postType){
        $posts = array();
        
        $postId = $request->input('postID');
        
        if (isset($postId)){
            $postRep = new PostRepository($companyID);
            $postRep->setId($postId);
                
            $postRep->update(array('post' => $request->input('postMessage')));

            $postNode = PostRepository::getNode($postId, $companyID);
            $post = PostRepository::getRepository($postNode);
            array_push($posts, $post);

            $result = array();
            $result['success'] = 1;
            $result['posts'] = $posts;
            $result['message'] = "Post successfully edited";
            return Response::json($result);     
        }
        
        $postRep = new PostRepository($companyID);
        $postRep->setCreatedBy($request->input('createdBy'));
        $postRep->setPost($request->input('postMessage'));
        $postRep->setTime($request->input('time'));
        
        if ($postType === PostType::Admin){
            $postRep->setType(PostType::Admin);
        }else if ($postType === PostType::Company){
            $postRep->setType(PostType::Company);
        }else{
            $result = array();
            $result['success'] = 0;
            $result['message'] = "post type not recognized";
            return Response::json($result);
        }
        
        $result = $postRep->create();
        
        $post = PostRepository::getRepository(PostRepository::getNode($result["postId"], $companyID));
        array_push($posts, $post);
        
        $result['posts'] = $posts;
        return Response::json($result);
    }
    
    public function createCompanyPromotion(Request $request,$companyID){
        $promotions = array();
        
        $promotionId = $request->input('promotionID');
        
        if (isset($promotionId)){
            $promotionRep = new PromotionRepository($companyID);
            $promotionRep->setId($promotionId);
            
            $properties = array();
            $properties["promotion"] = $request->input('promotionMessage');
            $properties["days"] = $request->input('days');
            $properties["title"] = $request->input('title');
            $properties["price"] = $request->input('price');
            $properties["duration"] = $request->input('duration');
            $properties["units"] = $request->input('units');
            
            $promotionRep->update($properties);
            
            $promotionNode = PromotionRepository::getNode($promotionId, $companyID);
            $promotion = PromotionRepository::getRepository($promotionNode);
            array_push($promotions, $promotion);

            $result = array();
            $result['success'] = 1;
            $result['promotions'] = $promotions;
            $result['message'] = "Promotion successfully edited";
            return Response::json($result);
            
        }
        
        $promotionRep = new PromotionRepository($companyID);
        $promotionRep->setCreatedBy($request->input('createdBy'));
        $promotionRep->setPromotion($request->input('promotionMessage'));
        $promotionRep->setTime($request->input('time'));
        $promotionRep->setWeekly($request->input('weekly'));
        $promotionRep->setOnceOff($request->input('onceOff'));
        $promotionRep->setDays($request->input('days'));
        $promotionRep->setTitle($request->input('title'));
        $promotionRep->setPrice($request->input('price'));
        $promotionRep->setDuration($request->input('duration'));
        $promotionRep->setUnits($request->input('units'));
            
        $result = $promotionRep->create();
        
        $promotionNode = PromotionRepository::getNode($result["promotionId"], $companyID);
        $promotion = PromotionRepository::getRepository($promotionNode);
        array_push($promotions, $promotion);
        
        $result['promotions'] = $promotions;
        return Response::json($result);
    }
    
    public function createCataloguePost(Request $request,$companyID){
        $cataloguePosts = array();
        
        $cataloguePostId = $request->input('cataloguePostID');
        
        if (isset($cataloguePostId)){
            $cataloguePostRep = new CataloguePostRepository($companyID);
            $cataloguePostRep->setId($cataloguePostId);
            
            $properties = array();
            $properties["post"] = $request->input('cataloguePostMessage');
            $properties["amenities"] = $request->input('amenities');
            $properties["title"] = $request->input('title');
            $properties["price"] = $request->input('price');
            $properties["duration"] = $request->input('duration');
            $properties["units"] = $request->input('units');
            
            $cataloguePostRep->update($properties);
            
            $cataloguePostNode = CataloguePostRepository::getNode($cataloguePostId, $companyID);
            $cataloguePost = CataloguePostRepository::getRepository($cataloguePostNode,$companyID);
            array_push($cataloguePosts, $cataloguePost);

            $result = array();
            $result['success'] = 1;
            $result['posts'] = $cataloguePosts;
            $result['message'] = "CataloguePost successfully edited";
            return Response::json($result);
        }
        
        $cataloguePostRep = new CataloguePostRepository($companyID);
        $cataloguePostRep->setCreatedBy($request->input('createdBy'));
        $cataloguePostRep->setPost($request->input('cataloguePostMessage'));
        $cataloguePostRep->setTime($request->input('time'));
        $cataloguePostRep->setAmenities($request->input('amenities'));
        $cataloguePostRep->setTitle($request->input('title'));
        $cataloguePostRep->setPrice($request->input('price'));
        $cataloguePostRep->setDuration($request->input('duration'));
        $cataloguePostRep->setUnits($request->input('units'));
            
        $result = $cataloguePostRep->create();
        
        $cataloguePostNode = CataloguePostRepository::getNode($result["postId"], $companyID);
        $cataloguePost = CataloguePostRepository::getRepository($cataloguePostNode,$companyID);
        array_push($cataloguePosts, $cataloguePost);
        
        $result['posts'] = $cataloguePosts;
        return Response::json($result);
    }
    
    public function uploadCatalogueFile(Request $request,$companyId,$postId){
        
        $file = $request->file('file');
        $result = CataloguePostRepository::uploadFile($file, $companyId, $postId);
        
        return Response::json($result);
    }
    
    public function deleteCompanyPromotion($companyID,$promotionID){
        
        $promotionRep = new PromotionRepository($companyID);
        $promotionRep->setId($promotionID);
        
        $result = PromotionRepository::delete($promotionID,$companyID);
        
        return Response::json($result);
    }
    
    public function deleteCataloguePost($companyID,$cataloguePostID){
        
        $cataloguePostRep = new CataloguePostRepository($companyID);
        $cataloguePostRep->setId($cataloguePostID);
        
        $result = CataloguePostRepository::delete($cataloguePostID,$companyID);
        
        return Response::json($result);
    }
    
    public function uploadCompanyPostPicture(Request $request,$companyID,$postID){
        
        try{
            $post = new PostRepository($companyID);
            $post->setId($postID);
            
            $file = $request->file('picture');
            
            $result = $post->uploadPicture($file);
            
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
    }
    
    public function deleteFile(Request $request,$companyID){
        $url = $request->input("url");
        $id=$request->input("id");
        $type=$request->input("type");
        
        $result = CompanyRepository::deleteFile($companyID,$url,$type,$id);
        return Response::json($result);
    }
    
    public function uploadCataloguePostPicture(Request $request,$companyID,$postID){
        
        try{
            $post = new CataloguePostRepository($companyID);
            $post->setId($postID);
            
            $file = $request->file('picture');
            
            $result = $post->uploadPicture($file);
            
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
        
    }
    
    public function uploadCompanyPromotionPicture(Request $request,$companyID,$promotionID){
        
        try{
            $promotion = new PromotionRepository($companyID);
            $promotion->setId($promotionID);
            
            $file = $request->file('picture');
            
            $result = $promotion->uploadPicture($file);
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
    }
    
    public function uploadCompanyProfilePicture(Request $request,$companyID){
        
        try{
            $companyRep = CompanyRepository::getRepository($companyID);
            
            $file = $request->file('picture');
            
            $result = $companyRep->uploadProfilePicture($file);
            
            //echo json_encode($result);
            return Response::json($result);
        }catch (Exception $ex){
            $result = array();
            $result['success'] = 0;
            $result['message'] = "File upload not found";
            
            return Response::json($result);
        }
        
    }
    
    public function setCompanyLocation(Request $request,$companyID){
        
        $locationRep = new LocationRepository($companyID);
        $locationRep->setLatitude($request->input('latitude'));
        $locationRep->setLongitude($request->input('longitude'));
        $result = $locationRep->create();
        
        return Response::json($result);
    }
    
    public function blockCompanyFollower($companyID,$userID)
    {
        FollowerRepository::blockFollower($userID, $companyID);
        
        $result['success'] = 1;
        $result['message'] = "Follower has been blocked";
        
        if (is_null(Auth::id())){
            return Response::json($result);
        }else{
            return redirect('followers');
        }
    }
    
    public function unblockCompanyFollower($companyID,$userID){
        
        FollowerRepository::unBlockFollower($userID, $companyID);
        
        $result['success'] = 1;
        $result['message'] = "FollowerRepository has been unblocked";
        
        if (is_null(Auth::id())){
            return Response::json($result);
        }else{
            return redirect('followers');
        }
    }
    
    public function followers()
    {
        if (!$this->authorizedMenu('followers')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $result = FollowerRepository::getFollowers($id);
        $data = $result['followers'];
        return view('follower')->with(['data'=>$data]);
    }
    
    public function settings()
    {
        if (!$this->authorizedMenu('settings')){
            return back();
        }
        
        $id = $this->getCompanyId();
        
        $company = CompanyRepository::getRepository($id,null,false);

        $businessHoursNode = BusinessHoursRepository::getBusinessHoursNode($id);
        
        if (is_null($businessHoursNode))
        {
            $businessHours = new BusinessHoursRepository($id);
            $businessHours->create();

            $businessHoursNode = BusinessHoursRepository::getBusinessHoursNode($id);
        }
        
        $monTimes = $businessHoursNode->mon;
        $tueTimes = $businessHoursNode->tue;
        $wedTimes = $businessHoursNode->wed;
        $thuTimes = $businessHoursNode->thu;
        $friTimes = $businessHoursNode->fri;
        $satTimes = $businessHoursNode->sat;
        $sunTimes = $businessHoursNode->sun;

        $data = array(
            'id' => $company->companyId,
            'address' => $company->address,
            'email' => $company->email,
            'about' => $company->about,
            'phoneNumber' => $company->phoneNumber,
            'sector' => $company->sector,
            'logTime' => $company->logTime,
            'deliveries' => $company->deliveries,
            'discoverRoutes' => $company->discoverRoutes,
            'routeClustering' => $company->routeClustering,
            'type' => $company->type,
            'countryCode' => $company->countryCode,
            'profilePicture' => $company->profilePicture,
            //Check LocationRepository model class. That is the one returned here if there
            //is a location for the company. Otherwise this is null
            //'locationLat'=>$company->location->getLatitude(),
            //'locationLng'=>$company->location->getLongitude(),
            'location' => $company->location,
            'hours'=> array(

                'monBegin' =>  strstr($monTimes, ',', True),
                'monEnd' => substr( $monTimes,  strpos($monTimes, ",") + 1 ),

                'tueBegin' => strstr($tueTimes, ',', True),
                'tueEnd' => substr( $tueTimes,  strpos($tueTimes, ",") + 1 ),

                'wedBegin' => strstr($wedTimes, ',', True),
                'wedEnd' => substr( $wedTimes,  strpos($wedTimes, ",") + 1 ),

                'thuBegin' => strstr($thuTimes, ',', True),
                'thuEnd' => substr( $thuTimes,  strpos( $thuTimes, ",") + 1 ),

                'friBegin' => strstr($friTimes, ',', True),
                'friEnd' => substr($friTimes,  strpos($friTimes, ",") + 1 ),

                'satBegin' => strstr($satTimes, ',', True),
                'satEnd' => substr( $satTimes,  strpos($satTimes, ",") + 1 ),

                'sunBegin' => strstr($sunTimes, ',', True),
                'sunEnd' => substr( $sunTimes,  strpos($sunTimes, ",") + 1 ),
                ),
            );

       // echo json_encode($result);
        return view('settings')->with(['data'=>$data]);
    }
    
    public function saveSettings(Request $request)
    {
        $id = $this->getCompanyId();
        $file = Input::file('image');
        $companyRep = CompanyRepository::getRepository($id);
        //S$countryCode = $request->input('location_countrycode');
        $locationAddress = $request->input('location_address');
        $locationLatitude = $request->input('location_latitude');
        $locationLongitude = $request->input('location_longitude');
        
        $businessHoursNode = BusinessHoursRepository::getBusinessHoursNode($id);
        if ($request->input('monBegin')!== null){
            $businessHoursNode->mon = $request->input('monBegin').','.$request->input('monEnd');
        }
        if ($request->input('tueBegin')!== null){
            $businessHoursNode->tue = $request->input('tueBegin').','.$request->input('tueEnd');
        }
        if ($request->input('wedBegin')!== null){
            $businessHoursNode->wed = $request->input('wedBegin').','.$request->input('wedEnd');
        }
        if ($request->input('thuBegin')!== null){
            $businessHoursNode->thu = $request->input('thuBegin').','.$request->input('thuEnd');
        }
        if ($request->input('friBegin')!== null){
            $businessHoursNode->fri = $request->input('friBegin').','.$request->input('friEnd');
        }
        if ($request->input('satBegin')!== null){
            $businessHoursNode->sat = $request->input('satBegin').','.$request->input('satEnd');
        }
        if ($request->input('sunBegin')!== null){
            $businessHoursNode->sun = $request->input('sunBegin').','.$request->input('sunEnd');
        }
        $businessHoursNode->save();
        
        $properties = array();
        if ($request->input('sector')!== null)
        {
            $properties['sector'] = $request->input('sector');
        }
        
        if ($request->input('logTimeValue')!== null){
            $logTime = $request->input('logTimeValue');
            if ($logTime === "on"){
                $properties['logTime'] = "true";
            }
        }else{
            $properties['logTime'] = "false";
        }
        
        if ($request->input('deliveriesValue') !== null){
            $deliveries = $request->input('deliveriesValue');
            //dd("deliveries set".$deliveries);
            if ($deliveries === "on"){
                $properties['deliveries'] = "true";
            }
        }else{
            //dd("deliveries not set");
            $properties['deliveries'] = "false";
        }
        
        if ($request->input('discoverRoutes') !== null){
            $discoverRoutes = $request->input('discoverRoutes');
            //dd("deliveries set".$deliveries);
            if ($discoverRoutes === "on"){
                $properties['discoverRoutes'] = "true";
            }
        }else{
            //dd("deliveries not set");
            $properties['discoverRoutes'] = "false";
        }
        
        if ($request->input('routeClustering') !== null){
            $routeClustering = $request->input('routeClustering');
            //dd("deliveries set".$deliveries);
            if ($routeClustering === "on"){
                $properties['routeClustering'] = true;
            }
        }else{
            //dd("deliveries not set");
            $properties['routeClustering'] = false;
        }
        
        if ($request->input('email')!== null){
            $properties['email'] = $request->input('email');
        }
        
        if ($request->input('about')!== null){
            $properties['about'] = $request->input('about');
        }
        
        if ($request->input('phoneNumber')!== null){
            $properties['phoneNumber'] = $request->input('phoneNumber');
        }
        
        if ($request->input('countryCode')!== null){
            $properties['countryCode'] = $request->input('countryCode');
        }
        
        if ($request->input('address')!== null){
            $properties['address'] = $request->input('address');
        }
        
        if ($request->input('type')!== null){
            $properties['type'] = $request->input('type');
        }
        
        $companyRep->update($properties);
        
        if (!empty($locationAddress) && !empty($locationLatitude) && !empty($locationLongitude)){
            $locationRep = new LocationRepository($id);
            $locationRep->setLatitude($locationLatitude);
            $locationRep->setLongitude($locationLongitude);
            $locationRep->create();
            
            $locationRep->update(array("address" => $locationAddress));
        }
        
        if (!is_null($file)){
            $companyRep = CompanyRepository::getRepository($id);
            $companyRep->uploadProfilePicture($file);
        }
            
        return redirect('settings');
    }
    
    public function replyNotification(Request $request){
        $companyId = $this->getCompanyId();
        $toCompanyId = $request->input('companyId');
        $notificationId = $request->input('notificationId');
        $reply = $request->input('reply');
        
        $notificationRep = new NotificationRepository($companyId,$toCompanyId);
        $notificationRep->notification = $reply;
        $notificationRep->create();
        
        if (isset($notificationId)){
            NotificationRepository::delete($notificationId);
        }
        
        return back();
    }
    
    public function deleteNotification(Request $request){
        $notificationId = $request->input('notificationId');
        
        if (isset($notificationId)){
            NotificationRepository::delete($notificationId);
        }
        
        return back();
    }
    
    public function approveCompany(Request $request){
        $companyId = $request->input('id');
        $companyRep = CompanyRepository::getRepository($companyId);
        $companyRep->update(array('approved' => "true"));

        return back()->with(['data'=>$this->getCompanies()]);
    }
    
    public function disapproveCompany(Request $request){
        $companyId = $request->input('id');
        $companyRep = CompanyRepository::getRepository($companyId);
        $companyRep->update(array('approved' => "false"));

        return back()->with(['data'=>$this->getCompanies()]);
    }
    
    public function deleteCompany(Request $request){
        $companyId = $request->input('id');
        CompanyRepository::delete($companyId);
        return back()->with(['data'=>$this->getCompanies()]);
    }
    
    public function companyApprovals(){
        return view('approve_companies')->with(['data'=>$this->getCompanies()]);
    }
    
    public function getCompanies()
    {
        $result = CompanyRepository::getCompanies();

        $data = $result["companies"];
        return $data;
    }
}

<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Http\Helpers\Mailer;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendResetLinkEmail(Request $request)
    {
        //echo base_path('vendor/phpmailer/phpmailer/class.pop3.php');

         Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomValidator($translator, $data, $rules, $messages);
        });

        $this->validate($request, ['email' => 'required|email|RegisteredEmail']);

        $toAddress =$request->input('email');
        //new password generator
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $length = 8;
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        $company = CompanyRepository::getCompanyByValue('email', $toAddress);
        $company->update(array('password_token' => $randomString));
        
        $mailer = new Mailer();
        $mailer->setToAddress($toAddress);
        $mailer->setSubject("NeceCity Password Reset");
        $mailer->setBody("please click on the link to reset password <html>"
                . "<a href='".url('password/reset/'.$randomString)."'>"
                . "Link</a></html>. Link will be valid for one hour") ;
        
        $result = $mailer->sendEmail();
        
        if ($result["success"] === 1){
            $response = "Password reset sent.";
        }else{
            $response = "Could not send password rest. Please try again.";
        }
        
        return back()->with('status', trans($response));
    }
}

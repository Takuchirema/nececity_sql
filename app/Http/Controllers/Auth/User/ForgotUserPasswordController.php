<?php namespace App\Http\Controllers\Auth\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\CustomValidator;
use App\Repositories\UserRepository;
use POP3;
use PHPMailer;

class ForgotUserPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showLinkRequestForm()
    {
        return view('auth.passwords.emailuser');
    }
    
    public function sendResetLinkEmail(Request $request)
    {
        //echo base_path('vendor/phpmailer/phpmailer/class.pop3.php');

        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomValidator($translator, $data, $rules, $messages);
        });

        $this->validate($request, ['email' => 'required|email|RegisteredUserEmail']);

        $toAddress =$request->input('email');
        //new password generator
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $length = 8;
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        
        $user = UserRepository::getCompanyByValue('email', $toAddress);
        $user->update(array('password_token' => $randomString));
        
        $mailer = new Mailer();
        $mailer->setToAddress($toAddress);
        $mailer->setSubject("NeceCity Password Reset");
        $mailer->setBody("please click on the link to reset password <html>"
                . "<a href='".url('password/user/reset/'.$randomString)."'>"
                . "Link</a></html>. Link will be valid for one hour") ;
        
        $result = $mailer->sendEmail();
        
        if ($result["success"] === 1){
            $response = "Password reset sent.";
        }else{
            $response = "Could not send password rest. Please try again.";
        }
        
        return back()->with('status', trans($response));
    }
}

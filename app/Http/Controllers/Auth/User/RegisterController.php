<?php namespace App\Http\Controllers\Auth\User;

//use App\UserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Auth\UserRepository\CustomUserValidator;
use App\Repositories\UserRepository;
use App\Http\Helpers\Constants;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/company_routes';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showRegistrationForms()
    {
        return view('auth_user.registeruser');

        //return view('loginuser', null);
        //return $this->loginForm();
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        echo print_r($data);
        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomUserValidator($translator, $data, $rules, $messages);
        });
       
        return Validator::make($data, [
             'name' => 'required|max:255|RegisteredUsername',
             'email' => 'required|max:255|RegisteredEmail',
             'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return UserRepository
     */
    
    protected function create(array $data)
    {
        //password_hash($data['password'], PASSWORD_BCRYPT);
        $hashPassword = password_hash($data['password'], PASSWORD_BCRYPT);
        
        $user = new UserRepository($data['name']);
        
        $user->setEmail($data['email']);
        $user->setPhoneNumber('');
        $user->setPassword($hashPassword);
        $user->setStatus('offline');
        
        $user->create();
        $this->followDefaultCompanies($user->userId);
        
        return $user;
    }
    
     public function followDefaultCompanies($userID){
        
        $companies = Company::query()
                    ->where('sector','=','Transportation')
                    ->where('type','=','public')
                    ->get();
        
        foreach ($companies as $company){
            $this->followCompany($userID, $company->companyId);
        }
    }
    
    public function followCompany($userID,$companyID){
        $result = array();
        
        $follower = new FollowerRepository($userID);
        $follower->companyId = $companyID;
        $follower->create();  
        
        $result["versions"] = array();
        $result["versions"] = Constants::getVersions();
        $result["admin_versions"] = Constants::getAdminVersions();
        return Response::json($result);
    }
}

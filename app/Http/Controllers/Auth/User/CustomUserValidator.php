<?php namespace App\Http\Controllers\Auth\User;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Auth;
use App\Repositories\CompanyRepository;
//use Response;

class CustomUserValidator extends \Illuminate\Validation\Validator {

    public function validateUniqueCompany($attribute, $value, $parameters)
    {
        //Coming in as name. Make name the id so instead of company.name -> company.id
    	$map = array(
            "name" => "companyId",
            "email" => "email",
	);
        
        $company = CompanyRepository::getRepository($map[$attribute],$value);
        
        if ($company){
            return false;
        }else{
            return true;
        }
    }
    
    public function validateLogin($attribute, $value, $parameters)
    {
        $request = $value[0];
        
        if (Auth::guard('web_user')->attempt(['username' =>  $request->input('username'), 'password' => $request->input('password') ])) {
            return true;
        }else{
            $user = UserRepository::getUser($request->input('username'));
            
            if (!$user)
            {
                $this->errors()->add('name', 'user not found');
            }else{
                $this->errors()->add('password','password is incorrect', PASSWORD_BCRYPT);
            }
            return false;
        }
    }
    
    protected function replaceUniqueCompany($message, $attribute, $rule, $parameters)
    {
        return $attribute." already taken";
    }

    public function validateRegisteredUsername($attribute, $value, $parameters)
    {
        $user = UserRepository::getUser($value);
        
        if ($user){
            return false;
        }else{
            return true;
        }
    }
    
    protected function replaceRegisteredUsername($message, $attribute, $rule, $parameters)
    {
        return "Username taken";
    }
    
    public function validateRegisteredEmail($attribute, $value, $parameters)
    {
        $user = UserRepository::getUserByValue('email',$value);
        if ($user){
            return false;
        }else{
            return true;
        }  
    }
}
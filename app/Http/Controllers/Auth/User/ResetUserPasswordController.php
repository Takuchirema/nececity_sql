<?php namespace  App\Http\Controllers\Auth\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Repositories\UserRepository;
use App\Models\User;

class ResetUserPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.resetuser')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $email = $request->input("email");
        $password_token = $request->input("token");
        $new_password = $request->input("password");
        $hashPassword = password_hash($new_password, PASSWORD_BCRYPT);

        
        $user = User::query()
                ->where('email','=',$email)
                ->where('password_token','=',$password_token)
                ->first();
        
        if($user)
        {       
            $user->password = $hashPassword;
            $user->salt = null;
            $user->save();
            
            $response= "password reset successfully";
            return back()->with('status', trans($response));
        } else
        {
            $response = "token incorrect";
            return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
        }
    }
}

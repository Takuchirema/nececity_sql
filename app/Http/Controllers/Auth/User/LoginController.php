<?php namespace App\Http\Controllers\Auth\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Auth\User\CustomUserValidator;
use Illuminate\Support\Facades\Redirect;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  //  use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        
    }
    use AuthenticatesUsers {
        showLoginForm as loginForm;
    }
    
    public function login(Request $request)
    {
        $data['request'] = array();
        array_push($data['request'], $request);
     
        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new CustomUserValidator($translator, $data, $rules, $messages);
        });
        
        $validator = Validator::make($data, [
             'request' => 'login',
        ]);
        
        if ($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }else{
            return Redirect('company_routes');
        }
    }

    public function showLoginForm()
    {
        return view('auth_user.loginuser');
    }
}


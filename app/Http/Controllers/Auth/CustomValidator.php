<?php namespace App\Http\Controllers\Auth;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Auth;
use App\Repositories\CompanyRepository;
use App\Repositories\EmployeeRepository;
//use Response;

class CustomValidator extends \Illuminate\Validation\Validator {

    public function validateUniqueCompany($attribute, $value, $parameters)
    {
        if ($attribute === "name"){
            $company = CompanyRepository::getRepository(null,$value);
        }
        
        if ($company){
            $this->errors()->add('name', 'A company with this name already exists.');
            return false;
        }else{
            return true;
        }
    }
    
    public function validateLogin($attribute, $value, $parameters)
    {   
        $request = $value[0];
        $username = $request->input('username');
        $companyname = $request->input('companyname');
        
        if (!isset($username) || empty(trim($username))){
            if (Auth::attempt(['companyname' =>  $request->input('companyname'), 'password' => $request->input('password')])) {
                return true;
            }
        }else{
            if (Auth::guard('web_employee')->attempt(['username' =>  $request->input('username'), 'companyname' =>  $request->input('companyname'), 'password' => $request->input('password')])) {
                return true;
            }
        }
            
        if (!isset($username) || empty(trim($username))){
            $company = CompanyRepository::getRepository(null,$companyname);

            if (!$company)
            {
                $this->errors()->add('companyname', 'company not found');
            }else{
                $this->errors()->add('password','password is incorrect', PASSWORD_BCRYPT);
            }

            return false;
        }

        $employee = EmployeeRepository::getRepository(null, null,$companyname,$username);

        if (!$employee)
        {
            $this->errors()->add('username', 'user not found');
        }else{
            $this->errors()->add('password','password is incorrect', PASSWORD_BCRYPT);
        }

        return false;
    }
    
    protected function replaceUniqueCompany($message, $attribute, $rule, $parameters)
    {
        return $attribute." already taken";
    }


      public function validateRegisteredEmail($attribute, $value, $parameters)
    {
        $company = CompanyRepository::getCompanyByValue('email',$value);
        if (!$company){
            return false;
        }else{
            return true;
        }  
    }
    
    protected function replaceRegisteredEmail($message, $attribute, $rule, $parameters)
    {
        return "Email not registered on our system.";
    }
    
    public function validateRegisteredUsername($attribute, $value, $parameters)
    {
        $user = UserRepository::getUser($value);
        
        if ($user){
            return false;
        }else{
            return true;
        }
    }
    
    protected function replaceRegisteredUserEmail($message, $attribute, $rule, $parameters)
    {
        return "Email not registered on our system as a user";
    }
}
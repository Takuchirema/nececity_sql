<?php namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class AdminPrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('web_company')->check()){
            $companyName = Auth::guard('web_company')->user()->companyName;
            
            if (strtolower($companyName) === strtolower(config('app.name'))){
                return $next($request);
            }
        }
        
        if (Auth::guard('web_employee')->check()){
            return back();
        }
        
        return redirect('/login');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$server = var_export($request->server, true);
        //Log::debug('Request Header - '.json_encode($request->headers->all()));
        //Log::debug('Request Body - '.json_encode($request->getContent()));
        //Log::debug('Request Server - '.$server);
        return $next($request);
    }
}

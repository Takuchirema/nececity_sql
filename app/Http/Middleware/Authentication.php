<?php namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('web_company')->check()){
            return $next($request);
        }
        
        if (Auth::guard('web_employee')->check()){
            return $next($request);
        }
        
        if (Auth::guard('web_user')->check()){
            return $next($request);
        }
        
        return redirect('/login');
    }
}

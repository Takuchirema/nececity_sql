<?php

namespace App\Http\Middleware;

use App\Repositories\UserRepository;
use App\Repositories\EmployeeRepository;
use App\Repositories\CompanyRepository;

use Response;
use Closure;

class ApiAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userID = $request->header('userID');
        $employeeID = $request->header('employeeID');
        $companyID =  $request->header('companyID');
        
        if ($userID !== null){
            $token = $request->header('token');
            $userNode = UserRepository::getUserNode($userID,null);
        }else if ($employeeID !== null){
            $token = $request->header('token');
            $companyID = $request->header('companyID');
            $userNode = EmployeeRepository::getNode($employeeID,$companyID);
        }
        else if($companyID !==null )
        {
            $token = $request->header('token');
            $companyID = $request->header('companyID');
            $userNode = CompanyRepository::getNode( $companyID);
        }
        
        if (isset($userNode)){
            $userToken = $userNode->token;
            
            if (strcmp(trim($userToken), trim($token)) !== 0){
                $result["success"] = 0;
                $result["message"] = "User Not Authorized";
                return Response::json($result);
            }
        }else{
            $result["success"] = 0;
            $result["message"] = "Other Not Authorized ".$userID;
            return Response::json($result);
        }
        
        $response = $next($request);
        $content = $response->content();

        return response(gzencode($content, 9))->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods'=> '*',
            'Content-type' => 'application/json; charset=utf-8',
            'Content-Encoding' => 'gzip'
        ]);
    }
}

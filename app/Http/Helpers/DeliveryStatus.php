<?php namespace App\Http\Helpers;

/**
 * Description of DeliveryStatus
 *
 * @author Takunda
 */
abstract class DeliveryStatus {
    
    const Pending = "Pending";
    const InTransit = "In transit";
    const Delivered = "Delivered";
    const Cancelled = "Cancelled";
}

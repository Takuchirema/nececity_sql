<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Helpers;

use PHPMailer;
/**
 * Description of Mailer
 *
 * @author Takunda
 */
class Mailer {
    var $fromAddress;
    var $toAddress;
    var $subject;
    var $body;
    
    //put your code here
    public function sendEmail(){
        
        if (!isset($this->toAddress)){
            $results['success']=0;
            $results['message']="Please set the to email addresses.";
            return $results;
        }
        
        $mail = $this->configureEmail();
        try{
            $mail->send();
        }
        catch (Exception $e) {
            $results['success']=0;
            $results['message']="Failed to send the email. Please try again.";
            $results['error'] = $e->errorMessage();
            
            return $results;
        }
        
        $results['success']=1;
        $results['message']="Email successfully sent";
        return $results;
    }
    
    function configureEmail(){
        $mail = new PHPMailer(true);
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = env('MAIL_HOST', 'smtp');
        $mail->Port = env('MAIL_PORT', 587);
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = env('MAIL_ENCRYPTION', 'tls');
        $mail->Username = env('MAIL_USERNAME',"nececity.southafrica@gmail.com");
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        
        //uivniipdinbgxzfb
        $mail->Password = env('MAIL_PASSWORD',"uivniipdinbgxzfb");
        $mail->setFrom(env('MAIL_FROM_ADDRESS','nececity.southafrica@gmail.com'));
        $mail->addReplyTo(env('MAIL_REPLY_TO','nececity.southafrica@gmail.com'));
        $mail->addAddress($this->toAddress);
        $mail->Subject = $this->subject;
        $mail->isHTML(true) ;
        $mail->AltBody = 'This is a plain-text message body';
        $mail->Body = $this->body;
        return $mail;
    }
    
    function getFromAddress() {
        return $this->fromAddress;
    }

    function getToAddress() {
        return $this->toAddress;
    }

    function getSubject() {
        return $this->subject;
    }

    function setFromAddress($fromAddress) {
        $this->fromAddress = $fromAddress;
    }

    function setToAddress($toAddress) {
        $this->toAddress = $toAddress;
    }

    function setSubject($subject) {
        $this->subject = $subject;
    }
    
    function getBody() {
        return $this->body;
    }

    function setBody($body) {
        $this->body = $body;
    }
}

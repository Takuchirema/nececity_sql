<?php namespace App\Http\Helpers;

use App\Repositories\AppVariableRepository;
/**
 * Description of RelationshipType
 *
 * @author Takunda Chirema
 */
class Constants {
    const clusterRefreshTime = 10; // 10 seconds
    const clusterMinDistance = 100; // 100 metres. The min distance between clusters.
    const clusterMinDirection = 360; // 360 degrees bearing. The difference bearing for each point in cluster < 120.
    const busStopWaitingTime = 2; // 3 mins.
    const locationUpdateTime = 20; // 20 seconds before we remove update from calculations.
    const deviceUpdateInterval = 300; // At most 5 minutes for gps trackers to update.
    const routeMatchPercentage = 80; // Percentage of same points to match routes
    const versions = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
    const VersionsAppVariable = "versions";
    const UseCache = false;
    const busPickingTime = 60; // Takes at least 60 secons to pick someone up.
    const busDelayTime = 15; // 15 minutes max for a bus to be delayed
    const timeTableRefreshFrequency = 30; // when time tables frequencies get higher than 10 refresh and delete unrepeated times
    const createBusStopFrequency = 10; // when a point is stopped at 10 times make it bus stop
    const minTravellingSpeed = 10; // Minimum speed to be considered moving
    const sameDirectionCutOff = 150; // The bearing at which vehicles are not in same direction
    
    public static function getVersions(){
        $versionsVariable = AppVariableRepository::getVariable("Versions");
        $integerVersions = array(0);
        if (isset($versionsVariable)){
            $integerVersions = array_map('intval', explode(',', $versionsVariable->getValue()));
        }
        return $integerVersions;
    }
    
    public static function getAdminVersions(){
        $versionsVariable = AppVariableRepository::getVariable("AdminVersions");
        $integerVersions = array(0);
        if (isset($versionsVariable)){
            $integerVersions = array_map('intval', explode(',', $versionsVariable->getValue()));
        }
        return $integerVersions;
    }
    
    public static function getBusPickingTime(){
        $busPickingTime = AppVariableRepository::getVariable("BusPickingTime");
        if (isset($busPickingTime)){
            return intval($busPickingTime->getValue());
        }
        return Constants::busPickingTime;
    }
    
    public static function getClusterRefreshTime(){
        $refreshTime = AppVariableRepository::getVariable("ClusterRefreshTime");
        if (isset($refreshTime)){
            return intval($refreshTime->getValue());
        }
        return Constants::clusterRefreshTime;
    }
    
    public static function getBusStopWaitingTime(){
        $waitingTime = AppVariableRepository::getVariable("BusStopWaitingTime");
        if (isset($waitingTime)){
            return intval($waitingTime->getValue());
        }
        return Constants::busStopWaitingTime;
    }
    
    public static function getTimeTableRefreshFrequency(){
        $frequency = AppVariableRepository::getVariable("TimeTableRefreshFrequency");
        if (isset($frequency)){
            return intval($frequency->getValue());
        }
        return Constants::timeTableRefreshFrequency;
    }
    
    public static function getCreateBusStopFrequency(){
        $frequency = AppVariableRepository::getVariable("CreateBusStopFrequency");
        if (isset($frequency)){
            return intval($frequency->getValue());
        }
        return Constants::createBusStopFrequency;
    }
    
    public static function getLocationUpdateTime(){
        $updateTime = AppVariableRepository::getVariable("LocationUpdateTime");
        if (isset($updateTime)){
            return intval($updateTime->getValue());
        }
        return Constants::locationUpdateTime;
    }
    
    public static function getClusterMinDistance(){
        $minDistance = AppVariableRepository::getVariable("ClusterMinDistance");
        if (isset($minDistance)){
            return intval($minDistance->getValue());
        }
        return Constants::clusterMinDistance;
    }
    
    public static function getClusterMinDirection(){
        $minDirection= AppVariableRepository::getVariable("ClusterMinDirection");
        if (isset($minDirection)){
            return intval($minDirection->getValue());
        }
        return Constants::clusterMinDirection;
    }
    
    public static function getRouteMatchPercentage(){
        $percentage= AppVariableRepository::getVariable("RouteMatchPercentage");
        if (isset($percentage)){
            return intval($percentage->getValue());
        }
        return Constants::routeMatchPercentage;
    }
    
    public static function getMinTravellingSpeed(){
        $minSpeed= AppVariableRepository::getVariable("MinTravellingSpeed");
        if (isset($minSpeed)){
            return intval($minSpeed->getValue());
        }
        return Constants::minTravellingSpeed;
    }
    
    public static function useCache(){
        $useCache= AppVariableRepository::getVariable("UseCache");
        if (isset($useCache)){
           if (strtolower($useCache->getValue()) == "true" || strtolower($useCache->getValue()) == "yes") {
               return true;
           }else{
               return false;
           }
        }
        return true;
    }
}
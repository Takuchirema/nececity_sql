<?php namespace App\Http\Helpers;

/**
 * Description of PostType
 *
 * @author Takunda Chirema
 */
abstract class PostType {
    
    const Admin = "admin";
    const Company = "company";
    
}

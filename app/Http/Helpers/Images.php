<?php namespace App\Http\Helpers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Images
 *
 * @author Takunda Chirema
 */
class Images {
    
    public static function compress($source, $destination) {
        $destination = str_replace('\\', '/', $destination);
        
        $info = getimagesize($source);
        $fileSize = filesize($source)/1024;
        
        $percentage = 9000/(float)$fileSize;
        
        if ($info['mime'] == 'image/jpeg'){
            $image = imagecreatefromjpeg($source);
        }elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source);
        }elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source);
        }
        
        $imageSize = Images::getImageSizeKeepAspectRatio($source,800,800);
        // Now read the image width = 200 and height = 200
        $image = imagescale($image,$imageSize['width'],$imageSize['height']);
        imagejpeg($image, $destination, ceil($percentage));
        
        imagedestroy($image);
        return $destination;
    }
    
    public static function getImageSizeKeepAspectRatio($imageUrl, $maxWidth, $maxHeight)
    {
	$imageDimensions = getimagesize($imageUrl);
	$imageWidth = $imageDimensions[0];
	$imageHeight = $imageDimensions[1];
	$imageSize['width'] = $imageWidth;
	$imageSize['height'] = $imageHeight;
	if($imageWidth > $maxWidth || $imageHeight > $maxHeight)
	{
		if ( $imageWidth > $imageHeight ) {
	    	$imageSize['height'] = floor(($imageHeight/$imageWidth)*$maxWidth);
  			$imageSize['width']  = $maxWidth;
		} else {
			$imageSize['width']  = floor(($imageWidth/$imageHeight)*$maxHeight);
			$imageSize['height'] = $maxHeight;
		}
	}
	return $imageSize;
    }
}

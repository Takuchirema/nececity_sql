<?php namespace App\Http\Helpers;

/**
 * Description of CacheType
 *
 * @author Takunda
 */
abstract class CacheType {
    
    const CompanyPosts = "company_posts";
    const CompanyCataloguePosts = "company_catalogue_posts";
    const CompanyEmployees = "company_employees";
    const CompanyPromotions = "company_promotions";
    const CompanyHours = "company_hours";
    const CompanyRoutes = "company_routes";
    const CompanyDeliveries = "company_deliveries";
    const LocationClusters = "location_clusters";
    const Companies = "companies";
    const AppVariables = "app_variables";
    const Deliveries = "deliveries";
    const Devices = "devices";
    const Users = "users";
    const GpsService = "gps_service";
    const Shifts = "shifts";
    const Schedules = "schedules";
}

<?php namespace App\Http\Helpers;

use App\Repositories\RoutePointRepository;
/**
 * Description of LatLngDetails
 *
 * @author Takunda
 */
class LatLngDetails {
    
    /* 
    * Given longitude and latitude in North America, return the address using The Google Geocoding API V3
    *
    */
    public function Get_Address_From_Google_Maps($lat, $lon) {

        $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false&key=AIzaSyCtua9sVGvDSAyzJ8eJMemCz1KphZwj14U";

        // Make the HTTP request
        $data = @file_get_contents($url);
        // Parse the json response
        $jsondata = json_decode($data,true);

        // If the json data is invalid, return empty array
        if (!$this->check_status($jsondata))   return array();

        $address = array(
            'country' => $this->google_getCountry($jsondata),
            'province' => $this->google_getProvince($jsondata),
            'city' => $this->google_getCity($jsondata),
            'suburb' => $this->google_getSuburb($jsondata),
            'street' => $this->google_getStreet($jsondata),
            'postal_code' => $this->google_getPostalCode($jsondata),
            'country_code' => $this->google_getCountryCode($jsondata),
            'formatted_address' => $this->google_getAddress($jsondata),
        );

        return $address;
    }

    /* 
    * Check if the json data from Google Geo is valid 
    */
    private function check_status($jsondata) {
        if ($jsondata["status"] == "OK") return true;
        return false;
    }

    /*
    * Given Google Geocode json, return the value in the specified element of the array
    */
    private function google_getCountry($jsondata) {
        return $this->Find_Long_Name_Given_Type("country", $jsondata["results"][0]["address_components"]);
    }
    private function google_getProvince($jsondata) {
        return $this->Find_Long_Name_Given_Type("administrative_area_level_1", $jsondata["results"][0]["address_components"], true);
    }
    private function google_getCity($jsondata) {
        return $this->Find_Long_Name_Given_Type("locality", $jsondata["results"][0]["address_components"]);
    }
    private function google_getSuburb($jsondata) {
        return $this->Find_Long_Name_Given_Type("sublocality", $jsondata["results"][0]["address_components"]);
    }
    private function google_getStreet($jsondata) {
        return $this->Find_Long_Name_Given_Type("street_number", $jsondata["results"][0]["address_components"]) . ' ' . $this->Find_Long_Name_Given_Type("route", $jsondata["results"][0]["address_components"]);
    }
    private function google_getPostalCode($jsondata) {
        return $this->Find_Long_Name_Given_Type("postal_code", $jsondata["results"][0]["address_components"]);
    }
    private function google_getCountryCode($jsondata) {
        return $this->Find_Long_Name_Given_Type("country", $jsondata["results"][0]["address_components"], true);
    }
    private function google_getAddress($jsondata) {
        return $jsondata["results"][0]["formatted_address"];
    }

    /*
    * Searching in Google Geo json, return the long name given the type. 
    * (If short_name is true, return short name)
    */
    private function Find_Long_Name_Given_Type($type, $array, $short_name = false) {
        foreach( $array as $value) {
            if (in_array($type, $value["types"])) {
                if ($short_name)    
                    return $value["short_name"];
                return $value["long_name"];
            }
        }
    }
    
    /**
    * Calculates the great-circle distance between two points, with
    * the Haversine formula.
    * @param float $latitudeFrom Latitude of start point in [deg decimal]
    * @param float $longitudeFrom Longitude of start point in [deg decimal]
    * @param float $latitudeTo Latitude of target point in [deg decimal]
    * @param float $longitudeTo Longitude of target point in [deg decimal]
    * @return float Distance between points in [m] (same as earthRadius)
    */
    public static function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $earthRadius = 6371000;
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
          cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
    
    public function Snap_Coordinates_To_Road($structuredCoordinates) {
        
        $latlngs = $structuredCoordinates['latlng1'];
                
        $count = 0;
        $path="";
        $newStructuredCoordinates = array();
        
        $newStructuredCoordinates["latlng1"] = array();
        $newStructuredCoordinates["latlng2"] = array();
        $newStructuredCoordinates["latlng3"] = array();
        $newStructuredCoordinates["time"] = array();
        $newStructuredCoordinates["time2"] = array();
        $newStructuredCoordinates["repeat"] = array();
        $newStructuredCoordinates["speed"] = array();
        $newStructuredCoordinates["bearing"] = array();
        
        foreach ($latlngs as $latlng){
            
            $path = $path.$latlng."|";
            
            $count++;
            
            if (($count % 100) == 0 || $count == count($latlngs)){
                $this->Get_Snapped_Coordinates(rtrim($path, '|'), $structuredCoordinates, $newStructuredCoordinates, true, max(0,$count-100));
                $path = "";
            }
        }
        
        return $newStructuredCoordinates;
    }
    
    public function Get_Snapped_Coordinates($path, $structuredCoordinates, &$newStructuredCoordinates, $interpolate, $startIndex){

        $interpolateStr = ($interpolate) ? 'true' : 'false';
        $url = "https://roads.googleapis.com/v1/snapToRoads?path=".$path."&interpolate=".$interpolateStr."&key=AIzaSyCtua9sVGvDSAyzJ8eJMemCz1KphZwj14U";
        
        $data = @file_get_contents($url);
        // Parse the json response
        $jsondata = json_decode($data,true);
        
        //var_dump($jsondata);
        $snappedPoints = $jsondata["snappedPoints"];
        $currentTime = "";
        $currentBearing = 0;
        for ($i=0;$i<count($snappedPoints);$i++){
            $point = $snappedPoints[$i];
            
            if (isset($point["originalIndex"])){
                $originalIndex = $startIndex + $point["originalIndex"];
            }else{
                $originalIndex = null;
            }
            $lat = $point["location"]["latitude"];
            $lng = $point["location"]["longitude"];
            
            $latlng1 = $lat.",".$lng;
            $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            $time = $currentTime;
            $bearing=$currentBearing;
            $time2 ="";
            $repeat=0;
            $speed=0;
            
            if (isset($originalIndex)){
                $currentTime = $structuredCoordinates["time"][$originalIndex];
                $currentBearing = $structuredCoordinates["bearing"][$originalIndex];
                
                $time = $structuredCoordinates["time"][$originalIndex];
                $time2 = $structuredCoordinates["time2"][$originalIndex];
                $repeat = $structuredCoordinates["repeat"][$originalIndex];
                $speed = $structuredCoordinates["speed"][$originalIndex];
                $bearing = $structuredCoordinates["bearing"][$originalIndex];
            }
            
            if (!isset($originalIndex)){
                if (in_array($latlng3,$newStructuredCoordinates["latlng3"])){
                    continue;
                }
            }
            
            array_push($newStructuredCoordinates["latlng1"],$latlng1);
            array_push($newStructuredCoordinates["latlng2"],$latlng2);
            array_push($newStructuredCoordinates["latlng3"],$latlng3);
            array_push($newStructuredCoordinates["time"],$time);
            array_push($newStructuredCoordinates["time2"],$time2);
            array_push($newStructuredCoordinates["repeat"],$repeat);
            array_push($newStructuredCoordinates["speed"],$speed);
            array_push($newStructuredCoordinates["bearing"],$bearing);
        }
        
        return $newStructuredCoordinates;
    }
}

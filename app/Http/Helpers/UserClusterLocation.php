<?php namespace App\Http\Helpers;

/**
 * Description of UserClusterLocation
 *
 * @author Takunda
 */
class UserClusterLocation {
    var $userId;
    var $latitude;
    var $longitude;
    var $direction;
    var $lastSeen;
}

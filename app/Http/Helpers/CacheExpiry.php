<?php namespace App\Http\Helpers;

/**
 * Each constant here must be exactly the same name as a CacheType constant.
 *
 * @author Takunda
 */
abstract class CacheExpiry {
    
    const CompanyPosts = 60; //One hour
    const CompanyCataloguePosts = 60; //One hour
    const CompanyEmployees = 1; //One Minute
    const CompanyPromotions = 60; //One hour
    const CompanyHours = 60; //One hour
    const CompanyRoutes = 60; //One hour
    const CompanyDeliveries = 1;
    const LocationClusters = 1; //One minute
    const Companies = 60; //One hour
    const AppVariables = 60; // One hour
    const Users = 30; // 30 minutes
    const Deliveries = 1;
    const Devices = 600;
    const GpsService = 3600;
    const Shifts = 2880;
    const Schedules = 3600;
}

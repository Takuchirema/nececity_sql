<?php namespace App\Http\Helpers;


/**
 * Description of DeleteFolder
 *
 * @author Takunda Chirema
 */
class DeleteFolder {
    
    public static function delTree($dir) {
        if (!file_exists($dir)){
            return;
        }
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
            (is_dir("$dir/$file")) ? DeleteFolder::delTree("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    } 
    
    public static function deleteFiles($dir){
        if (!file_exists($dir)){
            return;
        }
        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file)){
            //dd("deleting file ".$file);
            unlink($file); // delete file
          }
        }
    }
}

<?php namespace App\Http\Helpers;

/**
 * Description of CacheType
 *
 * @author Takunda
 */
abstract class ObjectType {
    const Post = "post";
    const CataloguePost = "catalogue_post";
    const Promotion = "promotion";
    const Employee = "employee";
    const Company = "company";
}

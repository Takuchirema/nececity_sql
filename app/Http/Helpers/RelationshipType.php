<?php 
namespace App\Http\Helpers;

/**
 * Description of RelationshipType
 *
 * @author Takunda Chirema
 */
abstract class RelationshipType {
    
    const Requested = "requested";
    const Visibility = "visibility";
    const FriendsWith = "friendsWith";
    const Following = "following";
    const HasBusinessHours = "hasBusinessHours";
    const HasLocation = "hasLocation";
    const HasSettings = "hasSettings";
    const HasAlert = "hasAlert";
    const LastPost="lastPost";
    const LastPromotion="lastPromotion";
    const SeenPost = "seenPost";
    const SeenAlert = "seenAlert";
    const Likes = "likes";
    const HasStop = "hasStop";
    const SeenPromotion = "seenPromotion";
    const PreviousPost = "previousPost";
}

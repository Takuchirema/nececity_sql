<?php

$ip_address = "0.0.0.0";
$port = "7331";

$GLOBALS["id_lengths"] = array(10,15);

// open a server on port 7331
$server_socket = stream_socket_server("tcp://$ip_address:$port", $errno, $errorMessage);

if ($server_socket === false) {
    die("stream_socket_server error: $errorMessage");
}

$client_sockets = array();
$client_ids = array();

while (true) {
    // First we want to read all client sockets that have connected to us.
    $read_sockets = $client_sockets;
    // Second we want to read server socket for any incoming connections.
    $read_sockets[] = $server_socket;

    // The socket that has data is the one which will remain in $read_sockets.
    // All the others will be removed by stream_select.
    // That means count of $read_sockets after this block will be 1.
    // It will either be the server_socket or one of the client sockets.
    $ready = stream_select($read_sockets, $write, $except, 300000);
    
    if ($ready === false || $ready === 0){
        die('stream_select error.');
    }

    // If the server socket is the one with data, then it is a new client that wants a connection.
    if(in_array($server_socket, $read_sockets)) {
        $new_client = stream_socket_accept($server_socket);

        if ($new_client) {
            //print remote client information, ip and port number
            //echo 'new connection: ' . stream_socket_get_name($new_client, true) . "\n";

            $client_sockets[] = $new_client;
            //echo "total clients: ". count($client_sockets) . "\n";

            // $output = "hello new client.\n";
            // fwrite($new_client, $output);
        }

        //After new client reading and being put into connections just continue.
        continue;
    }

    
    // Iterate all client connections so that we close and remove all those that are in-active.
    foreach ($client_sockets as $socket) {
        $data = fread($socket, 2000);
        
        //echo "data: ".$data." - ".stream_socket_get_name($socket, true)."\n";
        $response = "";		

        if (substr($data,0,3)==="*HQ"){
            $location_info = explode( '#', $data);
            foreach ($location_info as $location){
                $data_array = explode( ',', $location);
                h02_protocol($data_array);
            }
        }else{
            $location_info = explode( ';', $data);
            foreach ($location_info as $location){
                $data_array = explode( ',', $location);
                tk103_protocol($data_array);
            }
        }

        if (!$data) {
            unset($client_sockets[array_search($socket, $client_sockets)]);
            @fclose($socket);
            //echo "client disconnected. total clients: ". count($client_sockets) . "\n";
            continue;
        }

        if (isset($response) && strlen($response) > 0) {
            fwrite($socket, $response);
        }
    }
}

/**
 * Will handle this protocol. Sample data:
 * imei:865011030124284,ac alarm,190804103129,,F,103130.00,A,3357.42617,S,01832.70082,E,,;
 * ##,imei:359710049095095,A -> this requires a "LOAD" response
 * 359710049095095 -> heartbeat requires "ON" response
 * @param type $data_array
 * @return type
 */
function tk103_protocol($data_array){
    $response = null;
    switch (count($data_array)) {
        case 1:
            $response = "ON";
            //echo "sent ON to client\n";
            $id = preg_replace("/[^0-9]/", "", $data_array[0]);
            sendSocketInfo($id,"heart_beat");
            break;
        case 3: 
            if ($data_array[0] == "##") {
                $response = "LOAD";
                //echo "sent LOAD to client\n";
            }
            
            $id = explode( ':', $data_array[1])[1];
            sendSocketInfo($id, "new_connection");
            break;
        default:
            if (count($data_array) <= 10){
                break;
            }
            
            $id = substr($data_array[0], 5);
                
            if (!floatval($data_array[7]) || !floatval($data_array[9])){
                sendSocketInfo($id,"heart_beat");
                break;
            }

            $alarm = $data_array[1];
            $gps_time = tk103_time($data_array[2]);
            $latitude = degree_to_decimal($data_array[7], $data_array[8]);				
            $longitude = degree_to_decimal($data_array[9], $data_array[10]);

            if (abs($longitude) < 1 || abs($latitude) < 1){
                sendSocketInfo($id,"heart_beat");
                break;
            }

            $speed_in_knots = $data_array[11];
            $speed_in_kph = 1.852 * $speed_in_knots;
            $bearing = $data_array[12];

            if ($alarm == "help me") {
                $response = "**,imei:" + $id + ",E;";
            }

            sendLocationInfo($id, $gps_time, $latitude, $longitude, $speed_in_kph, $bearing);
            break;
    }
    
    return $response;
}

/**
 * Will handle h02 protocol. Sample data:
 * //*HQ,9170946442,V1,130943,A,1750.2293,S,03100.4486,E,000.00,000,240819,FFFFBBFF,648,01,199,26953#
 * @param type $data_array
 */
function h02_protocol($data_array){
    switch (count($data_array)) {
        case 3:
            //echo "h02 heart beat\n";
            $id = $data_array[1];
            sendSocketInfo($id,"heart_beat");
            break;
        default:
            if (count($data_array) <= 10){
                break;
            }
            
            $id = $data_array[1];
                
            if (!floatval($data_array[5]) || !floatval($data_array[7])){
                sendSocketInfo($id,"heart_beat");
                break;
            }

            $gps_time = h02_time($data_array[3],$data_array[11]);
            $latitude = degree_to_decimal($data_array[5], $data_array[6]);				
            $longitude = degree_to_decimal($data_array[7], $data_array[8]);

            if (abs($longitude) < 1 || abs($latitude) < 1){
                sendSocketInfo($id,"heart_beat");
                break;
            }

            $speed_in_knots = $data_array[9];
            $speed_in_kph = 1.852 * $speed_in_knots;
            $bearing = $data_array[10];

            sendLocationInfo($id, $gps_time, $latitude, $longitude, $speed_in_kph, $bearing);
            break;
    }
}

function isEmptyOrNullString($str){
    return (!isset($str) || trim($str) === '');
}

function sendSocketInfo($id, $info){
    
    if (!isset($id) || !in_array(strlen($id),$GLOBALS["id_lengths"])){
        return;
    }
    
    $url = 'http://127.0.0.1/api/gps/service';
    $data = array(  
            'id' => $id,
            'time' => null,
            'latitude' => null,
            'longitude' => null,
            'speed' => null,
            'bearing' => null,
            'info' => $info);

    // use key 'http' even if you send the request to https
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    file_get_contents($url, false, $context);
    
    //var_export($result, true);
}

function sendLocationInfo($id, $gps_time, $latitude, $longitude,$speed_in_kph, $bearing) {
    //echo $id." ".$gps_time." ".$latitude." ".$longitude;
    
    if (!isset($id) || !in_array(strlen($id),$GLOBALS["id_lengths"])){
        return;
    }
    
    $url = 'http://127.0.0.1/api/gps/service';
    $data = array(  
            'id' => $id,
            'time' => $gps_time,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'speed' => $speed_in_kph,
            'bearing' => $bearing,
            'info' => 'location');

    // use key 'http' even if you send the request to https
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    
    var_export($result, true);
}

//190804103129
function tk103_time($date_time){
    $year = substr($date_time,0,2);
    $month = substr($date_time,2,2);
    $day = substr($date_time,4,2);
    $hour = substr($date_time,6,2);
    $minute = substr($date_time,8,2);
    $second = substr($date_time,10,2);

    return date("Y-m-d H:i:s", mktime($hour,$minute,$second,$month,$day,$year));
}

function h02_time($date_time,$date_year){
    $day = substr($date_year,0,2);
    $month = substr($date_year,2,2);
    $year = substr($date_year,4,2);
    $hour = substr($date_time,0,2);
    $minute = substr($date_time,2,2);
    $second = substr($date_time,4,2);

    return date("Y-m-d H:i:s", mktime($hour,$minute,$second,$month,$day,$year));
}

function degree_to_decimal($coordinates_in_degrees, $direction){
    $degrees = (int)($coordinates_in_degrees / 100);
    $minutes = $coordinates_in_degrees - ($degrees * 100);
    $seconds = $minutes / 60;
    $coordinates_in_decimal = $degrees + $seconds;

    if (($direction == "S") || ($direction == "W")) {
        $coordinates_in_decimal = $coordinates_in_decimal * (-1);
    }

    return number_format($coordinates_in_decimal, 6,'.','');
}
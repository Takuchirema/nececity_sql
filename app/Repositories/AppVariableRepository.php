<?php namespace App\Repositories;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use App\Repositories\BaseRepository;
use App\Models\AppVariable;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;

/**
 * Description of AppVariableRepository
 *
 * @author Takunda
 */
class AppVariableRepository extends BaseRepository {
    
    var $id;
    var $code;
    var $value;
    var $description;
    
    function create(){
        
        $variable = AppVariable::query()
                ->where('code','=',$this->code)
                ->first();
        
        if (!is_null($variable)) {
            // There exists company with same name
            $response["success"] = 0;
            $response["message"] = "An app variable with this same code already exists";
            return $response;
        }
        
        $this->createVariable();
        
        BaseRepository::putItemIntoCache(null, CacheType::AppVariables, CacheExpiry::AppVariables, $this->code, $this);
        
        $response["success"] = 1;
        $response["message"] = "Variable Successfully Added!";
        
        return $response;
    }
    
    public function createVariable(){
        $variable = new AppVariable;
        $variable->code = $this->code;
        $variable->value = $this->value;

        $variable->save();
        
        return $variable;
    }
    
    public static function getVariables(){
        $response = array();
        $response['variables'] = array();
        
        $cachedVariables = BaseRepository::getListFromCache(null,CacheType::AppVariables);
        
        if (isset($cachedVariables)){
            $response['variables'] = $cachedVariables;
            $response["success"] = 1;
            $response["message"] = "Retrieved variables from cache";
            return $response; 
        }
        
        $variables = AppVariable::all();
        
        $cacheItems = array();
        foreach($variables as $variable) {
            $variableRep = AppVariableRepository::getRepository($variable);

            array_push($response["variables"], $variableRep);
            $cacheItems[$variableRep->getCode()] = $variableRep;
        }
        
        BaseRepository::putListIntoCache(null, CacheType::AppVariables, CacheExpiry::AppVariables, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved variables";
        return $response;
    }
    
    public static function getVariable($code){
        
        $cachedVariable = BaseRepository::getItemFromCache(CacheType::AppVariables,$code);
        
        if (isset($cachedVariable)){
            return $cachedVariable; 
        }
        
        $variableNode = AppVariable::query()
                ->where('code','=',$code)
                ->first();
        
        $variableRep = AppVariableRepository::getRepository($variableNode);
        
        BaseRepository::putItemIntoCache(null, CacheType::AppVariables, CacheExpiry::AppVariables, $code, $variableRep);
        
        return $variableRep;
    }
    
    public static function getRepository($variable){
        
        if (!$variable){  
            return null;
        }
        
        $variableRep = new AppVariableRepository();
        $variableRep->setCode($variable->code);
        $variableRep->setValue($variable->value);
        $variableRep->setDescription($variable->description);
        
        return $variableRep;
    }
    
    function update($properties){
        try{
            AppVariable::query()
                    ->where('code','=', $this->code)
                    ->update($properties);
            BaseRepository::putItemIntoCache(null, CacheType::AppVariables, CacheExpiry::AppVariables, $this->code, $this);
        }catch(\Illuminate\Database\QueryException $ex){
            $response["success"] = 1;
            $response["message"] = $ex->getMessage();
            return $response;
        }
        
        $response["success"] = 1;
        $response["message"] = "Value updated";
        return $response ;
    }
    
    public static function delete($code){      
        
        AppVariable::query()
                ->where('code','=',$code)
                ->delete();
        
        BaseRepository::deleteItemFromCache(null, CacheType::AppVariables, CacheExpiry::AppVariables,$code);
        
        $response["success"] = 1;
        $response["message"] = "Variable successfuly deleted";
        return $response;
    }
    
    function getCode() {
        return $this->code;
    }

    function getValue() {
        return $this->value;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setValue($value) {
        $this->value = $value;
    }
    
    function getDescription() {
        return $this->description;
    }

    function setDescription($description) {
        $this->description = $description;
    }


}

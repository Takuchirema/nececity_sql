<?php namespace App\Repositories;

use App\Repositories\EmployeeRepository;

/**
 * Description of RouteTimeLog
 *
 * @author Takunda Chirema
 */
class RouteTimeLogRepository extends BaseRepository {
    //put your code here
    var $logs;
    var $routeId;
    var $employeeId;
    var $employee;
    var $date;
    var $day;
    var $fileName;
    var $fileNameFull;
    var $companyId;
    
    function __construct($fileName,$companyId) {
       $this->companyId=$companyId;
       $this->setFileName($fileName);
    }
    
    function getLogs() {
        return $this->logs;
    }

    function getRouteId() {
        return $this->routeId;
    }

    function getEmployeeId() {
        return $this->employeeId;
    }

    function getEmployeeName() {
        return $this->employeeName;
    }

    function getDate() {
        return $this->date;
    }

    function setLogs($logs) {
        $this->logs = $logs;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    function setEmployeeName($employeeName) {
        $this->employeeName = $employeeName;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function getFileName() {
        return $this->fileName;
    }

    function setFileName($fileName) {
        $this->fileNameFull = $fileName;
        
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $this->fileName = basename($fileName, ".".$ext);
        //set the other parts of the Route Time Log
        //time log format: chrtak003_Claremont-Weekdays_2017-09-06       
        $details = explode('_', $this->fileName);
        $this->employeeId = $details[0];
        $this->routeId = $details[1];
        $this->date = $details[2];
        $this->day = date("d F Y", strtotime($this->date));
        
        $this->employee = EmployeeRepository::getRepository($this->employeeId,$this->companyId);
        
        $this->getLogFile();
    }
    
    public static function deleteLogFile($fileName,$employeeId,$companyId){
        
        $filePath = base_path('public/companies/'.$companyId.'/employees/'.$employeeId.'/logs/'.$fileName);
        
        if (!file_exists($filePath)){
            return;
        }
        
        unlink($filePath);
    }
    
    function getLogFile(){
        $filePath = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs/'.$this->fileNameFull);
        
        if (!file_exists($filePath)){
            return;
        }
        
        $logs = file_get_contents($filePath);
        $this->logs = preg_replace("/\n/m", '\n', $logs);
    }
    
    function getEmployee() {
        return $this->employee;
    }

    function getFileNameFull() {
        return $this->fileNameFull;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function setEmployee($employee) {
        $this->employee = $employee;
    }

    function setFileNameFull($fileNameFull) {
        $this->fileNameFull = $fileNameFull;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getDay() {
        return $this->day;
    }

    function setDay($day) {
        $this->day = $day;
    }
}

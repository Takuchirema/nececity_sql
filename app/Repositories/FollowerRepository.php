<?php namespace App\Repositories;

use Datetime;
use App\Models\Follower;
use App\Repositories\UserRepository;

/**
 * Description of Follower
 *
 * @author Takunda
 */

class FollowerRepository extends UserRepository{
    
    var $blocked;
    var $timeJoined;
    var $dayJoined;
    
    function create(){
        
        $this->createNode();
        
        // update the user cache to be following company
        UserRepository::updateCacheRepository($this->userId);
        
        $response["success"] = 1;
        $response["message"] = "Follower successfully created";
        return $response;
    }
    
    function createNode(){
        
        $follower = new Follower;
        
        $follower->userId = $this->userId;
        $follower->companyId = $this->companyId;
        $follower->blocked = 'false';
        $follower->time = new Datetime;
        
        $follower->save();
        
        return $follower;
    }
    
    public static function getFollowers($companyId){
        
        $response['followers'] = array();

        $followers = Follower::query()
                    ->where('companyId','=',$companyId)
                    ->get();
        
        if (!is_null($followers)) {
            foreach($followers as $follower) {
                $followerRep = FollowerRepository::getFollower($follower->userId, $companyId);
                array_push($response["followers"], $followerRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Followers";
        return $response;
    }
    
    public static function getFollower($userId, $companyId){
        $follower = Follower::query()
                    ->where('companyId','=',$companyId)
                    ->where('userId','=',$userId)
                    ->first();
        
        if (is_null($follower)) 
        {
            return null;
        }
        
        $followerRep = new FollowerRepository($follower->userId);
        $user = UserRepository::getUserNode($follower->userId, null);
        
        if (is_null($user)) 
        {
            return null;
        }
        
        $time = FollowerRepository::getDateTimeString($follower->time);

        $followerRep->setName($user->name);
        $followerRep->setFbName($user->fbName);
        $followerRep->setLatitude($user->latitude);
        $followerRep->setLongitude($user->longitude);
        $followerRep->setStatus($user->status);
        $followerRep->setLastSeen($user->lastSeen);
        $followerRep->setLocation($user->location);
        $followerRep->setPhoneNumber($user->phoneNumber);
        $followerRep->setCompanyId($companyId);
        $followerRep->setTimeJoined($time);
        $followerRep->setBlocked($follower->blocked);

        //check for the user profile picture
        $filename = public_path('profiles/'.$follower->userId);
        $file_url = url('profiles/'.$follower->userId);

        if (file_exists($filename)){

            $pictures = scandir($filename,1);
            $picture_url = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);

            if ($followerRep->isValidImage($picture_url)){
                $followerRep->setProfilePicture($picture_url);
            }
        }
        
        return $followerRep;
    }
    
    public static function blockFollower($userId,$companyId){
        
        $follower = Follower::query()
                    ->where('companyId','=',$companyId)
                    ->where('userId','=',$userId)
                    ->first();
        
        $follower->blocked = 'true';
        $follower->save();
    }
    
    public static function deleteFollower($userId,$companyId)
    {
        Follower::query()
                ->where('companyId','=',$companyId)
                ->where('userId','=',$userId)
                ->delete();
    }
    
    public static function unBlockFollower($userId,$companyId){
        
        $follower = Follower::query()
                    ->where('companyId','=',$companyId)
                    ->where('userId','=',$userId)
                    ->first();
        
        $follower->blocked = 'false';
        $follower->save();
    }
    
    function getBlocked() {
        return $this->blocked;
    }

    function setBlocked($blocked) {
        $this->blocked = $blocked;
    }

    function getTimeJoined() {
        return $this->timeJoined;
    }

    function setTimeJoined($timeJoined) {
        $this->timeJoined = $timeJoined;
        
        $dateTime = explode("_", $timeJoined);
        $date = $dateTime[0];
        
        $this->dayJoined = date("d F Y", strtotime($date));
    }
    
    function getDayJoined() {
        return $this->dayJoined;
    }

    function setDayJoined($dayJoined) {
        $this->dayJoined = $dayJoined;
    }
}

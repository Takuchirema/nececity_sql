<?php namespace App\Repositories;

use Datetime;
use App\Repositories\BaseRepository;
use App\Models\Alert;
use App\Models\Relationship;

/**
 * Description of Alert
 *
 * @author Takunda Chirema
 */

class AlertRepository extends BaseRepository{
    
    var $userId;
    var $employeeId;
    var $latitude;
    var $longitude;
    var $time;
    var $location;
    var $companyId;
    var $seen;
    
    var $dpLatitude;
    var $dpLongitude;
    
    /**
     * The method creates an alert for the user with given id
     * 
     */
    function create(){
        
        $this->removeAlert();
        
        $this->createAlert();
        
        $response["success"] = 1;
        $response["message"] = "Alert successfully created";
        return $response;
    }
    
    public static function getAlertModel($userId,$companyId){
        
        if (is_null($companyId)){
            $alert = Alert::query()
                    ->where('userId','=',$userId)->first();
        }else{
            $alert = Alert::query()
                    ->where('employeeId','=',$userId)
                    ->where('companyId','=',$companyId)->first();
        }
        
        return $alert;
    }
    
    function createAlert(){
        
        //These are for getting alerts that are nearby to a user
        //It uses decimal places of the geo location
        //Depending on how accurate we want to be we can change the DP
        //Now its on 0 decimal places
        $alert = new Alert;
        
        if (strpos($this->latitude,".") > 0){
            $dpLat = substr($this->latitude,0,strpos($this->latitude,".") );
            $dpLng = substr($this->longitude,0,strpos($this->longitude,".") );
        }else{
            $dpLat = $this->latitude;
            $dpLng = $this->longitude;
        }
        
        $date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $alert->time = $date;
        
        $alert->userId = $this->userId;
        $alert->employeeId = $this->employeeId;
        $alert->companyId = $this->companyId;
        $alert->latitude = $this->latitude;
        $alert->longitude = $this->longitude;
        $alert->location = $this->location;
        $alert->dpLatitude = $dpLat;
        $alert->dpLongitude = $dpLng;
        
        $alert->save();
        
        return $alert;
    }
    
    function removeAlert(){
        $alert = AlertRepository::getAlertModel($this->userId, $this->companyId);
        
        Relationship::query()
                ->where('relatedEntityId','=',$this->userId)
                ->where('type','=', RelationshipRepository::$employeeSeenAlert)
                ->orWhere('type','=', RelationshipRepository::$userSeenAlert)
                ->delete();
                
        if (!is_null($alert)){
            //remove an old has alert relationship first
            $alert->userId = null;
            $alert->employeeId = null;
            $alert->companyId = null;
            $alert->save();
        }
    }
    
    /**
     * Gets the alert of a certain user from a query template
     * @param
     */
    public static function getAlert($userId,$companyId){
        
        $alert = AlertRepository::getAlertModel($userId, $companyId);

        if (!is_null($alert)){     
            $time = AlertRepository::getDateTimeString($alert->time);
            
            $alertRep = new AlertRepository();
            $alertRep->setUserId($userId);
            $alertRep->setTime($time);
            $alertRep->setLatitude($alert->latitude);
            $alertRep->setLongitude($alert->longitude);
            $alertRep->setLocation($alert->location);
            return $alertRep;
        }
        
        return null;
    }
    
    /**
     * Gets all alerts in database
     * @param The latest date to be taken yyyy-mm-dd.
     */
    public static function getAlerts($date){
        
        $response['alerts'] = array();
        
        $alerts = Alert::query()
                    ->where('time','>',$date)->get();
        
        foreach ($alerts as $alert) {
            $time = AlertRepository::getDateTimeString($alert->time);
            
            $alertRep = new AlertRepository();
            $alertRep->setTime($time);
            $alertRep->setLatitude($alert->latitude);
            $alertRep->setLongitude($alert->longitude);
            $alertRep->setLocation($alert->location);
            array_push($response['alerts'], $alert);
        }
            
        $response['success'] = 1;
        $response['message'] = "alerts have been retreived";
        return $response;
    }
    
    public static function getLocationAlerts($latitude,$longitude)
    {
        $response['alerts'] = array();
        
        $alerts = Alert::query()
                    ->where('dpLatitude','=',$latitude)
                    ->where('dplongitude','=',$longitude)
                    ->get();
        
        foreach ($alerts as $alert) {
            $time = AlertRepository::getDateTimeString($alert->time);
            
            $alertRep = new AlertRepository();
            $alertRep->setTime($time);
            $alertRep->setLatitude($alert->latitude);
            $alertRep->setLongitude($alert->longitude);
            $alertRep->setLocation($alert->location);
            array_push($response['alerts'], $alert);
        }
            
        $response['success'] = 1;
        $response['message'] = "alerts have been retreived";
        return $response;
    }
    
    /**
     * Gets the AlertRepository of the alert model.
     * @param the model to be put into the Repository.
     */
    function getAlertRep($alert){
        
        $alertRep = new AlertRepository();
        $alertRep->setTime($alert->time);
        $alertRep->setLatitude($alert->latitude);
        $alertRep->setLongitude($alert->longitude);
        $alertRep->setLocation($alert->location);
        
        return $alertRep;
    }
    
    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getTime() {
        return $this->time;
    }

    function getLocation() {
        return $this->location;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setLocation($location) {
        $this->location = $location;
    }
    
    function getSeen() {
        return $this->seen;
    }

    function getDpLatitude() {
        return $this->dpLatitude;
    }

    function getDpLongitude() {
        return $this->dpLongitude;
    }

    function setSeen($seen) {
        $this->seen = $seen;
    }

    function setDpLatitude($dpLatitude) {
        $this->dpLatitude = $dpLatitude;
    }

    function setDpLongitude($dpLongitude) {
        $this->dpLongitude = $dpLongitude;
    }
    
    function getUserId() {
        return $this->userId;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
}

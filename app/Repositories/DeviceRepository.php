<?php namespace App\Repositories;

use App\Repositories\EmployeeRepository;
use App\Models\Device;
use App\Models\Route;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\Constants;
use App\Http\Helpers\LatLngDetails;
use Log;
use Datetime;

/**
 * Description of Device
 *
 * @author Takunda Chirema
 */
class DeviceRepository extends BaseRepository {
    
    var $deviceNode;
    var $deviceId;
    var $companyId;
    var $phoneNumber;
    var $busId;
    var $employee;
    var $employeeId;
    var $employeeName;
    var $lastLocationUpdate;
    var $appVersion;
    var $latitude;
    var $longitude;
    var $isLoggedOut="true";
    var $logout="false";
    var $scheduleId;
    
    //These fields are only used in memory.
    var $gpsInfo;
    var $gpsSpeed;
    var $gpsTime;
    var $gpsBearing;
    var $gpsLat;
    var $gpsLng;
    var $currentGpsTime=0;
    var $moving = false;
    var $lastGoodMove = true;
    var $badMove;
    
    // This will be used to check if a route can be created.
    // Total repeats should at least be equal to the size of coordinates.
    var $totalRepeats = 0;
    
    private $schedule;
    //If it is set and employeeId is not, every time the LatLng is set,
    //We create an employee in cache with employee Id of this route and give it,
    //The device coordiantes.
    private $routeId;
    
    //This is stored temporarily for mapping out unknown routes.
    //The structure is like this:
    //lat,lng,lat,lng,lat,lng;lat,lng,lat,lng,lat,lng;
    //The first lat,lng are unrounded.
    //The second is rounded to 2dp.
    //The third is rounded to 3dp
    private $routeCoordinates;
    
    //This is a hashmap with 3 keys:
    //First is "latlng"
    //Second is "latlng2"
    //Third is "latlng3"
    //{ 'latlng'{'13.12345,13.12345'} 'latlng2'{'13.12,13.12'} 'latlng3'{'13.123,13.123'} }
    //We will use this to check whether a route that is being plotted is now being repeated.
    //We check the 2dp or 3dp array.
    //When we repeat a coordinate, check whether it is a repeat of the beginning of route.
    //If it is remove all points which are before the repeated point.
    //Then create a new Unkown route which must be given a name and times.
    //To create one, we first check if there is no such existing route already.
    private $structuredRouteCoordinates;
    
    function __construct($deviceId,$companyId) {
        $this->deviceId=$deviceId;
        $this->companyId=$companyId;
    }
    
    function create(){
        $oldDevice = DeviceRepository::getNode($this->deviceId,$this->companyId);
        
        if (!isset($oldDevice)){
            $this->deviceNode = $this->createDeviceNode();
        }else{
            return;
        }
        
        $this->structureRouteCoordinates(null);

        $response["success"] = 1;
        $response["message"] = "Device has been created";
        return ($response);
    }
    
    public static function delete($deviceId,$companyId){
        Device::query()
                ->where('deviceId','=',$deviceId)
                ->where(function($query) use($companyId)
                        {
                            $query->where('companyId','=',$companyId)
                                  ->orWhereNull('companyId')
                                  ->orWhere('companyId','=','');
                        })
                ->delete();
        BaseRepository::deleteItemFromCache($companyId, CacheType::Devices, CacheExpiry::Devices, $deviceId);
    }
    
    public static function getNode($deviceId,$companyId){
	$device = Device::query()
                ->where('deviceId','=',$deviceId)
                ->where('companyId','=',$companyId)
                ->first();
        return $device;
    }
    
    public static function logoutDevice($deviceId,$companyId){
        
        $deviceNode = DeviceRepository::getNode($deviceId, $companyId);
        
        if (!isset($deviceNode)){
            return;
        }
        $employeeId = $deviceNode->employeeId;
        
        $employeeRep = new EmployeeRepository($employeeId, $companyId);
        $employeeRep->update(array("logout" => 'true'));
    }
    
    public static function loginDevice($deviceId,$companyId){
        
        $deviceNode = Device::getDeviceNode($deviceId, $companyId);
        
        if (!is_null($deviceNode)){
            $employeeId = $deviceNode->employeeId;
            $employeeNode = EmployeeRepository::getNode($employeeId, $companyId);
            
            if (!isset($employeeNode)){
                return;
            }
            
            $employeeNode->logout = 'false';
            $employeeNode->save();
        } 
    }
    
    function createDeviceNode(){
        $device = new Device;
        
        //create the new device
	$device->deviceId = $this->deviceId;
	$device->companyId = $this->companyId;
        
        $device->save();
        return $device;
    }
    
    public static function getDevices($companyId){
        
        $response['devices'] = array();
        
        if (isset($companyId)){
            $cachedDevices = BaseRepository::getListFromCache($companyId,CacheType::Devices);
        }
        
        if (isset($cachedDevices)){
            $response['devices'] = $cachedDevices;
            $response["success"] = 1;
            $response["message"] = "Retrieved devices from cache";
            return $response; 
        }
        
        if (isset($companyId)){
            $devices = Device::query()
                        ->where('companyId','=',$companyId)
                        ->get();
        }else {
            $devices = Device::query()
                        ->get();
        }

        $cacheItems = array();
        foreach($devices as $device) {
            $deviceRep = DeviceRepository::createRepository($device);
            array_push($response["devices"], $deviceRep);
            $cacheItems[$deviceRep->getId()] = $deviceRep;
        }
        
        if (isset($companyId)){
            BaseRepository::putListIntoCache($companyId, CacheType::Devices, CacheExpiry::Devices, $cacheItems);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Device";
        return $response;
    }
    
    public static function getOrCreateRepository($deviceId){
        if (!isset($deviceId)){
            return;
        }
        
        $repository = DeviceRepository::getRepository($deviceId);
        
        if (!isset($repository)){
            $deviceRep = new DeviceRepository($deviceId,null);
            $deviceRep->create();
        }
        
        return DeviceRepository::getRepository($deviceId);
    }
    
    public static function getRepository($deviceId, $cached=true){
        
        if ($cached){
            $cachedRepository = BaseRepository::getItemFromCache(CacheType::Devices, $deviceId);
        }
        
        if (isset($cachedRepository)){
            return $cachedRepository;
        }
        
        $device = Device::query()
                    ->where('deviceId','=',$deviceId)
                    ->first();
        
        if (!isset($device)){
            return null;
        }
        
        $deviceRep = DeviceRepository::createRepository($device);
        
        BaseRepository::putItemIntoCache($device->companyId, CacheType::Devices, CacheExpiry::Devices, $deviceId, $deviceRep);
        
        return $deviceRep;
    }
    
     public static function createRepository($device){
        
        $deviceRep = new DeviceRepository($device->deviceId,$device->companyId);

        $deviceRep->setCompanyID($device->companyId);
        $deviceRep->setPhoneNumber($device->phoneNumber);
        $deviceRep->setBusId($device->busId);
        $deviceRep->setCurrentEmployeeId($device->employeeId);
        $deviceRep->setAppVersion($device->appVersion);
        $deviceRep->setRouteCoordinates($device->routeCoordinates);
        $deviceRep->setScheduleId($device->scheduleId);
        $deviceRep->setIsLoggedOut();
        $deviceRep->setLogout();
        $deviceRep->setEmployee();
        $deviceRep->setLastLocationUpdate($device->lastLocationUpdate);
        
        
        return $deviceRep;
    }
    
    public function hasLatestVersion(){
        $files = scandir(public_path('apk/'), SCANDIR_SORT_DESCENDING);
        $newest_file = $files[0];
        
        if ($newest_file === "." || $newest_file === ".."){
            return true;
        }
        
        if ($newest_file.contains($this->appVersion)){
            return true;
        }
        
        return false;
    }
    
    public function getUpdateUrl(){
        $files = scandir(public_path('apk/'), SCANDIR_SORT_DESCENDING);
        $newest_file = $files[0];
        
        return url('apk/'.$newest_file);
    }
    
    /* Mainly for filling device with extra details
     */
    function update($properties)
    {
        $deviceRep = DeviceRepository::getRepository($this->deviceId);
        
        if (isset($deviceRep) && isset($deviceRep->employeeId)){
            $employeeRep = $deviceRep->getEmployee();
            if (isset($employeeRep) && isset($properties["latitude"]) && isset($properties["longitude"])){
                $employeeRep->update(array("latitude" => $properties["latitude"], "longitude" => $properties["longitude"]));
            }
        }
        
        try{
            if (isset($this->companyId)){
                Device::query()
                        ->where('companyId','=', $this->companyId)
                        ->where('deviceId','=', $this->deviceId)
                        ->update($properties);
            }else{
                Device::query()
                        ->where('deviceId','=', $this->deviceId)
                        ->update($properties);
            }
        }catch(\Illuminate\Database\QueryException $ex){
            echo "update fail: ".$ex->getMessage();
            $response["success"] = 0;
            $response["message"] = $ex->getMessage();
            return $response;
        }
        
        DeviceRepository::updateCacheRepository(null, $deviceRep->deviceId);
        
        $response["success"] = 1;
        $response["message"] = "Successfully updated!";
        return $response;
    }
    
    function getCompanyID() {
        return $this->companyId;
    }
    
    function getCompanyName(){
        if (!isset($this->companyId)){
            return null;
        }
        $companyRep = CompanyRepository::getRepository($this->companyId);
        if (!isset($companyRep)){
            return null;
        }
        return $companyRep->companyName;
    }
    
    function getCompany(){
        $companyRep = CompanyRepository::getRepository($this->companyId);
        return $companyRep;
    }

    function setCompanyID($companyId) {
        $this->companyId = $companyId;
    }

    function setDeviceNode($deviceNode) {
        $this->deviceNode = $deviceNode;
    }
    
    function getId() {
        return $this->deviceId;
    }

    function setId($deviceId) {
        $this->deviceId = $deviceId;
    }
    
    function getRouteId() {
        return $this->routeId;
    }
    
    function getRouteName(){
        if (!isset($this->routeId)){
            return null;
        }
        
        $routeRep = RouteRepository::getRepository($this->routeId, $this->companyId);
        
        if (!isset($routeRep)){
            return null;
        }
        
        return $routeRep->routeName;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function getBusId() {
        return $this->busId;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    function setBusId($busId) {
        $this->busId = $busId;
    }
    
    function getCurrentEmployeeId() {
        return $this->employeeId;
    }

    function setCurrentEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }
    
    function getCurrentEmployeeName() {
        return $this->employeeName;
    }

    function setCurrentEmployeeName($employeeName) {
        $this->employeeName = $employeeName;
    }
    
    function isLoggedOut() {
        return $this->isLoggedOut;
    }

    function setIsLoggedOut() {
        if (isset($this->employeeId) && isset($this->companyId)){
            $employeeNode = EmployeeRepository::getNode($this->employeeId, $this->companyId);
            if (!isset($employeeNode)){
                return;
            }
            if ($employeeNode->status === "offline"){
                $this->isLoggedOut = "true";
            }else if ($employeeNode->status === "online"){
                $this->isLoggedOut = "false";
            }
        }
    }
    
    function getLogout() {
        return $this->logout;
    }

    function setLogout() {
        if (isset($this->employeeId) && isset($this->companyId)){
            $employeeNode = EmployeeRepository::getNode($this->employeeId, $this->companyId);
            if (!isset($employeeNode)){
                return;
            }
            if ($employeeNode->logout === "true"){
                $this->logout = "true";
            }else if ($employeeNode->logout === "false"){
                $this->logout = "false";
            }
        }
    }
    
    public static function createUnknownEmployees($companyId){
        $devices = DeviceRepository::getDevices($companyId)["devices"];
        $employees = array();
        
        foreach ($devices as $device){
            $routeId = $device->getRouteId();
            //echo "*** r id: ".$routeId;
            if (BaseRepository::isEmptyOrNullString($device->employeeId) && isset($routeId)){
                //echo "unknown emp";
                $employee = new EmployeeRepository($device->deviceId,$companyId);
                $employee->setName("Unknown");
                $employee->setStatus('online');
                $employee->setRouteId($device->routeId);
                $employee->setLatitude($device->latitude);
                $employee->setLongitude($device->longitude);
                array_push($employees,$employee);
            }
        }
        return $employees;
    }
    
    function getEmployee() {
        $this->setEmployee(); 
        return $this->employee;
    }

    function setEmployee() {
        
        if (isset($this->employeeId)){

            $employee = EmployeeRepository::getRepository($this->employeeId,$this->companyId);

            if (isset($employee)){
                $this->employee = $employee;
            }
        }
    }
    
    function setEmployeeLocation($latlng,$value){
        $employeeRep = $this->getEmployee();
        
        if (!isset($employeeRep)){
            return;
        }
        
        $employeeRep->update(array($latlng => $value));
    }
    
    function getAppVersion() {
        return $this->appVersion;
    }

    function setAppVersion($appVersion) {
        $this->appVersion = $appVersion;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
    
    /**
     * This method must only be used to set the latitude and longitude of a moving vehicle.
     * This is when the company is flagged to record routes.
     * This method only updates cache values. Updating actual db values
     * @param type $latitude
     * @param type $longitude
     */
    function setLatLng($latitude, $longitude, $time, $speed, $bearing){
        
        if (!isset($this->companyId)){
            return;
        }
        
        if ($speed > Constants::getMinTravellingSpeed()){
            $this->moving = true;
        }else{
            $this->moving = false;
        }
        
        $gpsTime = strtotime($time);
        $currentTime = $this->isCurrentTime($gpsTime);
        $goodMove = $this->isGoodMove($latitude, $longitude);
        
        if ($currentTime && $goodMove){
            $this->currentGpsTime = $gpsTime;
            $this->latitude = $latitude;
            $this->longitude = $longitude;
            
            $this->update(array("latitude" => $latitude,"longitude" => $longitude,"lastLocationUpdate" => new DateTime()));
            
            $this->updateRouteDetails($latitude, $longitude, $time, $speed, $bearing);
            
            //Should be after route has been done so that employee shift is updated properly
            $this->updateEmployeeDetails($latitude, $longitude, null);
        }else if ($goodMove){
            $this->updateRouteDetails($latitude, $longitude, $time, $speed, $bearing, false);
        }
        
        DeviceRepository::updateCacheRepository($this);
    }
    
    function isCurrentTime($gpsTime){
        
        if ($gpsTime < $this->currentGpsTime){
            return false;
        }
        
        return true;
    }
    
    /**
     * This function is to check if the real-time details should be updated to 
     * avoid random jumps.
     */
    function isGoodMove($latitude, $longitude){
 
        if (count($this->structuredRouteCoordinates['latlng1']) == 0){
            return  true;
        }
        
        $lat  = floatval($latitude);
        $lng = floatval($longitude);
        
        $latlng1 = $lat.",".$lng;
        $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
        
        $goodMove = end($this->structuredRouteCoordinates['latlng1']);
        $recentLatLng2 = end($this->structuredRouteCoordinates['latlng2']);
        
        if (!$this->lastGoodMove){
            $fromLatLng = $this->badMove;
            $distanceFromBad = $this->getGoodMoveDistance($lat, $lng, $fromLatLng);
            $distanceToGood = $this->getGoodMoveDistance($lat, $lng, $goodMove);
            
            // We haven't distanced from bad enough
            if ($distanceFromBad < 100){
                Log::debug("** bad move no correct; dfb: ".$distanceFromBad." dtg: ".$distanceToGood." **");
                return false;
            }
            
            // If the last move was bad then correction move must distance itself from bad one toward good one.
            if ($distanceToGood < 1000){
                Log::debug("** bad move correct; dfb: ".$distanceFromBad." dtg: ".$distanceToGood." **");
                $this->lastGoodMove = true;
                return true;
            }else{
                // If the correction is still far from good, then reset to avoid bad routes created.
                Log::debug("** bad move correct and clear; dfb: ".$distanceFromBad." dtg: ".$distanceToGood." **");
                $this->resetDeviceRoute();
                $this->lastGoodMove = true;
                return true;
            }
        }
        
        // If the lat, lng to 2 dp are not the same we should check if distance is too big.
        if ($latlng2 !== $recentLatLng2){
            $fromLatLng = end($this->structuredRouteCoordinates['latlng1']);
            $distance = $this->getGoodMoveDistance($lat, $lng, $fromLatLng);
            
            // If we moved a km from last point in one go then do not update.
            if ($distance > 1000){
                Log::debug("** bad move: ".$distance." **");
                $this->badMove = $latlng1;
                $this->lastGoodMove = false;
                return false;
            }
        }
        
        return true;
    }
    
    function getGoodMoveDistance($lat, $lng, $fromLatLng){
            
        $fromLat = (float) explode(",", $fromLatLng)[0];
        $fromLng = (float) explode(",", $fromLatLng)[1];

        $distance = LatLngDetails::distance($fromLat, $fromLng, $lat, $lng);
        
        return $distance;
    }
    
    function updateRouteDetails($latitude, $longitude, $time, $speed, $bearing, $isCurrentTime=true){
        
        if (isset($this->routeId)){
            if (!RouteRepository::enroute($this->routeId, $this->companyId, $latitude, $longitude)){
                $this->routeId = null;
            }
        }
        
        $oldCount = count($this->structuredRouteCoordinates['latlng1']);
        $index = $this->insertCoordinates($latitude, $longitude, $time, $speed, $bearing);
        $newCount = count($this->structuredRouteCoordinates['latlng1']);
        
        $inserted = false;
        if ($oldCount < $newCount){
            $inserted = true;
        }
        
        if (!isset($index)){
            return;
        }
        
        //Check if the device is on an already existing route.
        if (!isset($this->routeId)){
            $routeRep = RouteRepository::matchUnknownRoute($this->companyId, $this->structuredRouteCoordinates);
            if (isset($routeRep)){
                $this->routeFound($routeRep->routeId);
                return;
            }
        }
        
        // Once we repeat a point more than once we save route.
        // Also if we repeat a value that is at the point we started we save the route.
        if ($isCurrentTime && $this->createNewRoute($index, $inserted, $speed)){
            //echo ("repeat: ".$repeat." ".$latlng3." ".$latlng1." index: ".$index." ".count($latlng3s));
            $newRouteId = RouteRepository::createUnknownRoute($this->companyId, $this->structuredRouteCoordinates);
            if (isset($newRouteId)){
                $this->routeFound($newRouteId);
                return;
            }
        }else if (!$isCurrentTime){
            Log::debug("** Create route not current time. inserted: ".$inserted." speed: ".$speed." **");
        }
        
        return;
    }
    
    /**
     * Inserts the coordinate and returns the repeat index if found.
     * If no repeat we return 0.
     * If no entry was inserted we return null.
     * @param type $latitude
     * @param type $longitude
     * @param type $repeat
     * @param type $time
     */
    function insertCoordinates($latitude, $longitude, $time, $speed, $bearing){
        $lat  = floatval($latitude);
        $lng = floatval($longitude);
        $repeat = 0;
        $index = null;
        $count = count($this->structuredRouteCoordinates["latlng3"]);
        
        $latlng1 = $lat.",".$lng;
        $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
        $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
        
        $latlng3s = $this->structuredRouteCoordinates["latlng3"];
        
        //Cannot start a new route while vehicle is moving.
        if ($count == 0 && $this->moving){
            return $index;
        }
        
        if (in_array($latlng3,$latlng3s)){
            $index = array_search($latlng3, $latlng3s);
            
            // check if this is a recent repeat not a duplicate.
            if (!$this->isValidRepeatTime($time, $index)){
                return null;
            }
            
            $repeat = $this->structuredRouteCoordinates["repeat"][$index] + 1;
            $this->structuredRouteCoordinates["repeat"][$index] = $repeat;
            $this->structuredRouteCoordinates["time2"][$index] = $time;
            $this->totalRepeats ++;
        }
        
        //Log::debug("location: ".$latlng3." ".$latlng1." ".count($latlng3s));
        // If we are repeating but device is stationary do not insert the point.
        if ($repeat > 0 && $speed < Constants::getMinTravellingSpeed()){
            return $index;
        }
        
        array_push($this->structuredRouteCoordinates["latlng1"],$latlng1);
        array_push($this->structuredRouteCoordinates["latlng2"],$latlng2);
        array_push($this->structuredRouteCoordinates["latlng3"],$latlng3);
        array_push($this->structuredRouteCoordinates["time"],$time);
        array_push($this->structuredRouteCoordinates["time2"],$time);
        array_push($this->structuredRouteCoordinates["repeat"],$repeat);
        array_push($this->structuredRouteCoordinates["speed"],$speed);
        array_push($this->structuredRouteCoordinates["bearing"],$bearing);
        
        return $index;
    }
    
    /**
     * time2 is for recording the time of a repeated point.
     * @param type $time
     * @param type $index
     * @return boolean
     */
    function isValidRepeatTime($time, $index){
        $oldTime = $this->structuredRouteCoordinates["time2"][$index];
        //Convert them to timestamps.
        $oldTimestamp = strtotime($oldTime);
        $newTimestamp = strtotime($time);

        //New time of point recording
        $this->structuredRouteCoordinates["time2"][$index] = $time;

        $difference = $newTimestamp - $oldTimestamp;

        //If the same position is repeated in last 60 seconds then we haven't moved
        if ($difference < Constants::deviceUpdateInterval){
            return false;
        }
        return true;
    }
    
    function createNewRoute($index, $inserted, $speed){
        
        if ($speed > Constants::getMinTravellingSpeed()){
            //return false;
        }
        
        if (!isset($this->structuredRouteCoordinates['latlng1'])){
            return false;
        }
        
        $count = count($this->structuredRouteCoordinates['latlng1']);
        
        if ($count < 10){
            return false;
        }
        
        $repeat = $this->structuredRouteCoordinates["repeat"][$index];
        
        if ($repeat <= 1){
            return false;
        }
        
        if (!$inserted){
            return false;
        }
        
        // Check if most points of the route have been repeated
        // We are assuming that a repeat indicates an original point that's been repeated.
        // e.g. 4 points with 2 repeats means original are 2 points.
        if ($this->totalRepeats < ($count - $this->totalRepeats)/2){
            return false;
        }
        
        return true;
    }
    
    function upperRoute($index){
        $count = count($this->structuredRouteCoordinates['latlng1']);
        $half = $count/2;
        if ($index > $half){
            return true;
        }
        return false;
    }
    
    function lowerRoute($index){
        return !$this->upperRoute($index);
    }
    
    function endOfRoute($index){
        $count = count($this->structuredRouteCoordinates['latlng1']);
        if ($index > ($count - 10)){
            return true;
        }
        return false;
    }
    
    function startOfRoute($index){
        if ($index < 5){
            return true;
        }
        return false;
    }
    
    function resetDeviceRoute(){
        $this->routeId = null;
        $this->structuredRouteCoordinates["latlng1"] = array();
        $this->structuredRouteCoordinates["latlng2"] = array();
        $this->structuredRouteCoordinates["latlng3"] = array();
        $this->structuredRouteCoordinates["time"] = array();
        $this->structuredRouteCoordinates["time2"] = array();
        $this->structuredRouteCoordinates["repeat"] = array();
        $this->structuredRouteCoordinates["speed"] = array();
        $this->structuredRouteCoordinates["bearing"] = array();
    }
    
    function routeFound($routeId){
        $this->resetDeviceRoute();
        $this->routeId=$routeId;
        $this->updateEmployeeDetails(null, null, $routeId);
    }
    
    function updateEmployeeDetails($latitude, $longitude, $routeId){
        //echo "** update employee details ".$this->scheduleId." **\n";
        
        $startShift = $this->startScheduleShift();
        if ($startShift){
            ScheduleRepository::startShift($this->deviceId);
            // Device cache has been refreshed so must update employeeId and scheduleId
            $deviceRep = DeviceRepository::getRepository($this->deviceId);
            $this->employeeId=$deviceRep->employeeId;
            $this->scheduleId=$deviceRep->scheduleId;
            return;
        }
        
        $employeeRep = $this->getEmployee();
        if (!isset($employeeRep)){
            return;
        }
        
        $date = new DateTime('now');
        $systemLastSeen = $date->format('Y-m-d_H:i:s');

        $properties = array();
        if (isset($latitude)){
            $properties["latitude"] = $latitude;
        }
        
        if (isset($longitude)){
            $properties["longitude"] = $longitude;
        }
        
        if (isset($routeId)){
            $properties["routeId"] = $routeId;
        }
        
        $properties["lastseen"] = $systemLastSeen;
        $employeeRep->update($properties);
        
        //Check if the shift should end from the schedule
        $endShift = $this->endScheduleShift();
        if ($endShift){
            ScheduleRepository::endShift($this->deviceId);
            // Device cache has been refreshed so must update employeeId and scheduleId
            $deviceRep = DeviceRepository::getRepository($this->deviceId);
            $this->employeeId=$deviceRep->employeeId;
            $this->scheduleId=$deviceRep->scheduleId;
        }
        
        if (!$endShift){
            //Update the employee shift
            ShiftRepository::updateCoordinates($employeeRep, $this);
        }
    }
    
    public static function updateCacheRepository($deviceRep,$deviceId=null){
        if (isset($deviceId)){
            $deviceRep = DeviceRepository::getRepository($deviceId, false);
        }
        BaseRepository::putItemIntoCache($deviceRep->companyId, CacheType::Devices, CacheExpiry::Devices, $deviceRep->deviceId, $deviceRep);
    }
    
    function startScheduleShift(){
        if (BaseRepository::isEmptyOrNullString($this->employeeId)){
            return true;
        }
        
        // If the schedule is set, we should not create another.
        // Instead the current one should end first.
        if (!BaseRepository::isEmptyOrNullString($this->scheduleId)){
            return false;
        }
        
        $employeeRep = EmployeeRepository::getRepository($this->employeeId);
        $shiftId = $employeeRep->shiftId;
        
        if (BaseRepository::isEmptyOrNullString($shiftId)){
            return true;
        }
        
        return false;
    }
    
    function endScheduleShift(){
        // If there is no schedule to end, do not try anything
        if (BaseRepository::isEmptyOrNullString($this->scheduleId)){
            return false;
        }

        //Only end the shift when vehicle is not moving
        if ($this->moving){
            return false;
        }
        
        $scheduleRep = $this->getSchedule();
        
        if (!isset($scheduleRep)){
            return false;
        }
        
        $hi = (int)date("Hi");
        
        //echo "** hi - ".$hi." st - ".$scheduleRep->start." et ".$scheduleRep->end." **\n";
        if ($scheduleRep->end > $scheduleRep->start){
            if ($hi > $scheduleRep->end){
               return true;
            }
        }else{
            if ($hi >= $scheduleRep->end && $hi <= $scheduleRep->start){
               return true;
            }
        }
        
        return false;
    }
    
    function getRouteCoordinates() {
        return $this->routeCoordinates;
    }

    function setRouteCoordinates($routeCoordinates) {
        $this->routeCoordinates = $routeCoordinates;
        $this->structureRouteCoordinates($routeCoordinates);
    }
    
    function structureRouteCoordinates($routeCoordinates){
  
        $coordinates = explode(";", $routeCoordinates);
        $latlng1s = array();
        $latlng2s = array();
        $latlng3s = array();
        
        $this->structuredRouteCoordinates["time"] = array();
        $this->structuredRouteCoordinates["time2"] = array();
        $this->structuredRouteCoordinates["repeat"] = array();
        $this->structuredRouteCoordinates["speed"] = array();
        $this->structuredRouteCoordinates["bearing"] = array();
        
        $this->structuredRouteCoordinates["latlng1"] = $latlng1s;
        $this->structuredRouteCoordinates["latlng2"] = $latlng2s;
        $this->structuredRouteCoordinates["latlng3"] = $latlng3s;
        
        if (!isset($routeCoordinates)){
            return;
        }
        
        foreach ($coordinates as $coordinate){
            $latlng = explode(",", $coordinate);
            
            array_push($latlng1s,$latlng[0].",".$latlng[1]);
            array_push($latlng2s,$latlng[2].",".$latlng[3]);
            array_push($latlng3s,$latlng[4].",".$latlng[5]);
        }
    }
    
    function getStructuredCoordinates(){
        if (isset($this->structuredRouteCoordinates["latlng1"])){
            return $this->structuredRouteCoordinates;
        }
        return null;
    }
    
    function getScheduleId() {
        return $this->scheduleId;
    }

    function setScheduleId($scheduleId) {
        $this->scheduleId = $scheduleId;
        if (isset($scheduleId)){
            $this->schedule = ScheduleRepository::getRepository($scheduleId);
        }
    }

    function getSchedule() {        
        if (isset($this->scheduleId)){
            $this->schedule = ScheduleRepository::getRepository($this->scheduleId);
        }
        return $this->schedule;
    }
    
    function getLastLocationUpdate() {
        return $this->lastLocationUpdate;
    }

    function setLastLocationUpdate($lastLocationUpdate) {
        $this->lastLocationUpdate = $lastLocationUpdate;
    }
    
    /**
     * Checks if the last location update was in the last 24 hours.
     * If not the device is not active
     */
    function isActive(){
        if (!isset($this->lastLocationUpdate)){
            return true;
        }
        
        $now = time(); // or your date as well
        $lastUpdate = strtotime($this->lastLocationUpdate);
        $datediff = $now - $lastUpdate;
        $daysDiff = $datediff / (60 * 60 * 24);
        
        if ($daysDiff >= 24){
            return false;
        }
        
        return true;
    }
}

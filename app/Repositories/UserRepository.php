<?php namespace App\Repositories;

use App\Http\Helpers\DeleteFolder;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;

use App\Repositories\AlertRepository;
use App\Repositories\SettingsRepository;
use App\Repositories\RelationshipRepository;
use App\Models\User;
use App\Models\Relationship;
use App\Models\Alert;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Description of UserController
 *
 * @author Takunda Chirema
 */

class UserRepository extends BaseRepository implements  AuthenticatableContract ,CanResetPasswordContract{
    
    var $userId;
    var $name;
    var $email;
    var $salt;
    private $password;
    var $phoneNumber;
    var $profilePicture;
    
    var $fbId;
    var $fbEmail;
    var $fbName;
    
    var $country;
    var $latitude;
    var $longitude;
    var $location;
    var $lastSeen;
    
    var $companyId;
    private $companies = array();
    
    var $status;
    var $alert;
    var $visibility = array();
    var $settings;
    var $mileage;
    
    var $token;
    private $firebasetoken;
    
    function __construct($userId) {
       $this->userId = $userId;	
    }

    /**
     * The method will try and save the given user in the neo4j database
     * using the userId and any additional details
     */
    function create(){
        
        $currentUser = User::query()
                        ->where('userId','=',$this->userId)
                        ->first();
        
        if (!is_null($currentUser)){
            // There exists user with same userId
            $response["success"] = 0;
            $response["message"] = "I'm sorry, this username is already in use";
            return $response;
        }
        
        $this->createUserNode();
       
        $response["success"] = 1;
        $response["message"] = "Username Successfully Added!";
        return $response;
        
    }
    
    public function createUserNode(){
        $user = new User;
        
        $user->userId = $this->userId;
        $user->name = $this->name;
        $user->password = $this->password;
        $user->salt = $this->salt;
        $user->phoneNumber = $this->phoneNumber;
        $user->email = $this->email;
        $user->status = "offline";
        $user->fbId = $this->fbId;
        $user->fbEmail = $this->fbEmail;
        $user->fbName = $this->fbName;
        $user->lastSeen = $this->lastSeen;

        $user->save();
        
        if (file_exists(base_path('public/profiles/'.$this->userId))){
            DeleteFolder::delTree(base_path('public/profiles/'.$this->userId));
        }
        
        $old = umask(0);
        mkdir(base_path('public/profiles/'.$this->userId), 0777, true);
        umask($old);
        
        return $user;
    }

    /**
     * This method returns users given a query
     */
    public static function getUsers(){
        $users = User::all();
        
        $response['users'] = array();

        if (!is_null($users)) {
            foreach($users as $user) {
                $userRep = UserRepository::getUser($user->userId);
                array_push($response["users"], $userRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response;   
    }
    
    public static function getUser($userId){
        $user = UserRepository::getUserNode($userId, null);
        
        $userRep = UserRepository::getUserRepository($user);
        
        return $userRep;
    }
    
    public static function getUserRepository($user)
    {
        if (!$user){
            return null;
        }
        
        $userRep = new UserRepository($user->userId);
        $userRep->name = $user->name;
        $userRep->salt = $user->salt;
        $userRep->setPassword($user->password);
        $userRep->latitude = $user->latitude;
        $userRep->longitude = $user->longitude;
        $userRep->status = $user->status;
        $userRep->lastSeen = $user->lastSeen;
        $userRep->location = $user->location;
        $userRep->phoneNumber = $user->phoneNumber;
        $userRep->email = $user->email;
        $userRep->fbEmail = $user->fbEmail;
        $userRep->fbId = $user->fbId;
        $userRep->fbName = $user->fbName;
        $userRep->mileage = $user->mileage;
        
        $userRep->setFirebasetoken($user->firebasetoken);
                
        $alert = AlertRepository::getAlert($user->userId, null);
        $userRep->setAlert($alert);
        
        $settings = SettingsRepository::getSettings($user->userId,null);
        $userRep->setSettings($settings);

        //check for the user profile picture
        $filename = public_path('profiles/'.$user->userId);
        $file_url = url('profiles/'.$user->userId);

        if (file_exists($filename)){

            $pictures = scandir($filename,1);
            $picture_url = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);

            if ($userRep->isValidImage($picture_url)){
                $userRep->setProfilePicture($picture_url);
            }
        }
        
        return $userRep;
    }
    
    public static function getUserByValue($property,$value)
    {
        try{
            $userNode = User::query()
                        ->where($property,'=',$value)
                        ->first();
            $userRep = UserRepository::getUserRepository($userNode);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
        
        return $userRep;
    }
    
    public static function getFriends($userId){
        $response['users'] = array();
        
        $friendsWith = Relationship::query()
                        ->where(function($query) use($userId)
                            {
                                $query->where('entityId','=',$userId)
                                ->orWhere('relatedEntityId','=',$userId);
                            })
                        ->where('type','=', RelationshipRepository::$userFriendsWith)
                        ->get();

        if (!is_null($friendsWith)) {
            foreach($friendsWith as $relation) {
                $relationshipRep = RelationshipRepository::getRelationship($relation->relationshipId);
                if ($relationshipRep->getProperty('state') === 'accepted'){
                    
                    if ($relation->entityId === $userId){
                        $friendId = $relation->relatedEntityId;
                    }else{
                        $friendId = $relation->entityId;
                    }
                    
                    $userRep = UserRepository::getUser($friendId);
                    
                    $userRep->visibility['visible'] = $relationshipRep->getProperty('/visibility/visible');
                    $userRep->visibility['setBy'] = $relationshipRep->getProperty('/visibility/setby');
                    
                    array_push($response["users"], $userRep);
                }
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response;   
    }
    
    public static function getRandomUsers($userId,$limit)
    {
        $response["users"] = array();
        $users = User::orderBy(DB::raw('RAND()'))
                ->take($limit)
                ->get();
        
        if (!is_null($users)) {
            foreach($users as $user) {
                $relationshipRep = UserRepository::getFriendRelation($userId,$user->userId);
                if (!$relationshipRep && $userId != $user->userId){
                    $userRep = UserRepository::getUser($user->userId);
                    array_push($response["users"], $userRep);
                }
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response;
    }
    
    public static function searchForUsers($userId,$searchString)
    {
        $response["users"] = array();
        $users = User::query()
                ->where('userId','like','%'.$searchString.'%')
                ->get();
        
        if (!is_null($users)) {
            foreach($users as $user) {
                $relationshipRep = UserRepository::getFriendRelation($userId,$user->userId);
                if (!$relationshipRep && $userId != $user->userId){
                    $userRep = UserRepository::getUser($user->userId);
                    array_push($response["users"], $userRep);
                }
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response;
    }
    
    /**
     * Recursive method. Very costly, use depths of 2 not more.
     * Will return results with duplicates, sort that out after the method call.
     * Also contains the users friends.
     * 
     * @param type $userId
     * @param type $depth
     * @return $response['users'] array with suggested friends of friends
     */
    public static function getFriendsOfFriends($userId,$depth){
        $response['users'] = array();
        
        if ($depth <= 0){
            $response['sucess'] = 1;
            $response['message'] = 'No Friends of Friends';
            return $response;
        }
        
        $friends = UserRepository::getFriends($userId)['users'];
        $response['users'] = $friends;
        
        foreach ($friends as $friend)
        {
            $depth = $depth - 1;
            $friendsOfFriend = UserRepository::getFriendsOfFriends($friend->userId,$depth)['users'];
            $response['users'] = array_merge($response['users'], $friendsOfFriend);
        }
        
        $response['sucess'] = 1;
        $response['message'] = 'Retrived Friends of Friends';
        return $response;
    }
    
    public static function getAlertingUsers($employeeId,$latitude,$longitude)
    {
        $response['users'] = array();
        
        $alerts = Alert::query()
                ->where('userId','<>',null)
                ->where('dpLatitude','=',$latitude)
                ->where('dpLongitude','=',$longitude)
                ->get();
        
        if (!is_null($alerts)) {
            foreach($alerts as $alert) {
                $userRep = UserRepository::getUser($alert->userId);
                $alertRep = AlertRepository::getAlert($alert->userId, null);
                
                $relation = Relationship::query()
                            ->where('entityId','=',$employeeId)
                            ->where('relatedEntityId','=',$alert->userId)
                            ->where('type','=', RelationshipRepository::$employeeSeenAlert)
                            ->first();
                
                if ($relation){
                    $alertRep->setSeen('true');
                }
                
                $userRep->setAlert($alertRep);
                
                array_push($response["users"], $userRep);
            }
        }
        
        $response['success'] = 1;
        $response['message'] = 'Retrieved Alerting Users';
        return $response;
    }
    
    public static function getRequestingUsers($userId)
    {
        $response['users'] = array();
        
        $friendsWith = Relationship::query()
                        ->where('relatedEntityId','=',$userId)
                        ->where('type','=', RelationshipRepository::$userFriendsWith)
                        ->get();

        if (!is_null($friendsWith)) {
            foreach($friendsWith as $relation) {
                $relationshipRep = RelationshipRepository::getRelationship($relation->relationshipId);
                if ($relationshipRep->getProperty('state') === 'pending'){
                    
                    $userRep = UserRepository::getUser($relation->entityId);
                    
                    $userRep->visibility['visible'] = $relationshipRep->getProperty('/visibility/visible');
                    $userRep->visibility['setBy'] = $relationshipRep->getProperty('/visibility/setby');
                    
                    array_push($response["users"], $userRep);
                }
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response; 
    }
    
    public static function getRequestedUsers($userId)
    {
        $response['users'] = array();
        
        $friendsWith = Relationship::query()
                        ->where('entityId','=',$userId)
                        ->where('type','=', RelationshipRepository::$userFriendsWith)
                        ->get();

        if (!is_null($friendsWith)) {
            foreach($friendsWith as $relation) {
                $relationshipRep = RelationshipRepository::getRelationship($relation->relationshipId);
                if ($relationshipRep->getProperty('state') === 'pending'){
                    
                    $userRep = UserRepository::getUser($relation->relatedEntityId);
                    
                    $userRep->visibility['visible'] = $relationshipRep->getProperty('/visibility/visible');
                    $userRep->visibility['setBy'] = $relationshipRep->getProperty('/visibility/setby');
                    
                    array_push($response["users"], $userRep);
                }
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Users";
        return $response; 
    }
    
    public static function updateVisibility($userId,$friendId,$visibility)
    {
        $relation = UserRepository::getFriendRelation($userId, $friendId);

        if (!is_null($relation)) {
            $relationshipRep = RelationshipRepository::getRelationship($relation->relationshipId);
            $relationshipRep->updateProperty('/visibility/visible', $visibility);
            $relationshipRep->updateProperty('/visibility/setby', $userId);
        }
    }
    
    public static function getFriendRelation($userId,$friendId){
        $relation = Relationship::query()
                    ->where(function($query) use($userId,$friendId)
                        {
                            $query->where('entityId','=',$userId)
                            ->orWhere('entityId','=',$friendId);
                        })
                    ->where(function($query) use($userId,$friendId)
                        {
                            $query->where('relatedEntityId','=',$friendId)
                            ->orWhere('relatedEntityId','=',$userId);
                        })
                    ->where('type','=', RelationshipRepository::$userFriendsWith)
                    ->first();
                        
        return $relation;
    }
    
    public static function updateFriendship($userId,$friendId,$property,$value)
    {
        $relation = UserRepository::getFriendRelation($userId, $friendId);

        if (!is_null($relation)) {
            $relationshipRep = RelationshipRepository::getRelationship($relation->relationshipId);
            $relationshipRep->updateProperty($property, $value);
        }
    }
    
    public static function sendRequest($userId,$friendId)
    {
        $result = array();
        
        $relation = UserRepository::getFriendRelation($userId, $friendId);
        
        if ($relation){
            $result['success'] = 1;
            $result['message'] = 'Friendship is pending acceptance';
            return $result;
        }
        
        $relationshipRep = new RelationshipRepository;
        $relationshipRep->entityId=$userId;
        $relationshipRep->relatedEntityId=$friendId;
        $relationshipRep->type = RelationshipRepository::$userFriendsWith;
        $relationshipRep->create();
        
        $relationshipRep->insertProperty('state');
        $relationshipRep->updateProperty('state', 'pending');
        
        $relationshipRep->insertProperty('visibility');
        $relationshipRep->insertProperty('visible','visibility');
        $relationshipRep->updateProperty('visibility/visible', 'yes');
        
        $relationshipRep->insertProperty('visibility');
        $relationshipRep->insertProperty('setby','visibility');
        $relationshipRep->updateProperty('visibility/setby', $userId);
        
        $result['success'] = 1;
        $result['message'] = 'Request has been sent';
        return $result;
    }
    
    public static function unfriend($userId,$friendId)
    {
        $relation = UserRepository::getFriendRelation($userId, $friendId);
        $relation->delete();
    }
    
    
    public function isValidImage($imageUrl)
    {
        if (preg_match('/\.(jpeg|jpg|png|gif)$/i', $imageUrl)) {
            return true;
        } else {
            return false;
        }
    }

    public function getAuthIdentifierName()
    {
        return "userId";
    }
    public function getAuthIdentifier()
    {
        return $this->userId;
    }
    public function getAuthPassword()
    {
            return $this->password;
    }
    public function getRememberToken()
    {
        return $this->token;
    }
    public function setRememberToken( $value)
    {
         $token = $value;
    }
    public function getRememberTokenName()
    {
        return "token";
    }
    /**
     * Gets the neo4j node of the user
     * 
     * @param type $userId
     * @param type $userCompanyId
     * @return type
     */
    public static function getUserNode($userId,$companyIdId){
        
        if (!$companyIdId){
            $user = User::query()
                    ->where('userId','=',$userId)
                    ->first();
        }else{
            $user = User::query()
                ->where('userId','=',$userId)
                ->where('companyId','=',$companyIdId)
                ->first();
        }
        
        return $user;
    }
    
    // Do not update the user cache here.
    // too many frequent location updates will degrade server performance if 
    // we update the cache the current way of doing it.
    function update($properties){
        
        try{
            if (!$this->companyId){
                User::query()
                        ->where('userId','=',$this->userId)
                        ->update($properties);
            }else{
                User::query()
                    ->where('userId','=',$this->userId)
                    ->where('companyId','=',$this->companyIdId)
                    ->update($properties);
            }
        }catch(\Illuminate\Database\QueryException $ex){
            $response["success"] = 1;
            $response["message"] = $ex->getMessage();
            return $response;
        }
        
        $response["success"] = 1;
        $response["message"] = "Successfully updated!";
        return $response;
    }
    
    public static function updateCacheRepository($userId){
        $user = UserRepository::getUserNode($userId, null);
        $userRep = UserRepository::getUserRepository($user);
        BaseRepository::putItemIntoCache(null, CacheType::Users, CacheExpiry::Users, $userId, $userRep);
    }
    
    function uploadProfilePicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/profiles/'.$this->userId);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        $file->move($path,$file->getClientOriginalName());
        //Images::compress($file,$path."/".$file->getClientOriginalName());
        
        $response["success"] = 1;
        $response["message"] = "Profile picture uploaded";
        $response["url"] = url('profiles/'.$this->userId.'/'.$file->getClientOriginalName());
        return $response;
        
    }
    
    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }
    function getSalt() {
        return $this->salt;
    }

    function getPassword() {
        return $this->password;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function getCountry() {
        return $this->country;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getLocation() {
        return $this->location;
    }

    function getStatus() {
        return $this->status;
    }

    function getResponse() {
        return $this->response;
    }

    function setSalt($salt) {
        $this->salt = $salt;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    function setCountry($country) {
        $this->country = $country;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setLocation($location) {
        $this->location = $location;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setResponse($response) {
        $this->response = $response;
    }
    
    function getProfilePicture() {
        return $this->profilePicture;
    }

    function getLastSeen() {
        return $this->lastSeen;
    }

    function setProfilePicture($profilePicture) {
        $this->profilePicture = $profilePicture;
    }

    function setLastSeen($lastSeen) {
        $this->lastSeen = $lastSeen;
    }
    
    function getId() {
        return $this->userId;
    }

    function setId($userId) {
        $this->userId = $userId;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getAlert() {
        return $this->alert;
    }

    function setAlert($alert) {
        $this->alert = $alert;
    }
    
    function getFriendID() {
        return $this->friendID;
    }

    function getVisibility() {
        return $this->visibility;
    }

    function setFriendID($friendID) {
        $this->friendID = $friendID;
    }

    function setVisibility($visibility) {
        $this->visibility = $visibility;
    }
    
    function setUserNode($userNode) {
        $this->userNode = $userNode;
    }
    function setUserToken($token)
    {   
        $this->token = $token;
    }
    
    function getSettings() {
        return $this->settings;
    }

    function setSettings($settings) {
        $this->settings = $settings;
    }
    function getEmailForPasswordReset(){
        return $this->email;
    }
    
    function sendPasswordResetNotification( $token)
    {
    }
    
    function getFbId() {
        return $this->fbId;
    }

    function getFbEmail() {
        return $this->fbEmail;
    }

    function getFbName() {
        return $this->fbName;
    }

    function setFbId($fbId) {
        $this->fbId = $fbId;
    }

    function setFbEmail($fbEmail) {
        $this->fbEmail = $fbEmail;
    }

    function setFbName($fbName) {
        $this->fbName = $fbName;
    }
    
    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }
    
    function getCompanies() {
        return $this->companies;
    }

    function setCompanies($companies) {
        $this->companies = $companies;
    }
    
    function getFirebasetoken() {
        return $this->firebasetoken;
    }

    function setFirebasetoken($firebasetoken) {
        $this->firebasetoken = $firebasetoken;
    }
    
    function getMileage() {
        return $this->mileage;
    }

    function setMileage($mileage) {
        $this->mileage = $mileage;
    }
}

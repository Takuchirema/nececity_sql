<?php namespace App\Repositories;

use Datetime;
use App\Repositories\BaseRepository;
use App\Models\Comment;

/**
 * Description of Comment
 *
 * @author Takunda Chirema
 */

class CommentRepository extends BaseRepository{
    
    var $commentId;
    var $userId;
    var $postId;
    var $promotionId;
    var $cataloguePostId;
    var $companyId;
    var $time;
    var $formattedTime;
    var $color;
    var $comment;
    
    function __construct($userId,$comment) {
       $this->userId=$userId;
       $this->comment=$comment;
       
       $this->stringToColorCode($userId);
    }
    
    function create(){
        
        $comment = $this->createNode();
        $this->commentId = $comment->commentId;
        
        $response["success"] = 1;
        $response["commentId"] = $comment->commentId;
        $response['comment'] = $this->comment;
        $response['id'] = $this->commentId;
        $response['userID'] = $this->userId;
        $response['time'] = $this->time;
        $response['company'] = $this->companyId;
        
        if (isset($this->postId)){
            $response['postId'] = $this->postId;
        }else{
            $response['promotionId'] = $this->promotionId;
        }
 
        $response["message"] = "Comment has been posted!";
        return $response;
    }
    
    function createNode(){
        
        $comment = new Comment;
        
        $date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $comment->time = $date;
        
        if (isset($this->postId)){
            $comment->postId = $this->postId;
        }else if (isset($this->promotionId)){
            $comment->promotionId = $this->promotionId;
        }else if (isset($this->cataloguePostId)){
            $comment->cataloguePostId = $this->cataloguePostId;
        }
        
        $comment->comment = $this->comment;
        $comment->companyId = $this->companyId;
        $comment->userId = $this->userId;

        $comment->save();
        
        return $comment;
    }
    
    public static function getComments($postId,$promotionId,$cataloguePostId){
        
        $response['comments'] = array();
        
        if (!is_null($postId)){
            $comments = Comment::query()
                    ->where('postId','=',$postId)
                    ->get();
        }elseif (!is_null($promotionId)){
            $comments = Comment::query()
                    ->where('promotionId','=',$promotionId)
                    ->get();
        }elseif (!is_null($cataloguePostId)){
            $comments = Comment::query()
                    ->where('cataloguePostId','=',$cataloguePostId)
                    ->get();
        }
        
        foreach ($comments as $comment) {
              
            $commentRep = new CommentRepository($comment->userId,$comment->comment);
            
            $time = CommentRepository::getDateTimeString($comment->time);
            
            $commentRep->setTime($time);
            $commentRep->setCompanyId($comment->companyId);
            $commentRep->setPostId($comment->postId);
            $commentRep->setPromotionId($comment->promotionId);

            array_push($response["comments"], $commentRep);
        }
        
        $response['success'] = 1;
        $response['message'] = 'Retrieved Comments';
        return $response;
    }
    
    function stringToColorCode($str) {
        $code = dechex(crc32($str));
        $code = "#".substr($code, 0, 6);
        $this->color = $code;
    }
    
    function getColor() {
        return $this->color;
    }

    function setColor($color) {
        $this->color = $color;
    }

        function getUserID() {
        return $this->userID;
    }

    function getTime() {
        return $this->time;
    }

    function getComment() {
        return $this->comment;
    }

    function getCommentId() {
        return $this->commentId;
    }

    function setUserID($userID) {
        $this->userID = $userID;
    }

    function setTime($time) {
        $this->time = $time;
        
        $timeArray = explode("_", $time);
        $date = $timeArray[0];
        
        $this->formattedTime = date("d F Y", strtotime($date))." ".$timeArray[1];
    }
    
    function getFormattedTime() {
        return $this->formattedTime;
    }

    function setFormattedTime($formattedTime) {
        $this->formattedTime = $formattedTime;
    }

    function setComment($comment) {
        $this->comment = $comment;
    }

    function setCommentId($commentId) {
        $this->commentId = $commentId;
    }
    
    function getPostId() {
        return $this->postId;
    }

    function setPostId($postId) {
        $this->postId = $postId;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    function getPromotionId() {
        return $this->promotionId;
    }

    function setPromotionId($promotionId) {
        $this->promotionId = $promotionId;
    }
    
    function getCataloguePostId() {
        return $this->cataloguePostId;
    }

    function setCataloguePostId($cataloguePostId) {
        $this->cataloguePostId = $cataloguePostId;
    }


}

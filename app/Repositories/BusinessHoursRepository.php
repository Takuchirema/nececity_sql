<?php namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\BusinessHours;

/**
 * Description of BusinessHours
 *
 * @author Takunda Chirema
 */
class BusinessHoursRepository extends BaseRepository {
    
    var $companyId;
    var $mon;
    var $tue;
    var $wed;
    var $thu;
    var $fri;
    var $sat;
    var $sun;
    
    function __construct($companyId) {
       $this->companyId=$companyId;
    }
    
    function create(){
        
        $businessHours = BusinessHours::query()->where('companyId','=', $this->companyId)->first();
        
        if (!is_null($businessHours)){
            $response["success"] = 0;
            $response["message"] = "Business Hours already exist";
            return $response;
        }
        
        $this->createBusinessHours();
    }
    
    function createBusinessHours(){
        
        $businessHours = new BusinessHours;
        
        $businessHours->companyId = $this->companyId;

        $businessHours->mon = $this->mon;
        $businessHours->tue = $this->tue;
        $businessHours->wed = $this->wed;
        $businessHours->thu = $this->thu;
        $businessHours->fri = $this->fri;
        $businessHours->sat = $this->sat;
        $businessHours->sun = $this->sun;
        
        $businessHours->save();
        
        return $businessHours;
    }
    
    function businessHoursExists(){
        
        $businessHours = BusinessHours::query()
                        ->where('companyId','=',$this->companyId);
        
        if (is_null($businessHours)){
            return false;
        }
        
        return true;
    }
    
    /*
     * Returns the model object.
     */
    public static function getBusinessHoursNode($companyId){
        
        $businessHours = BusinessHours::query()
                        ->where('companyId','=',$companyId)->first();
        
        return $businessHours;
    }
    
    public static function getBusinessHours($companyId){
        
        $businessHoursNode = BusinessHoursRepository::getBusinessHoursNode($companyId);
        
        if (!isset($businessHoursNode)){
            return null;
        }
            
        $businessHours = new BusinessHoursRepository($businessHoursNode->companyId);

        $businessHours->setMon($businessHoursNode->mon);
        $businessHours->setTue($businessHoursNode->tue);
        $businessHours->setWed($businessHoursNode->wed);
        $businessHours->setThu($businessHoursNode->thu);
        $businessHours->setFri($businessHoursNode->fri);
        $businessHours->setSat($businessHoursNode->sat);
        $businessHours->setSun($businessHoursNode->sun);
        
        return $businessHours;
    }

    function getMon() {
        return $this->mon;
    }

    function getTue() {
        return $this->tue;
    }

    function getWed() {
        return $this->wed;
    }

    function getThu() {
        return $this->thu;
    }

    function getFri() {
        return $this->fri;
    }

    function getSat() {
        return $this->sat;
    }

    function getSun() {
        return $this->sun;
    }

    function setMon($mon) {
        $this->mon = $mon;
    }

    function setTue($tue) {
        $this->tue = $tue;
    }

    function setWed($wed) {
        $this->wed = $wed;
    }

    function setThu($thu) {
        $this->thu = $thu;
    }

    function setFri($fri) {
        $this->fri = $fri;
    }

    function setSat($sat) {
        $this->sat = $sat;
    }

    function setSun($sun) {
        $this->sun = $sun;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
}

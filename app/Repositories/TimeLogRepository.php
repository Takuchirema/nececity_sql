<?php namespace App\Repositories;

use Datetime;
use App\Repositories\EmployeeRepository;
use App\Models\TimeLog;
/**
 * Description of RouteTimeLog
 *
 * @author Takunda Chirema
 */
class TimeLogRepository extends BaseRepository {
    
    var $timeLogId;
    var $routeId;
    var $routePointId;
    var $employeeId;
    var $employeeName;
    var $date;
    var $arrivalTime;
    var $departureTime;
    var $companyId;
    
    function __construct($employeeId,$companyId) {
       $this->companyId=$companyId;
       $this->employeeId = $employeeId;
    }
    
    function create(){
        $timeLog = new TimeLog;
        $timeLog->employeeId = $this->employeeId;
        $timeLog->companyId = $this->companyId;
        $timeLog->routeId = $this->routeId;
        $timeLog->routePointId = $this->routePointId;
        
        $date = Datetime::createFromFormat($this->dateFormat, $this->date);
        $timeLog->date = $date;
        
        $arrivalTime = Datetime::createFromFormat($this->timeFormat, $this->arrivalTime);
        $timeLog->arrivalTime = $arrivalTime;
        
        $departureTime = Datetime::createFromFormat($this->timeFormat, $this->departureTime);
        $timeLog->departureTime = $departureTime;
        
        $timeLog->save();
    }
    
    public static function getTimeLogRepository($timeLog){
        $timeLogRep = new TimeLogRepository($timeLog->employeeId,$timeLog->companyId);
        $timeLogRep->timeLogId = $timeLog->timeLogId;
        
        $employeeRep = EmployeeRepository::getRepository($timeLog->employeeId, $timeLog->companyId);
        $timeLogRep->setEmployeeName($employeeRep->name);
        
        $timeLogRep->routeId = $timeLog->routeId;
        $timeLogRep->routePointId = $timeLog->routePointId;
        
        $date = TimeLogRepository::getDateString($timeLog->date);
        $timeLogRep->date = $date;
        
        $arrivalTime = TimeLogRepository::getTimeString($timeLog->arrivalTime);
        $timeLogRep->arrivalTime = $arrivalTime;
        
        $departureTime = TimeLogRepository::getTimeString($timeLog->departureTime);
        $timeLogRep->departureTime = $departureTime;
        
        return $timeLogRep;
    }
    
    function getRouteTimeLogs($routeId){
        $response = array();
        
        $response['timeLogs'] = array();
        
        $timeLogs = TimeLog::query()
                    ->where('companyId','=',$this->companyId)
                    ->where('employeeId','=',$this->employeeId)
                    ->where('routeId','=',$routeId)
                    ->orderBy('arrivalTime', 'asc')
                    ->get();
        
        foreach ($timeLogs as $timeLog){
            $timeLogRep = TimeLogRepository::getTimeLogRepository($timeLog);
            TimeLogRepository::setDateArray($response['timeLogs'], $timeLogRep->date, $timeLogRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Time Logs";
        return $response; 
    }
    
    function getDateTimeLogs($dateStr){
        $response = array();
        
        $response['timeLogs'][$dateStr] = array();
        
        $timeLogs = TimeLog::query()
                    ->where('companyId','=',$this->companyId)
                    ->where('employeeId','=',$this->employeeId)
                    ->where('date','=',$dateStr)
                    ->orderBy('arrivalTime', 'asc')
                    ->get();
        
        foreach ($timeLogs as $timeLog){
            $timeLogRep = TimeLogRepository::getTimeLogRepository($timeLog);
            TimeLogRepository::setDateArray($response['timeLogs'], $timeLogRep->date, $timeLogRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Time Logs";
        return $response; 
    }
    
    function getRouteDateTimeLogs($routeId,$dateStr){
        $response = array();
        
        $response['timeLogs'][$dateStr] = array();
        
        $query = TimeLog::query()
                ->where('companyId','=',$this->companyId);
                
        if ($routeId){
            $query->where('routeId','=',$routeId);
        }
        
        if ($dateStr){
            $query->where('date','=',$dateStr);
        }
        
        if ($this->employeeId){
            $query->where('employeeId','=',$this->employeeId);
        }
        
        $query = $query
                ->orderBy('arrivalTime', 'asc');
        
        $timeLogs = $query->get();
        
        foreach ($timeLogs as $timeLog){
            $timeLogRep = TimeLogRepository::getTimeLogRepository($timeLog);
            TimeLogRepository::setDateArray($response['timeLogs'], $timeLogRep->date, $timeLogRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Time Logs";
        return $response; 
    }
    
    public static function deteleTimeLogs($companyId,$dateStr,$employeeId,$routeId){
        TimeLog::query()
            ->where('companyId','=',$companyId)
            ->where('employeeId','=',$employeeId)
            ->where('date','=',$dateStr)
            ->where('routeId', '=',$routeId)
            ->delete();
    }
    
    public static function getAllTimeLogs($companyId){
        $response['timeLogs'] = array();
        
        $currentDate = new Datetime;
        $currentDateStr = date_format($currentDate,'Y-m-d');
        
        $backdate = date('Y-m-d', strtotime($currentDateStr.' - 30 days'));
        
        $timeLogs = TimeLog::query()
                    ->where('companyId','=',$companyId)
                    ->where('date','>=',$backdate)
                    ->orderBy('arrivalTime', 'asc')
                    ->get();
        
        foreach ($timeLogs as $timeLog){
            $timeLogRep = TimeLogRepository::getTimeLogRepository($timeLog);
            TimeLogRepository::setDateArray($response['timeLogs'], $timeLogRep->date, $timeLogRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Time Logs";
        return $response; 
    }
    
    public static function setDateArray(& $logsArray,$date,$timeLogRep)
    {
        $employeeId = $timeLogRep->employeeId;
        if (isset($logsArray[$date])){
            TimeLogRepository::setEmployeeArray($logsArray[$date], $employeeId, $timeLogRep);
        }else{
            $logsArray[$date] = array();
            TimeLogRepository::setEmployeeArray($logsArray[$date], $employeeId, $timeLogRep);
        }
    }
    
    public static function setEmployeeArray(& $dateArray,$employeeId,$timeLogRep){
        $routeId = $timeLogRep->routeId;
        if (isset($dateArray[$employeeId])){
            TimeLogRepository::setRouteArray($dateArray[$employeeId], $routeId, $timeLogRep);
        }else{
            $dateArray[$employeeId] = array();
            TimeLogRepository::setRouteArray($dateArray[$employeeId], $routeId, $timeLogRep);
        }
    }
    
    public static function setRouteArray(& $employeeArray,$routeId,$timeLogRep)
    {
        if (isset($employeeArray[$routeId])){
            array_push($employeeArray[$routeId], $timeLogRep);
        }else{
            $employeeArray[$routeId] = array();
            array_push($employeeArray[$routeId], $timeLogRep);
        }
    }
    
    
    function getTimeLogId() {
        return $this->timeLogId;
    }

    function getRouteId() {
        return $this->routeId;
    }

    function getRoutePointId() {
        return $this->routePointId;
    }

    function getEmployeeId() {
        return $this->employeeId;
    }

    function getEmployeeName() {
        return $this->employeeName;
    }

    function getDate() {
        return $this->date;
    }

    function getArrivalTime() {
        return $this->arrivalTime;
    }

    function getDepartureTime() {
        return $this->departureTime;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function setTimeLogId($timeLogId) {
        $this->timeLogId = $timeLogId;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }

    function setRoutePointId($routePointId) {
        $this->routePointId = $routePointId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    function setEmployeeName($employeeName) {
        $this->employeeName = $employeeName;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setArrivalTime($arrivalTime) {
        $this->arrivalTime = $arrivalTime;
    }

    function setDepartureTime($departureTime) {
        $this->departureTime = $departureTime;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

}

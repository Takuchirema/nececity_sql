<?php namespace App\Repositories;

use App\Models\Route;
use App\Repositories\RouteRepository;
use App\Repositories\TimeRepository;
use App\Repositories\LocationClusterRepository;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\Constants;
use App\Http\Helpers\LatLngDetails;
use App\Http\Helpers\UserClusterLocation;
use SimpleXMLElement;
use Datetime;
use DateTimeZone;
use Log;

/**
 * Description of Route
 *
 * @author Takunda Chirema
 */

class RouteRepository extends BaseRepository{
    
    var $lastClusterCalculation;
    var $points = array();
    var $routeId;
    var $routeName;
    var $companyId;
    var $routeNode;
    var $lastTimetableRefresh;
    var $color;
    var $basicColor;
    //xml string
    var $routePoints;
    
    var $locationClusters = array();
    var $userClusterLocations = array();
    
    //This is stored temporarily for mapping out unknown routes.
    //The structure is like this:
    //lat,lng,lat,lng,lat,lng;lat,lng,lat,lng,lat,lng;
    //The first lat,lng are unrounded.
    //The second is rounded to 2dp.
    //The third is rounded to 3dp
    private $routeCoordinates;
    
    //This is a hashmap with 3 keys:
    //First is "latlng"
    //Second is "latlng2"
    //Third is "latlng3"
    //{ 'latlng'{'13.12345,13.12345'} 'latlng2'{'13.12,13.12'} 'latlng3'{'13.123,13.123'} }
    //We will use this to check whether a route that is being plotted is now being repeated.
    //We check the 2dp or 3dp array.
    //When we repeat a coordinate, check whether it is a repeat of the beginning of route.
    //If it is remove all points which are before the repeated point.
    //Then create a new Unkown route which must be given a name and times.
    //To create one, we first check if there is no such existing route already.
    private $structuredRouteCoordinates = array();
    
    //This will have latlng1 as the key and the routpoint as the value.
    public $busStopPoints = array();
    
    function __construct($routeId) {
       $this->routeId=$routeId;
    }
    
    function create(){
        //Remove old route with similar Id
        Route::query()
                    ->where('companyId','=',$this->companyId)
                    ->where('routeName','=',$this->routeName)
                    ->delete();
        
        $routeNode = $this->createNode();
        $this->routeNode = $routeNode;
        $this->routeId = $routeNode->routeId;
        
        RouteRepository::updateCacheRepository($this->companyId, $routeNode->routeId);
        
        $response["success"] = 1;
        $response["routeName"] = $this->routeName;
        $response["routeId"] = $routeNode->routeId;
        $response["message"] = "Route has been created";
        return ($response);
    }
    
    function createNode(){
        $routeNode = new Route;
        
        //create the new route
	$routeNode->routeName = $this->routeName;
	$routeNode->companyId = $this->companyId;
        $routeNode->color = $this->color;
        
        $date = new DateTime();
        $routeNode->lastTimetableRefresh = $date;
        
        $routeNode->save();
        
        return $routeNode;
    }
    
    public static function createUnknownRoute($companyId, $structuredCoordinates, $sort = true){
        if (!isset($companyId)){
            return;
        }
        
        $companyRep = CompanyRepository::getRepository($companyId);
        
        if ($companyRep->discoverRoutes !== "true"){
            return;
        }
        
        if ($sort){
            //print_r($structuredCoordinates['latlng3']);
            $structuredCoordinates = RouteRepository::sortCoordinates($structuredCoordinates);
            //print_r($structuredCoordinates['latlng3']);
        }
        
        //echo "** count b4: ".count($structuredCoordinates['latlng1'])." **";
        $locationDetails = new LatLngDetails();
        $structuredCoordinates = $locationDetails->Snap_Coordinates_To_Road($structuredCoordinates);
        //Remove any points that are not repeated
        //RouteRepository::prepareStructuredCoordinates($structuredCoordinates);
        //echo "** count after: ".count($structuredCoordinates['latlng1'])." **";
        
        //Check if route does not already exist.
        $route = RouteRepository::matchUnknownRoute($companyId, $structuredCoordinates);
        
        if (isset($route)){
            //Log::debug("Unknown Route Matched");
            return;
        }
        
        $routeName = RouteRepository::getUnknownRouteName($structuredCoordinates["latlng1"]);
        
        $routeRep = new RouteRepository(null);
        $routeRep->setRouteName($routeName);
        $routeRep->setCompanyId($companyId);
        $routeRep->setColor('#0000A0');
        $routeRep->create();
        
        //echo "** route name: ".$routeName." size: ".count($structuredCoordinates['latlng1'])." **";
        $xml = RouteRepository::encodeStructuredCoordinates($structuredCoordinates, true);
        //echo "** xml len: ".strlen($xml);
        $routeRep->update(array('routePoints' => $xml));
        
        return $routeRep->routeId;
    }
    
    public static function sortCoordinates($structuredCoordinates){
        $times = $structuredCoordinates['time'];
        
        $sortedTimes = array();
        $indexedCoordinates = array();
        
        //put the array into key values with key being the time
        for($i = 0; $i < count($times); $i++){
            $time = strtotime($times[$i]);
            
            array_push($sortedTimes, $time);
            $indexedCoordinates['latlng1'][$time] = $structuredCoordinates['latlng1'][$i];
            $indexedCoordinates['latlng2'][$time] = $structuredCoordinates['latlng2'][$i];
            $indexedCoordinates['latlng3'][$time] = $structuredCoordinates['latlng3'][$i];
            $indexedCoordinates['repeat'][$time] = $structuredCoordinates['repeat'][$i];
            $indexedCoordinates['speed'][$time] = $structuredCoordinates['speed'][$i];
            $indexedCoordinates['bearing'][$time] = $structuredCoordinates['bearing'][$i];
        }
        
        //sort the times
        sort($sortedTimes);
        
        //put the new items back into structured coordinates but now in order
        $structuredCoordinates["latlng1"] = array();
        $structuredCoordinates["latlng2"] = array();
        $structuredCoordinates["latlng3"] = array();
        $structuredCoordinates["repeat"] = array();
        $structuredCoordinates["speed"] = array();
        $structuredCoordinates["bearing"] = array();
        
        for($i = 0; $i < count($sortedTimes); $i++){
            $sortedTime = $sortedTimes[$i];
            
            array_push($structuredCoordinates["latlng1"],$indexedCoordinates["latlng1"][$sortedTime]);
            array_push($structuredCoordinates["latlng2"],$indexedCoordinates["latlng2"][$sortedTime]);
            array_push($structuredCoordinates["latlng3"],$indexedCoordinates["latlng3"][$sortedTime]);
            array_push($structuredCoordinates["repeat"],$indexedCoordinates["repeat"][$sortedTime]);
            array_push($structuredCoordinates["speed"],$indexedCoordinates["speed"][$sortedTime]);
            array_push($structuredCoordinates["bearing"],$indexedCoordinates["bearing"][$sortedTime]);
        }
        
        return $structuredCoordinates;
    }
    
    public static function getUnknownRouteName($latlngs){
        $locationDetails = new LatLngDetails();
        
        $fromLat = explode(",", $latlngs[0])[0];
        $fromLng = explode(",", $latlngs[0])[1];
        
        $toLatLng = RouteRepository::getFarthestPointFromStart($latlngs);
        $toLat = explode(",", $toLatLng)[0];
        $toLng = explode(",", $toLatLng)[1];
        
        $fromDetails = $locationDetails->Get_Address_From_Google_Maps($fromLat, $fromLng);
        $toDetails = $locationDetails->Get_Address_From_Google_Maps($toLat, $toLng);
        
        $from = "Unknown";
        $to = "Unknown";
        if (isset($fromDetails['suburb'])){
            $from = $fromDetails['suburb'];
        }else if (isset($fromDetails['street'])){
            $from = $fromDetails['street'];
        }
        
        if (isset($toDetails['suburb'])){
            $to = $toDetails['suburb'];
        }else if (isset($toDetails['street'])){
            $to = $toDetails['street'];
        }
        
        return $from." - ".$to;
    }
    
    public static function getFarthestPointFromStart($latlngs){
        //We'll split into 4 parts and use these not all the coordinates
        $steps = max(count($latlngs)/4,1);
        $distance = 0;
        
        $fromLat = (float) explode(",", $latlngs[0])[0];
        $fromLng = (float) explode(",", $latlngs[0])[1];
        
        for($i = $steps; $i < count($latlngs); $i+=$steps){
            $latlng = $latlngs[$i];
            $toLat = (float) explode(",", $latlng)[0];
            $toLng = (float) explode(",", $latlng)[1];
            
            $calcdistance = LatLngDetails::distance($fromLat, $fromLng, $toLat, $toLng);
            if ($calcdistance > $distance){
                $distance = $calcdistance;
                $toLatLng = $latlng;
            }
        }
        
        if (isset($toLatLng)) {return $toLatLng;}
        
        return null;
    }
    
    public static function getLocationName($latitude, $longitude){
        $locationDetails = new LatLngDetails();
        $details = $locationDetails->Get_Address_From_Google_Maps($latitude, $longitude);
        $locationName = null;
        
        if (isset($details['street'])){
            $locationName = $details['street'];
        }
        
        return $locationName;
    }
    
    //This method removes points that were not repeated
    //We are Assuming they are not part of the main route it was a detour
    public static function prepareStructuredCoordinates($structuredCoordinates){
        $repeats = $structuredCoordinates["repeat"];
        
        for($i = 0; $i < count($repeats); ++$i){
            if ($repeats[$i] == 0){
                unset($structuredCoordinates["latlng1"][$i]);
                unset($structuredCoordinates["latlng2"][$i]);
                unset($structuredCoordinates["latlng3"][$i]);
            }
        }
        
        return $structuredCoordinates;
    }
    
    //Takes coordinates of an unknown route and see if it matches an already existing one.
    //It is expensive, but the number of calls to match unknown routes will not be numerous.
    //If degrades server performance then might have to rethink.
    public static function matchUnknownRoute($companyId, $structuredCoordinates){
        $latlngs = $structuredCoordinates["latlng3"];
        
        $routes = RouteRepository::getRoutes($companyId)["routes"];   
        $count = count($latlngs);
        $hits = 0;
        
        if ($count == 0){
            return null;
        }
        
        foreach($routes as $route){
            $routeLatLngs = $route->getStructuredRouteCoordinates()["latlng3"];
            foreach($latlngs as $latlng){
                if (in_array($latlng,$routeLatLngs)){
                    $hits = $hits + 1;
                }
            }
            $hitPercentage = ($hits/(float)$count)*100;
            $matchPercentage = Constants::getRouteMatchPercentage();
            if ($hitPercentage > $matchPercentage){
                ///echo "route matched: ".$matchPercentage;
                return $route;
            }
        }
        return null;
    }
    
    // Checks if a point is still part of the given route
    // If so returns true else false.
    public static function enroute($routeId, $companyId, $lat, $lng){
        $routeRep = RouteRepository::getRepository($routeId, $companyId);
        if (!isset($routeRep)){
            return false;
        }
        $latlng3s = $routeRep->getStructuredRouteCoordinates()["latlng3"];
        $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
        
        if (in_array($latlng3,$latlng3s)){
            return true;
        }
        return false;
    }

    public static function delete($companyId, $routeId){
        Route::query()
                    ->where('companyId','=',$companyId)
                    ->where('routeId','=',$routeId)
                    ->delete();
        
        BaseRepository::deleteItemFromCache($companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $routeId);
    }
    
    public static function encodeStructuredCoordinates($coordinates, $getLocationNames=false){
        $latlngs = $coordinates["latlng1"];
        //$median = (int)$this->getMedianRepeat($coordinates);
        //will hold the stops already registered to avoid repeating.
        
        $xml = new SimpleXMLElement('<xml/>');
        
        $prevTimeDiff=0;
        $distanceTravelled=0;
        $stopCount = 1;
        $pickingTime=Constants::getBusPickingTime();
        
        $count = count($latlngs);
        for ($i = 0; $i < $count; $i++){
            $latlng = $latlngs[$i];
            $latlngFrom = $latlngs[max($i-1,0)];
            
            $speed=$coordinates['speed'][$i];
            $bearing=$coordinates['bearing'][$i];
            
            $time1=strtotime($coordinates['time'][$i]);
            $time2=strtotime($coordinates['time'][min($i+1,$count-1)]);
            $timeDiff = $time2 - $time1;
            
            //echo '*** time diff: '.$timeDiff.' ***';
            $latitude = explode(",", $latlng)[0];
            $longitude = explode(",", $latlng)[1];
            
            $latitudeFrom = explode(",", $latlngFrom)[0];
            $longitudeFrom = explode(",", $latlngFrom)[1];
            $distanceFrom = LatLngDetails::distance($latitudeFrom, $longitudeFrom, $latitude, $longitude);
            
            $distanceTravelled += $distanceFrom;
            
            $xmlPoint = $xml->addChild('point');
            $xmlPoint->addAttribute('pointId',$i+1);
            $xmlPoint->addAttribute('latitude',$latitude);
            $xmlPoint->addAttribute('longitude',$longitude);
            $xmlPoint->addAttribute('time',$time1);
            $xmlPoint->addAttribute('speed',$speed);
            $xmlPoint->addAttribute('bearing',$bearing);
            $xmlPoint->addAttribute('distance',$distanceTravelled);
            
            if ($prevTimeDiff < $pickingTime && $timeDiff >= $pickingTime)
            {
                $name = 'Bus Stop '.$stopCount;
                if ($getLocationNames){
                    $locationName = RouteRepository::getLocationName($latitude,$longitude);
                    if (!BaseRepository::isEmptyOrNullString($locationName)){
                        $name = $locationName;
                    }
                }
                
                //echo "** bs time: ".$time1." diff: ".$timeDiff." **";
                $xmlPoint->addAttribute('busStop','true');
                $xmlPoint->addAttribute('name',$name);
                RouteRepository::createTimeTable($xmlPoint, $time1);
                $stopCount++;
            }else{
                $xmlPoint->addAttribute('busStop','false');
                $xmlPoint->addAttribute('name','');
            }
            
            $prevTimeDiff = $timeDiff;
        }
        
        $newXML = preg_replace("/\s+/", " ", $xml->asXML());
       
        return $newXML;
    }
    
    public static function createTimeTable(&$xmlPoint, $time){
        $timetable = $xmlPoint->addChild('timetable');
        
        $hr_min = date('H:i', $time);
        $timetable->addChild('time',$hr_min);
        
        return $xmlPoint;
    }
    
    function encodeToXml($points){
        $xml = new SimpleXMLElement('<xml/>');
        
        $distanceTravelled = 0;
        
        //The keys here might not be simply 0,1,2
        //They are like point1,point2 etc. Must change it though
        $keys = array_keys($points);
        for ($i=0;$i<count($points);$i++){
            $point = $points[$keys[$i]];
            $pointFrom = $points[$keys[max($i-1,0)]];
            
            if (!isset($point['id'])){
                continue;
            }
            
            $distanceFrom = LatLngDetails::distance($pointFrom['latitude'], $pointFrom['longitude'], $point['latitude'], $point['longitude']);
            $distanceTravelled += $distanceFrom;
            
            $xmlPoint = $xml->addChild('point');
            $xmlPoint->addAttribute('pointId',$point['id']);
            $xmlPoint->addAttribute('name',$point['name']);
            $xmlPoint->addAttribute('busStop',$point['busStop']);
            $xmlPoint->addAttribute('latitude',$point['latitude']);
            $xmlPoint->addAttribute('longitude',$point['longitude']);
            $xmlPoint->addAttribute('distance',$distanceTravelled);
            $xmlPoint->addAttribute('bearing',0);
            
            if ($point['busStop'] != 'true'){
                continue;
            }
            
            $timetable = $xmlPoint->addChild('timetable');
            //put all times into the stop node
            if (isset($point['timeTable']) && $this->is_iterable($point['timeTable'])){
                foreach($point['timeTable'] as $time){
                    $time = $timetable->addChild('time',$time);
                    $time->addAttribute('frequency',1);
                    $time->addAttribute('bearing',0);
                }
            }
        }
        
        $newXML = preg_replace("/\s+/", " ", $xml->asXML());
        
        $this->update(array("routePoints" => $newXML));
        
        return $newXML;
    }
    
    function encodeRoutePointsToXml($points){
        
        $xml = new SimpleXMLElement('<xml/>');
        $distanceTravelled = 0;
        
        for ($i=0;$i<count($points);$i++){
            $point = $points[$i];
            
            if (!isset($point)){
                continue;
            }
            
            $pointFrom = $points[max($i-1,0)];
            
            $distanceFrom = LatLngDetails::distance($pointFrom->latitude, $pointFrom->longitude, $point->latitude, $point->longitude);
            $distanceTravelled += $distanceFrom;
            
            $xmlPoint = $xml->addChild('point');
            $xmlPoint->addAttribute('pointId',$point->id);
            $xmlPoint->addAttribute('name',$point->name);
            $xmlPoint->addAttribute('busStop',$point->busStop);
            $xmlPoint->addAttribute('latitude',$point->latitude);
            $xmlPoint->addAttribute('longitude',$point->longitude);
            $xmlPoint->addAttribute('distance',$distanceTravelled);
            $xmlPoint->addAttribute('bearing',$point->bearing);
            $xmlPoint->addAttribute('speed',$point->getSpeed());
            $xmlPoint->addAttribute('stopFrequency',$point->getStopFrequency());
            
            if ($point->busStop === 'true'){
                $timetable = $xmlPoint->addChild('timetable');
                //put all times into the stop node
                for ($j=0;$j<count($point->timeTable);$j++){
                    $time = $timetable->addChild('time',$point->timeTable[$j]);
                    
                    if (count($point->getTimeDetails()) < 1){
                        continue;
                    }
                    
                    $timeDetail = $point->getTimeDetails()[$j];
                    $time->addAttribute('frequency',$timeDetail->frequency);
                    $time->addAttribute('bearing',$timeDetail->bearing);
                }
            }
        }
        
        $newXML = preg_replace("/\s+/", " ", $xml->asXML());
        //dd($this->routeName);
        //echo $newXML;
        $this->update(array("routePoints" => $newXML));
        
        return $newXML;
    }
    
    public static function decodeFromXml($points){
        $routePoints = array();
        
        $maxBusStopTimeFrequency = 0;
        $maxBusStopFrequency = 0;
        
        if ($points){
            $xml = new SimpleXMLElement($points);
        }else{
            return $routePoints;
        }
        
        foreach ($xml->children() as $point) {
            $routePoint = new RoutePointRepository((string)$point['pointId'][0]);
            $routePoint->setName((string)$point['name'][0]);
            $routePoint->setBusStop((string)$point['busStop'][0]);
            $routePoint->setLatitude((string)$point['latitude'][0]);
            $routePoint->setLongitude((string)$point['longitude'][0]);
            if (isset($point['time'])){$routePoint->setTime((string)$point['time'][0]);}
            if (isset($point['speed'])){$routePoint->setSpeed((string)$point['speed'][0]);}
            if (isset($point['bearing'])){$routePoint->setBearing((string)$point['bearing'][0]);}
            if (isset($point['distance'])){$routePoint->setDistance((string)$point['distance'][0]);}
            if (isset($point['stopFrequency'])){$routePoint->setStopFrequency((string)$point['stopFrequency'][0]);}
            if ((int)$routePoint->getStopFrequency() > $maxBusStopFrequency){$maxBusStopFrequency = (int)$routePoint->getStopFrequency();}
            
            foreach ($point->children() as $times) {
                $timeTable = array();
                $timeDetails = array();
                foreach ($times->children() as $time) {
                    $timeRep = new TimeRepository((string)$time[0]);
                    if (isset($time['bearing'])){$timeRep->bearing=(string)$time['bearing'][0];}
                    if (isset($time['frequency'])){$timeRep->frequency=(string)$time['frequency'][0];}
                    if ($timeRep->frequency > $maxBusStopTimeFrequency){$maxBusStopTimeFrequency = $timeRep->frequency;}
                    
                    array_push($timeTable,(string)$time[0]);
                    array_push($timeDetails,$timeRep);
                }
                $routePoint->setTimeTable($timeTable);
                $routePoint->setTimeDetails($timeDetails);
            }
            array_push($routePoints,$routePoint);
        }
        
        // Get first point and set it's maxBusStopTimeFrequency
        if (count($routePoints)>0){
            $routePoints[0]->setMaxBusStopTimeFrequency($maxBusStopTimeFrequency);
            $routePoints[0]->setMaxBusStopFrequency($maxBusStopFrequency);
        }
        
        return $routePoints;
    }
    
    public static function reversePoints($companyId, $routeId){
        $repository = RouteRepository::getRepository($routeId, $companyId);
        $routePoints = $repository->points;
        
        $newRoutePoints = array();
        $count = 1;
        for ($i = count($routePoints) - 1; $i >= 0; $i--) {
            $routePoint = $routePoints[$i];
            
            $routePoint->setId((string)($count));
            if ($routePoint->busStop != 'true'){
                $routePoint->setName((string)($count));
            }
            array_push($newRoutePoints,$routePoint);
            $count++;
        }
        
        $repository->encodeRoutePointsToXml($newRoutePoints);
    }
    
    /**
     * The bus stops we are getting here are being fed from the shift directly.
     * So we take those ones and see if there are some on our route that matches to this one.
     * If there is we will update the stop on the route.
     * 
     * @param type $busStops
     */
    function updateTimeTables($shiftBusStops){
        $latlng3s = $this->structuredRouteCoordinates['latlng3'];
        
        //var_dump($shiftBusStops);
        $count = 1;
        for ($i = 0; $i < count($shiftBusStops['latlng3']); $i++){
            $latlng3 = $shiftBusStops['latlng3'][$i];
            //$shiftStopRecordCount = count(array_keys($shiftBusStops['latlng3'],$latlng3));
            
            //search for a route point corresponding to this shift busstop
            if (!in_array($latlng3,$latlng3s)){
                continue;
            }
            
            $shiftBusStopPoint = $shiftBusStops['points'][$i];
            $closestPoint = $this->getBestBusStopPoint($shiftBusStopPoint->bearing, $latlng3);
            $index = $closestPoint->id - 1;
            
            //echo "** index - ".$index." closest among ".count($indexes)." **\n";
            // The route point will either be made into a stop,
            // Or if it's already a stop, a time will be added or updated
            $point = $this->updateStopTime($closestPoint, $shiftBusStopPoint, $index, $count);

            $this->points[$index] = $point;
        }
        
        if ($this->doRefreshTimeTables()){
            $this->refreshTimeTables();
        }
        
        $this->encodeRoutePointsToXml($this->points);
    }
    
    function doRefreshTimeTables(){
        $daysDiff = $this->getDaysSinceLastRefresh();
        
        if ($daysDiff >= Constants::getTimeTableRefreshFrequency()){
            return true;
        }
        
        return false;
    }
    
    /**
     * This method is called only when a certain number of days have passed.
     * Then we refresh the timetables.
     */
    function refreshTimeTables(){
        // The top frequency at a stop
        $maxBusStopTimeFrequency = $this->points[0]->getMaxBusStopTimeFrequency();
        $maxBusStopFrequency = $this->points[0]->getMaxBusStopFrequency();
        
        $cutOffStopFreq = $maxBusStopFrequency / 2;
        $cutOffStopTimeFreq = $maxBusStopTimeFrequency / 2;
        
        for ($i = 0; $i < count($this->points); $i++){
            $point = $this->points[$i];
            $timeTable = $point->timeTable;
            $timeDetails = $point->getTimeDetails();
            
            if ($point->busStop==="true"){
                if ($point->getStopFrequency() < $cutOffStopFreq){
                    $this->clearBusStop($point);
                }
                // Also clear any stops close by which must not be there
                $latlng3s = $this->structuredRouteCoordinates['latlng3'];
                $latlng3 = $latlng3s[$point->id - 1];
                
                $this->getBestBusStopPoint($point->bearing, $latlng3);
            }
            
            $point->setStopFrequency(0);
            
            if ($point->busStop==="false"){
                continue;
            }
            
            // If timetable times haven't accumulated enough then don't update.
            if ($maxBusStopTimeFrequency < Constants::getCreateBusStopFrequency()){
                continue;
            }
            
            for ($j=count($timeTable)-1;$j >= 0;$j--){
                $timeDetail = $timeDetails[$j];

                //echo"*** d1: ".$date1." d2: ".$date2." diff: ".$diff." ***\n";
                if ($timeDetail->frequency < $cutOffStopTimeFreq){
                    unset($timeTable[$j]);
                    unset($timeDetails[$j]);
                }else{
                    $timeDetails[$j]->frequency=1;
                }
            }
            
            $point->timeTable = $timeTable;
            $point->setTimeDetails($timeDetails);
        }
        $this->update(array("lastTimetableRefresh" => new DateTime()));
    }
    
    function clearBusStop(&$point){
        $point->name="";
        $point->busStop="false";

        $timeTable = array();
        $timeDetails = array();

        $point->timeTable = $timeTable;
        $point->setTimeDetails($timeDetails);
        
        return $point;
    }
    
    /**
     * This function gets the bus stop closest to the latlng.
     * If no bus stop is close it gets the closest point.
     * The function also clears other bus stops close to the returned bus stop. 
     * @param type $bearing
     * @param type $latlng3
     * @return type
     */
    function getBestBusStopPoint($bearing, $latlng3){
        $closestPoint = null;
        $availableStops = array();
        
        $latlng3s = $this->structuredRouteCoordinates['latlng3'];
        $indexes = array_keys($latlng3s, $latlng3);
        
        foreach ($indexes as $index){
            $point = $this->points[$index];
            //check that the bearings are not too distant apart
            $bearingDiff = abs($point->bearing - $bearing);
            if ($bearingDiff > Constants::sameDirectionCutOff){
                continue;
            }
            
            if ($point->busStop === 'true'){
                array_push($availableStops, $point);
            }
        }
        
        if (count($availableStops) > 0){
            $closestPoint = array_pull($availableStops,0);
        }else{
            $closestPoint = $this->points[$indexes[0]];
        }
        
        // Make every point left in available stops a non bus-stop
        foreach ($availableStops as $stop){
            $point = $this->clearBusStop($stop);
            $this->points[$stop->id - 1] = $point;
        }
        
        //echo "** closest index - ".$closestIndex;
        return $closestPoint;
    }
    
    /**
     * The method takes a route point and a shift busstop point.
     * Then tries to update the route point based off of the busstop point.
     * @param type $point
     * @param type $busStopPoint
     * @param type $index
     * @param type $count
     * @param type $maintain
     * @return type
     */
    function updateStopTime($point, $shiftBusStopPoint, $index, &$count){ 
        
        if (!isset($point)){
            return $point;
        }
        
        // Increase frequency of getting to stop
        $point->setStopFrequency($point->getStopFrequency() + 1);
        
        // Chech if point now qualifies for being a bus-stop
        if ($point->busStop === 'false'){
            $createFrequency = Constants::getCreateBusStopFrequency();
            
            if ($point->getStopFrequency() < $createFrequency){
                return $point;
            }
            
            $name = RouteRepository::getLocationName($point->latitude,$point->longitude);
            if (BaseRepository::isEmptyOrNullString($name)){
                $name = "Bus Stop ".$count;
            }
            
            $point->name=$name;
            $point->busStop="true";
            $count++;
        }
        
        $stopTimes = $shiftBusStopPoint->timeTable;
        $timeTable = $point->timeTable;
        $timeDetails = $point->getTimeDetails();

        // get a time close to the one from the shift recorded bus stops.
        // if we find one we update it's frequency.
        for ($i=0;$i<count($stopTimes);$i++){
            $date1 = strtotime($stopTimes[$i]);
            for ($j=count($timeTable)-1;$j >= 0;$j--){
                $date2 = strtotime($timeTable[$j]);
                $diff = abs(($date2-$date1)/60);
                $timeDetail = $timeDetails[$j];
                
                //echo"*** d1: ".$date1." d2: ".$date2." diff: ".$diff." ***\n";
                if ($diff < Constants::busDelayTime){
                    $timeDetail->frequency += 1;
                    continue 2;
                }
            }
            
            $timeDetail = new TimeRepository($stopTimes[$i]);
            array_push($timeTable,$stopTimes[$i]);
            array_push($timeDetails,$timeDetail);
        }
        
        $point->timeTable = $timeTable;
        $point->setTimeDetails($timeDetails);
        
        return $point;
    }
    
    function update($properties)
    {
        try{
            Route::query()
                   ->where('companyId','=', $this->companyId)
                   ->where('routeId','=', $this->routeId)
                   ->update($properties);
           RouteRepository::updateCacheRepository($this->companyId, $this->routeId);
        }catch(\Illuminate\Database\QueryException $ex){
            //echo $ex;
            return null;
        }
    }
    
    public static function updateCacheRepository($companyId,$routeId){
        $routeRep = RouteRepository::getRepository($routeId, $companyId, false);
        BaseRepository::putItemIntoCache($companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $routeId, $routeRep);
    
        //Also update the company repository
        CompanyRepository::updateCacheRepository($companyId);
    }
    
    function is_iterable($var) {
        return (is_array($var) || $var instanceof Traversable);
    }
    
    /**
     * This method returns the essential route details without extra stuff that has too much data.
     * @param type $companyId
     * @return string
     */
    public static function getRoutesDetails($companyId){
        $routes = Route::query()
                    ->where('companyId','=',$companyId)
                    ->get();
        
        $response['routes'] = array();

        if (!is_null($routes)) {
            foreach($routes as $route) {
                $routeRep = RouteRepository::createRepository($route);
                
                $minifiedRouteRep = RouteRepository::minifyRoute($routeRep);
                
                array_push($response["routes"], $minifiedRouteRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Route Details";
        return $response; 
    }
    
    /**
     * This method returns the essential route details without extra stuff that has too much data.
     * @param type $routeId
     * @return string
     */
    public static function getRouteDetails($routeId, $companyId){
        if (!isset($routeId)){
            return null;
        }
        $routeRep = RouteRepository::getRepository($routeId, $companyId);

        $minifiedRouteRep = RouteRepository::minifyRoute($routeRep);
        
        return $minifiedRouteRep; 
    }
    
    /**
     * Removes some data for a lighter payload.
     * @param type $routeRep
     * @return type
     */
    public static function minifyRoute($routeRep){
        if (!isset($routeRep)){
            return null;
        }
        $routeRep->busStopPoints = $routeRep->busStopPoints["points"];
        $routeRep->routePoints = null;
        $routeRep->points = array();
        $routeRep->structuredRouteCoordinates = array();
        $routeRep->locationClusters = array();
        $routeRep->userClusterLocations = array();
        
        return $routeRep;
    }
    
    public static function getRoutes($companyId, $cached=true){
        $response['routes'] = array();
        
        if($cached){
            $cachedRoutes = BaseRepository::getListFromCache($companyId,CacheType::CompanyRoutes);
        }
        
        if (isset($cachedRoutes)){
            $response['routes'] = $cachedRoutes;
            $response["success"] = 1;
            $response["message"] = "Retrieved routes from cache";
            return $response; 
        }
        
        $routes = Route::query()
                    ->where('companyId','=',$companyId)
                    ->get();

        $cacheItems = array();
        if (!is_null($routes)) {
            foreach($routes as $route) {
                $routeRep = RouteRepository::getRepository($route->routeId, $companyId);
                array_push($response["routes"], $routeRep);
                $cacheItems[$routeRep->getId()] = $routeRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Routes";
        return $response; 
    }
    
    public static function getRepository($routeId, $companyId, $cached=true){
        
        if ($cached){
            $cachedRoute = BaseRepository::getItemFromCache(CacheType::CompanyRoutes,$routeId);
        }
        
        if (isset($cachedRoute)){
            $cachedRoute->findClusters();
            return $cachedRoute; 
        }
        
        $route = Route::query()
                    ->where('companyId','=',$companyId)
                    ->where('routeId','=',$routeId)
                    ->first();
        
        if (!isset($route)){
            return null;
        }
        
        $routeRep = RouteRepository::createRepository($route);
        
        BaseRepository::putItemIntoCache($companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $routeId, $routeRep);
        
        return $routeRep;
    }
    
    public static function createRepository($route){
        
        $routeRep = new RouteRepository($route->routeId);
        $routeRep->setRouteName($route->routeName);
        $routeRep->setCompanyId($route->companyId);
        $routeRep->setColor($route->color);
        $routeRep->setRoutePoints($route->routePoints);
        $routeRep->setPoints(RouteRepository::decodeFromXml($route->routePoints));
        $routeRep->processRouteCoordinates();
        $routeRep->setlastTimetableRefresh($route->lastTimetableRefresh);

        return $routeRep;
    }
    
    function findClusters(){
        $companyRep = CompanyRepository::getRepository($this->companyId);
        if(!$companyRep->routeClustering){
            return;
        }
        
        $lastCalculation = Datetime::createFromFormat('Y-m-d_H:i:s', $this->lastClusterCalculation);
        $currentDate = new DateTime('now');

        if (isset($this->lastClusterCalculation) && $lastCalculation){
            $diffInSeconds = abs($lastCalculation->getTimestamp() - $currentDate->getTimestamp());
        }else{
            $diffInSeconds = -1;
        }
        
        //error_log("last calc diff seconds: ".$diffInSeconds);
        if ($diffInSeconds < 0 || $diffInSeconds > Constants::getClusterRefreshTime()){
            $clusters = LocationClusterRepository::findClusters($this->userClusterLocations);
            //error_log("---- calculated clusters ----");
            //$this->printCluster($clusters);
            $date = new DateTime('now');
            $this->lastClusterCalculation = $date->format('Y-m-d_H:i:s');
            $this->updateClusterIds($clusters);
            BaseRepository::putItemIntoCache($this->companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $this->routeId, $this);
            //error_log("---- new clusters ----");
            //$this->printCluster($this->locationClusters);
        }
    }
    
    function printCluster($clusters){
        foreach ($clusters as $cluster){
            $info = "cluster id: ".$cluster->id.
                    " lat: ".$cluster->latitude.
                    " lng: ".$cluster->longitude.
                    " dir: ".$cluster->direction.
                    " time: ".$cluster->calculatedAt.
                    " size: ".$cluster->clusterSize;
            error_log($info);
        }
    }
    
    /**
     * Tries to reconcile new clusters to old clusters for continuity.
     * This is done by comparing to the nearest cluster.
     */
    function updateClusterIds($newClusters){
        
        $matchedClusterIds = array();
        
        if (!isset($this->locationClusters) || !isset($newClusters)){
            return;
        }
        
        // Set the id of each new cluster to be the one of the closest old clusters
        foreach ($newClusters as $newCluster){
            $minDistance = 0;
            $clusterId = $newCluster->id;
            $clusterMinDirection = Constants::getClusterMinDirection();
            
            foreach ($this->locationClusters as $cluster){
                
                // If already matched do not match again.
                if (in_array($cluster->id,$matchedClusterIds)){
                    continue 1;
                }
                
                $directionDiff = abs($cluster->direction - $newCluster->direction);
                
                if ($directionDiff > $clusterMinDirection){
                    continue 1;
                }
                
                $lat1 = $newCluster->latitude;
                $lng1 = $newCluster->longitude;
                $lat2 = $cluster->latitude;
                $lng2 = $cluster->longitude;
                $distance = LocationClusterRepository::computeDistance($lat1, $lng1, $lat2, $lng2);
                if ($minDistance === 0 || $distance<$minDistance){
                    $minDistance = $distance;
                    $clusterId = $cluster->id;
                    array_push($matchedClusterIds,$cluster->id);
                }
            }
            
            $newCluster->id = $clusterId;
        }
        
        //For all old remaining clusters, keep them if they are still xx mins old.
        //The bus might just be stopping briefly at a bus stop
        foreach ($this->locationClusters as $cluster){
            
            if (in_array($cluster->id,$matchedClusterIds)){
                continue;
            }
            
            $calculatedDate = Datetime::createFromFormat('Y-m-d_H:i:s', $cluster->calculatedAt);
            $currentDate = new DateTime('now');
            
            $diff = $currentDate->diff($calculatedDate);
            
            if ($diff->i < Constants::getBusStopWaitingTime()){
                array_push($newClusters,$cluster);
            }
        }
        $this->locationClusters = $newClusters;
    }
    
    function getPoints() {
        return $this->points;
    }
    
    function getBusStopPoints() {
        return $this->busStopPoints;
    }

    function setBusStopPoints($busStopPoints) {
        $this->busStopPoints = $busStopPoints;
    }

    function getId() {
        return $this->routeId;
    }

    function setPoints($points) {
        $this->points = $points;
    }
    
    function getRoutePoints() {
        return $this->routePoints;
    }

    function setRoutePoints($routePoints) {
        $this->routePoints = $routePoints;
    }

    function setId($routeId) {
        $this->routeId = $routeId;
    }

    function setRouteNode($routeNode) {
        $this->routeNode = $routeNode;
    }
    function getColor() {
        return $this->color;
    }

    function setColor($color) {
        $this->color = $color;
        $this->basicColor = $color;
                
        //basic color has only 6 hex digits not 8
        $digits = substr($color,1);
        $digitsLength = strlen($digits);
        
        if ($digitsLength > 6){
            $this->basicColor = "#".substr($color,($digitsLength + 1) - 6);
        }else if ($digitsLength < 6){
            $this->basicColor = "#".str_pad($digits, 6, "0", STR_PAD_RIGHT);
        }
    }
    
    function getRouteId() {
        return $this->routeId;
    }

    function getCompanyId() {
        return $this->companyId;
    }
    
    function getRouteName() {
        return $this->routeName;
    }

    function setRouteName($routeName) {
        $this->routeName = $routeName;
    }

    function getStructuredRouteCoordinates() {
        return $this->structuredRouteCoordinates;
    }

    function setStructuredRouteCoordinates($structuredRouteCoordinates) {
        $this->structuredRouteCoordinates = $structuredRouteCoordinates;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getLocationClusters() {
        return $this->locationClusters;
    }

    function getLastClusterCalculation() {
        return $this->lastClusterCalculation;
    }

    function setLocationClusters($locationClusters) {
        $this->locationClusters = $locationClusters;
    }

    function setLastClusterCalculation($lastClusterCalculation) {
        $this->lastClusterCalculation = $lastClusterCalculation;
    }
    
    function getUserLocations() {
        return $this->userClusterLocations;
    }
    
    function setUserClusterLocation($userId, $direction, $lastSeen, $latitude, $longitude){
        
        $location = new UserClusterLocation();
        $location->userId = $userId;
        $location->latitude=$latitude;
        $location->lastSeen = $lastSeen;
        $location->longitude=$longitude;
        $location->direction=$direction;
        
        $this->userClusterLocations[$userId] = $location;
        BaseRepository::putItemIntoCache($this->companyId, CacheType::CompanyRoutes, CacheExpiry::CompanyRoutes, $this->routeId, $this);
    }
    
    function getRouteCoordinates() {
        return $this->routeCoordinates;
    }
    
    function getNextBusStop($routePoint){
        $busStops = $this->busStopPoints["points"];
        if (count($busStops) == 0){
            return;
        }
        for($i=0;$i<count($busStops);$i++){
            $busStop = $busStops[$i];
            if ($busStop->id > $routePoint->id){
                return $busStop;
            }
        }
        return $busStops[0];
    }
    
    /**
     * For get previous, if the point coincides with a stop it will be
     * previous itself since it's moving on just then
     * @param type $routePoint
     * @return type
     */
    function getPreviousBusStop($routePoint){
        $busStops = $this->busStopPoints["points"];
        if (count($busStops) == 0){
            return;
        }
        for($i=count($busStops)-1;$i>=0;$i--){
            $busStop = $busStops[$i];
            if ($busStop->id <= $routePoint->id){
                return $busStop;
            }
        }
        return end($busStops);
    }
    
    /**
     * This method gets the point corresponding to the latlng3 and bearing.
     * @param type $latlng3
     * @param type $bearing
     */
    function getLatLng3Point($latlng3, $bearing){
        $latlng3s = $this->structuredRouteCoordinates['latlng3'];
        $indexes = array_keys($latlng3s,$latlng3);
        
        foreach ($indexes as $index){
            $latlngBearing = $this->structuredRouteCoordinates['bearing'][$index];
            
            //check that the bearings are not too distant apart
            $bearingDiff = abs($bearing - $latlngBearing);
            if ($bearingDiff > Constants::sameDirectionCutOff){
                continue;
            }
            
            $routePoint = $this->structuredRouteCoordinates['points'][$index];
            return $routePoint;
        }
    }
    
    /**
     * This method creates the routeCoordinates string for routes that were manually created.
     */
    function processRouteCoordinates(){
        //$routeCoordinates = "";
        $this->structuredRouteCoordinates["latlng1"] = array();
        $this->structuredRouteCoordinates["latlng2"] = array();
        $this->structuredRouteCoordinates["latlng3"] = array();
        $this->structuredRouteCoordinates["points"] = array();
        $this->structuredRouteCoordinates["bearing"] = array();
        
        $this->busStopPoints["latlng1"] = array();
        $this->busStopPoints["latlng2"] = array();
        $this->busStopPoints["latlng3"] = array();
        $this->busStopPoints["points"] = array();
        $this->busStopPoints["bearing"] = array();
        
        foreach ($this->points as $routePoint){
            $lat  = floatval($routePoint->latitude);
            $lng = floatval($routePoint->longitude);

            $latlng1 = $lat.",".$lng;
            $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            
            array_push($this->structuredRouteCoordinates["latlng1"],$latlng1);
            array_push($this->structuredRouteCoordinates["latlng2"],$latlng2);
            array_push($this->structuredRouteCoordinates["latlng3"],$latlng3);
            array_push($this->structuredRouteCoordinates["points"],$routePoint);
            array_push($this->structuredRouteCoordinates["bearing"],$routePoint->bearing);
            //$routeCoordinates = $routeCoordinates.";".$latlng1.",".$latlng2.",".$latlng3;
            
            if ($routePoint->busStop === "true"){
                array_push($this->busStopPoints["latlng1"],$latlng1);
                array_push($this->busStopPoints["latlng2"],$latlng2);
                array_push($this->busStopPoints["latlng3"],$latlng3);
                array_push($this->busStopPoints["points"],$routePoint);
                array_push($this->busStopPoints["bearing"],$routePoint->bearing);
            }
        }
    }
    
    function getlastTimetableRefresh() {
        return $this->lastTimetableRefresh;
    }

    function setlastTimetableRefresh($lastTimetableRefresh) {
        $this->lastTimetableRefresh = $lastTimetableRefresh;
    }
    
    function getDaysSinceLastRefresh(){
        $now = time(); // or your date as well
        $lastRefresh = strtotime($this->lastTimetableRefresh);
        $datediff = $now - $lastRefresh;
        $daysDiff = $datediff / (60 * 60 * 24);
        
        return round($daysDiff);
    }
}

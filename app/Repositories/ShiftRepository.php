<?php namespace App\Repositories;

use App\Repositories\RouteRepository;
use App\Repositories\ShiftSummaryRepository;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\Constants;
use App\Http\Helpers\LatLngDetails;
use App\Models\Shift;
use SimpleXMLElement;
use Datetime;
use Log;
/**
 * Description of ShiftRepository
 *
 * @author Takunda
 */
class ShiftRepository extends BaseRepository {
    
    var $shiftId;
    var $employeeId;
    var $companyId;
    var $routeId;
    var $deviceId;
    var $mileage;
    var $topSpeed;
    var $start;
    var $end;
    
    //xml string
    var $routePoints;
    var $points = array();
    var $busStopPoints = array();
    
    //Only used in memory
    var $shiftNode;
    private $structuredRouteCoordinates = array();
    
    var $shiftSummary;
    var $shiftLog = array();
    var $shiftCycles = 0;
    
    var $employeeName;
    var $routeName;
    var $status;
    var $speed;
    var $lastSeen;
    
    function __construct($employeeId, $companyId, $routeId, $deviceId) {
        $this->employeeId=$employeeId;
        $this->companyId=$companyId;
        $this->routeId=$routeId;
        $this->deviceId=$deviceId;
        
        $this->initialiseCoordinatesArray();
    }
    
    function initialiseCoordinatesArray(){
        $this->structuredRouteCoordinates["latlng1"] = array();
        $this->structuredRouteCoordinates["latlng2"] = array();
        $this->structuredRouteCoordinates["latlng3"] = array();
        $this->structuredRouteCoordinates["time"] = array();
        $this->structuredRouteCoordinates["time2"] = array();
        $this->structuredRouteCoordinates["repeat"] = array();
        $this->structuredRouteCoordinates["speed"] = array();
        $this->structuredRouteCoordinates["bearing"] = array();
    }
    
    public function create(){
        $shiftNode = $this->createNode();
        $this->shiftNode = $shiftNode;
        $this->shiftId = $shiftNode->shiftId;
        
        $employeeRep = new EmployeeRepository($this->employeeId, $this->companyId);
        $employeeRep->update(array("shiftId" => $shiftNode->shiftId));
        
        ShiftRepository::updateCacheRepository($this->companyId, $this->employeeId, $this->shiftId);
        
        $response["success"] = 1;
        $response["shiftId"] = $this->shiftId;
        $response["message"] = "Shift has been created";
        return ($response);
    }
    
    function createNode(){
        $shiftNode = new Shift;
        
	$shiftNode->companyId = $this->companyId;
	$shiftNode->employeeId = $this->employeeId;
        $shiftNode->deviceId = $this->deviceId;
        $shiftNode->routeId = $this->routeId;
	$shiftNode->mileage = $this->mileage;
        
        $date = new DateTime();
        $shiftNode->start = $date;
        $shiftNode->save();
        
        return $shiftNode;
    }
    
    public static function delete($shiftId){
        Shift::query()
                    ->where('shiftId','=',$shiftId)
                    ->delete();
        
        BaseRepository::deleteItemFromCache(null, CacheType::Shifts, CacheExpiry::Shifts, $shiftId);
    }
    
    /**
     * Will do necessary calculations for data and also 
     * must update time-tables of the route the shift was on
     */
    function endShift(){
        $structuredCoordinates = RouteRepository::sortCoordinates($this->structuredRouteCoordinates);
        
        $date = new DateTime();
        $mileageAndSpeed = $this->getMileageAndSpeed($structuredCoordinates);
        $mileage = $mileageAndSpeed[0];
        $speed = $mileageAndSpeed[1];
        $routePoints = RouteRepository::encodeStructuredCoordinates($structuredCoordinates);
        
        $this->points = RouteRepository::decodeFromXml($routePoints);
        $this->setBusStops();
        
        $this->update(array("mileage" => $mileage,"topSpeed" => $speed, "routePoints" => $routePoints, "end" => $date));
        
        //$this->routeId = $this->createUnknownRoute();
        $this->updateRouteTimeTables();
    }
    
    function updateRouteTimeTables(){
        
        if (!isset($this->routeId)){
            return;
        }
        
        $routeRep = $this->getRoute();
        if (!isset($routeRep)){
            return;
        }
        
        //Log::debug('shift update times');
        $routeRep->updateTimeTables($this->busStopPoints);
    }
    
    function getMedianRepeat($coordinates){
        $repeats = $coordinates['repeat'];
        
        if (count($repeats) <= 0){
            return 0;
        }
        
        $uniqueRepeats = array_unique($repeats);
        sort($uniqueRepeats);
        $median = $uniqueRepeats[round(count($uniqueRepeats)/2)];
        return $median;
    }
    
    function getMileageAndSpeed($coordinates){
        $topSpeed = 0;
        $mileage = 0;
        
        for ($i=0; $i < count($coordinates['latlng1'])-1; $i++){
            $speed = $coordinates['speed'][$i];
            
            if ($speed > $topSpeed){
                $topSpeed = $speed;
            }
            
            $fromLatLng = $coordinates['latlng1'][$i];
            $toLatLng = $coordinates['latlng1'][$i+1];
            
            $fromLat = (float) explode(",", $fromLatLng)[0];
            $fromLng = (float) explode(",", $fromLatLng)[1];
            
            $toLat = (float) explode(",", $toLatLng)[0];
            $toLng = (float) explode(",", $toLatLng)[1];
            
            $distance = LatLngDetails::distance($fromLat, $fromLng, $toLat, $toLng);
            $mileage += $distance;
        }
        
        return array($mileage,$topSpeed);
    }
    
    /**
     * Gets the employee shifts depending on the parameters given
     * @param type $companyId
     * @param type $employeeId
     * @param type $from
     * @param type $to
     * @param type $routeId
     */
    public static function getShifts($companyId, $employeeId, $routeId, $from, $to){
        
        $result = array();
        $result["shifts"] = array();
        
        $shifts = Shift::query()
                    ->where('companyId','=',$companyId);
        
        if (!BaseRepository::isEmptyOrNullString($employeeId)){
            $shifts = $shifts->where("employeeId",'=',$employeeId);
        }
        
        if (!BaseRepository::isEmptyOrNullString($routeId)){
            $shifts = $shifts->where("routeId",'=',$routeId);
        }
        
        if (!BaseRepository::isEmptyOrNullString($from)){
            $fromDate = date($from);
            //dd($fromDate);
            $shifts = $shifts->where("start",'>=',$fromDate);
        }
        
        if (!BaseRepository::isEmptyOrNullString($to)){
            $toDate = date($to);
            $shifts = $shifts->where("end",'<=',$toDate);
        }
        
        $shifts = $shifts->get();
        
        if (!isset($shifts)){
            $result['success'] = 0;
            $result['message'] = "no shifts found";
            return $result;
        }
        
        foreach($shifts as $shift) {
            $shiftRep = ShiftRepository::createRepository($shift);
            array_push($result["shifts"], $shiftRep);
        }
        
        $result['success'] = 1;
        $result['message'] = "Shifts found";
        return $result;
    }
    
    public static function getRepository($shiftId, $cached=true, $cachedOnly=false){
        
        if ($cached){
            $cachedRepository = BaseRepository::getItemFromCache(CacheType::Shifts, $shiftId);
        }
        
        if (isset($cachedRepository)){
            return $cachedRepository;
        }
        
        if ($cachedOnly){
            return null;
        }
        
        $shift = Shift::query()
                    ->where('shiftId','=',$shiftId)
                    ->first();
        
        if (!isset($shift)){
            return null;
        }
        
        $shiftRep = ShiftRepository::createRepository($shift);
        
        return $shiftRep;
    }
    
    public static function createRepository($shift){
        $shiftRep = new ShiftRepository($shift->employeeId,$shift->companyId,$shift->routeId,$shift->deviceId);
        $startTime = BaseRepository::getDateTimeString($shift->start);
        if ($shift->end){
            $endTime = BaseRepository::getDateTimeString($shift->end);
        }else{
            $endTime = null;
        }
        
        $shiftRep->setShiftId($shift->shiftId);
        $shiftRep->setMileage($shift->mileage);
        $shiftRep->setTopSpeed($shift->topSpeed);
        $shiftRep->setRoutePoints($shift->routePoints);
        $shiftRep->setPoints(RouteRepository::decodeFromXml($shift->routePoints));
        $shiftRep->setBusStops();
        $shiftRep->setStart($startTime);
        $shiftRep->setEnd($endTime);
        $shiftRep->setEmployeeName();
        $shiftRep->createShiftLog();
        
        return $shiftRep;
    }
    
    public static function getShiftSummary($shiftId){
        $shiftSummary = null;
        $shiftRep = ShiftRepository::getRepository($shiftId, true, true);
        if(isset($shiftRep)){
            $shiftSummary = $shiftRep->shiftSummary;
        }
        return $shiftSummary;
    }
    
    function updateShiftSummary(){
        //Log::debug("** update shift summary **");
        if (!isset($this->shiftSummary)){
            $this->shiftSummary = new ShiftSummaryRepository($this->shiftId);
            $this->shiftSummary->companyId = $this->companyId;
        }
        
        //Route Id might keep on changing
        $this->shiftSummary->routeId = $this->routeId;
        
        $performUpdate = $this->performSummaryUpdate();
        $update = $performUpdate[0];
        $staleTime = $performUpdate[1];
        
        if (!$update){
            return;
        }
        
        $count = count($this->structuredRouteCoordinates['latlng1']);
        $latlng = $this->structuredRouteCoordinates['latlng1'][$count-1];
        $latlng3 = $this->structuredRouteCoordinates['latlng3'][$count-1];
        $bearing = $this->structuredRouteCoordinates['bearing'][$count-1];
        $time = $this->structuredRouteCoordinates['time'][$count-1];

        $summaryStops = $this->getSummaryStops($latlng, $latlng3, $bearing, $time);
        
        if ($staleTime || isset($summaryStops)){
            $this->shiftSummary->lastStop = $summaryStops[0];
            $this->shiftSummary->nextStop = $summaryStops[1];
            //echo "** shift summary points **\n";
            //print_r($summaryStops);
        }
    }
    
    /**
     * Checks if we should do an update of the summary.
     * If so, check whether it's because we have a stop or we have passed the time limit.
     * @return boolean
     */
    function performSummaryUpdate(){
        //echo "** summary not set **\n";
        $count = count($this->structuredRouteCoordinates['latlng1']);
        $performUpdate = false;
        $staleTime = false;
        
        if ($count === 0){
            //echo "** coords are 0 **\n";
            return array($performUpdate,$staleTime);
        }
        
        $pickingTime=Constants::getBusPickingTime();
        $time1=strtotime($this->structuredRouteCoordinates['time'][$count-1]);
        $time2=strtotime($this->structuredRouteCoordinates['time'][max(0,$count-2)]);
        
        $timeDiff = abs($time2 - $time1);
        // If this is a stop then update.
        if ($timeDiff > $pickingTime){
            $performUpdate = true;
        }
        
        // Check if there was a set up ETA that has been passed.
        // In that case we must perform summary update with last stop coordinates still.
        if (isset($this->shiftSummary->nextStop)){
            $nextStopTime = $this->shiftSummary->nextStop->getEstimatedArrivalTime();
            if (time() > $nextStopTime){
                $performUpdate = true;
                $staleTime = true;
            }
        }else{
            $performUpdate = true;
        }
        
        return array($performUpdate,$staleTime);
    }
    
    /**
     * Takes in parameters of the latest stop.
     * The latest stop will be matched to already existing route stops and next stop will be checked for.
     * @param type $latlng
     * @param type $latlng3
     * @param type $bearing
     * @param type $time
     * @return type
     */
    function getSummaryStops($latlng, $latlng3, $bearing, $currentTime){
        
        $employeeRep = EmployeeRepository::getRepository($this->employeeId, $this->companyId);
        $routeRep = RouteRepository::getRepository($employeeRep->routeId, $employeeRep->companyId);
        
        if (!isset($routeRep)){
            // device route is always up to date and more indicative of actual route device is on
            $deviceRep = DeviceRepository::getRepository($this->deviceId);
            $routeRep = RouteRepository::getRepository($deviceRep->getRouteId(), $deviceRep->companyId);
        }
        
        //Log::debug("** get s stops 1 **");
        if (!isset($routeRep) && isset($this->routeId)){
            $routeRep = RouteRepository::getRepository($this->routeId, $this->companyId);
        }
        
        //Log::debug("** get s stops 2 **");
        if (!isset($routeRep)){
            return;
        }
        
        //Log::debug("** get s route name - ".$routeRep->routeName." **");
        $routePoint = $routeRep->getLatLng3Point($latlng3, $bearing);
        
        //Log::debug("** get s stops 3 **");
        if (!isset($routePoint)){
            return;
        }
        
        // Get the last and next busstops
        $nextStop = $routeRep->getNextBusStop($routePoint);
        $lastStop = $routeRep->getPreviousBusStop($routePoint);
        
        if (!isset($nextStop) || !isset($lastStop)){
            return;
        }
        
        //distance in kilometers
        $distanceBetween = abs(floatval($routePoint->distance) - floatval($nextStop->distance)) / 1000;
        //distance = speed * time
        $averageSpeed = $this->getAverageSpeed(10);
        //time is in hours
        $time = $distanceBetween  / (float)$averageSpeed;
        
        //For now will use server time
        //$stopTime = strtotime($currentTime);
        
        $nextStopTime = ($time * 3600) + time();
        $nextStop->estimatedArrival = date($this->timeFormat, $nextStopTime);
        $nextStop->setEstimatedArrivalTime($nextStopTime);
        //Log::debug("** ETA!! : current - ".$currentTime." dist - ".$distanceBetween." avg speed - ".$averageSpeed." eta - ".$nextStop->estimatedArrival."\n");
        
        return array($lastStop,$nextStop);
    }
    
    /**
     * Count is the latest speeds to be averaged
     * 
     * @param type $count
     */
    public function getAverageSpeed($countDown){
        $totalSpeed = 0;
        $speeds = $this->structuredRouteCoordinates["speed"];
        
        $count = 0;
        for ($i=count($speeds)-1;$i>=0;$i--){
            $totalSpeed += $speeds[$i];
            $count++;
            if ($count >= $countDown){
                break;
            }
        }
        //echo "** speed total: ".$totalSpeed." count down ".$countDown." **\n";
        return $totalSpeed / (float)$countDown;
    }
    
    /**
     * Updates the shift details for the given employee.
     * @param type $employeeRep
     * @param type $deviceRep
     */
    public static function updateCoordinates($employeeRep, $deviceRep, $shiftRep=null){
        $shiftId = $employeeRep->shiftId;
        
        //mainly just for testing we pass in shiftRep
        if (!isset($shiftRep)){
            $shiftRep = ShiftRepository::getRepository($shiftId);
        }
        $shiftRep->insertCoordinates($deviceRep->getStructuredCoordinates());
    }
    
    /**
     * Takes the structured coordinates from device rep and puts them in shift coordinates
     * @param type $structuredCoordinates
     */
    function insertCoordinates($deviceCoordinates){
        //check if end point is the same if not put it in.
        $count = count($this->structuredRouteCoordinates["latlng1"]);
        $deviceCount = count($deviceCoordinates["latlng1"]);
        
        //Log::debug('Device Coords: '.$deviceCount." ".$count);
        if ($count == 0){
            $this->structuredRouteCoordinates = $deviceCoordinates;
            ShiftRepository::updateCacheRepository($this->companyId, $this->employeeId, $this->shiftId, $this);
            return;
        }else if ($deviceCount == 0){
            return;
        }
        
        $lastLatLng = end($this->structuredRouteCoordinates["latlng3"]);
        $deviceLatLng = end($deviceCoordinates["latlng3"]);
        
        if ($lastLatLng === $deviceLatLng){
            $this->structuredRouteCoordinates["latlng1"][$count-1] = $deviceCoordinates["latlng1"][$deviceCount-1];
            $this->structuredRouteCoordinates["latlng2"][$count-1] = $deviceCoordinates["latlng2"][$deviceCount-1];
            $this->structuredRouteCoordinates["latlng3"][$count-1] = $deviceCoordinates["latlng3"][$deviceCount-1];
            $this->structuredRouteCoordinates["time"][$count-1] = $deviceCoordinates["time"][$deviceCount-1];
            $this->structuredRouteCoordinates["time2"][$count-1] = $deviceCoordinates["time2"][$deviceCount-1];
            $this->structuredRouteCoordinates["repeat"][$count-1] = $deviceCoordinates["repeat"][$deviceCount-1];
            $this->structuredRouteCoordinates["speed"][$count-1] = $deviceCoordinates["speed"][$deviceCount-1];
            $this->structuredRouteCoordinates["bearing"][$count-1] = $deviceCoordinates["bearing"][$deviceCount-1];
        }else{
            array_push($this->structuredRouteCoordinates["latlng1"],$deviceCoordinates["latlng1"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["latlng2"],$deviceCoordinates["latlng2"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["latlng3"],$deviceCoordinates["latlng3"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["time"],$deviceCoordinates["time"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["time2"],$deviceCoordinates["time2"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["repeat"],$deviceCoordinates["repeat"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["speed"],$deviceCoordinates["speed"][$deviceCount-1]);
            array_push($this->structuredRouteCoordinates["bearing"],$deviceCoordinates["bearing"][$deviceCount-1]);
        }
        
        //$this->lastSeen = last($this->structuredRouteCoordinates["time"]);
        $this->lastSeen = date("Y-m-d H:i");
        $this->speed = round(last($this->structuredRouteCoordinates["speed"]),2);
        $this->updateShiftSummary();
        ShiftRepository::updateCacheRepository($this->companyId, $this->employeeId, $this->shiftId, $this);
    }
    
    public static function updateCacheRepository($companyId, $employeeId, $shiftId, $repository=null){
        if (!isset($repository)){
            $repository = ShiftRepository::getRepository($shiftId, false);
        }
        BaseRepository::putItemIntoCache($companyId."_".$employeeId, CacheType::Shifts, CacheExpiry::Shifts, $shiftId, $repository);
    }
    
    function update($properties){
        try{
            Shift::query()
                   ->where('shiftId','=', $this->shiftId)
                   ->update($properties);
           ShiftRepository::updateCacheRepository($this->companyId, $this->employeeId, $this->shiftId);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    function setBusStops(){
        $this->busStopPoints["points"] = array();
        $this->busStopPoints["latlng1"] = array();
        $this->busStopPoints["latlng2"] = array();
        $this->busStopPoints["latlng3"] = array();
        $this->busStopPoints["bearing"] = array();
        
        foreach ($this->points as $routePoint){
            $lat  = floatval($routePoint->latitude);
            $lng = floatval($routePoint->longitude);

            $latlng1 = $lat.",".$lng;
            $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            
            if ($routePoint->busStop === "true"){
                array_push($this->busStopPoints["points"],$routePoint);
                array_push($this->busStopPoints["latlng1"],$latlng1);
                array_push($this->busStopPoints["latlng2"],$latlng2);
                array_push($this->busStopPoints["latlng3"],$latlng3);
                array_push($this->busStopPoints["bearing"],$routePoint->bearing);
            }
        }
    }
    
    function createUnknownRoute(){
        
        $companyRep = CompanyRepository::getRepository($this->companyId);
        
        if ($companyRep->discoverRoutes !== "true"){
            return;
        }
        
        $latlng2Cluster = array();
        $routePoints = $this->createNewRoutePoints($latlng2Cluster);
        //dd($latlng2Cluster);
        
        $this->sortNewRoutePoints($routePoints,$latlng2Cluster);
        
        $structuredRouteCoordinates = $this->processRouteCoordinates($routePoints);
        
        $routeId = RouteRepository::createUnknownRoute($this->companyId, $structuredRouteCoordinates, false);
        
        return $routeId;
    }
    
    /**
     * This method creates the routeCoordinates string for routes that were manually created.
     */
    function processRouteCoordinates(){
        //$routeCoordinates = "";
        $this->structuredRouteCoordinates["latlng1"] = array();
        $this->structuredRouteCoordinates["latlng2"] = array();
        $this->structuredRouteCoordinates["latlng3"] = array();
        $this->structuredRouteCoordinates["points"] = array();
        $this->structuredRouteCoordinates["bearing"] = array();
        $this->structuredRouteCoordinates["speed"] = array_fill(0,count($this->points),0);
        $this->structuredRouteCoordinates["time"] = array_fill(0,count($this->points),strtotime("now"));
        $this->structuredRouteCoordinates["repeat"] = array_fill(0,count($this->points),1);
        
        $this->busStopPoints["latlng1"] = array();
        $this->busStopPoints["latlng2"] = array();
        $this->busStopPoints["latlng3"] = array();
        $this->busStopPoints["points"] = array();
        $this->busStopPoints["bearing"] = array();
        
        foreach ($this->points as $routePoint){
            $lat  = floatval($routePoint->latitude);
            $lng = floatval($routePoint->longitude);

            $latlng1 = $lat.",".$lng;
            $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            
            array_push($this->structuredRouteCoordinates["latlng1"],$latlng1);
            array_push($this->structuredRouteCoordinates["latlng2"],$latlng2);
            array_push($this->structuredRouteCoordinates["latlng3"],$latlng3);
            array_push($this->structuredRouteCoordinates["points"],$routePoint);
            array_push($this->structuredRouteCoordinates["bearing"],$routePoint->bearing);
            //$routeCoordinates = $routeCoordinates.";".$latlng1.",".$latlng2.",".$latlng3;
            
            if ($routePoint->busStop === "true"){
                array_push($this->busStopPoints["latlng1"],$latlng1);
                array_push($this->busStopPoints["latlng2"],$latlng2);
                array_push($this->busStopPoints["latlng3"],$latlng3);
                array_push($this->busStopPoints["points"],$routePoint);
                array_push($this->busStopPoints["bearing"],$routePoint->bearing);
            }
        }
    }
    
    /**
     * Will create the route points by only having those close it each other.
     * @return array
     */
    function createNewRoutePoints(&$latlng2Cluster){
        $routePoints = array();
        $latlng3Cluster = array();
        
        for ($i=0;$i<count($this->points);$i++){
            $point = $this->points[$i];
            
            $lat = $point->latitude;
            $lng = $point->longitude;
            
            $bearing1 = $point->bearing;
            
            if ($bearing1 == 0){
                continue;
            }
            
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            $latlng2 = bcdiv($lat,1,2).",".bcdiv($lng,1,2);
            
            if (!isset($latlng3Cluster[$latlng3])){
                $latlng3Cluster[$latlng3] = array();
                
                $this->insertIntoBearings($point);
                $this->insertIntoPoints($point, $latlng3, $routePoints, $latlng3Cluster);
                $this->insertIntoCluster($point, $latlng2, $latlng2Cluster);
                continue;
            }
            
            $latlng3Points =  $latlng3Cluster[$latlng3];
            foreach ($latlng3Points as $latlng3Point){
                $bearing2 = $latlng3Point->bearing;
                if (abs($bearing1 - $bearing2) < Constants::sameDirectionCutOff){
                    continue 2;
                }
            }
            
            $this->insertIntoBearings($point);
            $this->insertIntoPoints($point, $latlng3, $routePoints, $latlng3Cluster);
            $this->insertIntoCluster($point, $latlng2, $latlng2Cluster);
        }
        
        return $routePoints;
    }
    
    function insertIntoBearings($point){
        
    }
    
    function insertIntoCluster($point, $latlng, &$cluster){
        $pb = $point->bearing;
        
        if (!isset($cluster[$latlng])){
            $cluster[$latlng] = array();
            array_push($cluster[$latlng], $point);
            return;
        }else{
            $clusterPoints =  $cluster[$latlng];

            foreach ($clusterPoints as $clusterPoint){
                $cb = $clusterPoint->bearing;
                if (abs($pb - $cb) < Constants::sameDirectionCutOff){
                    return;
                }
            }
        }
        
        array_push($cluster[$latlng], $point);
    }
    
    function insertIntoPoints($point, $latlng3, &$routePoints, &$latlng3Cluster){
        
        if (count($routePoints) > 0){
            $prevPoint = $routePoints[max(0,count($routePoints)-2)];
        } else{
            $prevPoint = $point;
        }
        
        /*$lat = $point->latitude;
        $lng = $point->longitude;

        $prevLat = $prevPoint->latitude;
        $prevLng = $prevPoint->longitude;

        $distance = LatLngDetails::distance($lat, $lng, $prevLat, $prevLng);
        if (abs($distance) > 1000){
            return;
        }*/
        
        array_push($routePoints, $point);
        array_push($latlng3Cluster[$latlng3], $point);  
    }
    
    function sortNewRoutePoints(&$routePoints, $latlng2Cluster){
        // Will sort these according to bearing
        usort($routePoints,function($a, $b) use ($latlng2Cluster) {
            $lat_a = $a->latitude;
            $lng_a = $a->longitude;
            
            $lat_b = $b->latitude;
            $lng_b = $b->longitude;
            
            $latlng2_a = bcdiv($lat_a,1,2).",".bcdiv($lng_a,1,2);
            $latlng2_b = bcdiv($lat_b,1,2).",".bcdiv($lng_b,1,2);
            
            $ab = $a->bearing;
            $bb = $b->bearing;
            
            //Compare the id's of their latlng2 clusters
            $cluster_a = $latlng2Cluster[$latlng2_a];
            $cluster_b = $latlng2Cluster[$latlng2_b];
            foreach ($cluster_a as $clusterPoint){
                if (abs($ab - $clusterPoint->bearing) < Constants::sameDirectionCutOff){
                    $a_id = $clusterPoint->id;
                    break;
                }
            }
            
            foreach ($cluster_b as $clusterPoint){
                if (abs($bb - $clusterPoint->bearing) < Constants::sameDirectionCutOff){
                    $b_id = $clusterPoint->id;
                    break;
                }
            }
            
            // Only sort points in close proximity
            if ($latlng2_a !== $latlng2_b){
                //return $a_id - $b_id;
            }
            
            $distance = LatLngDetails::distance($a->latitude, $a->longitude, $b->latitude, $b->longitude);
            if (abs($distance) > 100){
                //return $a_id - $b_id;
            }
            
            if ($a->id == 1){
                //dd("a: ".$a->id." b: ".$b->id);
                //return -1;
            }
            
            if ($b->id == 1){
                //dd("b: ".$b->id." a: ".$a->id);
                //return 1;
            }
            
            $bearingBetween = $a->calculateBearing($b);
            //check if they are in same direction
            if (abs($ab-$bb) < Constants::sameDirectionCutOff){
                if ($bearingBetween > Constants::sameDirectionCutOff){
                    //if ($b->id == 13 || $a->id == 13)
                      //  dd("a front * b: ".$b->id." a: ".$a->id);
                    if ($ab < 180){
                        return 1;
                    }else{
                        return -1;
                    }
                }else{
                    //if ($b->id == 13 || $a->id == 13)
                      //  dd("b front * b: ".$b->id." a: ".$a->id);
                    if ($ab < 180){
                        return -1;
                    }else{
                        return 1;
                    }
                }
            }
            
            //dd("a: ".$a_id." b: ".$b_id);
            return $a_id - $b_id;
        });
        
        $id = 1;
        foreach ($routePoints as $point){
            $point->id=$id;
            $id++;
        }
    }
    
    /**
     * This method will create data needed by javascript in the ui
     */
    function prepareInfo(){
        
        if ($this->isComplete()){
            return;
        }
        
        $employeeRep = $this->getEmployee();
        $this->employeeName=$employeeRep->name;
        
        $structuredCoordinates = RouteRepository::sortCoordinates($this->structuredRouteCoordinates);
        $this->structuredRouteCoordinates = $structuredCoordinates;
        
        $mileageAndSpeed = $this->getMileageAndSpeed($structuredCoordinates);
        $this->setMileage($mileageAndSpeed[0],2);
        $this->topSpeed = round($mileageAndSpeed[1],2);
        
        $routePoints = RouteRepository::encodeStructuredCoordinates($structuredCoordinates);
        $this->points = RouteRepository::decodeFromXml($routePoints);
        $this->setBusStops();
        
        if (!isset($this->routeId)){
            return;
        }
        
        //Log::debug('shift get route: '.$this->routeId." co: ".$this->companyId);
        $routeRep = RouteRepository::getRepository($this->routeId, $this->companyId);
        if (!isset($routeRep)){
            return;
        }
        $this->routeName = $routeRep->routeName;
    }
    
    /**
     * gets the times each bus stop was stopped at.
     * does it by comparing each 
     */
    function createShiftLog(){
        if (count($this->shiftLog) > 0){
            return $this->shiftLog;
        }
        
        $this->prepareInfo();
        
        if (count($this->points) === 0){
            return;
        }
        
        //These are the incremental bus stops found while iterating the shift points.
        //They will start with the shift route bus stops.
        $foundBusStops = array();
        $foundBusStops["latlng3"] = array();
        $foundBusStops["points"] = array();
        
        $routeRep = RouteRepository::getRepository($this->routeId, $this->companyId);
        if (isset($routeRep)){
            $foundBusStops["latlng3"] = $routeRep->busStopPoints["latlng3"];
            $foundBusStops["points"] = $routeRep->busStopPoints["points"];
        }
        
        // When we come accross the first point at same bearing/direction
        // increase the index.
        $firstLat = $this->points[0]->latitude;
        $firstLng = $this->points[0]->longitude;
        $firstBearing = $this->points[0]->bearing;
        $firstLatLng3 = bcdiv($firstLat,1,3).",".bcdiv($firstLng,1,3);
        
        // this is the current index we are inserting for each stop
        // it represents the round of the shift. If a stop misses a round,
        // it means driver did not stop there for that round.
        $shiftCycle = 0;
        foreach($this->points as $point){
            
            $lat = $point->latitude;
            $lng = $point->longitude;
            
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            
            $closeToIndexes = RoutePointRepository::closeTo(bcdiv($lat,1,3), bcdiv($lng,1,3), array($firstLatLng3));
            if (count($closeToIndexes) > 0 && abs($firstBearing-$point->bearing) < Constants::sameDirectionCutOff){
                $shiftCycle++;
            }
            
            if (!$point->isBusStop()){
                continue;
            }
            
            $closeToIndexes = RoutePointRepository::closeTo(bcdiv($lat,1,3), bcdiv($lng,1,3), $foundBusStops["latlng3"]);
            
            $update = true;
            foreach ($closeToIndexes as $index){
                $updated = $this->updateShiftLog($point, $foundBusStops, $index, $shiftCycle);
                if ($updated){
                    $update = false;
                    break;
                }
            }
            
            if ($update){
                $this->updateShiftLog($point, $foundBusStops, null, $shiftCycle);
            }
        }
        
        // Sort the bus stops so they are in order.
        uksort($this->shiftLog,function($a, $b) {
            $an = (int)explode(" ", $a)[0];
            $bn = (int)explode(" ", $b)[0];
            return $an - $bn;
        });
        
        $this->shiftCycles = $shiftCycle;
        return $this->shiftLog;
    }
    
    /**
     * latlng3 is the coordinate that the point is close to
     * @param type $point
     * @param type $foundBusStops
     * @param string $latlng3
     * @param type $shiftCycle
     * @return type
     */
    function updateShiftLog($point, &$foundBusStops, $index, $shiftCycle){
        
        if (!isset($index)){
            $this->putShiftLogPoint($point, $point, $foundBusStops, $index, $shiftCycle);
            return true;
        }
        
        //find bus stops close to the latlng point is close to
        $busStop = $foundBusStops["points"][$index];
        
        $stopBearing = $busStop->bearing;
        $pointBearing = $point->bearing;
        $bearingDiff = abs($stopBearing-$pointBearing);
        if ($bearingDiff > Constants::sameDirectionCutOff && $stopBearing > 0){
            return false;
        }
        
        $this->putShiftLogPoint($point, $busStop, $foundBusStops, $index, $shiftCycle);  
        return true;
    }
    
    /**
     * Both point and busStop are bus stop points.
     * Point is the new one to be added if it is the same as existing ones with same bearing.
     * @param type $point
     * @param type $busStop
     * @param array $foundBusStops
     */
    function putShiftLogPoint($point, $busStop, &$foundBusStops, $index, $shiftCycle){
        //shift log array will contain the times for each stop.
        //each stop will have a key of point id - stop name.
        //e.g. 12 - Rosebank.
        $id = $busStop->id." - ".$busStop->name;
        if (!isset($this->shiftLog[$id])){
            $this->shiftLog[$id] = array();
        }

        $time = date("H:i", $point->getTime());
        $this->shiftLog[$id][$shiftCycle] = $time;
        
        if (!isset($index)){
            $lat = $point->latitude;
            $lng = $point->longitude;
            $latlng3 = bcdiv($lat,1,3).",".bcdiv($lng,1,3);
            
            array_push($foundBusStops["latlng3"], $latlng3);
            array_push($foundBusStops["points"], $point);
        }
    }
    

    function getEmployeeName() {
        return $this->employeeName;
    }

    function setEmployeeName() {
        $employeeRep = $this->getEmployee();
        if (isset($employeeRep)){
            $this->employeeName = $employeeRep->name;
        }
    }
   
    function getShiftId() {
        return $this->shiftId;
    }

    function getEmployeeId() {
        return $this->employeeId;
    }
    
    function getEmployee(){
        $employeeRep = EmployeeRepository::getRepository($this->employeeId, $this->companyId);
        return $employeeRep;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function getRouteId() {
        return $this->routeId;
    }
    
    function getRoute(){
        $routeRep = RouteRepository::getRepository($this->routeId, $this->companyId);
        return $routeRep;
    }

    function getDeviceId() {
        return $this->deviceId;
    }

    function getMileage() {
        return $this->mileage;
    }

    function getTopSpeed() {
        return $this->topSpeed;
    }

    function getStart() {
        return $this->start;
    }

    function getEnd() {
        return $this->end;
    }

    function getRoutePoints() {
        return $this->routePoints;
    }

    function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }

    function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
    }

    /**
     * Mileage is in km.
     * @param type $mileage
     */
    function setMileage($mileage) {
        $mileageKm = (float)$mileage/1000;
        $this->mileage = round($mileageKm,2);
    }

    function setTopSpeed($topSpeed) {
        $this->topSpeed = round($topSpeed,2);
        $this->speed = round($topSpeed,2);
    }

    function setStart($start) {
        $this->start = $start;
    }

    function setEnd($end) {
        if (!BaseRepository::isEmptyOrNullString($end)){
            $this->status = "Completed";
        }else{
            $this->status = "In Progress";
        }
        
        $this->end = $end;
    }
    
    function isComplete(){
        if (!BaseRepository::isEmptyOrNullString($this->end)){
            return true;
        }
        return false;
    }

    function setRoutePoints($routePoints) {
        $this->routePoints = $routePoints;
    }
    
    function getStructuredRouteCoordinates() {
        return $this->structuredRouteCoordinates;
    }

    function setStructuredRouteCoordinates($structuredRouteCoordinates) {
        $this->structuredRouteCoordinates = $structuredRouteCoordinates;
    }
    
    function setPoints($points) {
        $this->points = $points;
    }
    
    function getLastSeen() {
        return $this->lastSeen;
    }

    function setLastSeen($lastSeen) {
        $this->lastSeen = $lastSeen;
    }
}

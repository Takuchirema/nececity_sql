<?php namespace App\Repositories;

use Mail;
use Datetime;
use App\Models\Notification;
use App\Repositories\BaseRepository;
use App\Repositories\CompanyRepository;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\Mailer;

/**
 * Description of NotificationRepository
 *
 * @author Takunda
 */
class NotificationRepository extends BaseRepository {
    var $notificationId;
    var $time;
    var $notification;
    var $email;
    
    var $companyId;
    var $toCompanyId;
    
    var $toEmail;
    var $seen;
    
    function __construct($companyId,$toCompanyId) {
       $this->companyId=$companyId;
       $this->toCompanyId=$toCompanyId;
    }
    
    function create(){
        $notificationNode = $this->createNode();
        $this->notificationId = $notificationNode->notificationId;
        
        //Send an email to the company
        $this->sendEmail();
        
        $response["success"] = 1;
        $response["notificationId"] = $notificationNode->notificationId;
        $response["message"] = "Notification has been created!";
        return ($response);
        
    }
    
    function sendEmail()
    {
        if (isset($this->company->email) && isset($this->toCompany->email)){
            $mailer = new Mailer();
            
            $mailer->setToAddress($this->toCompany->email);
            $mailer->setSubject("NeceCity Notifications");
            $mailer->setBody($this->notification) ;
            $mailer->sendEmail();
        }
    }
    
    function createNode(){
        $notification = new Notification;
        
        $date = date($this->dateTimeFormat);
        //$date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $notification->time = $date;
	$notification->notification = $this->notification;
	$notification->companyId = $this->companyId;
	$notification->toCompanyId = $this->toCompanyId;
        $notification->seen=false;
        $notification->save();
        
        return $notification;
    }
    
    public static function delete($notificationId){
        $notificationNode = NotificationRepository::getNode($notificationId);
        $toCompanyId = $notificationNode->toCompanyId;
        
        Notification::query()
                ->where('notificationId','=',$notificationId)
                ->delete();
        
        //BaseRepository::deleteFromCache($companyId, CacheType::CompanyNotifications, CacheExpiry::CompanyNotifications, $notificationId);
        CompanyRepository::updateCacheRepository($toCompanyId);
        
        $response["success"] = 1;
        $response["notificationId"] = $notificationId;
        $response["message"] = "Notification has been deleted";
        return $response;
    }
    
    public static function getNode($notificationId){
        
        $notification = Notification::query()
                ->where('notificationId','=',$notificationId)
                ->first();
        
        return $notification;
    }
    
    public static function getNotifications($companyId){
        $response['notifications'] = array();
        
        //$cachedNotifications = BaseRepository::getFromCache($companyId,CacheType::CompanyNotifications);
        
        /*if (isset($cachedNotifications)){
            $response['notifications'] = $cachedNotifications;
            $response["success"] = 1;
            $response["message"] = "Retrieved notifications from cache";
            return $response; 
        }*/
        
        $notifications = Notification::query()
                ->where('toCompanyId','=',$companyId)
                ->get();
        
        //$cacheItems = array();
        if (!is_null($notifications)) {
            foreach($notifications as $notification) {

                $notificationRep = NotificationRepository::getRepository($notification);
                
                array_push($response["notifications"], $notificationRep);
                //$cacheItems[$notificationRep->getId()] = $notificationRep;
            }
        }
        
        //BaseRepository::putIntoCache($companyId, CacheType::CompanyNotifications, CacheExpiry::CompanyNotifications, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Notifications";
        return $response; 
    }
    
    public static function getRepository($notification){
        
        $notificationRep = new NotificationRepository($notification->companyId,$notification->toCompanyId);
        $time = NotificationRepository::getDateTimeString($notification->time);

        $notificationRep->setTime($time);
        $notificationRep->setNotificationId($notification->notificationId);
        $notificationRep->setNotification($notification->notification);
        $notificationRep->setSeen($notification->seen);
        
        return $notificationRep;
    }
    
    public static function seenNotification($notificationId){

        $notificationNode = Notification::query()
                   ->where('notificationId','=', $notificationId)
                   ->first();
        
        if (isset($notificationNode)){
            $notificationNode->seen = true;
            $notificationNode->save();
        }
    }
    
    function getCompanyName(){
        $companyRep = CompanyRepository::getRepository($this->companyId);
        return $companyRep->companyName;
    }
    
    function getToCompanyName(){
        $companyRep = CompanyRepository::getRepository($this->toCompanyId);
        return $companyRep->companyName;
    }
    
    function getNotificationId() {
        return $this->notificationId;
    }

    function getTime() {
        return $this->time;
    }

    function getNotification() {
        return $this->notification;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function getToCompanyId() {
        return $this->toCompanyId;
    }

    function getSeen() {
        return $this->seen;
    }

    function setNotificationId($notificationId) {
        $this->notificationId = $notificationId;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setNotification($notification) {
        $this->notification = $notification;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setToCompanyId($toCompanyId) {
        $this->toCompanyId = $toCompanyId;
    }

    function setSeen($seen) {
        $this->seen = $seen;
    }
}

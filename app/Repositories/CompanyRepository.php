<?php namespace App\Repositories;

use App\Http\Helpers\DeleteFolder;
use App\Http\Helpers\Images;
use App\Http\Helpers\ObjectType;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;

use Datetime;
use App\Repositories\BaseRepository;
use App\Repositories\LocationRepository;
use App\Repositories\BusinessHoursRepository;
use App\Repositories\PostRepository;
use App\Repositories\CataloguePostRepository;
use App\Repositories\PromotionRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\AppVariableRepository;

use App\Models\Company;
use App\Models\Post;
use App\Models\Promotion;
use App\Models\CataloguePost;
use App\Models\BusinessHours;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Description of UserController
 *
 * @author Takunda Chirema
 */

class CompanyRepository extends BaseRepository implements AuthenticatableContract ,CanResetPasswordContract{
    
    var $companyId;
    var $companyName;
    var $noSpaceId;
    var $address;
    var $hours;
    var $about;
    var $phoneNumber;
    var $sector;
    var $type;
    var $profilePicture;
    var $hasProfilePicture="false";
    
    var $location;
    var $businessHours;
    var $routes;
    var $email;
    var $response = array();
    var $blocked;
    var $following=false;
    var $approved;
    var $countryCode;
    
    var $logTime;
    var $discoverRoutes;
    var $routeClustering;
    var $deliveries;
    var $token;
    
    private $notifications = array();
    private $password;
    private $devices;
    
    //used on the dashboard
    var $menu="all";
    
    function __construct($companyName) {
       $this->companyName= $companyName;
       $this->noSpaceId = str_replace(' ', '_',$companyName);
    }
    
    /**
     * The method will try and save the given company in the neo4j database
     * using the name and any additional details
     */
    function create(){
        $company = Company::query()
                ->where('companyName','=',$this->companyName)
                ->first();
        
        if (!is_null($company)) {
            // There exists company with same name
            $response["success"] = 0;
            $response["message"] = "I'm sorry, this company is already in use";
            return $response;
        }
        
        $companyNode = $this->createCompany();
        $this->companyId = $companyNode->companyId;
        
        $this->createCompanyDirectory();
        
        $companyRep = CompanyRepository::getAppCompany();
        
        if (isset($companyRep)){
            $welcomeNotification = new NotificationRepository($companyRep->companyId,$this->companyId);
            $welcomeNotification->notification = 
                    "Welcome to NeceCity! Please complete your profile. NeceCity will approve your company after "
                    . "all necessary details are filled.";
            $welcomeNotification->create();

            $newCompanyNotification = new NotificationRepository($this->companyId,$companyRep->companyId);
            $newCompanyNotification->notification = 
                    "Hello! A new company ".$this->companyName." has been create. Please review it.";
            $newCompanyNotification->create();
        }
        
        $response["success"] = 1;
        $response["message"] = "Company Successfully Added!";
        
        return $response;
    }
    
    function createCompany(){
        $company = new Company;
        $company->companyName = $this->companyName;
        $company->phoneNumber = $this->phoneNumber;
        $company->address = $this->address;
        $company->sector = $this->sector;
        $company->about = $this->about;
        $company->type = $this->type;
        $company->password = $this->password;
        $company->email = $this->email;
        $company->deliveries = "false";
        $company->discoverRoutes = "false";
        $company->logTime = "false";
        
        $company->save();
        return $company;
    }
    
    public static function getAppCompanyName(){
        return AppVariableRepository::getVariable("AppName")->value;
    }
    
    public static function getAppCompany(){
        $companyRep = CompanyRepository::getRepository(null,config('app.name'));
        return $companyRep;
    }
    
    function createCompanyDirectory(){
        if (file_exists(base_path('public/companies/'.$this->companyId))){
            DeleteFolder::delTree(base_path('public/companies/'.$this->companyId));
        }

        $old = umask(0);
        mkdir(base_path('public/companies/'.$this->companyId), 0777, true);
        umask($old);
    }
    
    public static function delete($companyId){      
        
        Post::query()
                ->where('companyId','=',$companyId)
                ->delete();
        
        Promotion::query()
                ->where('companyId','=',$companyId)
                ->delete();
        
        CataloguePost::query()
                ->where('companyId','=',$companyId)
                ->delete();
        
        Company::query()
                ->where('companyId','=',$companyId)
                ->delete();
        
        BusinessHours::query()
                ->where('companyId','=',$companyId)
                ->delete();
        
        BaseRepository::deleteItemFromCache(null, CacheType::Companies, CacheExpiry::Companies, $companyId);
        
        $response["success"] = 1;
        $response["message"] = "Company successfuly deleted";
        return $response;
    }
    
    public static function deleteFile($companyId,$url,$type,$id){
        $companyRep = CompanyRepository::getRepository($companyId);
        $companyName = $companyRep->companyName;
        
        $factoredCompanyId = str_replace(' ', '%20', $companyName);
        $result = array();
        $filePath = str_replace(url(''), public_path(''),$url);
        
        if (strpos($filePath, $factoredCompanyId) !== false){
            if (!unlink($filePath)){
                $result["success"] = 0;
                $result["message"] = "Could not delete the file ".$filePath;
                return $result;
            }
        }
        
        if (strtolower($type) == ObjectType::Post){
            PostRepository::updateCacheRepository($companyId, $id);
        }else if (strtolower($type) == ObjectType::CataloguePost){
            CataloguePostRepository::updateCacheRepository($companyId, $id);
        }else if (strtolower($type) == ObjectType::Promotion){
            PromotionRepository::updateCacheRepository($companyId, $id);
        }
        
        $result["success"] = 1;
        $result["message"] = "The file ".$url." has been deleted";
        return $result;
    }
    
    public static function getRepository($companyId, $companyName=null, $cached = true){
        
        if ($cached){
            $cachedRepository = BaseRepository::getItemFromCache(CacheType::Companies, $companyId);
        }
        
        if (isset($cachedRepository)){
            return $cachedRepository;
        }
        
        $company = CompanyRepository::getNode($companyId,$companyName);
        
        if (!isset($company)){
            return null;
        }
        
        $companyRep = CompanyRepository::createRepository($company);
        
        BaseRepository::putItemIntoCache(null, CacheType::Companies, CacheExpiry::Companies, $companyRep->companyId, $companyRep);
        
        return $companyRep;
    }
    
    
    public static function createRepository($company)
    {
        if (!$company){
            return null;
        }
        
        $companyRep = new CompanyRepository($company->companyName);
        $companyRep->setCompanyId($company->companyId);
        $companyRep->setPassword($company->password);
        $companyRep->setAbout($company->about);
        $companyRep->setAddress($company->address);
        $companyRep->setSector($company->sector);
        $companyRep->setPhoneNumber($company->phoneNumber);
        $companyRep->setType($company->type);
        $companyRep->setLocation($company->location);
        $companyRep->setEmail($company->email);
        $companyRep->setLogTime($company->logTime);
        $companyRep->setDiscoverRoutes($company->discoverRoutes);
        $companyRep->setRouteClustering($company->routeClustering);
        $companyRep->setDeliveries($company->deliveries);
        $companyRep->setToken($company->token);
        $companyRep->setApproved($company->approved);
        $companyRep->setCountryCode($company->countryCode);

        $companyRep->setLocation(LocationRepository::getLocation($company->companyId));
        $companyRep->setBusinessHours(BusinessHoursRepository::getBusinessHours($company->companyId));
        $companyRep->setRoutes(RouteRepository::getRoutesDetails($company->companyId)["routes"]);
        $companyRep->setNotifications(NotificationRepository::getNotifications($company->companyId)["notifications"]);
        
        $pictureUrl = CompanyRepository::getCompanyProfilePic($company);
        $companyRep->setProfilePicture($pictureUrl);

        $companyRep->setHasProfilePicture($companyRep->getHasProfilePicture());
        
        return $companyRep;
    }
    
    public static function getCompanyProfilePic($companyRep){
        $companyName = $companyRep->companyName;
        $companyId = $companyRep->companyId;
        
        if (file_exists(public_path('companies/'.$companyId.'/profile'))){
            $filename = public_path('companies/'.$companyId.'/profile');
            $file_url = url('companies/'.$companyId.'/profile');
        }else{
            $filename = public_path('companies/'.$companyName.'/profile');
            $file_url = url('companies/'.$companyName.'/profile');
        }
        
        if (file_exists($filename)){

            $pictures = scandir($filename,1);
            $picture_url = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);

            return $picture_url;
        }
        
        return null;
    }
    
    public static function getNode($companyId, $companyName=null){
        if (!isset($companyId)){
            $company = Company::query()
                    ->where('companyName','=',$companyName)
                    ->first(); 
        }else{
            $company = Company::query()
                        ->where('companyId','=',$companyId)
                        ->first();      
        }
        return $company;
    }
    
    public static function getCompanies(){
        $response = array();
        $response['companies'] = array();
        
        $companies = Company::all();
        
        foreach($companies as $company) {
            //Get if user is blocked from the controller.
            $companyRep = CompanyRepository::createRepository($company);

            array_push($response["companies"], $companyRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Companies";
        return $response;
    }
    
    public static function updateCacheRepository($companyId){
        $companyRep = CompanyRepository::getRepository($companyId,null,false);
        BaseRepository::putItemIntoCache($companyId, CacheType::Companies, CacheExpiry::Companies, $companyId, $companyRep);
    }
    
    public static function getCompanyByValue($property,$value)
    {
        try{
            $companyNode = Company::query()
                        ->where($property,'=',$value)
                        ->first();
            $companyRep = CompanyRepository::createRepository($companyNode);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
        
        return $companyRep;
    }
    
    public static function getFollowedCompanies($userId, $countryCode=null){
        $response['companies'] = array();
        
        // check if the user is cached and has companies
        $userRep = BaseRepository::getItemFromCache(CacheType::Users, $userId);
        
        if (isset($userRep) && !empty($userRep->getCompanies())){
            $response["companies"] = $userRep->getCompanies();
            $response["success"] = 1;
            $response["message"] = "Retrieved followed companies from cache";
            return $response;
        }
        
        if (isset($countryCode)){
            $companies = DB::table('companies')
                ->select('companies.*','followers.blocked')
                ->join('followers', 'followers.companyId', '=', 'companies.companyId')
                ->where('followers.userId','=', $userId)
                ->where('companies.approved','=','true')
                ->where(function($query) use($countryCode)
                        {
                            $query->where('companies.countryCode','=',$countryCode)
                                  ->orWhere('companies.countryCode','=','ALL');
                        })
                ->get();
        }else{
            $companies = DB::table('companies')
                ->select('companies.*','followers.blocked')
                ->join('followers', 'followers.companyId', '=', 'companies.companyId')
                ->where('followers.userId','=', $userId)
                ->where('companies.approved','=','true')
                ->get();
        }
        
        foreach($companies as $company) {
            //Get if user is blocked from the controller.
            $companyRep = CompanyRepository::createRepository($company);
            $companyRep->blocked = $company->blocked;
            $companyRep->following = true;
            array_push($response["companies"], $companyRep);
        }
        
        if (isset($userRep)){
            $userRep->setCompanies($response["companies"]);
            BaseRepository::putItemIntoCache(null, CacheType::Users, CacheExpiry::Users, $userId, $userRep);
        }else{
            $user = UserRepository::getUserNode($userId, null);
            $userRep = UserRepository::getUserRepository($user);
            $userRep->setCompanies($response["companies"]);
            BaseRepository::putItemIntoCache(null, CacheType::Users, CacheExpiry::Users, $userId, $userRep);
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Companies";
        return $response;
    }
    
    public static function getUnfollowedCompanies($userId, $countryCode=null){
        $response['companies'] = array();
        
        if (isset($countryCode)){
            $companies = Company::query()
                            ->where('companies.approved','=','true')
                            ->where(function($query) use($countryCode)
                                    {
                                        $query->where('companies.countryCode','=',$countryCode)
                                              ->orWhere('companies.countryCode','=','ALL');
                                    })
                            ->get();
        }else{
            $companies = Company::query()
                            ->where('companies.approved','=','true')
                            ->get();
        }
        
        foreach($companies as $company) {
            if ($company->approved === "false"){
                continue;
            }
            //Get if user is blocked from the controller.
            $follower = FollowerRepository::getFollower($userId, $company->companyId);
            if (is_null($follower)){
                $companyRep = CompanyRepository::createRepository($company);
                array_push($response["companies"], $companyRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Companies";
        return $response;
    }
    
    public static function getEmployeeLogs($companyId,$routeId=null,$dateString=null)
    {
        $results = TimeLogRepository::getAllTimeLogs($companyId);
        return $results;
    }
    
    function getEmployees(){
        $employees = EmployeeRepository::getEmployees($this->companyId)['employees'];
        return $employees;
    }
    
    function uploadProfilePicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$this->companyId."/profile");
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        $response["success"] = 1;
        $response["message"] = "Company profile uploaded";
        $response["url"] = url('companies/'.$this->companyId.'/profile/'.$file->getClientOriginalName());
        return $response;
        
    }
    
    function update($properties){
        
        try{
            Company::query()
                    ->where('companyId','=', $this->companyId)
                    ->update($properties);
        }catch(\Illuminate\Database\QueryException $ex){
            $response["success"] = 1;
            $response["message"] = $ex->getMessage();
            return $response;
        }
        
        CompanyRepository::updateCacheRepository($this->companyId);
        
        $response["success"] = 1;
        $response["message"] = "value updated";
        return $response ;
    }
    
    function getAdress() {
        return $this->address;
    }
    
    function getPhoneNumber() {
        return $this->phoneNumber;
    }
    
    function getHours() {
        return $this->hours;
    }
    
    function getAbout() {
        return $this->about;
    }
    
    function getSector() {
        return $this->sector;
    }
    
    function getType() {
        return $this->type;
    }
    
    function setAddress($address) {
        return $this->address=$address;
    }
    function setEmail($email){

        return $this->email = $email;

    }

    function setPassword($password){
        return $this->password = $password;
    }
    
    function getPassword(){
        return $this->password;
    }
    
    function setPhoneNumber($phoneNumber) {
        return $this->phoneNumber = $phoneNumber;
    }
    
    function setHours($hours) {
        return $this->hours = $hours;
    }
    
    function setAbout($about) {
        return $this->about=$about;
    }
    
    function setSector($sector) {
        return $this->sector=$sector;
    }
    
    function setType($type) {
        return $this->type=$type;
    }

    function getLocation() {
        return $this->location;
    }

    function getResponse() {
        return $this->response;
    }
    
    function getNoSpaceId() {
        return $this->noSpaceId;
    }

    function setNoSpaceId($noSpaceId) {
        $this->noSpaceId = $noSpaceId;
    }

    
    function setLocation($location) {
        $this->location = $location;
    }

    function setResponse($response) {
        $this->response = $response;
    }
    
    function getBusinessHours() {
        return $this->businessHours;
    }

    function setBusinessHours($businessHours) {
        $this->businessHours = $businessHours;
    }   
    
    function getProfilePicture() {
        return $this->profilePicture;
    }

    function setProfilePicture($profilePicture) {
        $this->profilePicture = $profilePicture;
    }
    
    function getHasProfilePicture() {
        if ($this->profilePicture === null){
            return "false";
        }else{
            if (preg_match('/(\.jpg|\.png|\.bmp)$/i', $this->profilePicture)) {
                return "true";
            }else{
                return "false";
            }
        }
    }

    function setHasProfilePicture($hasProfilePicture) {
        $this->hasProfilePicture = $hasProfilePicture;
    }
    
    function getRoutes() {
        return $this->routes;
    }

    function setRoutes($routes) {
        $this->routes = $routes;
    }

    public function getAuthIdentifierName()
    {
        return "companyId";
    }
    
    public function getAuthIdentifier()
    {
        return $this->companyId;
    }
    
    public function getAuthPassword()
    {
            return $this->password;
    }
    
    public function getRememberToken()
    {
        return $this->token;
    }
    
    public function setRememberToken( $value)
    {
         $this->token = $value;
    }
    
    public function getRememberTokenName()
    {
        return "token";
    }
    
    function getEmailForPasswordReset(){
        return $this->email;
    }
    
    function sendPasswordResetNotification($token)
    {   
    }
    
    function getBlocked() {
        return $this->blocked;
    }

    function setBlocked($blocked) {
        $this->blocked = $blocked;
    }
    
    function getApproved() {
        return $this->approved;
    }

    function setApproved($approved) {
        $this->approved = $approved;
    }
    
    function getLogTime() {
        return $this->logTime;
    }

    function setLogTime($logTime) {
        $this->logTime = $logTime;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getCompanyName() {
        return $this->companyName;
    }

    function setCompanyName($companyName) {
        $this->companyName = $companyName;
        $this->noSpaceId = str_replace(' ', '_', $companyName);
    }

        
    function getToken() {
        return $this->token;
    }

    function setToken($token) {
        $this->token = $token;
    }
    
    function getNotifications() {
        return $this->notifications;
    }

    function setNotifications($notifications) {
        $this->notifications = $notifications;
    }
    
    function getCountryCode() {
        return $this->countryCode;
    }

    function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
    }

    function getMenu() {
        return $this->menu;
    }

    function setMenu($menu) {
        $this->menu = $menu;
    }
    
    function getDeliveries() {
        return $this->deliveries;
    }

    function setDeliveries($deliveries) {
        $this->deliveries = $deliveries;
    }
    
    function getDevices() {
        return DeviceRepository::getDevices($this->companyId)["devices"];
    }

    function setDevices($devices) {
        $this->devices = $devices;
    }
    
    function getDiscoverRoutes() {
        return $this->discoverRoutes;
    }

    function setDiscoverRoutes($discoverRoutes) {
        $this->discoverRoutes = $discoverRoutes;
    }
    
    function getRouteClustering() {
        return $this->routeClustering;
    }

    function setRouteClustering($routeClustering) {
        $this->routeClustering = $routeClustering;
    }
    
}

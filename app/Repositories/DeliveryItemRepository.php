<?php namespace App\Repositories;

/**
 * Description of DeliveryItemRepository
 *
 * @author Takunda
 */
class DeliveryItemRepository {
    var $id;
    var $name;
    var $description;
    var $url;
    var $price;
    
    function __construct($id) {
       $this->id = $id;
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getUrl() {
        return $this->url;
    }

    function getPrice() {
        return $this->price;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setPrice($price) {
        $this->price = $price;
    }

}

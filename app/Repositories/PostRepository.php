<?php namespace App\Repositories;

use Datetime;
use App\Http\Helpers\Images;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\DeleteFolder;
use App\Models\Post;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use App\Models\Relationship;

/**
 * Description of Post
 *
 * @author Takunda Chirema
 */
class PostRepository extends BaseRepository {
    
    var $companyId;
    var $createdBy;
    var $post;
    var $time;
    var $day;
    var $postId;
    var $likes;
    var $type;
    
    var $comments = array();
    var $pictureUrls = array();
    
    var $pictureUrl;
    
    var $seen;
    var $liked;
    
    function __construct($companyId) {
       $this->companyId=$companyId;
    }
    
    function create(){
        
        $postNode = $this->createNode();
        $this->postId = $postNode->postId;
        
        if (file_exists(public_path('companies/'.$this->companyId."/posts/".$postNode->postId))){
            DeleteFolder::delTree(public_path('companies/'.$this->companyId."/posts/".$postNode->postId));
        }
        
        $old = umask(0);
        mkdir(public_path('companies/'.$this->companyId."/posts/".$postNode->postId), 0777, true);
        umask($old);
        
        PostRepository::updateCacheRepository($this->companyId, $postNode->postId);
        
        $response["success"] = 1;
        $response["postId"] = $postNode->postId;
        $response["message"] = "Post has been Posted!";
        return ($response);
        
    }
    
    function createNode(){
        $post = new Post;
        
        $date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $post->time = $date;
	$post->post = $this->post;
	$post->companyId = $this->companyId;
	$post->type = $this->type;
        $post->createdBy=$this->createdBy;
        $post->save();
        
        return $post;
    }
    
    public static function delete($postId, $companyId, $type){
        Comment::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->delete();
        
        Post::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->where('type','=',$type)
                ->delete();

        DeleteFolder::delTree(public_path('companies/'.$companyId."/posts/".$postId));
        
        BaseRepository::deleteListItemFromCache($companyId, CacheType::CompanyPosts, CacheExpiry::CompanyPosts, $postId);
        
        $response["success"] = 1;
        $response["postId"] = $postId;
        $response["message"] = "post has been deleted";
        return $response;
    }
    
    public static function getNode($postId,$companyId){
        
        $post = Post::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->first();
        
        return $post;
    }
    
    public static function getPosts($companyId,$type){
        $response['posts'] = array();
        
        $cachedPosts = BaseRepository::getListFromCache($companyId,CacheType::CompanyPosts);
        
        if (isset($cachedPosts)){
            $response['posts'] = $cachedPosts;
            $response["success"] = 1;
            $response["message"] = "Retrieved posts from cache";
            return $response; 
        }
        
        $posts = Post::query()
                ->where('companyId','=',$companyId)
                ->where('type','=',$type)
                ->get();
        
        $cacheItems = array();
        if (!is_null($posts)) {
            foreach($posts as $post) {

                $postRep = PostRepository::getRepository($post);
                
                array_push($response["posts"], $postRep);
                $cacheItems[$postRep->getId()] = $postRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyPosts, CacheExpiry::CompanyPosts, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Posts";
        return $response; 
    }
    
    public static function getRepository($post){
        
        $postRep = new PostRepository($post->companyId);
        $time = PostRepository::getDateTimeString($post->time);

        $postRep->setId($post->postId);
        $postRep->setLikes($post->likes);
        $postRep->setTime($time);
        $postRep->setType($post->type);
        $postRep->setPost($post->post);

        $comments = CommentRepository::getComments($post->postId,null,null)["comments"];
        $postRep->setComments($comments);

        //check for the post picture
        $filename = public_path('companies/'.$post->companyId.'/posts/'.$post->postId);
        $file_url = url('companies/'.$post->companyId.'/posts/'.$post->postId);

        if (file_exists($filename) && count(glob("$filename/*")) != 0){
            $pictureUrls = array();
            $allFiles = scandir($filename,1);
            $pictures = array_diff($allFiles, array('.', '..'));
            
            $pictureUrl = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);
            //array_push($pictureUrls,$pictureUrl);
            foreach ($pictures as $picture){
                array_push($pictureUrls,str_replace(' ', '%20', $file_url.'/'.$picture));
            }
            $postRep->setPictureUrl($pictureUrl);
            $postRep->setPictureUrls($pictureUrls);
        }
        
        return $postRep;
    }
    
    public static function getUserPosts($companyId,$userId)
    {
        $response = array();
        
        $posts = PostRepository::getPosts($companyId,'company')['posts'];
        
        foreach ($posts as $post){
            
            $seen = Relationship::query()
                    ->where('entityId','=',$userId)
                    ->where('relatedEntityId','=',$post->postId)
                    ->where('type','=', RelationshipRepository::$userSeenPost)
                    ->first();
            
            $liked = Relationship::query()
                    ->where('entityId','=',$userId)
                    ->where('relatedEntityId','=',$post->postId)
                    ->where('type','=', RelationshipRepository::$userLikedPost)
                    ->first();
            
            if (!is_null($seen)){
                $post->setSeen('true');
            }else{
                $post->setSeen('false');   
            }
            
            if (!is_null($liked)){
                $post->setLiked('true');
            }else{
                $post->setLiked('false');   
            }
        }
        
        $response['posts'] = $posts;
        $response['success'] = 1;
        $response['message'] = "retrieved posts";
        return $response;
    }
    
    public static function getEmployeePosts($companyId,$employeeId){
        $response['posts'] = array();
        
        $posts = Post::query()
                ->where('companyId','=',$companyId)
                ->where('createdBy','=',$employeeId)
                ->get();
        
        if (!is_null($posts)) {
            foreach($posts as $post) {
                $postRep = PostRepository::getRepository($post);
                array_push($response["posts"], $postRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Posts";
        return $response; 
    }
    
    function update($properties)
    {
        try{
            Post::query()
                   ->where('companyId','=', $this->companyId)
                   ->where('postId','=', $this->postId)
                   ->update($properties);
            PostRepository::updateCacheRepository($this->companyId, $this->postId);
         }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    public static function updateCacheRepository($companyId,$postId){
        $post = PostRepository::getNode($postId, $companyId);
        $postRep = PostRepository::getRepository($post);
        BaseRepository::putListItemIntoCache($companyId, CacheType::CompanyPosts, CacheExpiry::CompanyPosts, $postId, $postRep);
    }
    
    function uploadPicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$this->companyId."/posts/".$this->postId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        PostRepository::updateCacheRepository($this->companyId, $this->postId);
        
        $response["success"] = 1;
        $response["message"] = "Post picture uploaded";
        return $response;
    }
    
    public static function editPostPicture($file,$companyId,$postId){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/posts/".$postId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        $response["success"] = 1;
        $response["message"] = "Post picture uploaded";
        return $response;
    }
    
    public static function uploadPictures($files,$companyId,$postId){
        
        if (is_null($files)){
            $response["success"] = 0;
            $response["message"] = "Files is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/posts/".$postId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        foreach($files as $file){
            if (!is_null($file)){
                Images::compress($file,$path."/".$file->getClientOriginalName());
            }
        }
        
        PostRepository::updateCacheRepository($companyId,$postId);
        
        $response["success"] = 1;
        $response["message"] = "Post pictures uploaded";
        return $response;
    }
    
    public static function likePost($postId,$companyId)
    {
        $post = PostRepository::getNode($postId, $companyId);
        $likes = $post->likes;
        
        if ($likes){
            $likes = ((int)$likes) + 1;
        }else{
            $likes = 1;
        }
        
        $post->likes = $likes;
        $post->save();
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function getPost() {
        return $this->post;
    }

    function getTime() {
        return $this->time;
    }

    function getId() {
        return $this->postId;
    }

    function getLikes() {
        return $this->likes;
    }

    function getType() {
        return $this->type;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setPost($post) {
        $this->post = $post;
    }

    function setTime($time) {
        $this->time = $time;
        
        $dateTime = explode("_", $time);
        $date = $dateTime[0];
        
        $this->day = date("d F Y", strtotime($date));
    }

    function setId($postId) {
        $this->postId = $postId;
    }

    function setLikes($likes) {
        $this->likes = $likes;
    }

    function setType($type) {
        $this->type = $type;
    }
    
    function getComments() {
        return $this->comments;
    }

    function setComments($comments) {
        if (isset($comments)){
            $this->comments = $comments;
        }
    }
    
    function getPictureUrl() {
        return $this->pictureUrl;
    }

    function setPictureUrl($pictureUrl) {
        $this->pictureUrl = $pictureUrl;
    }
    
    function getPictureUrls() {
        return $this->pictureUrls;
    }

    function setPictureUrls($pictureUrls) {
        $this->pictureUrls = $pictureUrls;
    }

    function getSeen() {
        return $this->seen;
    }

    function setSeen($seen) {
        $this->seen = $seen;
    }
    
    function getLiked() {
        return $this->liked;
    }

    function setLiked($liked) {
        $this->liked = $liked;
    }
    
    function getDay() {
        return $this->day;
    }

    function setDay($day) {
        $this->day = $day;
    }
    
    function getCreatedBy() {
        return $this->createdBy;
    }

    function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
}

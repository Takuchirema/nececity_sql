<?php namespace App\Repositories;

/**
 * Description of RoutePoint
 *
 * @author Takunda Chirema
 */
class RoutePointRepository extends BaseRepository {
    
    var $id;
    var $name;
    var $busStop;
    var $latitude;
    var $longitude;
    //Represents the distance travelled from point to point up to here
    var $distance = 0;
    var $timeTable = array();
    private $timeDetails = array();
    
    // This is a record of how often we have stopped at this point
    // It is only used to identify candidate busstops when the figure is more than the cutoff.
    private $stopFrequency=0;
    
    // This is a record of the maximum stop frequency in a route.
    // It will be set when route is decoded and the first point in route will have the record
    private $maxBusStopFrequency = 0;
    
    //This is a record of the time and direction when the vehicle stopped at this stop on a shift.
    //Will be used to update routes now and again when shifts are sufficient size
    var $time;
    var $date;
    var $speed=0;
    var $bearing=0;
    
    // This will be set during route db access.
    // It is the maximum frequency of stops at a bus stop as to-date.
    // It will be used to clear out in-frequent stops and times.
    // This will always be set on the first route point in the points array and can be used anywhere after that.
    // Like to put it in bus-stop points when they are made.
    private $maxBusStopTimeFrequency = 1;
            
    // This one is used in memory to show estimated arrival time.
    // The format is just H:s to be used directly for UI purposes
    var $estimatedArrival;
    
    // This will store the actual time in seconds i.e. just the strtotime output
    private $estimatedArrivalTime;
    
    function __construct($id) {
       $this->id = $id;
    }
    
    function calculateBearing( $point )
    {
        return $this->getRhumbLineBearing( $this->latitude, $point->longitude, $point->latitude, $this->longitude );
    }

    function getRhumbLineBearing($lat1, $lon1, $lat2, $lon2) {
        //difference in longitudinal coordinates
        $dLon = deg2rad($lon2) - deg2rad($lon1);

        //difference in the phi of latitudinal coordinates
        $dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));

        //we need to recalculate $dLon if it is greater than pi
        if(abs($dLon) > pi()) {
          if($dLon > 0) {
            $dLon = (2 * pi() - $dLon) * -1;
          }
          else {
            $dLon = 2 * pi() + $dLon;
          }
        }
        //return the angle, normalized
        return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
    }

    function getCompassDirection( $bearing )
    {
        static $cardinals = array( 'N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW', 'N' );
        return $cardinals[round( $bearing / 45 )];
    }
    
    /**
     * Checks whether given lat lng is close to one of the points in given array
     * Uses decimal increases to do this.
     * @param type $lat
     * @param type $lng
     * @param type $latlngs
     * @return type
     */
    public static function closeTo($lat, $lng, $latlngs){
        if (count($latlngs) == 0){
            return array();
        }
        
        $latlng = $lat.",".$lng;
        
        $indexes = array_keys($latlngs,$latlng);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $decimals = strlen(substr(strrchr($lat, "."), 1));
        $increment = '.' . str_repeat('0', $decimals-1) . '1';
        $lat_p1 = $lat + $increment;
        $lng_p1 = $lng + $increment;
        
        $lat_m1 = $lat - $increment;
        $lng_m1 = $lng - $increment;
        
        $lat_p1lng = $lat_p1.",".$lng;
        
        $indexes = array_keys($latlngs,$lat_p1lng);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $latlng_p1 = $lat.",".$lng_p1;
        
        $indexes = array_keys($latlngs,$latlng_p1);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $lat_p1lng_p1 = $lat_p1.",".$lng_p1;
        
        $indexes = array_keys($latlngs,$lat_p1lng_p1);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $lat_m1lng = $lat_m1.",".$lng;
        
        $indexes = array_keys($latlngs,$lat_m1lng);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $latlng_m1 = $lat.",".$lng_m1;
        
        $indexes = array_keys($latlngs,$latlng_m1);
        if (count($indexes)>0){
            return $indexes;
        }
        
        $lat_m1lng_m1 = $lat_m1.",".$lng_m1;
        
        $indexes = array_keys($latlngs,$lat_m1lng_m1);
        if (count($indexes)>0){
            return $indexes;
        }
        
        //dd($latlng." - ".$lat_p1lng." - ".$latlng_p1." - ".$lat_m1lng." - ".$latlng_m1);
        return array();
    }
    
    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getBusStop() {
        return $this->busStop;
    }
    
    function isBusStop(){
        if ($this->busStop==="true"){
            return true;
        }
        return false;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setBusStop($busStop) {
        $this->busStop = $busStop;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }
    
    function getTimeTable() {
        return $this->timeTable;
    }

    function setTimeTable($timeTable) {
        $this->timeTable = $timeTable;
    }
    
    function getTimeDetails() {
        return $this->timeDetails;
    }

    function setTimeDetails($timeDetails) {
        $this->timeDetails = $timeDetails;
    }
    
    function getTime() {
        return $this->time;
    }

    function getSpeed() {
        return $this->speed;
    }
    
    function getDistance() {
        return $this->distance;
    }

    function setDistance($distance) {
        $this->distance = $distance;
    }

    function getBearing() {
        return $this->bearing;
    }

    function setTime($time) {
        if (BaseRepository::isEmptyOrNullString($time)){
            return;
        }
        $this->time = $time;
        $this->date = date("d/m/Y H:i:s", $time);
    }

    function setSpeed($speed) {
        $this->speed = $speed;
    }

    function setBearing($bearing) {
        $this->bearing = $bearing;
    }
    
    // gets the maximum frequency of the times
    function getMaxFrequency(){
        $frequency = 0;
        foreach ($this->timeDetails as $time){
            if ($time->frequency > $frequency){
                $frequency = $time->frequency;
            }
        }
        
        return $frequency;
    }
    
    function getMaxBusStopTimeFrequency() {
        return $this->maxBusStopTimeFrequency;
    }

    function setMaxBusStopTimeFrequency($maxBusStopTimeFrequency) {
        $this->maxBusStopTimeFrequency = $maxBusStopTimeFrequency;
    }
    
    function getStopFrequency() {
        return $this->stopFrequency;
    }

    function setStopFrequency($stopFrequency) {
        $this->stopFrequency = (int)$stopFrequency;
    }
    
    function getEstimatedArrivalTime() {
        return $this->estimatedArrivalTime;
    }

    function setEstimatedArrivalTime($estimatedArrivalTime) {
        $this->estimatedArrivalTime = $estimatedArrivalTime;
    }
    
    function getMaxBusStopFrequency() {
        return $this->maxBusStopFrequency;
    }

    function setMaxBusStopFrequency($maxBusStopFrequency) {
        $this->maxBusStopFrequency = (int)$maxBusStopFrequency;
    }
}

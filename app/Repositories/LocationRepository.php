<?php namespace App\Repositories;

use Datetime;
use App\Models\Location;
use App\Repositories\BaseRepository;

/**
 * Description of Location
 *
 * @author Takunda Chirema
 */
class LocationRepository extends BaseRepository {
    
    var $latitude;
    var $longitude;
    var $locationNode;
    var $companyId;
    var $address;
    var $countryCode;
    var $time;
    
    function __construct($companyId) {
       $this->companyId=$companyId;
    }
    
    function create(){
        //Remove old locations
        Location::query()
                    ->where('companyId','=',$this->companyId)
                    ->delete();
        
        $this->locationNode = $this->createLocationNode();

        $response["success"] = 1;
        $response["message"] = "Location has been Locationed!";
        return ($response);
    }
    
    function createLocationNode(){
        
        $locationNode = new Location;
        
        $locationNode->companyId = $this->companyId;
	$locationNode->latitude = $this->latitude;
	$locationNode->longitude = $this->longitude;
        $locationNode->time = new Datetime();
        
        $locationNode->save();
        
        return $locationNode;
    }
    
    public static function getLocation($companyId){

        $location = Location::query()
                    ->where('companyId','=',$companyId)
                    ->first();
        
        if (!is_null($location)){
            $locationRep = new LocationRepository($companyId);
            $locationRep->setLatitude($location->latitude);
            $locationRep->setLongitude($location->longitude);
            $locationRep->setAddress($location->address);
            $locationRep->setCountryCode($location->countryCode);
            return $locationRep;
        }
        
        return null;
    }
    
    function update($properties)
    {
        try{
            Location::query()
                    ->where('companyId','=', $this->companyId)
                    ->update($properties);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getCompany() {
        return $this->company;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setCompany($company) {
        $this->company = $company;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getLocationNode() {
        return $this->locationNode;
    }

    function setLocationNode($locationNode) {
        $this->locationNode = $locationNode;
    }

    function getAddress() {
        return $this->address;
    }

    function setAddress($address) {
        $this->address = $address;
    }
    
    function getCountryCode() {
        return $this->countryCode;
    }

    function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
    }
}

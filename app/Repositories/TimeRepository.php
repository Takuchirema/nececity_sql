<?php namespace App\Repositories;

/**
 * Description of TimeRepository
 *
 * @author Takunda
 */
class TimeRepository {
    var $time;
    var $bearing=0;
    var $frequency=1;
    var $hour;
    var $min;
    
    function __construct($time) {
        $this->time=$time;
        $this->frequency=1;
        $this->bearing=0;
    }
    
    function getTime() {
        return $this->time;
    }

    function getBearing() {
        return $this->bearing;
    }

    function getHour() {
        return $this->hour;
    }

    function getMin() {
        return $this->min;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setBearing($bearing) {
        $this->bearing = $bearing;
    }

    function setHour($hour) {
        $this->hour = $hour;
    }

    function setMin($min) {
        $this->min = $min;
    }
    
    function getFrequency() {
        return $this->frequency;
    }

    function setFrequency($frequency) {
        $this->frequency = $frequency;
    }
}

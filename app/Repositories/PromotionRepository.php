<?php namespace App\Repositories;

use Datetime;
use App\Http\Helpers\Images;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\DeleteFolder;
use App\Models\Promotion;
use App\Repositories\CommentRepository;
use App\Models\Comment;
use App\Models\Relationship;

/**
 * Description of Promotion
 *
 * @author Takunda Chirema
 */
class PromotionRepository extends BaseRepository {
    
    var $companyId;
    var $createdBy;
    var $promotion;
    var $title;
    var $time;
    var $promotionId;
    var $likes;
    var $liked;
    
    var $comments = array();
    var $pictureUrls = array();
    
    var $pictureUrl;
    var $weekly;
    var $onceOff;
    var $days;
    var $duration;
    var $from;
    var $to;
    var $price;
    var $units;
    
    var $seen;
    
    function __construct($companyId) {
       $this->companyId=$companyId;
    }
    
    function create(){
        
        $promotionNode = $this->createNode();
        $this->promotionId = $promotionNode->promotionId;
        
        if (file_exists(public_path('companies/'.$this->companyId."/promotions/".$this->promotionId))){
            DeleteFolder::delTree(public_path('companies/'.$this->companyId."/promotions/".$this->promotionId));
        }
        
        $old = umask(0);
        mkdir(public_path('companies/'.$this->companyId."/promotions/".$this->promotionId), 0777, true);
        umask($old);
        
        PromotionRepository::updateCacheRepository($this->companyId, $promotionNode->promotionId);
        
        $response["success"] = 1;
        $response["promotionId"] = $this->promotionId;
        $response["message"] = "Promotion has been set!";
        return ($response);
        
    }
    
    function createNode(){
        $promotion = new Promotion;
        
        $date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $promotion->time = $date;
	
        $promotion->promotion = $this->promotion;
	$promotion->companyId = $this->companyId;
        $promotion->createdBy=$this->createdBy;
	$promotion->weekly = $this->weekly;
	$promotion->onceOff = $this->onceOff;
	$promotion->days = $this->days;
        $promotion->title = $this->title;
        $promotion->duration = $this->duration;
        $promotion->price = $this->price;
        $promotion->units = $this->units;
        $promotion->save();
        
        return $promotion;
    }
    
    public static function getNode($promotionId,$companyId){
        $promotion = Promotion::query()
                ->where('promotionId','=',$promotionId)
                ->where('companyId','=',$companyId)
                ->first();
        
        return $promotion;
    }
    
    public static function getPromotions($companyId){
        $response['promotions'] = array();
        
        $cachedPromotions = BaseRepository::getListFromCache($companyId,CacheType::CompanyPromotions);
        
        if (isset($cachedPromotions)){
            $response['promotions'] = $cachedPromotions;
            $response["success"] = 1;
            $response["message"] = "Retrieved promotions from cache";
            return $response; 
        }
        
        $promotions = Promotion::query()
                ->where('companyId','=',$companyId)
                ->get();

        $cacheItems = array();
        if (!is_null($promotions)) {
            foreach($promotions as $promotion) {
                
                $promotionRep = PromotionRepository::getRepository($promotion);
                array_push($response["promotions"], $promotionRep);
                $cacheItems[$promotionRep->getId()] = $promotionRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyPromotions, CacheExpiry::CompanyPromotions, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Promotions";
        return $response; 
    }
    
    public static function getRepository($promotion){
        
        $promotionRep = new PromotionRepository($promotion->companyId);
        $time = PromotionRepository::getDateTimeString($promotion->time);

        $promotionRep->setId($promotion->promotionId);
        $promotionRep->setTitle($promotion->title);
        $promotionRep->setLikes($promotion->likes);
        $promotionRep->setTime($time);
        $promotionRep->setPromotion($promotion->promotion);
        $promotionRep->setWeekly($promotion->weekly);
        $promotionRep->setOnceOff($promotion->onceOff);
        $promotionRep->setDays($promotion->days);
        $promotionRep->setDuration($promotion->duration);
        $promotionRep->setPrice($promotion->price);
        $promotionRep->setUnits($promotion->units);

        $comments = CommentRepository::getComments(null,$promotion->promotionId,null)["comments"];
        $promotionRep->setComments($comments);

        //check for the promotion picture
        $filename = public_path('companies/'.$promotion->companyId.'/promotions/'.$promotion->promotionId);
        $file_url = url('companies/'.$promotion->companyId.'/promotions/'.$promotion->promotionId);

        if (file_exists($filename) && count(glob("$filename/*")) != 0){
            $pictureUrls = array();
            $allFiles = scandir($filename,1);
            $pictures = array_diff($allFiles, array('.', '..'));
            
            $pictureUrl = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);
            //array_push($pictureUrls,$pictureUrl);
            foreach ($pictures as $picture){
                array_push($pictureUrls,str_replace(' ', '%20', $file_url.'/'.$picture));
            }
            $promotionRep->setPictureUrl($pictureUrl);
            $promotionRep->setPictureUrls($pictureUrls);
        }
        
        return $promotionRep;
    }
    
    public static function delete($promotionId,$companyId){
        Comment::query()
                ->where('promotionId','=',$promotionId)
                ->where('companyId','=',$companyId)
                ->delete();
        
        Promotion::query()
                ->where('promotionId','=',$promotionId)
                ->where('companyId','=',$companyId)
                ->delete();
        
        DeleteFolder::delTree(public_path('companies/'.$companyId."/promotions/".$promotionId));
        
        BaseRepository::deleteListItemFromCache($companyId, CacheType::CompanyPromotions, CacheExpiry::CompanyPromotions, $promotionId);
        
        $response["success"] = 1;
        $response["promotionId"] = $promotionId;
        $response["message"] = "Promotion successfully deleted";
        return $response;
    }
    
    public static function getUserPromotions($companyId,$userId)
    {
        $response = array();
        
        $promotions = PromotionRepository::getPromotions($companyId)['promotions'];
        
        foreach ($promotions as $promotion){
            
            $seen = Relationship::query()
                    ->where('entityId','=',$userId)
                    ->where('relatedEntityId','=',$promotion->promotionId)
                    ->where('type','=', RelationshipRepository::$userSeenPromotion)
                    ->first();
            
            $liked = Relationship::query()
                    ->where('entityId','=',$userId)
                    ->where('relatedEntityId','=',$promotion->promotionId)
                    ->where('type','=', RelationshipRepository::$userLikedPromotion)
                    ->first();
            
            if (!is_null($seen)){
                $promotion->setSeen('true');
            }else{
                $promotion->setSeen('false');   
            }
            
            if (!is_null($liked)){
                $promotion->setLiked('true');
            }else{
                $promotion->setLiked('false');   
            }
        }
        
        $response['promotions'] = $promotions;
        $response['success'] = 1;
        $response['message'] = "retrieved promotions";
        return $response;
    }
    
    public static function getEmployeePromotions($companyId, $employeeId){
        $response['promotions'] = array();
        
        $promotions = Promotion::query()
                ->where('companyId','=',$companyId)
                ->where('createdBy','=',$employeeId)
                ->get();

        if (!is_null($promotions)) {
            foreach($promotions as $promotion) {
                
                $promotionRep = PromotionRepository::getRepository($promotion);
                array_push($response["promotions"], $promotionRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Promotions";
        return $response; 
    }
    
    function update($properties)
    {
        try{
            Promotion::query()
                   ->where('companyId','=', $this->companyId)
                   ->where('promotionId','=', $this->promotionId)
                   ->update($properties);
            PromotionRepository::updateCacheRepository($this->companyId, $this->promotionId);
         }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    public static function updateCacheRepository($companyId,$promotionId){
        $promotion = PromotionRepository::getNode($promotionId, $companyId);
        $promotionRep = PromotionRepository::getRepository($promotion);
        BaseRepository::putListItemIntoCache($companyId, CacheType::CompanyPromotions, CacheExpiry::CompanyPromotions, $promotionId, $promotionRep);
    }
    
    function uploadPicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/promotions/".$promotionId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        //delete all previous images
        //DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        PromotionRepository::updateCacheRepository($this->companyId, $this->promotionId);
        
        $response["success"] = 1;
        $response["message"] = "Promotion picture uploaded";
        return $response;
    }
    
    public static function editPicture($file,$companyId,$promotionId){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/promotions/".$promotionId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        //delete all previous images
        //DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        PromotionRepository::updateCacheRepository($companyId, $promotionId);
        
        $response["success"] = 1;
        $response["message"] = "Promotion picture uploaded";
        return $response;
    }
    
    public static function uploadPictures($files,$companyId,$promotionId){
        
        if (is_null($files)){
            $response["success"] = 0;
            $response["message"] = "Files is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/promotions/".$promotionId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        foreach($files as $file){
            if (!is_null($file)){
                Images::compress($file,$path."/".$file->getClientOriginalName());
            }
        }
        
        PromotionRepository::updateCacheRepository($companyId,$promotionId);
        
        $response["success"] = 1;
        $response["message"] = "Promotion pictures uploaded";
        return $response;
    }
    
    public static function likePromotion($promotionId,$companyId)
    {
        $promotion = PromotionRepository::getNode($promotionId, $companyId);
        $likes = $promotion->likes;
        
        if ($likes){
            $likes = ((int)$likes) + 1;
        }else{
            $likes = 1;
        }
        
        $promotion->likes = $likes;
        $promotion->save();
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function getPromotion() {
        return $this->promotion;
    }

    function getTime() {
        return $this->time;
    }

    function getId() {
        return $this->promotionId;
    }

    function getLikes() {
        return $this->likes;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setPromotion($promotion) {
        $this->promotion = $promotion;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setId($promotionId) {
        $this->promotionId = $promotionId;
    }

    function setLikes($likes) {
        $this->likes = $likes;
    }
    
    function getComments() {
        return $this->comments;
    }

    function setComments($comments) {
        if (isset($comments)){
            $this->comments = $comments;
        }
    }
    
    function getPictureUrl() {
        return $this->pictureUrl;
    }

    function setPictureUrl($pictureUrl) {
        $this->pictureUrl = $pictureUrl;
    }
    
    function getPictureUrls() {
        return $this->pictureUrls;
    }

    function setPictureUrls($pictureUrls) {
        $this->pictureUrls = $pictureUrls;
    }

    function getSeen() {
        return $this->seen;
    }

    function setSeen($seen) {
        $this->seen = $seen;
    }
    
    function getWeekly() {
        return $this->weekly;
    }

    function getOnceOff() {
        return $this->onceOff;
    }

    function getDays() {
        return $this->days;
    }

    function setWeekly($weekly) {
        $this->weekly = $weekly;
    }

    function setOnceOff($onceOff) {
        $this->onceOff = $onceOff;
    }

    function setDays($days) {
        $this->days = $days;
    }

    function getLiked() {
        return $this->liked;
    }

    function setLiked($liked) {
        $this->liked = $liked;
    }
    
    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }
    
    function getDuration() {
        return $this->duration;
    }

    function setDuration($duration) {
        $this->duration = $duration;
        if (isset($duration)){
            $durations = explode(',', $duration);
            if (count($durations) !== 2){
                return;
            }
            $this->from = $durations[0];
            $this->to = $durations[1];
        }
    }
    
    function getPrice() {
        return $this->price;
    }

    function getUnits() {
        return $this->units;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setUnits($units) {
        $this->units = $units;
    }
    
    function getCreatedBy() {
        return $this->createdBy;
    }

    function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
}

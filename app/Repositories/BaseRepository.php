<?php namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Datetime;

/**
 * Description of Alert
 *
 * @author Takunda Chirema
 */

abstract class BaseRepository {
    
    public $days = array('sunday', 'monday', 'tuesday', 'wednesday','thursday','friday', 'saturday');
    public $dateTimeFormat='Y-m-d_H:i';
    public $dateFormat='Y-m-d';
    public $timeFormat='H:i';
    /**
     * The Model instance.
     *
     * @var Illuminate\Database\Eloquent\Model
     */

    protected $model;
    
    /**
     * Get number of records.
     *
     * @return array
     */

    public function create()
    {
    }

    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */

    public function destroy($id)
    {
    }

    /**
     * Get Model by id.
     *
     * @param  int  $id
     * @return App\Models\Model
     */

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }
    
    /*
     * Returns datetime from the default format
     */
    public static function getDateTime($input)
    {
        $dateTime = Datetime::createFromFormat('Y-m-d H:i:s', $input);
        return $dateTime;
    }
    
    /*
     * Converts from default format to the format in variable $dateTimeFormat
     */
    public static function getDateTimeString($input)
    {
        $dateTime = Datetime::createFromFormat('Y-m-d H:i:s', $input);
        return date_format($dateTime,'Y-m-d_H:i');
    }
    
    public static function getDateString($input)
    {
        $dateTime = Datetime::createFromFormat('Y-m-d', $input);
        return date_format($dateTime,'Y-m-d');
    }
    
    /*
     * Converts from default format to the format in variable $dateTimeFormat
     */
    public static function getTimeString($input)
    {
        $dateTime = Datetime::createFromFormat('Y-m-d H:i:s', $input);
        return date_format($dateTime,'H:i');
    }
    
    public static function isEmptyOrNullString($str){
        return (!isset($str) || trim($str) === '');
    }
    
    /**
     * Every cached item must have a prefix. Prefix cannot be null.
     * Cached items are in an array. This used when updating the cache.
     * If array is empty we assume it expired and so it must be reloaded.
     * @param type $prefix
     * @param type $cacheType
     * @return type
     */
    public static function getListFromCache($prefix, $cacheType){
        
        if (isset($prefix)){
            $cachedItems = Cache::get($cacheType."_".$prefix."_List");
        }else{
            $cachedItems = Cache::get($cacheType."_List");
        }
        if (isset($cachedItems) && !empty($cachedItems)){
            return array_values($cachedItems);
        }
        return null;
    }
    
    public static function getItemFromCache($cacheType, $cacheItemId){
        if (BaseRepository::isEmptyOrNullString($cacheItemId)){
            return;
        }
        $cachedItem = Cache::get($cacheType."_".$cacheItemId);
        return $cachedItem;
    }
    
    /**
     * Rule: Only use this method IF getItemFromCache returned null.
     * @param type $prefix
     * @param type $cacheType
     * @param type $cacheExpiry
     */
    public static function putListIntoCache($prefix, $cacheType, $cacheExpiry, $cacheList){
        if (!isset($cacheList)){
            return;
        }
        if (isset($prefix)){
            Cache::put($cacheType."_".$prefix."_List",$cacheList,$cacheExpiry);
        }else{
            Cache::put($cacheType."_List",$cacheList,$cacheExpiry);
        }
    }
    
    // Only if the item is already in the list.
    public static function putListItemIntoCache($prefix, $cacheType,$cacheExpiry, $cacheItemId, $cacheItem){
        if (!isset($cacheItem)){
            return;
        }
        
        if (BaseRepository::isEmptyOrNullString($cacheItemId)){
            return;
        }
        
        if (isset($prefix)){
            $cachedItems = Cache::get($cacheType."_".$prefix."_List");
        }else{
            $cachedItems = Cache::get($cacheType."_List");
        }
        
        // Cached Items must have something so that we are not putting single elements in it.
        if (!isset($cachedItems)){
            return;
        }
        
        $cachedItems[$cacheItemId] = $cacheItem;
        BaseRepository::putListIntoCache($prefix, $cacheType, $cacheExpiry, $cachedItems);
    }
    
    /**
     * Rule: Only use this method IF getItemFromCache returned null.
     * @param type $prefix
     * @param type $cacheType
     * @param type $cacheExpiry
     * @param type $cacheItemId
     * @param type $cacheItem
     * @return type
     */
    public static function putItemIntoCache($prefix, $cacheType,$cacheExpiry, $cacheItemId, $cacheItem){
        if (!isset($cacheItem)){
            return;
        }
        
        if (BaseRepository::isEmptyOrNullString($cacheItemId)){
            return;
        }
        
        BaseRepository::putListItemIntoCache($prefix, $cacheType, $cacheExpiry, $cacheItemId, $cacheItem);
        
        Cache::put($cacheType."_".$cacheItemId,$cacheItem,$cacheExpiry);
    }
    
    public static function deleteListItemFromCache($prefix, $cacheType, $cacheExpiry, $cacheItemId){
        if (isset($prefix)){
            $cachedItems = Cache::get($cacheType."_".$prefix."_List");
        }else{
            $cachedItems = Cache::get($cacheType."_List");
        }
        
        if (!isset($cachedItems)){
            return;
        }
        
        unset($cachedItems[$cacheItemId]);
        BaseRepository::putListIntoCache($prefix, $cacheType, $cacheExpiry, $cachedItems);
    }
    
    public static function deleteItemFromCache($prefix, $cacheType, $cacheExpiry, $cacheItemId){
        BaseRepository::deleteListItemFromCache($prefix, $cacheType, $cacheExpiry, $cacheItemId);
        
        Cache::forget($cacheType."_".$cacheItemId);
    }
}
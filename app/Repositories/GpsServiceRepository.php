<?php namespace App\Repositories;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Datetime;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;

/**
 * Description of GpsServiceRepository
 *
 * @author Takunda
 */
class GpsServiceRepository extends BaseRepository{
    
    private $ipAddress = "127.0.0.1";
    private $port = "7331";
    
    var $connectedDevices = array();
    
    // The constructor is private
    // to prevent initiation with outer code.
    private function __construct()
    {
      // The expensive process (e.g.,db connection) goes here.
    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getRepository()
    {
        $serviceRep = BaseRepository::getItemFromCache(CacheType::GpsService, 1);
    
        if (!isset($serviceRep))
        {
            $serviceRep = new GpsServiceRepository();
            BaseRepository::putItemIntoCache(null, CacheType::GpsService, CacheExpiry::GpsService, 1, $serviceRep);
        }

        return $serviceRep;
    }
    
    public static function clearCache(){
        $gpsRep = new GpsServiceRepository();
        BaseRepository::putItemIntoCache(null, CacheType::GpsService, CacheExpiry::GpsService, 1, $gpsRep);
    }
    
    public static function recordInfo($request){
        $deviceId = $request->input('id');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $speed = $request->input('speed');
        $bearing = $request->input('bearing');
        $info = $request->input('info');
        
        $date = new DateTime('now');
        $time = $date->format('Y-m-d H:i:s');

        $gpsRep = BaseRepository::getItemFromCache(CacheType::GpsService, 1);
        if (!isset($gpsRep)){
            $gpsRep = new GpsServiceRepository();
        }
        
        if (!isset($gpsRep->connectedDevices[$deviceId])){
            $connectedDevice = new DeviceRepository($deviceId,null);
        }else{
           $connectedDevice = $gpsRep->connectedDevices[$deviceId];
        }
        
        $deviceRep = BaseRepository::getItemFromCache(CacheType::Devices, $deviceId);
        if (!isset($deviceRep)){
            $deviceRep = $connectedDevice;
        }
        
        if (isset($info) && $info === "location"){
            $deviceRep->gpsSpeed = $speed;
            $deviceRep->gpsBearing = $bearing;
            $deviceRep->gpsLat = $latitude;
            $deviceRep->gpsLng = $longitude;
        }else{
            $deviceRep->gpsSpeed = $connectedDevice->gpsSpeed;
            $deviceRep->gpsBearing = $connectedDevice->gpsBearing;
            $deviceRep->gpsLat = $connectedDevice->gpsLat;
            $deviceRep->gpsLng = $connectedDevice->gpsLng;
        }
        
        $deviceRep->gpsTime = $time;
        $deviceRep->gpsInfo = $info;
        
        $gpsRep->connectedDevices[$deviceId] = $deviceRep;
        BaseRepository::putItemIntoCache(null, CacheType::GpsService, CacheExpiry::GpsService, 1, $gpsRep);
    }
    
    function startServer(){
        false;
    }
    
    function restartServer(){
        $pid = shell_exec('sudo netstat -nlp | grep :7331');
        if (!BaseRepository::isEmptyOrNullString($pid)){
            $kill = shell_exec('kill -9 '.$pid);
        }
        $output = shell_exec('nohup php /var/www/html/nececity/app/Scripts/GpsService.php </dev/null >/dev/null 2>&1 &');
        false;
    }
    
    /* Will not be killing server in code for now*/
    function stopServer(){
        false;
    }
    
    function isServerRunning(){
        $connection = @fsockopen($this->ipAddress, $this->port);
        if (is_resource($connection))
        {
            fclose($connection);
            return true;
        }
        return false;
    }
}

<?php namespace App\Repositories;

/**
 * Description of ShiftSummaryRepository
 *
 * @author Takunda
 */
class ShiftSummaryRepository {
    
    var $shiftId;
    var $routeId;
    var $companyId;
    var $lastStop;
    var $nextStop;
    var $expectedArrival;
    
    function __construct($shiftId) {
       $this->shiftId = $shiftId;
    }
    
    function getShiftId() {
        return $this->shiftId;
    }

    function getLastStop() {
        return $this->lastStop;
    }

    function getNextStop() {
        return $this->nextStop;
    }

    function getExpectedArrival() {
        return $this->expectedArrival;
    }

    function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }

    function setLastStop($lastStop) {
        $this->lastStop = $lastStop;
    }

    function setNextStop($nextStop) {
        $this->nextStop = $nextStop;
    }

    function setExpectedArrival($expectedArrival) {
        $this->expectedArrival = $expectedArrival;
    }
}

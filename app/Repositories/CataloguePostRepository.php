<?php namespace App\Repositories;

use Datetime;
use App\Http\Helpers\Images;
use App\Http\Helpers\DeleteFolder;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\CacheExpiry;
use App\Models\CataloguePost;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use App\Models\Relationship;

use Illuminate\Support\Facades\Cache;
/**
 * Description of CataloguePostRepository
 *
 * @author Takunda
 */
class CataloguePostRepository extends BaseRepository {
    var $companyId;
    var $createdBy;
    var $post;
    var $time;
    var $day;
    var $postId;
    var $likes;
    var $title;
    var $duration;
    var $from;
    var $to;
    var $price;
    var $units;
    var $amenities;
    
    var $comments = array();
    var $pictureUrls = array();
    
    var $pictureUrl;
    var $fileUrl;
    
    var $seen;
    var $liked;
    
    function __construct($companyId) {
       $this->companyId=$companyId;
    }
    
    function create(){
        
        $postNode = $this->createNode();
        $this->postId = $postNode->postId;
        
        if (file_exists(public_path('companies/'.$this->companyId."/catalogue_posts/".$postNode->postId))){
            DeleteFolder::delTree(public_path('companies/'.$this->companyId."/catalogue_posts/".$postNode->postId));
        }
        
        $old = umask(0);
        mkdir(public_path('companies/'.$this->companyId."/catalogue_posts/".$postNode->postId), 0777, true);
        umask($old);
        
        CataloguePostRepository::updateCacheRepository($this->companyId, $postNode->postId);
        
        $response["success"] = 1;
        $response["postId"] = $postNode->postId;
        $response["message"] = "Catlogue Post has been Posted!";
        return ($response);
        
    }
    
    function createNode(){
        $post = new CataloguePost;
        
        $date = Datetime::createFromFormat($this->dateTimeFormat, $this->time);
        $post->time = $date;
	$post->post = $this->post;
        $post->title=$this->title;
	$post->companyId = $this->companyId;
        $post->createdBy=$this->createdBy;
        $post->duration=$this->duration;
        $post->amenities=$this->amenities;
        $post->price=$this->price;
        $post->units=$this->units;
        $post->save();
        
        return $post;
    }
    
    public static function delete($postId, $companyId){
        Comment::query()
                ->where('cataloguePostId','=',$postId)
                ->where('companyId','=',$companyId)
                ->delete();
        
        CataloguePost::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->delete();
        
        DeleteFolder::delTree(public_path('companies/'.$companyId."/catalogue_posts/".$postId));
        
        BaseRepository::deleteListItemFromCache($companyId, CacheType::CompanyCataloguePosts, CacheExpiry::CompanyCataloguePosts, $postId);
        
        $response["success"] = 1;
        $response["postId"] = $postId;
        $response["message"] = "Catalogue post has been deleted";
        return $response;
    }
    
    public static function getNode($postId,$companyId){
        
        $post = CataloguePost::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->first();
        
        return $post;
    }
    
    public static function getPosts($companyId,$type){
        $response['posts'] = array();
        
        $cachedPosts = null;// BaseRepository::getFromCache($companyId,CacheType::CompanyCataloguePosts);
        
        if (isset($cachedPosts)){
            $response['posts'] = $cachedPosts;
            $response["success"] = 1;
            $response["message"] = "Retrieved posts from cache";
            return $response; 
        }
        
        $posts = CataloguePost::query()
                ->where('companyId','=',$companyId)
                ->get();

        $cacheItems = array();
        if (!is_null($posts)) {
            foreach($posts as $post) {

                $postRep = CataloguePostRepository::getRepository($post,$companyId);
                array_push($response["posts"], $postRep);
                $cacheItems[$postRep->getId()] = $postRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyCataloguePosts, CacheExpiry::CompanyCataloguePosts, $cacheItems);
        //var_dump(Cache::get(CacheType::CompanyCataloguePosts."_".$companyId));
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Posts";
        return $response; 
    }
    
    public static function getEmployeePosts($companyId,$employeeId){
        $response['posts'] = array();
        
        $posts = CataloguePost::query()
                ->where('companyId','=',$companyId)
                ->where('createdBy','=',$employeeId)
                ->get();

        if (!is_null($posts)) {
            foreach($posts as $post) {

                $postRep = CataloguePostRepository::getRepository($post,$companyId);
                array_push($response["posts"], $postRep);
            }
        }
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Posts";
        return $response; 
    }
    
    public static function getRepository($post,$companyId){
        $postRep = new CataloguePostRepository($companyId);
        $time = CataloguePostRepository::getDateTimeString($post->time);

        $postRep->setId($post->postId);
        $postRep->setTitle($post->title);
        $postRep->setLikes($post->likes);
        $postRep->setTime($time);
        $postRep->setPost($post->post);
        $postRep->setDuration($post->duration);
        $postRep->setPrice($post->price);
        $postRep->setAmenities($post->amenities);
        $postRep->setUnits($post->units);

        $comments = CommentRepository::getComments(null,null,$post->postId)["comments"];
        $postRep->setComments($comments);

        //check for the post picture
        $filename = public_path('companies/'.$post->companyId.'/catalogue_posts/'.$post->postId);
        $file_url = url('companies/'.$post->companyId.'/catalogue_posts/'.$post->postId);

        if (file_exists($filename) && count(glob("$filename/*")) != 0){
            $pictureUrls = array();
            $allFiles = scandir($filename,1);
            $pictures = array_diff($allFiles, array('.', '..'));
            
            //array_push($pictureUrls,$pictureUrl);
            foreach ($pictures as $picture){
                if(is_file($filename.'/'.$picture)){
                    array_push($pictureUrls,str_replace(' ', '%20', $file_url.'/'.$picture));
                }
            }
            
            if (count($pictureUrls) > 0){
                $pictureUrl = str_replace(' ', '%20', $file_url.'/'.$pictureUrls[0]);
                $postRep->setPictureUrl($pictureUrl);
                $postRep->setPictureUrls($pictureUrls);
            }
        }
        
        //check for the post files
        $filename = public_path('companies/'.$post->companyId.'/catalogue_posts/'.$post->postId.'/files');
        $file_url = url('companies/'.$post->companyId.'/catalogue_posts/'.$post->postId.'/files');

        if (file_exists($filename) && count(glob("$filename/*")) != 0){
            $allFiles = scandir($filename,1);
            $files = array_diff($allFiles, array('.', '..'));
            
            $fileUrl = str_replace(' ', '%20', $file_url.'/'.$files[0]);
            $postRep->setFileUrl($fileUrl);
        }
        
        return $postRep;
    }
    
    public static function getPostRepository($postId, $companyId,$type){
        
        $post = CataloguePost::query()
                ->where('postId','=',$postId)
                ->where('companyId','=',$companyId)
                ->first();
        
        $postRep = CataloguePostRepository::getRepository($post,$companyId); 
        return $postRep;
    }
    
    function update($properties)
    {
        try{
            CataloguePost::query()
                   ->where('companyId','=', $this->companyId)
                   ->where('postId','=', $this->postId)
                   ->update($properties);
           CataloguePostRepository::updateCacheRepository($this->companyId, $this->postId);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    public static function updateCacheRepository($companyId,$postId){
        $post = CataloguePostRepository::getNode($postId, $companyId);
        $postRep = CataloguePostRepository::getRepository($post, $companyId);
        BaseRepository::putListItemIntoCache($companyId, CacheType::CompanyCataloguePosts, CacheExpiry::CompanyCataloguePosts, $postId, $postRep);
        //dd(var_dump(BaseRepository::getFromCache($companyId, CacheType::CompanyCataloguePosts)));
    }
    
    function uploadPicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$this->companyId."/catalogue_posts/".$this->postId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        //delete all previous images
        //DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        //$file->move($path,$file->getClientOriginalName());
        
        CataloguePostRepository::updateCacheRepository($this->companyId,$this->postId);
        
        $response["success"] = 1;
        $response["message"] = "Catalogue post picture uploaded";
        return $response;
    }
    
    public static function uploadPictures($files,$companyId,$postId){
        
        if (is_null($files)){
            $response["success"] = 0;
            $response["message"] = "Files is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$companyId."/catalogue_posts/".$postId);
        $path = str_replace('\\', '/', $path);
        
        if (!file_exists($path)){
            mkdir($path, 0777, true);
        }
        
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        foreach($files as $file){
            if (!is_null($file)){
                Images::compress($file,$path."/".$file->getClientOriginalName());
            }
        }
        
        CataloguePostRepository::updateCacheRepository($companyId,$postId);
        
        $response["success"] = 1;
        $response["message"] = "Catalogue post pictures uploaded";
        return $response;
    }
    
    public static function uploadFile($file,$companyId,$postId){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }

        $path = public_path('companies/'.$companyId.'/catalogue_posts/'.$postId.'/files');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        DeleteFolder::deleteFiles($path);

        $file->move($path,$file->getClientOriginalName());
        //file_put_contents($path."/".$file->getClientOriginalName(), $file);
        
        CataloguePostRepository::updateCacheRepository($companyId,$postId);
        
        $result = array();
        $result['success'] = 1;
        $result['message'] = "Catalogue post file uploaded";
        $result['fileName'] = $file->getClientOriginalName();
        
        return $result;
    }
    
    public static function editPostPicture($file,$companyId,$postId){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = public_path('companies/'.$companyId."/catalogue_posts/".$postId);
        
        //delete all previous images
        //DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        
        CataloguePostRepository::updateCacheRepository($companyId,$postId);
        
        $response["success"] = 1;
        $response["message"] = "Catalogue post picture uploaded";
        return $response;
    }
    
    public static function likePost($postId,$companyId)
    {
        $post = CataloguePostRepository::getNode($postId, $companyId);
        $likes = $post->likes;
        
        if ($likes){
            $likes = ((int)$likes) + 1;
        }else{
            $likes = 1;
        }
        
        $post->likes = $likes;
        $post->save();
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function getPost() {
        return $this->post;
    }

    function getTime() {
        return $this->time;
    }

    function getId() {
        return $this->postId;
    }

    function getLikes() {
        return $this->likes;
    }

    function getType() {
        return $this->type;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setPost($post) {
        $this->post = $post;
    }

    function setTime($time) {
        $this->time = $time;
        
        $dateTime = explode("_", $time);
        $date = $dateTime[0];
        
        $this->day = date("d F Y", strtotime($date));
    }

    function setId($postId) {
        $this->postId = $postId;
    }

    function setLikes($likes) {
        $this->likes = $likes;
    }
    
    function getComments() {
        return $this->comments;
    }

    function setComments($comments) {
        if (isset($comments)){
            $this->comments = $comments;
        }
    }
    
    function getPictureUrl() {
        return $this->pictureUrl;
    }

    function setPictureUrl($pictureUrl) {
        $this->pictureUrl = $pictureUrl;
    }

    function getPictureUrls() {
        return $this->pictureUrls;
    }
    
    function setPictureUrls($pictureUrls) {
        $this->pictureUrls = $pictureUrls;
    }
    
    function getFileUrl() {
        return $this->fileUrl;
    }

    function setFileUrl($fileUrl) {
        $this->fileUrl = $fileUrl;
    }

    function getSeen() {
        return $this->seen;
    }

    function setSeen($seen) {
        $this->seen = $seen;
    }
    
    function getLiked() {
        return $this->liked;
    }

    function setLiked($liked) {
        $this->liked = $liked;
    }
    
    function getDay() {
        return $this->day;
    }

    function setDay($day) {
        $this->day = $day;
    }
    
    function getDuration() {
        return $this->duration;
    }

    function getPrice() {
        return $this->price;
    }

    function getUnits() {
        return $this->units;
    }

    function getAmenities() {
        return $this->amenities;
    }

    function setDuration($duration) {
        $this->duration = $duration;
        if (isset($duration)){
            $durations = explode(',', $duration);
            if (count($durations) !== 2){
                return;
            }
            $this->from = $durations[0];
            $this->to = $durations[1];
        }
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setUnits($units) {
        $this->units = $units;
    }

    function setAmenities($amenities) {
        $this->amenities = $amenities;
    }
    function getTitle() {
        return $this->title;
    }

    function setTitle($title) {
        $this->title = $title;
    }
    
    function getCreatedBy() {
        return $this->createdBy;
    }

    function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }
}

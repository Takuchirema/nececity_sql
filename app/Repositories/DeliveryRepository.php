<?php namespace App\Repositories;

use App\Models\Delivery;
use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\DeliveryStatus;
use SimpleXMLElement;

/**
 * Description of Delivery
 *
 * @author Takunda
 */
class DeliveryRepository extends BaseRepository{
    
    var $deliveryId;
    var $userId;
    var $employeeId;
    var $companyId;
    var $address;
    var $phoneNumber;
    var $status; //pending, inprogress, delivered
    var $time;
    var $latitude;
    var $longitude;
    var $items; //items will be stored as xml
    
    var $deliveryItems = array();
    var $employee;
    
    function __construct($userId, $companyId) {
       $this->userId=$userId;
       $this->companyId=$companyId;
    }
    
    function create(){
        $this->createDeliveryNode();
        
        DeliveryRepository::updateCacheRepository($this->companyId, $this->deliveryId);
        
        $response["success"] = 1;
        $response["deliveryId"] = $this->deliveryId;
        $response["message"] = "Delivery has been created";
        return ($response);
        
    }
    
    function createDeliveryNode(){
        $deliveryNode = new Delivery;
        
        //create the new delivery
        $deliveryNode->userId = $this->userId;
	$deliveryNode->companyId = $this->companyId;
        $deliveryNode->address = $this->address;
        $deliveryNode->latitude = $this->latitude;
        $deliveryNode->longitude = $this->longitude;
        $deliveryNode->phoneNumber = $this->phoneNumber;
        $deliveryNode->status= DeliveryStatus::Pending;
        $deliveryNode->save();
        
        $this->deliveryId = $deliveryNode->deliveryId;
        
        return $deliveryNode;
    }

    public static function delete($companyId,$deliveryId){
        Delivery::query()
                    ->where('deliveryId','=',$deliveryId)
                    ->delete();
        
        BaseRepository::deleteListItemFromCache($companyId, CacheType::CompanyDeliveries, CacheExpiry::CompanyDeliveries, $deliveryId);
    }
    
    function encodeToXml($items){
        
        $xml = new SimpleXMLElement('<xml/>');
        
        foreach ($items as $item){
            
            if (!isset($item['id'])){
                continue;
            }
            
            $xmlItem = $xml->addChild('item');
            $xmlItem->addAttribute('id',$item['id']);
            $xmlItem->addAttribute('price',$item['price']);
            $xmlItem->addAttribute('name',$item['name']);
            $xmlItem->addAttribute('description',$item['description']);
            $xmlItem->addAttribute('url',$item['url']); 
        }
        
        $newXML = preg_replace("/\s+/", " ", $xml->asXML());
        
        $deliveryNode = $this->getDeliveryNode();
        $deliveryNode->items = $newXML;
        $deliveryNode->save();
        
        DeliveryRepository::updateCacheRepository($this->companyId, $this->deliveryId);
        
        return $newXML;
    }
    
    public static function decodeFromXml($items){
        $deliveryItems = array();
        
        if ($items){
            $xml = new SimpleXMLElement($items);
        }else{
            return $deliveryItems;
        }
        
        foreach ($xml->children() as $item) {
            $deliveryItem = new DeliveryItemRepository((string)$item['id'][0]);
            $deliveryItem->setName((string)$item['name'][0]);
            $deliveryItem->setPrice((string)$item['price'][0]);
            $deliveryItem->setDescription((string)$item['description'][0]);
            $deliveryItem->setName((string)$item['name'][0]);
            $deliveryItem->setUrl((string)$item['url'][0]);
            
            array_push($deliveryItems,$deliveryItem);
        }
        return $deliveryItems;
    }
    
    function update($properties)
    {
        try{
            Delivery::query()
                   ->where('deliveryId','=', $this->deliveryId)
                   ->update($properties);
           DeliveryRepository::updateCacheRepository($this->companyId, $this->deliveryId);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    public static function updateCacheRepository($companyId, $deliveryId){
        $deliveryRep = DeliveryRepository::getRepository($deliveryId, false);
        BaseRepository::putListItemIntoCache($companyId, CacheType::Deliveries, CacheExpiry::Deliveries, $deliveryId, $deliveryRep);
    }
    
    function is_iterable($var) {
        return (is_array($var) || $var instanceof Traversable);
    }
    
    public static function getDeliveries($companyId){
        $response['deliveries'] = array();
        
        $cachedDeliveries = BaseRepository::getListFromCache($companyId,CacheType::CompanyDeliveries);
        
        if (isset($cachedDeliveries)){
            $response['deliveries'] = $cachedDeliveries;
            $response["success"] = 1;
            $response["message"] = "Retrieved deliveries from cache";
            return $response; 
        }
        
        $deliveries = Delivery::query()
                    ->where('companyId','=',$companyId)
                    ->get();

        $cacheItems = array();
        if (!is_null($deliveries)) {
            foreach($deliveries as $delivery) {
                $deliveryRep = DeliveryRepository::getRepository($delivery->deliveryId, $companyId);
                array_push($response["deliveries"], $deliveryRep);
                $cacheItems[$deliveryRep->getId()] = $deliveryRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyDeliveries, CacheExpiry::CompanyDeliveries, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Deliveries";
        return $response; 
    }
    
    public static function getRepository($deliveryId,$cached=true){
        
        /*if ($cached){
            $cachedDelivery = BaseRepository::getItemFromCache(null,CacheType::CompanyDeliveries,$deliveryId);
        }*/
        
        $delivery = Delivery::query()
                    ->where('deliveryId','=',$deliveryId)
                    ->first();
        
        if (!isset($delivery)){
            return null;
        }

        $companyId = $delivery->companyId;
        $userId = $delivery->userId;
        $deliveryRep = new DeliveryRepository($userId,$companyId);
        
        if (!is_null($delivery)){
            $deliveryRep->setDeliveryId($delivery->deliveryId);
            $deliveryRep->setAddress($delivery->address);
            $deliveryRep->setEmployeeId($delivery->employeeId);
            $deliveryRep->setLatitude($delivery->latitude);
            $deliveryRep->setLongitude($delivery->longitude);
            $deliveryRep->setStatus($delivery->status);
            $deliveryRep->setTime($delivery->time);
            $deliveryRep->setUserId($delivery->userId);
            $deliveryRep->setPhoneNumber($delivery->phoneNumber);
            $deliveryRep->setEmployee();
            $deliveryRep->setDeliveryItems(DeliveryRepository::decodeFromXml($delivery->items));
        }
        
        BaseRepository::putListItemIntoCache($companyId, CacheType::CompanyDeliveries, CacheExpiry::CompanyDeliveries, $deliveryId, $deliveryRep);
        
        return $deliveryRep;
    }

    function getId() {
        return $this->deliveryId;
    }

    function getDeliveryNode() {
        
        $deliveryNode = Delivery::query()
                    ->where('deliveryId','=',$this->deliveryId)
                    ->first();
        
        return $deliveryNode;
    }

    function setId($deliveryId) {
        $this->deliveryId = $deliveryId;
    }

    function setDeliveryNode($deliveryNode) {
        $this->deliveryNode = $deliveryNode;
    }
    
    function getDeliveryId() {
        return $this->deliveryId;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function setDeliveryId($deliveryId) {
        $this->deliveryId = $deliveryId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getUserId() {
        return $this->userId;
    }

    function getEmployeeId() {
        return $this->employeeId;
    }

    function getAddress() {
        return $this->address;
    }

    function getStatus() {
        return $this->status;
    }

    function getTime() {
        return $this->time;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getItems() {
        return $this->items;
    }

    function getDeliveryItems() {
        return $this->deliveryItems;
    }

    function getEmployee() {
        return $this->employee;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setTime($time) {
        $this->time = $time;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setItems($items) {
        $this->items = $items;
    }

    function setDeliveryItems($deliveryItems) {
        $this->deliveryItems = $deliveryItems;
    }
    
    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Will set the employee from the cache.
     * This is so that we know position of delivery if it is in transit.
     */
    function setEmployee() {
        if (isset($this->employeeId) && isset($this->companyId)){
            $this->employee = EmployeeRepository::getRepository($this->employeeId, $this->companyId);
        }
    }
}

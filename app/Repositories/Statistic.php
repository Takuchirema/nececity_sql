<?php namespace App\Repositories;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Cypher;

/**
 * Description of UserController
 *
 * @author Takunda Chirema
 */

class Statistic {
    var $totalRegisteredUsers;
    var $totalOnlineUser;
    var $totalCompany;
    var $routeNode;
    var $color;
    var $basicColor;
    
     function __construct() {
         
        include(base_path('vendor/neo4j/InitialVariables.php'));
          
        $queryTemplate = "MATCH (n:user) RETURN count(n) as statistic";
	
	try {
            $query = new Cypher\Query($client, $queryTemplate, array());
            $result = $query->getResultSet();
        }
        catch (PDOException $ex) {
            $response["success"] = 0;
            $response["message"] = "Could not retreive the last statistic";
            return $response;
        }
	
	if ($result)
	{ 
            if (isset($result[0][0])){ 
                $this->totalRegisteredUsers = $result[0][0];
            }
	}
        
        $queryTemplate = "MATCH (n:company) RETURN count(n) as statistic";
	
	try {
            $query = new Cypher\Query($client, $queryTemplate, array());
            $result = $query->getResultSet();
        }
        catch (PDOException $ex) {
            $response["success"] = 0;
            $response["message"] = "Could not retreive the last statistic";
            return $response;
        }
	
	if ($result)
	{  
            if (isset($result[0][0])){
                $this->totalCompany = $result[0][0];
            }
	}
        
        $queryTemplate = "MATCH (n:user{status:'online'}) RETURN count(n) as statistic";
	
	try {
            $query = new Cypher\Query($client, $queryTemplate, array());
            $result = $query->getResultSet();
        }
        catch (PDOException $ex) {
            $response["success"] = 0;
            $response["message"] = "Could not retreive the last statistic";
            return $response;
        }
	
	if ($result)
	{
            if (isset($result[0][0])){
                $this->totalOnlineUser = $result[0][0];
            }
	}
        return;
    }
}
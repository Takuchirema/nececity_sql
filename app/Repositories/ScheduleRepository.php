<?php namespace App\Repositories;

use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Models\Schedule;

/**
 * Description of ScheduleRepository
 *
 * @author Takunda
 */
class ScheduleRepository extends BaseRepository {
    
    var $scheduleId;
    var $companyId;
    var $employeeId;
    var $deviceId;
    var $days;
    var $start;
    var $end;
    var $routeId;
    var $shiftId;
    var $active;
    
    function __construct($companyId,$employeeId,$deviceId) {
       $this->employeeId = $employeeId;	
       $this->companyId=$companyId;
       $this->deviceId=$deviceId;
    }
    
    function create(){
      
        $schedules = ScheduleRepository::getSchedules
                ($this->companyId, null, $this->deviceId, null, $this->days, $this->start, $this->end)["schedules"];
        
        if (count($schedules) > 0) {
            // There exists user with same employeeId
            $response["success"] = 0;
            $response["message"] = "There are existing schedules for this bus on the given days and times. Please create non-conflicting schedules.";
            return $response;
        }
        
        $scheduleNode= $this->createNode(); 
        $this->scheduleId = $scheduleNode->scheduleId;
        
        ScheduleRepository::updateCacheRepository($this->companyId, $this->scheduleId);

        $response["success"] = 1;
        $response["message"] = "Schedule Successfully Created!";
        return $response;
        
    }
    
    function createNode(){
        $schedule = new Schedule;
        
        $schedule->companyId = $this->companyId;
        $schedule->employeeId = $this->employeeId;
        $schedule->deviceId = $this->deviceId;
        $schedule->days = $this->days;
        $schedule->start = $this->start;
        $schedule->end = $this->end;
        $schedule->routeId = $this->routeId;
        $schedule->active = 1;
        $schedule->save();
        
        return $schedule;
    }
    
    public static function delete($scheduleId){
        Schedule::query()
                    ->where('scheduleId','=',$scheduleId)
                    ->delete();
        
        BaseRepository::deleteItemFromCache(null, CacheType::Schedules, CacheExpiry::Schedules, $scheduleId);
    }
    
    /**
     * This gets the schedules assigned to the device checks which ones to start.
     * This method is called from DeviceRepository when the device has not been assigned an employee yet.
     * @param type $deviceId
     */
    public static function startShift($deviceId){
        $scheduleRep = null;
        $days = date('w');
        $hi = (int)date("Hi");
        //Get schedules of this device for today or all days of the week
        $scheduleReps = ScheduleRepository::getSchedules(null, null, $deviceId, null, $days, null, null)["schedules"];
        foreach ($scheduleReps as $schedule){
            if ($schedule->end > $schedule->start){
                //echo "** 1 schedule start: ".$schedule->start." -> ".$schedule->end;
                if ($hi >= $schedule->start && $hi < $schedule->end){
                   //echo "** 2 schedule start: ".$schedule->start." -> ".$schedule->end;
                   $scheduleRep = $schedule;
                   break;
                }
            }else{
                //echo "** 3 schedule start: ".$schedule->start." -> ".$schedule->end;
                if ($hi >= $schedule->start && $hi < 2359){
                   //echo "** 4 schedule start: ".$schedule->start." -> ".$schedule->end;
                   $scheduleRep = $schedule;
                   break;
                }else if ($hi >= 0 && $hi < $schedule->end){
                    //echo "** 5 schedule start: ".$schedule->start." -> ".$schedule->end;
                    $scheduleRep = $schedule;
                    break;
                }
            }
        }
        
        if (!isset($scheduleRep)){
            return;
        }
        
        $shiftRep = $scheduleRep->getShift();
        if (isset($shiftRep)){
            $shiftRep->endShift();
        }
        
        $scheduleId = $scheduleRep->scheduleId;
        $employeeId = $scheduleRep->employeeId;
        $companyId = $scheduleRep->companyId;
        $routeId = $scheduleRep->routeId;
        
        $employeeRep = EmployeeRepository::getRepository($employeeId);
        
        $properties = array();
        $properties["routeId"] = $routeId;
        $properties["deviceId"] = $deviceId;
        $properties["status"] = "online";
        $employeeRep->update($properties);
        
        //Create the shift
        $shiftRep = new ShiftRepository($employeeId, $companyId, $routeId, $deviceId);
        $shiftRep->create();
        
        $scheduleRep->update(array("shiftId" => $shiftRep->shiftId));
        
        $deviceRep = DeviceRepository::getRepository($deviceId);
        $deviceRep->update(array("scheduleId" => $scheduleId));
    }
    
    /**
     * This method is called from the shift repository.
     * Only when there is a stop do we check if the device is on a schedule and if the schedule is done.
     * @param type $deviceId
     * @return type
     */
    public static function endShift($deviceId){
        $deviceRep = DeviceRepository::getRepository($deviceId);
        $scheduleRep = $deviceRep->getSchedule();
        
        if (!isset($scheduleRep)){
            return;
        }
        
        $shiftRep = $scheduleRep->getShift();
        if (!isset($shiftRep)){
            return;
        }
        
        //echo "** end shift rep id: ".$shiftRep->shiftId." employee: ".$shiftRep->employeeId;
        $shiftRep->endShift();
        
        $employeeId = $scheduleRep->employeeId;
        $companyId = $scheduleRep->companyId;
        $routeId = $scheduleRep->routeId;
        
        $employeeRep = new EmployeeRepository($employeeId,$companyId);
        
        $properties = array();
        $properties["routeId"] = $routeId;
        $properties["deviceId"] = "";
        $properties["shiftId"] = "";
        $properties["status"] = "offline";
        $employeeRep->update($properties);
        
        $deviceRep->update(array("scheduleId" => null));
        
        $scheduleRep->update(array("shiftId" => null));
    }
    
    function getShift(){
        $shiftRep = null;
        if (isset($this->shiftId)){
            $shiftRep = ShiftRepository::getRepository($this->shiftId);
        }
        return $shiftRep;
    }
    
    public static function getRepository($scheduleId, $shiftId=null, $cached=true){
        
        if ($cached){
            $cachedRepository = BaseRepository::getItemFromCache(CacheType::Schedules, $scheduleId);
        }
        
        if (isset($cachedRepository)){
            return $cachedRepository;
        }
        
        if (isset($scheduleId)){
            $schedule = Schedule::query()
                            ->where('scheduleId','=',$scheduleId)
                            ->first();
        }else{
            $schedule = Schedule::query()
                            ->where('shiftId','=',$shiftId)
                            ->first();
        }
        
        if (!isset($schedule)){
            return null;
        }
        
        $scheduleRep = ScheduleRepository::createRepository($schedule);
        
        BaseRepository::putItemIntoCache(null, CacheType::Schedules, CacheExpiry::Schedules, $scheduleRep->companyId, $scheduleRep);
        
        return $scheduleRep;
    }
    
    public static function getSchedules($companyId, $employeeId, $deviceId, $routeId, $days, $start, $end, $active=true){
        $result = array();
        $result["schedules"] = array();
        
        $schedules = Schedule::query();
        
        if ($active){
            $schedules = $schedules->where("active",'=',1);
        }
        
        if (!BaseRepository::isEmptyOrNullString($companyId)){
            $schedules = $schedules->where("companyId",'=',$companyId);
        }            
                    
        if (!BaseRepository::isEmptyOrNullString($employeeId)){
            $schedules = $schedules->where("employeeId",'=',$employeeId);
        }
        
        if (!BaseRepository::isEmptyOrNullString($routeId)){
            $schedules = $schedules->where("routeId",'=',$routeId);
        }
        
        if (!BaseRepository::isEmptyOrNullString($deviceId)){
            $schedules = $schedules->where("deviceId",'=',$deviceId);
        }
        
        if (!BaseRepository::isEmptyOrNullString($days)){
            $schedules = $schedules->where(function ($schedules) use ($days){
                $splitDays = str_split($days);
                foreach ($splitDays as $day){
                    $schedules->orWhere("days", "like", "%".$day."%");
                }
            });
        }
        
        if (!BaseRepository::isEmptyOrNullString($start)){
            $startDate = date($start);
            //dd($startDate);
            $schedules = $schedules->where("start",">=",$startDate);
        }
        
        if (!BaseRepository::isEmptyOrNullString($end)){
            $endDate = date($end);
            $schedules = $schedules->where("end","<=",$endDate);
        }
        
        /*$query = str_replace(array('?'), array('\'%s\''), $schedules->toSql());
        $query = vsprintf($query, $schedules->getBindings());
        echo("*** ".$query." ***\n");*/
        
        $schedules = $schedules->get();
        
        if (!isset($schedules)){
            $result['success'] = 0;
            $result['message'] = "no schedules found";
            return $result;
        }
        
        foreach($schedules as $schedule) {
            $scheduleRep = ScheduleRepository::createRepository($schedule);
            array_push($result["schedules"], $scheduleRep);
        }
        
        $result['success'] = 1;
        $result['message'] = "Schedules found";
        return $result;
    }
    
    public static function createRepository($schedule){
        
        $scheduleRep = new ScheduleRepository($schedule->companyId,$schedule->employeeId,$schedule->deviceId);
        
        $scheduleRep->setScheduleId($schedule->scheduleId);
        $scheduleRep->setDays($schedule->days);
        $scheduleRep->setStart($schedule->start);
        $scheduleRep->setEnd($schedule->end);
        $scheduleRep->setRouteId($schedule->routeId);
        $scheduleRep->setActive($schedule->active);
        $scheduleRep->setShiftId($schedule->shiftId);
        
        return $scheduleRep;
    }
    
    public static function updateCacheRepository($companyId, $scheduleId){
        $scheduleRep = ScheduleRepository::getRepository($scheduleId, null, false);
        BaseRepository::putListItemIntoCache($companyId, CacheType::Schedules, CacheExpiry::Schedules, $scheduleId, $scheduleRep);
    }
    
    function update($properties){
        try{
            Schedule::query()
                   ->where('scheduleId','=', $this->scheduleId)
                   ->update($properties);
           ScheduleRepository::updateCacheRepository($this->companyId, $this->scheduleId);
        }catch(\Illuminate\Database\QueryException $ex){
            return null;
        }
    }
    
    function getEmployee(){
        if (isset($this->employeeId)){
            $employeeRep = EmployeeRepository::getRepository($this->employeeId);
            return $employeeRep;
        }
        return null;
    }
    
    function getScheduleId() {
        return $this->scheduleId;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function getEmployeeId() {
        return $this->employeeId;
    }

    function getDeviceId() {
        return $this->deviceId;
    }

    function getDays() {
        return $this->days;
    }

    function getStart() {
        return $this->start;
    }

    function getEnd() {
        return $this->end;
    }

    function getRouteId() {
        return $this->routeId;
    }

    function setScheduleId($scheduleId) {
        $this->scheduleId = $scheduleId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }

    function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
    }

    function setDays($days) {
        $this->days = $days;
    }

    function setStart($start) {
        $this->start = $start;
    }

    function setEnd($end) {
        $this->end = $end;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }
    
    function getActive() {
        return $this->active;
    }

    function setActive($active) {
        $this->active = $active;
    }
    
    function getShiftId() {
        return $this->shiftId;
    }

    function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }
}

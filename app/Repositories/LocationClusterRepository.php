<?php namespace App\Repositories;

use App\Http\Helpers\Constants;
use DateTime;
use DateTimeZone;

/**
 * Description of LocationCluster
 *
 * @author Takunda
 */
class LocationClusterRepository extends BaseRepository {
    
    var $id;
    var $latitude;
    var $longitude;
    
    // direction is bearing of previous location to current location
    // If difference between directions > 120 then they are going in opposite directions
    var $direction;
    var $calculatedAt;
    var $clusterSize = 0;
    
    private $userClusterLocations = array();
    private $userIds = array();
    
    // $locations[0][0] = latitude;
    // $locations[0][1] = longitude
    private $locations = array();
    
    function __construct($latitude,$longitude) {
       $this->latitude=$latitude;
       $this->longitude = $longitude;
       $this->calculatedAt = date('Y-m-d_H:i:s');
    }
    
    /**
     * Returns LocationClusters from the points given.
     * @param type $userClusterLocations
     */
    public static function findClusters($userClusterLocations){
        $clusters = array();
        $locationUpdateTime = Constants::getLocationUpdateTime();
        $clusterMinDistance = Constants::getClusterMinDistance();
        $clusterMinDirection = Constants::getClusterMinDirection();
        
        foreach ($userClusterLocations as $location){
            $found = false;
            
            $lastSeen = Datetime::createFromFormat('Y-m-d_H:i:s', $location->lastSeen);
            $currentDate = new DateTime('now', new DateTimeZone('Africa/Johannesburg'));

            $diffInSeconds = abs($lastSeen->getTimestamp() - $currentDate->getTimestamp());
            
            //fwrite(STDOUT, "User add to cluster: ".$diffInSeconds." ".$lastSeen->format('Y-m-d H:i:s')." ".$currentDate->format('Y-m-d H:i:s')."\n");
            if ($diffInSeconds > $locationUpdateTime){
                continue;
            }
            
            foreach ($clusters as $cluster){
                $directionDiff = abs($cluster->direction - $location->direction);
                
                //fwrite(STDOUT, "Cluster direction diff: ".$directionDiff." ".$cluster->direction." ".$location->direction."\n");
                // Check if cluster and point are going the same direction and are close enough.
                // If not the point does not belong to that cluster.
                if ($directionDiff > $clusterMinDirection){
                    //error_log("direction min diff: ".$clusterMinDirection." diff: ".$directionDiff);
                    continue;
                }
                
                $lat1 = (float)$cluster->latitude;
                $lng1 = (float)$cluster->longitude;
                $lat2 = (float)$location->latitude;
                $lng2 = (float)$location->longitude;
                
                $distance = LocationClusterRepository::computeDistance($lat1, $lng1, $lat2, $lng2);
                
                if ($distance > $clusterMinDistance){
                    //error_log("ditance min diff: ".$clusterMinDistance." dist: ".$distance);
                    continue;
                }
                
                $cluster->insertClusterLocation($location);
                $found = true;
                break;
            }
            
            if (!$found){
                $cluster = new LocationClusterRepository($location->latitude,$location->longitude);
                $cluster->id = rand(count($clusters) + 1 , count($clusters) + 1000);
                $cluster->direction = $location->direction;
                $cluster->insertClusterLocation($location);
                array_push($clusters,$cluster);
            }
        }
        
        return $clusters;
    }
    
    public static function arrayDiff($a, $b) {
        $map = array();
        foreach($a as $val){ $map[$val] = 1;}
        foreach($b as $val){ unset($map[$val]);}
        return array_keys($map);
    }
    
    /**
    * Computes the distance between two coordinates.
    *
    * Implementation based on reverse engineering of
    * <code>google.maps.geometry.spherical.computeDistanceBetween()</code>.
    *
    * @param float $lat1 Latitude from the first point.
    * @param float $lng1 Longitude from the first point.
    * @param float $lat2 Latitude from the second point.
    * @param float $lng2 Longitude from the second point.
    * @param float $radius (optional) Radius in meters.
    *
    * @return float Distance in meters.
    */
   public static function computeDistance($lat1, $lng1, $lat2, $lng2)
   {
       $radius = 6378137;
       static $x = M_PI / 180;
       $lat1 *= $x; $lng1 *= $x;
       $lat2 *= $x; $lng2 *= $x;
       $distance = 2 * asin(sqrt(pow(sin(($lat1 - $lat2) / 2), 2) + cos($lat1) * cos($lat2) * pow(sin(($lng1 - $lng2) / 2), 2)));

       return $distance * $radius;
   }
    
    function GetCenterFromDegrees(array $data) {
        if (!count($data)) {
            return false;
        }

        $numCoords = count($data);

        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        for($i = 0; $i < count($data); $i++){
                $lat = $data[$i][0] * pi() / 180;
                $lon = $data[$i][1] * pi() / 180;

                $a = cos($lat) * cos($lon);
                $b = cos($lat) * sin($lon);
                $c = sin($lat);

                $X += $a;
                $Y += $b;
                $Z += $c;
        }

        $X /= $numCoords;
        $Y /= $numCoords;
        $Z /= $numCoords;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        $newX = ($lat * 180 / pi());
        $newY = ($lon * 180 / pi());

        return [$newX, $newY];
    }
    
    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getDirection() {
        return $this->direction;
    }

    function getCalculatedAt() {
        return $this->calculatedAt;
    }

    function getUserClusterLocations() {
        return $this->userClusterLocations;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setDirection($direction) {
        $this->direction = $direction;
    }

    function setCalculatedAt($calculatedAt) {
        $this->calculatedAt = $calculatedAt;
    }
    
    function insertClusterLocation($clusterLocation){
        $userId = $clusterLocation->userId;
        $this->clusterSize++;
        
        $location = array($clusterLocation->latitude,$clusterLocation->longitude);
        array_push ($this->userIds,$userId);
        array_push ($this->locations,$location);
    }
}

<?php namespace App\Repositories;

use App\Models\Relationship;
use SimpleXMLElement;

/**
 * Description of Alert
 *
 * @author Takunda Chirema
 */

class RelationshipRepository extends BaseRepository{
    
    var $relationshipId;
    var $entityId;
    var $relatedEntityId;
    var $type;
    var $properties;
    var $relationshipNode;
    
    /**
     * Relationship Types.
     */
    static $userSeenPost = 'USER_SEEN_POST';
    static $userSeenCataloguePost = 'USER_SEEN_CATALOGUE_POST';
    static $userSeenAlert = 'USER_SEEN_ALERT';
    static $userSeenPromotion = 'USER_SEEN_PROMOTION';
    static $userLikedPromotion = 'USER_LIKED_PROMOTION';
    static $userLikedPost = 'USER_LIKED_POST';
    static $userLikedCataloguePost = 'USER_LIKED_CATALOGUE_POST';
    
    static $employeeSeenPost = 'EMPLOYEE_SEEN_POST';
    static $employeeSeenAlert = 'EMPLOYEE_SEEN_ALERT';
    static $employeeSeenPromotion = 'EMPLOYEE_SEEN_PROMOTION';
    static $employeeLikedPromotion = 'EMPLOYEE_LIKED_PROMOTION';
    static $employeeLikedPost = 'EMPLOYEE_LIKED_POST';
    
    static $userFriendsWith = 'USER_FRIENDS_WITH';
    
    function create(){
        $relationship = new Relationship;
        
        $relationship->entityId = $this->entityId;
        $relationship->relatedEntityId = $this->relatedEntityId;
        $relationship->type = $this->type;
        $relationship->properties = $this->properties;
        
        $relationship->save();
        
        $this->relationshipId = $relationship->relationshipId;
    }
    
    function insertProperty($property,$path=null){
        $properties = $this->getProperties();
        
        if ($properties){
            $xml = new SimpleXMLElement($properties);
            if ($path){
                $propertyNodes = $xml->xpath('properties/'.$path)[0];
            }else{
                $propertyNodes = $xml->xpath('properties')[0];
            }
        }else{
            $xml = new SimpleXMLElement('<xml/>');
            $propertyNodes = $xml->addChild('properties');
        }
        
        $propertyNodes->addChild($property);
        
        $this->saveProperties($xml);
    }
    
    function updateProperty($property,$value){
        $properties = $this->getProperties();
        
        if ($properties){
            $xml = new SimpleXMLElement($properties);
            $propertyNode = $xml->xpath('properties/'.$property)[0];
            
            $propertyNode[0] = $value;
            $this->saveProperties($xml);
        }
    }
    
    function saveProperties($xml){
        $newXML = preg_replace("/\s+/", " ", $xml->asXML());
        
        $relationshipNode = RelationshipRepository::getRelationshipNode($this->relationshipId);

        if ($relationshipNode){
            $relationshipNode->properties = $newXML;
            $relationshipNode->save();
        }
    }
    
    function getProperty($property){
        $properties = $this->getProperties();
        
        if ($properties){
            $xml = new SimpleXMLElement($properties);
            $propertyNode = $xml->xpath('properties/'.$property);

            return (string) $propertyNode[0];
        }
        
        return null;
    }
    
    public static function getRelationship($relationshipId){
        $relationship = RelationshipRepository::getRelationshipNode($relationshipId);
        
        $relationshipRep = new RelationshipRepository;
        $relationshipRep->entityId = $relationship->entityId;
        $relationshipRep->relatedEntityId = $relationship->relatedEntityId;
        $relationshipRep->type = $relationship->type;
        $relationshipRep->properties = $relationship->properties;
        $relationshipRep->relationshipId = $relationship->relationshipId;
        
        return $relationshipRep;
    }
    
    function getEntityId() {
        return $this->entityId;
    }

    function getRelatedEntityId() {
        return $this->relatedEntityId;
    }

    function getType() {
        return $this->type;
    }

    function getProperties() {
        $relationship = Relationship::query()
                        ->where('relationshipId','=',$this->relationshipId)
                        ->first();
        return $relationship->properties;
    }

    function setEntityId($entityId) {
        $this->entityId = $entityId;
    }

    function setRelatedEntityId($relatedEntityId) {
        $this->relatedEntityId = $relatedEntityId;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setProperties($properties) {
        $this->properties = $properties;
    }
    
    function getRelationshipId() {
        return $this->relationshipId;
    }

    public static function getRelationshipNode($relationshipId) {
        if ($relationshipId){
            $relationship = Relationship::query()
                            ->where('relationshipId','=',$relationshipId)
                            ->first();
            return $relationship;
        }
        return null;
    }

    function setRelationshipId($relationshipId) {
        $this->relationshipId = $relationshipId;
    }

    function setRelationshipNode($relationshipNode) {
        $this->relationshipNode = $relationshipNode;
    }
}


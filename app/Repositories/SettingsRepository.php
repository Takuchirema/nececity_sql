<?php namespace App\Repositories;

use App\Models\Settings;

/**
 * Description of Settings
 *
 * @author Takunda Chirema
 */

class SettingsRepository extends BaseRepository{
    
    var $userId;
    var $companyId;
    var $postsRefreshInterval;
    var $messagesRefreshInterval;
    var $companiesRefreshInterval;
    var $usersRefreshInterval;
    var $visibilityRegion;
    
    function __construct($userId) {
       $this->userId = $userId;
    }
    
    function create(){
        $this->createSettingsNode();
    }
    
    function createSettingsNode(){
        $settings = new Settings;
        
        $settings->userId = $this->userId;
        $settings->postsRefreshInterval = $this->postsRefreshInterval;
        $settings->messagesRefreshInterval = $this->messagesRefreshInterval;
        $settings->usersRefreshInterval = $this->usersRefreshInterval;
        $settings->companiesRefreshInterval = $this->companiesRefreshInterval;
        $settings->visibilityRegion = $this->visibilityRegion;
        $settings->save();

        return $settings;
    }
    
    public static function getSettingsNode($userId,$companyId){
        
        if (is_null($companyId)){
            $settings = Settings::query()
                        ->where('userId','=',$userId)
                        ->first();
        }else{
            $settings = Settings::query()
                    ->where('userId','=',$userId)
                    ->where('companyId','=',$companyId)
                    ->first();
        }
        
        return $settings;
    }
    
    function update($properties)
    {
        try{
            Settings::query()
                    ->where('companyId','=', $this->companyId)
                    ->where('userId','=', $this->userId)
                    ->update($properties);
        }catch(\Illuminate\Database\QueryException $ex){
            $response["success"] = 1;
            $response["message"] = $ex->getMessage();
            $response["settings"] = null;
            return $response;
        }
        
        $settingsNode = SettingsRepository::getSettingsNode($this->userId, $this->companyId);
        
        $settings = new SettingsRepository($settingsNode->userId);
        $settings->setCompaniesRefreshInterval($settingsNode->companiesRefreshInterval);
        $settings->setMessagesRefreshInterval($settingsNode->messagesRefreshInterval);
        $settings->setPostsRefreshInterval($settingsNode->postsRefreshInterval);
        $settings->setUsersRefreshInterval($settingsNode->usersRefreshInterval);
        $settings->setVisibilityRegion($settingsNode->visibilityRegion);
        
        $response["success"] = 1;
        $response["message"] = "Setting successfully updated";
        $response["settings"] = $settings;
        
        return $response;
    }
    
    public static function getSettings($userId,$companyId){
        
        $settingsNode = SettingsRepository::getSettingsNode($userId, $companyId);
        
        $settings = new SettingsRepository($userId);
        
        if (!is_null($settingsNode)){
            
            $settings->setCompaniesRefreshInterval($settingsNode->companiesRefreshInterval);
            $settings->setMessagesRefreshInterval($settingsNode->messagesRefreshInterval);
            $settings->setPostsRefreshInterval($settingsNode->postsRefreshInterval);
            $settings->setUsersRefreshInterval($settingsNode->usersRefreshInterval);
            $settings->setVisibilityRegion($settingsNode->visibilityRegion);
        }
        
        return $settings;
    }
    
    function getUserId() {
        return $this->userId;
    }

    function getCompanyId() {
        return $this->companyId;
    }

    function getPostsRefreshInterval() {
        return $this->postsRefreshInterval;
    }

    function getMessagesRefreshInterval() {
        return $this->messagesRefreshInterval;
    }

    function getCompaniesRefreshInterval() {
        return $this->companiesRefreshInterval;
    }

    function getUsersRefreshInterval() {
        return $this->usersRefreshInterval;
    }

    function getVisibilityRegion() {
        return $this->visibilityRegion;
    }

    function setUserId($userId) {
        $this->userId = $userId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }

    function setPostsRefreshInterval($postsRefreshInterval) {
        $this->postsRefreshInterval = $postsRefreshInterval;
    }

    function setMessagesRefreshInterval($messagesRefreshInterval) {
        $this->messagesRefreshInterval = $messagesRefreshInterval;
    }

    function setCompaniesRefreshInterval($companiesRefreshInterval) {
        $this->companiesRefreshInterval = $companiesRefreshInterval;
    }

    function setUsersRefreshInterval($usersRefreshInterval) {
        $this->usersRefreshInterval = $usersRefreshInterval;
    }

    function setVisibilityRegion($visibilityRegion) {
        $this->visibilityRegion = $visibilityRegion;
    }


}

<?php namespace App\Repositories;

use App\Http\Helpers\CacheExpiry;
use App\Http\Helpers\CacheType;
use App\Http\Helpers\DeleteFolder;
use App\Repositories\RouteTimeLogRepository;
use App\Models\Employee;
use App\Http\Helpers\Images;
use Log;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
/**
 * Description of Employee
 *
 * @author Takunda Chirema
 */
class EmployeeRepository extends BaseRepository implements AuthenticatableContract ,CanResetPasswordContract{
    
    var $employeeId;
    var $employeeCode;
    var $name;
    var $fbName;
    var $email;
    var $salt;
    var $menu;
    private $password;
    private $token;
    var $phoneNumber;
    var $profilePicture;
    var $privilege;
    
    var $country;
    var $latitude;
    var $longitude;
    var $location;
    var $lastSeen;
    var $lastSeenReadable;
    var $companyId;
    var $alert;
    var $settings;
    var $mileage;
    
    var $status;
    var $logout;
    var $routePoints = array();
    var $route;
    var $routeId;
    var $shiftId;
    var $shiftSummary;
    
    private $firebasetoken;
    private $deviceId;
    
    function __construct($employeeId,$companyId) {
       $this->employeeId = $employeeId;	
       $this->companyId=$companyId;
    }
    
    /**
     * The method will try and save the given employee in the neo4j database
     * using the employeeId and any additional details. When created the password is
     * plain text of the employeeID. Only when changed on android will it be
     * hashed and given a random salt by android
     */
    function create(){
        
        $employeeNode = $this->getNode($this->employeeCode, $this->companyId);
        
        if (!is_null($employeeNode)) {
            // There exists user with same employeeId
            $response["success"] = 0;
            $response["message"] = "This employee code is already in use. Please use another one.";
            return $response;
        }
        
        $employeeNode = $this->createNode(); 
        $this->employeeId = $employeeNode->employeeId;
        
        if (file_exists(base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId))){
            DeleteFolder::delTree(base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId));
        }

        $old = umask(0);
        mkdir(base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/profile'), 0777, true);
        umask($old);
        
        EmployeeRepository::updateCacheRepository($this->companyId, $this->employeeId);

        $response["success"] = 1;
        $response["message"] = "Employeename Successfully Added!";
        return $response; 
    }
    
    function createNode(){
        $employee = new Employee;
        
        $employee->employeeCode = $this->employeeCode;
        $employee->password = $this->password;
        $employee->phoneNumber = $this->phoneNumber;
        $employee->status = "offline";
        $employee->lastSeen = $this->lastSeen;
        $employee->companyId = $this->companyId;
        $employee->name = $this->name;
        $employee->privilege = $this->privilege;
        $employee->menu = $this->menu;
        $employee->save();
        
        return $employee;
    }
    
    public function delete(){
        $response = array();
        
        Employee::query()
                    ->where('companyId','=',$this->companyId)
                    ->where('employeeId','=',$this->employeeId)
                    ->delete();
        
        BaseRepository::deleteListItemFromCache($this->companyId, CacheType::CompanyEmployees, CacheExpiry::CompanyEmployees, $this->employeeId);
        
        $response['success'] = 1;
        $response['message'] = "Employee successfully deleted";
        
        return $response;
    }
    
    public static function getRepository($employeeId,$companyId=null,$companyName=null,$employeeCode=null){
        
        if (!isset($companyId) && isset($companyName)){
            $companyRep = CompanyRepository::getRepository(null, $companyName);
            if (isset($companyRep)){
                $companyId = $companyRep->companyId;
            }
        }
        
        if (isset($employeeId)){
            $employee = Employee::query()
                        ->where('employeeId','=',$employeeId)
                        ->first();
        }else{
            $employee = Employee::query()
                        ->where('companyId','=',$companyId)
                        ->where('employeeCode','=',$employeeCode)
                        ->first();
        }
        
        if (!isset($employee)){
            return null;
        }
        
        $employeeRep = EmployeeRepository::createRepository($employee);
        
        return $employeeRep;
    }
    
    public static function createRepository($employee){
        $employeeRep = new EmployeeRepository($employee->employeeId,$employee->companyId);
        
        $employeeRep->setEmployeeCode($employee->employeeCode);
        $employeeRep->setName($employee->name);
        $employeeRep->setEmployeeCode($employee->employeeCode);
        $employeeRep->setSalt($employee->salt);
        $employeeRep->setPassword($employee->password);
        $employeeRep->setLatitude($employee->latitude);
        $employeeRep->setLongitude($employee->longitude);
        $employeeRep->setStatus($employee->status);
        $employeeRep->setLogout($employee->logout);
        $employeeRep->setLastSeen($employee->lastSeen);
        $employeeRep->setPhoneNumber($employee->phoneNumber);
        $employeeRep->setCompanyId($employee->companyId);
        $employeeRep->setPrivilege($employee->privilege);
        $employeeRep->setRouteId($employee->routeId);
        $employeeRep->setToken($employee->token);
        $employeeRep->setEmail($employee->email);
        $employeeRep->setMenu($employee->menu);
        $employeeRep->setFirebasetoken($employee->firebasetoken);
        $employeeRep->setRoute(RouteRepository::getRouteDetails($employee->routeId, $employee->companyId));
        $employeeRep->setMileage($employee->mileage);
        $employeeRep->setShiftId($employee->shiftId);
        $employeeRep->setDeviceId($employee->deviceId);
        $employeeRep->updateShiftSummary();
        
        $alert = AlertRepository::getAlert($employee->employeeId,$employee->companyId);
        $employeeRep->setAlert($alert);

        $settings = SettingsRepository::getSettings($employee->employeeId,$employee->companyId);
        $employeeRep->setSettings($settings);

        $filename = public_path('companies/'.$employee->companyId.'/employees/'.$employee->employeeId.'/profile');
        $file_url = url('companies/'.$employee->companyId.'/employees/'.$employee->employeeId.'/profile');
        
        if (file_exists($filename)){

            $pictures = scandir($filename,1);
            $picture_url = str_replace(' ', '%20', $file_url.'/'.$pictures[0]);
            
            $employeeRep->setProfilePicture($picture_url);
        }
        
        return $employeeRep;
    }
    
    /**
     * This method returns employees given a query
     */
    public static function getEmployees($companyId){
        $response['employees'] = array();
        
        $cachedEmployees = BaseRepository::getListFromCache($companyId,CacheType::CompanyEmployees);
        
        if (isset($cachedEmployees)){
            EmployeeRepository::updateShiftSummaries($cachedEmployees);
            $response['employees'] = $cachedEmployees;
            $response["success"] = 1;
            $response["message"] = "Retrieved employees from cache";
            return $response; 
        }
        
        $employees = Employee::query()
                    ->where('companyId','=',$companyId)
                    ->get();

        $cacheItems = array();
        if (!is_null($employees)) {
            foreach($employees as $employee) {
                
                $employeeRep = EmployeeRepository::getRepository($employee->employeeId, $companyId);
                
                array_push($response["employees"], $employeeRep);
                $cacheItems[$employeeRep->getEmployeeId()] = $employeeRep;
            }
        }
        
        BaseRepository::putListIntoCache($companyId, CacheType::CompanyEmployees, CacheExpiry::CompanyEmployees, $cacheItems);
        
        $response["success"] = 1;
        $response["message"] = "Retrieved Employees";
        return $response;
    }
    
    public static function updateCacheRepository($companyId,$employeeId){
        $employeeRep = EmployeeRepository::getRepository($employeeId, $companyId);
        BaseRepository::putListItemIntoCache($companyId, CacheType::CompanyEmployees, CacheExpiry::CompanyEmployees, $employeeId, $employeeRep);
    }
    
    /**
     * Gets the DB node of the employee
     * 
     * @param type $employeeId
     * @param type $employeeCompany
     * @return type
     */
    public static function getNode($employeeCode,$companyId){
        $employee = Employee::query()
                    ->where('companyId','=',$companyId)
                    ->where('employeeCode','=',$employeeCode)
                    ->first();
        
        return $employee;
    }
    
    function uploadPicture($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $path = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/profile');
        
        //delete all previous images
        DeleteFolder::deleteFiles($path);
        
        Images::compress($file,$path."/".$file->getClientOriginalName());
        //$file->move($path,$file->getClientOriginalName());
        
        EmployeeRepository::updateCacheRepository($this->companyId,$this->employeeId);
        
        $response["success"] = 1;
        $response["message"] = "Employee picture uploaded";
        $response["url"] = url('companies/'.$this->companyId.'/employees/'.$this->employeeId.'/profile/'.$file->getClientOriginalName());
        return $response;
        
    }
    
    function recordTimeLog($file){
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        
        //employeeId+"_"+routeId+"_"+date format - ("yyyy-MM-dd");
        $details = explode("_", $fileName);
        $routeId = $details[1];
        $date = $details[2];
        
        $contents = file_get_contents($file);
        $lines = explode("\n", $contents); // this is your array of words
        
        foreach ($lines as $line)
        {
            $logDetails = explode(",",$line);
            
            if (sizeof($logDetails) < 3){
                continue;
            }
            
            $routePointId = $logDetails[0];
            $arrivalTime = $logDetails[1];
            $departureTime = $logDetails[2];
            
            $timeLogRep = new TimeLogRepository($this->employeeId,$this->companyId);
            $timeLogRep->setRouteId($routeId);
            $timeLogRep->setRoutePointId($routePointId);
            $timeLogRep->setDate($date);
            $timeLogRep->setArrivalTime($arrivalTime);
            $timeLogRep->setDepartureTime($departureTime);

            $timeLogRep->create();
        }
        
        $response["success"] = 1;
        $response["message"] = "Logs Recorded";
        $response["fileName"] = $file->getClientOriginalName();
        return $response;
    }
    
    function uploadLogs($file){
        
        if (is_null($file)){
            $response["success"] = 0;
            $response["message"] = "File is null";
            return $response;
        }
        
        $dirPath = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs');
        
        if (!file_exists($dirPath)){
            mkdir($dirPath, 0777, true);
        }
        
        $fileName = $file->getClientOriginalName();
        $path = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs/'.$fileName);
        
        if (!file_exists($path)){
            $fh = fopen($path, 'wb');
            fclose($fh);
        }
        
        $current = file_get_contents($path);
        $logs = file_get_contents($file);
        
        // Append a new person to the file
        $current .= $logs;
        // Write the contents back to the file
        file_put_contents($path, $current);
        
        $response["success"] = 1;
        $response["message"] = "Logs uploaded";
        $response["url"] = url('companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs/'.$file->getClientOriginalName());
        return $response;
        
    }
    
    public function getLogs($routeId,$date){
        $newRouteId = str_replace(' ', '-', $routeId);
        $fileName = $this->employeeId."_".$newRouteId."_".$date.".txt";
        
        $filePath = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs/'.$fileName);
        
        if (!file_exists($filePath)){
            return;
        }
        
        $logs = file_get_contents($filePath);
        return $logs;
    }
    
    function getRouteTimeLogs($routeId){
        $timeLogRep = new TimeLogRepository($this->employeeId,$this->companyId);
        $result = $timeLogRep->getRouteTimeLogs($routeId);
        return $result;
    }
    
    function getDateTimeLogs($date){
        $timeLogRep = new TimeLogRepository($this->employeeId,$this->companyId);
        $result = $timeLogRep->getDateTimeLogs($date);
        return $result;
    }
    
    function getRouteDateTimeLogs($routeId,$date){
        $timeLogRep = new TimeLogRepository($this->employeeId,$this->companyId);
        $result = $timeLogRep->getRouteDateTimeLogs($routeId,$date);
        return $result;
    }
    
    /**
     * Must only take the last 5 days from this one
     * Other days must be specified
     * **/
    public function getAllLogs($routeId = null,$dateString = null){
        $currentDate = new \DateTime('now');
        
        $results = array();
        
        $filesPath = base_path('public/companies/'.$this->companyId.'/employees/'.$this->employeeId.'/logs/');
        
        if (!file_exists($filesPath)){
            return null;
        }
        
        $files = array_diff(scandir($filesPath), array('.', '..'));
        
        foreach ($files as $file){
            $timeLog = new RouteTimeLogRepository($file,$this->companyId);
            
            if (isset($routeId) && !empty(trim($routeId))){
                $newRouteId = str_replace(' ', '-', $routeId);
                if (strpos($timeLog->fileName, $newRouteId) === false){
                    continue;
                }
            }
            
            if (isset($dateString) && !empty(trim($dateString))){
                if (strpos($timeLog->fileName, $dateString) === false){
                    continue;
                }
            }
            
            //log date
            $logDateTime = new \DateTime();
            $logDateTime = $logDateTime->createFromFormat('Y-m-d', $timeLog->date);
            
            if ($this->isRecentDate(30,$currentDate,$logDateTime)){
                array_push($results, $timeLog);
            }
        }
        
        return $results;
    }
    
    /**
     * Checks whether a date is greater than the number of days ago given
     * **/
    public function isRecentDate($daysAgo,$currentDate,$date){ 
        $diff = $currentDate->diff($date);
        if ($diff->days > $daysAgo){
            return false;
        }
        return true;
    }
    
    function update($properties){
        
        if (isset($properties["deviceId"])){
            $deviceRep = DeviceRepository::getRepository($properties["deviceId"]);
            if (isset($deviceRep)){
                $employeeRep = EmployeeRepository::getRepository($deviceRep->employeeId, $this->companyId);
                if (isset($employeeRep)){
                    if ($employeeRep->employeeId !== $this->employeeId){
                        $employeeRep->update(array("deviceId" => ""));
                    }
                }
                $deviceRep->update(array("employeeId" => $this->employeeId));
            }else{
                $deviceRep = DeviceRepository::getRepository($this->deviceId);
                if (isset($deviceRep)){
                    $deviceRep->update(array("employeeId" => ""));
                }
            }
        }
        
        try{
            Employee::query()
                    ->where('companyId','=', $this->companyId)
                    ->where('employeeId','=', $this->employeeId)
                    ->update($properties);
        }catch(\Illuminate\Database\QueryException $ex){
            $response["success"] = 0;
            $response["message"] = $ex->getMessage();
            //Log::debug('update error - '.json_encode($request->headers->all()));
            return $response;
        }
        
        EmployeeRepository::updateCacheRepository($this->companyId, $this->employeeId);
        
        $response["success"] = 1;
        $response["message"] = "Successfully updated!";
        return $response;
    }
    
    function setOnline(){
        
        if (isset($this->lastSeen) && $this->lastSeen !== "null"){
            date_default_timezone_set('Africa/Johannesburg');

            //2017-09-02_20:35
            //date("Y-m-d_H:i")
            $datetime = new \DateTime();
            $lastSeenDate = $datetime->createFromFormat('Y-m-d_H:i:s', $this->lastSeen);

            $currentDate = new \DateTime('now');

            $diff = $currentDate->diff($lastSeenDate);
            $hours = $diff->h + ($diff->days * 24);

            if ($hours>=1){
                $this->status="offline";
            }
        }
    }
    
    function timeElapsedString($ago, $full = false) {
        $now = new \DateTime;
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    
    public function getAuthIdentifierName()
    {
        return "employeeId";
    }
    public function getAuthIdentifier()
    {
        //dd(debug_print_backtrace());
        return $this->employeeId."_".$this->companyId;
    }
    public function getAuthPassword()
    {
            return $this->password;
    }
    public function getRememberToken()
    {
        return $this->token;
    }
    public function setRememberToken( $value)
    {
         $token = $value;
    }
    
    public function getRememberTokenName()
    {
        return "token";
    }
    
    public function getEmailForPasswordReset() {
        
    }

    public function sendPasswordResetNotification($token) {
        
    }
    
    
    
    function getEmail() {
        return $this->email;
    }

    function setEmail($email) {
        $this->email = $email;
    }
    function getSalt() {
        return $this->salt;
    }

    function getPassword() {
        return $this->password;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function getCountry() {
        return $this->country;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getLocation() {
        return $this->location;
    }

    function getStatus() {
        return $this->status;
    }

    function getResponse() {
        return $this->response;
    }

    function setSalt($salt) {
        $this->salt = $salt;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    function setCountry($country) {
        $this->country = $country;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setLocation($location) {
        $this->location = $location;
    }

    function setStatus($status) {
        $this->status = $status;
    }
    
    function setLogout($logout){
        $this->logout=$logout;
    }
    
    function getLogout() {
        return $this->logout;
    }

    function setResponse($response) {
        $this->response = $response;
    }
    
    function getProfilePicture() {
        return $this->profilePicture;
    }

    function getLastSeen() {
        return $this->lastSeen;
    }

    function setProfilePicture($profilePicture) {
        $this->profilePicture = $profilePicture;
    }

    function setLastSeen($lastSeen) {
        
        $this->lastSeen = $lastSeen;
        
        if (isset($this->lastSeen) && $this->lastSeen !== "null"){
            //2017-09-02_20:35
            //date("Y-m-d_H:i:s")
            $datetime = new \DateTime();
            $lastSeenDate = $datetime->createFromFormat('Y-m-d_H:i:s', $this->lastSeen);
            if ($lastSeenDate === false){
                $lastSeenDate = $datetime->createFromFormat('Y-m-d_H:i', $this->lastSeen);
            }
            //echo "last seen employee: ".$this->lastSeen;
            $this->lastSeenReadable = $this->timeElapsedString($lastSeenDate);
        }else{
            $this->lastSeenReadable = "Never";
        }
    }
    
    function getEmployeeCode() {
        return $this->employeeCode;
    }

    function setEmployeeCode($employeeCode) {
        $this->employeeCode = $employeeCode;
    }
    
    function getEmployeeId() {
        return $this->employeeId;
    }

    function setEmployeeId($employeeId) {
        $this->employeeId = $employeeId;
    }
    
    function getCompanyId() {
        return $this->companyId;
    }

    function setCompanyId($companyId) {
        $this->companyId = $companyId;
    }
    
    function getAlert() {
        return $this->alert;
    }

    function setAlert($alert) {
        $this->alert = $alert;
    }
    
    function getSettings() {
        return $this->settings;
    }

    function setSettings($settings) {
        $this->settings = $settings;
    }
    
    function getName() {
        return $this->name;
    }

    function setName($name) {
        $this->name = $name;
    }
    
    function getRoutePoints() {
        return $this->routePoints;
    }

    function setRoutePoints($routePoints) {
        $this->routePoints = $routePoints;
    }
    
    function getRoute() {
        return $this->route;
    }

    function setRoute($route) {
        $this->route = $route;
    }
    
    function getFbName() {
        return $this->fbName;
    }

    function setFbName($fbName) {
        $this->fbName = $fbName;
    }
    
    function getPrivilege() {
        return $this->privilege;
    }

    function setPrivilege($privilege) {
        $this->privilege = $privilege;
    }
    
    function getRouteId() {
        return $this->routeId;
    }

    function setRouteId($routeId) {
        $this->routeId = $routeId;
    }
    
    function getLastSeenReadable() {
        return $this->lastSeenReadable;
    }

    function setLastSeenReadable($lastSeenReadable) {
        $this->lastSeenReadable = $lastSeenReadable;
    }
    
    function setToken($token){
        $this->token=$token;
    }
    
    function getToken(){
        return $this->token;
    }
    
    function getMenu() {
        return $this->menu;
    }

    function setMenu($menu) {
        $this->menu = $menu;
    }
    
    function getFirebasetoken() {
        return $this->firebasetoken;
    }

    function setFirebasetoken($firebasetoken) {
        $this->firebasetoken = $firebasetoken;
    }
    
    function getMileage() {
        return $this->mileage;
    }

    function setMileage($mileage) {
        $this->mileage = $mileage;
    }
    
    function getDeviceId() {
        return $this->deviceId;
    }

    function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
    }
    
    function getDevice() {
        if (BaseRepository::isEmptyOrNullString($this->deviceId)){
            return null;
        }
        return DeviceRepository::getRepository($this->deviceId);
    }
    
    function getShiftId() {
        return $this->shiftId;
    }
    
    function updateShiftSummary(){
        if (isset($this->shiftId)){
            $this->shiftSummary = ShiftRepository::getShiftSummary($this->shiftId);
        }
    }
    
    public static function updateShiftSummaries($employees){
        foreach ($employees as $employeeRep){
            $employeeRep->updateShiftSummary();
        }
    }

    function setShiftId($shiftId) {
        $this->shiftId = $shiftId;
    }
    
    function getShift(){
        if (isset($this->shiftId)){
            $shiftRep = ShiftRepository::getRepository($this->shiftId);
            return $shiftRep;
        }
        return null;
    }
}
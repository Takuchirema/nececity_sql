<?php namespace App\Providers;

//use App\UserRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use App\Repositories\EmployeeRepository;
use App\Auth\CompanyProvider;
use App\Auth\WebUserProvider;
use App\Auth\EmployeeProvider;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Auth;

class CustomAuthProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->registerPolicies();

        Auth::provider('companies', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            
            return new CompanyProvider(new CompanyRepository(""));
        });
        
        Auth::provider('employees', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            
            return new EmployeeProvider(new CompanyRepository(""));
        });
        
         Auth::provider('users', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...

            return new WebUserProvider(new UserRepository(""));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}